import { INCREASE_NUMBER } from '../actions';

const INITIAL_STATE = {
    number: 0
  };
  
  export default (state = INITIAL_STATE, action) => {
    switch (action.type) {
      case INCREASE_NUMBER:
        return { ...state, number: action.payload };
      default:
        return state;
    }
  };
  