import { combineReducers, createStore, applyMiddleware, compose } from 'redux';
import MainDrawerNavigator from '../helpers/PageStructure';

import CounterReducer from './CounterReducer';

const navReducer = (state, action) => {
    const newState = MainDrawerNavigator.router.getStateForAction(action, state)
    return newState || state
}

export default combineReducers({
    nav: navReducer,
});