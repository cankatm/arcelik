import React, { Component } from 'react';
import { 
    View, 
    Text, 
    StyleSheet, 
    StatusBar,
    Dimensions,
    Image
} from 'react-native';
import ImageZoom from 'react-native-image-pan-zoom';

import { ZoomView } from '../components/ZoomView';
import { BackHeader } from '../components/Headers';
import * as colors from '../helpers/ColorPalette';
import styles from './styles';

const WINDOW_WIDTH = Dimensions.get('window').width;
const WINDOW_HEIGHT = Dimensions.get('window').height;

class ProductImageShowPage extends Component {
    static navigationOptions = ({navigation}) => ({
        header: <BackHeader onPress={() => navigation.goBack()} />
    })
    render() {
        return (
            <View style={{ flex: 1, backgroundColor: colors.white }} >
                <ImageZoom 
                    cropWidth={WINDOW_WIDTH}
                    cropHeight={WINDOW_HEIGHT}
                    imageWidth={WINDOW_WIDTH}
                    imageHeight={WINDOW_HEIGHT}
                    minScale={1}
                    maxScale={4}
                >
                    <Image style={{width:WINDOW_WIDTH, height:WINDOW_HEIGHT, resizeMode: 'contain' }}
                        source={{ uri:this.props.navigation.state.params.imgSource}}/>
                </ImageZoom>
            </View>
        );
    }
}

export default ProductImageShowPage;