import React, { Component } from 'react';
import { 
    View, 
    Text, 
    StyleSheet, 
    StatusBar,
    FlatList 
} from 'react-native';

import { AirCondBuyItem } from '../components/AirCondBuyItems';
import { BackHeader, WhichPageHeader } from '../components/Headers';
import styles from './styles';
import * as colors from '../helpers/ColorPalette';
import { airCondItems } from '../helpers/Writings';

class AirCondBuyPage extends Component {
    render() {
        return (
            <View style={{ flex: 1, backgroundColor: colors.white }} >
                <WhichPageHeader headerTitle='KLİMA SATIN ALMA REHBERİ' />
                <FlatList
                    data={airCondItems}
                    renderItem={({item}) => <AirCondBuyItem imgSource={item.image} headerTitle={item.headerTitle} content={item.content} />}
                    keyExtractor={(item, index) => item.id}
                    showsHorizontalScrollIndicator={false}
                />
                <View style={{ height: 16 }} />
            </View>
        );
    }
}

export default AirCondBuyPage;