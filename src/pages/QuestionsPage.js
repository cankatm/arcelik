import React, { Component } from 'react';
import { 
    View, 
    Text, 
    TouchableOpacity,
    FlatList,
    Image,
    ImageBackground,
    Dimensions
} from 'react-native';
import { NavigationActions } from 'react-navigation';

import * as questions from '../helpers/questions';
import * as colors from '../helpers/ColorPalette';
import styles from './styles';
import { WhichPageHeader, ExplanationHeader } from '../components/Headers';

import bgImage from '../../assets/images/red-geometric-background.jpg';
import bireyselImage from '../../assets/images/avatar.png';
import kurumsalImage from '../../assets/images/briefcase.png';

const WINDOW_WIDTH = Dimensions.get('window').width;
const WINDOW_HEIGHT = Dimensions.get('window').height;

/* Services */
import Api from '../services/WooCommerce/Api'

import _ from 'lodash'

class QuestionsPage extends Component {
    constructor(props) {
        super(props);
        this.state = {
            counter: 1,
            to: '',
            questionsArray: null,
            answers: [],
            questionsForAnswersArray: [],
            mainQuestion: '',
            value: '',
            imgSource: null
        }
    }

    componentWillMount() {
        this.detectParams();
    }

    componentDidMount() {
        this.renderQuestions();  
    }

    detectParams = () => {
        let counterM = 1;
        let toM = '';
        let answersM = [];
        let questionsForAnswersArrayM = [];

        if(this.props.navigation.state.params) {
            counterM = this.props.navigation.state.params.counter;
            toM = this.props.navigation.state.params.to;
            answersM = this.props.navigation.state.params.answers;
            questionsForAnswersArrayM = this.props.navigation.state.params.answers;
        }
    
        this.setState({ 
            counter: counterM,
            to: toM,
            answers: answersM,
            questionsForAnswersArray: questionsForAnswersArrayM,
        });     
    };

    countAnswers = (obj) => {
        const { questionsForAnswersArray } = this.state
        console.log(`--1-- ${questionsForAnswersArray.length}`)

        questionsForAnswersStringArray = []
        questionsForAnswersArray.filter((word, index) => index > 2).forEach((el, index) => {
            if(el.value !== null)
                questionsForAnswersStringArray.push({ option: el.content })
        })    
        //console.log('q',questionsForAnswersArray)
        //console.log('qs',questionsForAnswersStringArray)

        if(questionsForAnswersArray.length >= 3) {
            //console.log(`catId : ${questionsForAnswersArray[2].value}`)
            
            Api.get('products',{
                per_page: 100,
                category: questionsForAnswersArray[2].value,
                status:'publish'
            })
            .then((data) => {
                // console.log('data geldi')
                // console.log(data)

                data = data.filter(urun => {
                    let attrs = [] // Urun attr optionslarini string array yaptik.
                    urun.attributes.forEach(at => {
                        //console.log(at)
                        at.options.forEach(opt => {
                            attrs.push({ option: opt })
                        })                        
                    })
                    //console.log(1, attrs)
                    //console.log(2, questionsForAnswersStringArray)
                    return _.isMatch(attrs, questionsForAnswersStringArray)
                })

                prods = []
                data.forEach(element => {
                    prods = prods.concat(element.attributes)
                });

                obj.questionsArray.forEach(an => {
                    //console.log(an.content)
                    var filtered = []
                    filtered = prods.filter(p=> {
                        if(p.options.includes(an.content))
                            return true
                        return false
                    })
                    if(filtered != null)
                        an.len = filtered.length
                    else
                        an.len = 0
                    //console.log(filtered.length)
                })
                return this.setState({ 
                    questionsArray: obj.questionsArray, 
                    mainQuestion: obj.mainQuestion, 
                    imgSource: obj.imgSource 
                })
            })
        } else
        {
            return this.setState({ 
                questionsArray: obj.questionsArray, 
                mainQuestion: obj.mainQuestion, 
                imgSource: obj.imgSource 
            })
        }        
    }

    renderQuestions = () => {
        switch (this.state.to) {
            case 'bireysel':
                return this.countAnswers({ 
                    questionsArray: questions.mainQuestionsBireysel, 
                    mainQuestion: questions.mainQuestionsBireysel[0], 
                    imgSource: bireyselImage 
                })
            case 'bireyselBeyazEsya':
                return this.countAnswers({ 
                    questionsArray: questions.mainQuestionsBireyselBeyazEsya, 
                    mainQuestion: questions.mainQuestionsBireyselBeyazEsya[0], 
                    imgSource: bireyselImage 
                })
            case 'bireyselAnkastre':
                return this.countAnswers({ 
                    questionsArray: questions.mainQuestionsBireyselAnkastre, 
                    mainQuestion: questions.mainQuestionsBireyselAnkastre[0], 
                    imgSource: bireyselImage 
                })
            case 'bireyselIsitmaSogutma':
                return this.countAnswers({ 
                    questionsArray: questions.mainQuestionsBireyselIsitmaSogutma, 
                    mainQuestion: questions.mainQuestionsBireyselIsitmaSogutma[0], 
                    imgSource: bireyselImage 
                })
            case 'kurumsal':
                return this.countAnswers({ 
                    questionsArray: questions.mainQuestionsKurumsal, 
                    mainQuestion: questions.mainQuestionsKurumsal[0], 
                    imgSource: kurumsalImage 
                })
            case 'kurumsalBeyazEsya':
                return this.countAnswers({ 
                    questionsArray: questions.mainQuestionsKurumsalBeyazEsya, 
                    mainQuestion: questions.mainQuestionsKurumsalBeyazEsya[0], 
                    imgSource: kurumsalImage 
                })
            case 'kurumsalAnkastre':
                return this.countAnswers({ 
                    questionsArray: questions.mainQuestionsKurumsalAnkastre, 
                    mainQuestion: questions.mainQuestionsKurumsalAnkastre[0], 
                    imgSource: kurumsalImage 
                })
            case 'kurumsalIsitmaSogutma':
                return this.countAnswers({ 
                    questionsArray: questions.mainQuestionsKurumsalIsitmaSogutma, 
                    mainQuestion: questions.mainQuestionsKurumsalIsitmaSogutma[0], 
                    imgSource: kurumsalImage 
                })
            case 'kurumsalBuzdolabi1':
                return this.countAnswers({ 
                    questionsArray: questions.kurumsalBuzdolabi1, 
                    mainQuestion: questions.kurumsalBuzdolabi1[0], 
                    imgSource: kurumsalImage 
                })
            case 'kurumsalBuzdolabi2':
                return this.countAnswers({ 
                    questionsArray: questions.kurumsalBuzdolabi2, 
                    mainQuestion: questions.kurumsalBuzdolabi2[0], 
                    imgSource: kurumsalImage 
                })
            case 'kurumsalBuzdolabi3':
                return this.countAnswers({ 
                    questionsArray: questions.kurumsalBuzdolabi3, 
                    mainQuestion: questions.kurumsalBuzdolabi3[0], 
                    imgSource: kurumsalImage 
                })
            case 'kurumsalBuzdolabi4':
                return this.countAnswers({ 
                    questionsArray: questions.kurumsalBuzdolabi4, 
                    mainQuestion: questions.kurumsalBuzdolabi4[0].question, 
                    imgSource: kurumsalImage 
                })
            case 'kurumsalBuzdolabi5':
                return this.countAnswers({ 
                    questionsArray: questions.kurumsalBuzdolabi5, 
                    mainQuestion: questions.kurumsalBuzdolabi5[0].question, 
                    imgSource: kurumsalImage 
                })
            case 'kurumsalBuzdolabi6':
                return this.countAnswers({ 
                    questionsArray: questions.kurumsalBuzdolabi6, 
                    mainQuestion: questions.kurumsalBuzdolabi6[0].question, 
                    imgSource: kurumsalImage 
                })
            case 'kurumsalBuzdolabi7':
                return this.countAnswers({ 
                    questionsArray: questions.kurumsalBuzdolabi7, 
                    mainQuestion: questions.kurumsalBuzdolabi7[0].question, 
                    imgSource: kurumsalImage 
                })
            case 'kurumsalBuzdolabi8':
                return this.countAnswers({ 
                    questionsArray: questions.kurumsalBuzdolabi8, 
                    mainQuestion: questions.kurumsalBuzdolabi8[0], 
                    imgSource: kurumsalImage 
                })
            case 'kurumsalDerinDondurucu1':
                return this.countAnswers({ 
                    questionsArray: questions.kurumsalDerinDondurucu1, 
                    mainQuestion: questions.kurumsalDerinDondurucu1[0], 
                    imgSource: kurumsalImage 
                })
            case 'kurumsalDerinDondurucu2':
                return this.countAnswers({ 
                    questionsArray: questions.kurumsalDerinDondurucu2, 
                    mainQuestion: questions.kurumsalDerinDondurucu2[0], 
                    imgSource: kurumsalImage 
                })
            case 'kurumsalDerinDondurucu3':
                return this.countAnswers({ 
                    questionsArray: questions.kurumsalDerinDondurucu3, 
                    mainQuestion: questions.kurumsalDerinDondurucu3[0], 
                    imgSource: kurumsalImage 
                })
            case 'kurumsalDerinDondurucu4':
                return this.countAnswers({ 
                    questionsArray: questions.kurumsalDerinDondurucu4, 
                    mainQuestion: questions.kurumsalDerinDondurucu4[0], 
                    imgSource: kurumsalImage 
                })
            case 'kurumsalDerinDondurucu5':
                return this.countAnswers({ 
                    questionsArray: questions.kurumsalDerinDondurucu5, 
                    mainQuestion: questions.kurumsalDerinDondurucu5[0], 
                    imgSource: kurumsalImage 
                })
            case 'kurumsalDerinDondurucu6':
                return this.countAnswers({ 
                    questionsArray: questions.kurumsalDerinDondurucu6, 
                    mainQuestion: questions.kurumsalDerinDondurucu6[0], 
                    imgSource: kurumsalImage 
                })
            case 'kurumsalDerinDondurucu7':
                return this.countAnswers({ 
                    questionsArray: questions.kurumsalDerinDondurucu7, 
                    mainQuestion: questions.kurumsalDerinDondurucu7[0], 
                    imgSource: kurumsalImage 
                })
            case 'kurumsalCamasirMakinesi1':
                return this.countAnswers({ 
                    questionsArray: questions.kurumsalCamasirMakinesi1, 
                    mainQuestion: questions.kurumsalCamasirMakinesi1[0], 
                    imgSource: kurumsalImage 
                })
            case 'kurumsalCamasirMakinesi2':
                return this.countAnswers({ 
                    questionsArray: questions.kurumsalCamasirMakinesi2, 
                    mainQuestion: questions.kurumsalCamasirMakinesi2[0], 
                    imgSource: kurumsalImage 
                })
            case 'kurumsalCamasirMakinesi3':
                return this.countAnswers({ 
                    questionsArray: questions.kurumsalCamasirMakinesi3, 
                    mainQuestion: questions.kurumsalCamasirMakinesi3[0], 
                    imgSource: kurumsalImage 
                })
            case 'kurumsalCamasirMakinesi4':
                return this.countAnswers({ 
                    questionsArray: questions.kurumsalCamasirMakinesi4, 
                    mainQuestion: questions.kurumsalCamasirMakinesi4[0], 
                    imgSource: kurumsalImage 
                })
            case 'kurumsalBulasikMakinesi1':
                return this.countAnswers({ 
                    questionsArray: questions.kurumsalBulasikMakinesi1, 
                    mainQuestion: questions.kurumsalBulasikMakinesi1[0], 
                    imgSource: kurumsalImage 
                })
            case 'kurumsalBulasikMakinesi2':
                return this.countAnswers({ 
                    questionsArray: questions.kurumsalBulasikMakinesi2, 
                    mainQuestion: questions.kurumsalBulasikMakinesi2[0], 
                    imgSource: kurumsalImage 
                })
            case 'kurumsalBulasikMakinesi3':
                return this.countAnswers({ 
                    questionsArray: questions.kurumsalBulasikMakinesi3, 
                    mainQuestion: questions.kurumsalBulasikMakinesi3[0], 
                    imgSource: kurumsalImage 
                })
            case 'kurumsalKurutmaMakinesi1':
                return this.countAnswers({ 
                    questionsArray: questions.kurumsalKurutmaMakinesi1, 
                    mainQuestion: questions.kurumsalKurutmaMakinesi1[0], 
                    imgSource: kurumsalImage 
                })
            case 'kurumsalKurutmaMakinesi2':
                return this.countAnswers({ 
                    questionsArray: questions.kurumsalKurutmaMakinesi2, 
                    mainQuestion: questions.kurumsalKurutmaMakinesi2[0], 
                    imgSource: kurumsalImage 
                })
            case 'kurumsalKurutmaMakinesi3':
                return this.countAnswers({ 
                    questionsArray: questions.kurumsalKurutmaMakinesi3, 
                    mainQuestion: questions.kurumsalKurutmaMakinesi3[0], 
                    imgSource: kurumsalImage 
                })
            case 'kurumsalKurutmaMakinesi4':
                return this.countAnswers({ 
                    questionsArray: questions.kurumsalKurutmaMakinesi4, 
                    mainQuestion: questions.kurumsalKurutmaMakinesi4[0], 
                    imgSource: kurumsalImage 
                })
            case 'kurumsalKurutmaliCamasirMakinesi1':
                return this.countAnswers({ 
                    questionsArray: questions.kurumsalKurutmaliCamasirMakinesi1, 
                    mainQuestion: questions.kurumsalKurutmaliCamasirMakinesi1[0], 
                    imgSource: kurumsalImage 
                })
            case 'kurumsalKurutmaliCamasirMakinesi2':
                return this.countAnswers({ 
                    questionsArray: questions.kurumsalKurutmaliCamasirMakinesi2, 
                    mainQuestion: questions.kurumsalKurutmaliCamasirMakinesi2[0], 
                    imgSource: kurumsalImage 
                })
            case 'kurumsalFirin1':
                return this.countAnswers({ 
                    questionsArray: questions.kurumsalFirin1, 
                    mainQuestion: questions.kurumsalFirin1[0], 
                    imgSource: kurumsalImage 
                })
            case 'kurumsalFirin2':
                return this.countAnswers({ 
                    questionsArray: questions.kurumsalFirin2, 
                    mainQuestion: questions.kurumsalFirin2[0], 
                    imgSource: kurumsalImage 
                })
            case 'kurumsalFirin3':
                return this.countAnswers({ 
                    questionsArray: questions.kurumsalFirin3, 
                    mainQuestion: questions.kurumsalFirin3[0], 
                    imgSource: kurumsalImage 
                })
            case 'kurumsalFirin4':
                return this.countAnswers({ 
                    questionsArray: questions.kurumsalFirin4, 
                    mainQuestion: questions.kurumsalFirin4[0], 
                    imgSource: kurumsalImage 
                })
            case 'kurumsalFirin5':
                return this.countAnswers({ 
                    questionsArray: questions.kurumsalFirin5, 
                    mainQuestion: questions.kurumsalFirin5[0], 
                    imgSource: kurumsalImage 
                })
            case 'kurumsalFirin6':
                return this.countAnswers({ 
                    questionsArray: questions.kurumsalFirin6, 
                    mainQuestion: questions.kurumsalFirin6[0], 
                    imgSource: kurumsalImage 
                })
            case 'kurumsalSetUstuFirin1':
                return this.countAnswers({ 
                    questionsArray: questions.kurumsalSetUstuFirin1, 
                    mainQuestion: questions.kurumsalSetUstuFirin1[0], 
                    imgSource: kurumsalImage 
                })
            case 'kurumsalSetUstuFirin2':
                return this.countAnswers({ 
                    questionsArray: questions.kurumsalSetUstuFirin2, 
                    mainQuestion: questions.kurumsalSetUstuFirin2[0], 
                    imgSource: kurumsalImage 
                })
            case 'kurumsalSetUstuFirin3':
                return this.countAnswers({ 
                    questionsArray: questions.kurumsalSetUstuFirin3, 
                    mainQuestion: questions.kurumsalSetUstuFirin3[0], 
                    imgSource: kurumsalImage 
                })
            case 'kurumsalMikrodalgaFirin1':
                return this.countAnswers({ 
                    questionsArray: questions.kurumsalMikrodalgaFirin1, 
                    mainQuestion: questions.kurumsalMikrodalgaFirin1[0], 
                    imgSource: kurumsalImage 
                })
            case 'kurumsalMikrodalgaFirin2':
                return this.countAnswers({ 
                    questionsArray: questions.kurumsalMikrodalgaFirin2, 
                    mainQuestion: questions.kurumsalMikrodalgaFirin2[0], 
                    imgSource: kurumsalImage 
                })
            case 'kurumsalSetUstuOcak1':
                return this.countAnswers({ 
                    questionsArray: questions.kurumsalSetUstuOcak1, 
                    mainQuestion: questions.kurumsalSetUstuOcak1[0], 
                    imgSource: kurumsalImage 
                })
            case 'kurumsalSetUstuOcak2':
                return this.countAnswers({ 
                    questionsArray: questions.kurumsalSetUstuOcak2, 
                    mainQuestion: questions.kurumsalSetUstuOcak2[0], 
                    imgSource: kurumsalImage 
                })
            case 'kurumsalSetUstuOcak3':
                return this.countAnswers({ 
                    questionsArray: questions.kurumsalSetUstuOcak3, 
                    mainQuestion: questions.kurumsalSetUstuOcak3[0], 
                    imgSource: kurumsalImage 
                })
            case 'kurumsalSetUstuOcak4':
                return this.countAnswers({ 
                    questionsArray: questions.kurumsalSetUstuOcak4, 
                    mainQuestion: questions.kurumsalSetUstuOcak4[0], 
                    imgSource: kurumsalImage 
                })
            case 'kurumsalAspirator1':
                return this.countAnswers({ 
                    questionsArray: questions.kurumsalAspirator1, 
                    mainQuestion: questions.kurumsalAspirator1[0], 
                    imgSource: kurumsalImage 
                })
            case 'kurumsalSuAritmaCihazi1':
                return this.countAnswers({ 
                    questionsArray: questions.kurumsalSuAritmaCihazi1, 
                    mainQuestion: questions.kurumsalSuAritmaCihazi1[0], 
                    imgSource: kurumsalImage 
                })
            case 'kurumsalSuAritmaCihazi2':
                return this.countAnswers({ 
                    questionsArray: questions.kurumsalSuAritmaCihazi2, 
                    mainQuestion: questions.kurumsalSuAritmaCihazi2[0], 
                    imgSource: kurumsalImage 
                })
            case 'kurumsalSuAritmaCihazi3':
                return this.countAnswers({ 
                    questionsArray: questions.kurumsalSuAritmaCihazi3, 
                    mainQuestion: questions.kurumsalSuAritmaCihazi3[0], 
                    imgSource: kurumsalImage 
                })
            case 'kurumsalAnkastreFirin1':
                return this.countAnswers({ 
                    questionsArray: questions.kurumsalAnkastreFirin1, 
                    mainQuestion: questions.kurumsalAnkastreFirin1[0], 
                    imgSource: kurumsalImage 
                })
            case 'kurumsalAnkastreFirin2':
                return this.countAnswers({ 
                    questionsArray: questions.kurumsalAnkastreFirin2, 
                    mainQuestion: questions.kurumsalAnkastreFirin2[0], 
                    imgSource: kurumsalImage 
                })
            case 'kurumsalAnkastreFirin3':
                return this.countAnswers({ 
                    questionsArray: questions.kurumsalAnkastreFirin3, 
                    mainQuestion: questions.kurumsalAnkastreFirin3[0], 
                    imgSource: kurumsalImage 
                })
            case 'kurumsalAnkastreFirin4':
                return this.countAnswers({ 
                    questionsArray: questions.kurumsalAnkastreFirin4, 
                    mainQuestion: questions.kurumsalAnkastreFirin4[0], 
                    imgSource: kurumsalImage 
                })
            case 'kurumsalAnkastreMikrodalgaFirin1':
                return this.countAnswers({ 
                    questionsArray: questions.kurumsalAnkastreMikrodalgaFirin1, 
                    mainQuestion: questions.kurumsalAnkastreMikrodalgaFirin1[0], 
                    imgSource: kurumsalImage 
                })
            case 'kurumsalAnkastreMikrodalgaFirin2':
                return this.countAnswers({ 
                    questionsArray: questions.kurumsalAnkastreMikrodalgaFirin2, 
                    mainQuestion: questions.kurumsalAnkastreMikrodalgaFirin2[0], 
                    imgSource: kurumsalImage 
                })
            case 'kurumsalAnkastreOcak1':
                return this.countAnswers({ 
                    questionsArray: questions.kurumsalAnkastreOcak1, 
                    mainQuestion: questions.kurumsalAnkastreOcak1[0], 
                    imgSource: kurumsalImage 
                })
            case 'kurumsalAnkastreOcak2':
                return this.countAnswers({ 
                    questionsArray: questions.kurumsalAnkastreOcak2, 
                    mainQuestion: questions.kurumsalAnkastreOcak2[0], 
                    imgSource: kurumsalImage 
                })
            case 'kurumsalAnkastreOcak3':
                return this.countAnswers({ 
                    questionsArray: questions.kurumsalAnkastreOcak3, 
                    mainQuestion: questions.kurumsalAnkastreOcak3[0], 
                    imgSource: kurumsalImage 
                })
            case 'kurumsalAnkastreOcak4':
                return this.countAnswers({ 
                    questionsArray: questions.kurumsalAnkastreOcak4, 
                    mainQuestion: questions.kurumsalAnkastreOcak4[0], 
                    imgSource: kurumsalImage 
                })
            case 'kurumsalAnkastreDavlumbaz1':
                return this.countAnswers({ 
                    questionsArray: questions.kurumsalAnkastreDavlumbaz1, 
                    mainQuestion: questions.kurumsalAnkastreDavlumbaz1[0], 
                    imgSource: kurumsalImage 
                })
            case 'kurumsalAnkastreDavlumbaz2':
                return this.countAnswers({ 
                    questionsArray: questions.kurumsalAnkastreDavlumbaz2, 
                    mainQuestion: questions.kurumsalAnkastreDavlumbaz2[0], 
                    imgSource: kurumsalImage 
                })
            case 'kurumsalAnkastreBulasikMakinesi1':
                return this.countAnswers({ 
                    questionsArray: questions.kurumsalAnkastreBulasikMakinesi1, 
                    mainQuestion: questions.kurumsalAnkastreBulasikMakinesi1[0], 
                    imgSource: kurumsalImage 
                })
            case 'kurumsalAnkastreBulasikMakinesi2':
                return this.countAnswers({ 
                    questionsArray: questions.kurumsalAnkastreBulasikMakinesi2, 
                    mainQuestion: questions.kurumsalAnkastreBulasikMakinesi2[0], 
                    imgSource: kurumsalImage 
                })
            case 'kurumsalAnkastreBuzdolabi1':
                return this.countAnswers({ 
                    questionsArray: questions.kurumsalAnkastreBuzdolabi1, 
                    mainQuestion: questions.kurumsalAnkastreBuzdolabi1[0], 
                    imgSource: kurumsalImage 
                })
            case 'kurumsalAnkastreBuzdolabi2':
                return this.countAnswers({ 
                    questionsArray: questions.kurumsalAnkastreBuzdolabi2, 
                    mainQuestion: questions.kurumsalAnkastreBuzdolabi2[0], 
                    imgSource: kurumsalImage 
                })
            case 'kurumsalAnkastreBuzdolabi3':
                return this.countAnswers({ 
                    questionsArray: questions.kurumsalAnkastreBuzdolabi3, 
                    mainQuestion: questions.kurumsalAnkastreBuzdolabi3[0], 
                    imgSource: kurumsalImage 
                })
            case 'kurumsalAnkastreAspirator1':
                return this.countAnswers({ 
                    questionsArray: questions.kurumsalAnkastreAspirator1, 
                    mainQuestion: questions.kurumsalAnkastreAspirator1[0], 
                    imgSource: kurumsalImage 
                })
            case 'kurumsalAnkastreAspirator2':
                return this.countAnswers({ 
                    questionsArray: questions.kurumsalAnkastreAspirator2, 
                    mainQuestion: questions.kurumsalAnkastreAspirator2[0], 
                    imgSource: kurumsalImage 
                })
            case 'kurumsalAnkastreSicakTutmaCekmecesi1':
                return this.countAnswers({ 
                    questionsArray: questions.kurumsalAnkastreSicakTutmaCekmecesi1, 
                    mainQuestion: questions.kurumsalAnkastreSicakTutmaCekmecesi1[0], 
                    imgSource: kurumsalImage 
                })
            case 'kurumsalAnkastreCamasirMakinesi1':
                return this.countAnswers({ 
                    questionsArray: questions.kurumsalAnkastreCamasirMakinesi1, 
                    mainQuestion: questions.kurumsalAnkastreCamasirMakinesi1[0], 
                    imgSource: kurumsalImage 
                })
            case 'kurumsalAnkastreKurutmaliCamasirMakinesi1':
                return this.countAnswers({ 
                    questionsArray: questions.kurumsalAnkastreKurutmaliCamasirMakinesi1, 
                    mainQuestion: questions.kurumsalAnkastreKurutmaliCamasirMakinesi1[0], 
                    imgSource: kurumsalImage 
                })
            case 'kurumsalAnkastreKahveMakinesi1':
                return this.countAnswers({ 
                    questionsArray: questions.kurumsalAnkastreKahveMakinesi1, 
                    mainQuestion: questions.kurumsalAnkastreKahveMakinesi1[0], 
                    imgSource: kurumsalImage 
                })
            case 'kurumsalAnkastreSet1':
                return this.countAnswers({ 
                    questionsArray: questions.kurumsalAnkastreSet1, 
                    mainQuestion: questions.kurumsalAnkastreSet1[0], 
                    imgSource: kurumsalImage 
                })
            case 'kurumsalAnkastreSet2':
                return this.countAnswers({ 
                    questionsArray: questions.kurumsalAnkastreSet2, 
                    mainQuestion: questions.kurumsalAnkastreSet2[0], 
                    imgSource: kurumsalImage 
                })
            case 'kurumsalKombi1':
                return this.countAnswers({ 
                    questionsArray: questions.kurumsalKombi1, 
                    mainQuestion: questions.kurumsalKombi1[0], 
                    imgSource: kurumsalImage 
                })
            case 'kurumsalKombi2':
                return this.countAnswers({ 
                    questionsArray: questions.kurumsalKombi2, 
                    mainQuestion: questions.kurumsalKombi2[0], 
                    imgSource: kurumsalImage 
                })
            case 'kurumsalKombi3':
                return this.countAnswers({ 
                    questionsArray: questions.kurumsalKombi3, 
                    mainQuestion: questions.kurumsalKombi3[0], 
                    imgSource: kurumsalImage 
                })
            case 'kurumsalSofben1':
                return this.countAnswers({ 
                    questionsArray: questions.kurumsalSofben1, 
                    mainQuestion: questions.kurumsalSofben1[0], 
                    imgSource: kurumsalImage 
                })
            case 'kurumsalTermosifon1':
                return this.countAnswers({ 
                    questionsArray: questions.kurumsalTermosifon1, 
                    mainQuestion: questions.kurumsalTermosifon1[0], 
                    imgSource: kurumsalImage 
                })
            case 'kurumsalTermosifon2':
                return this.countAnswers({ 
                    questionsArray: questions.kurumsalTermosifon2, 
                    mainQuestion: questions.kurumsalTermosifon2[0], 
                    imgSource: kurumsalImage 
                })
            case 'kurumsalAniSuIsiticisi1':
                return this.countAnswers({ 
                    questionsArray: questions.kurumsalAniSuIsiticisi1, 
                    mainQuestion: questions.kurumsalAniSuIsiticisi1[0], 
                    imgSource: kurumsalImage 
                })
            case 'kurumsalVantilator1':
                return this.countAnswers({ 
                    questionsArray: questions.kurumsalVantilator1, 
                    mainQuestion: questions.kurumsalVantilator1[0], 
                    imgSource: kurumsalImage 
                })
            case 'kurumsalOdaTermostati1':
                return this.countAnswers({ 
                    questionsArray: questions.kurumsalOdaTermostati1, 
                    mainQuestion: questions.kurumsalOdaTermostati1[0], 
                    imgSource: kurumsalImage 
                })
            case 'kurumsalOdaTermostati2':
                return this.countAnswers({ 
                    questionsArray: questions.kurumsalOdaTermostati2, 
                    mainQuestion: questions.kurumsalOdaTermostati2[0], 
                    imgSource: kurumsalImage 
                })
            case 'kurumsalFlavelSoba1':
                return this.countAnswers({ 
                    questionsArray: questions.kurumsalFlavelSoba1, 
                    mainQuestion: questions.kurumsalFlavelSoba1[0], 
                    imgSource: kurumsalImage 
                })
            case 'kurumsalFlavelSoba2':
                return this.countAnswers({ 
                    questionsArray: questions.kurumsalFlavelSoba2, 
                    mainQuestion: questions.kurumsalFlavelSoba2[0], 
                    imgSource: kurumsalImage 
                })
            case 'kurumsalFlavelSoba3':
                return this.countAnswers({ 
                    questionsArray: questions.kurumsalFlavelSoba3, 
                    mainQuestion: questions.kurumsalFlavelSoba3[0], 
                    imgSource: kurumsalImage 
                })
            case 'kurumsalKlima1':
                return this.countAnswers({ 
                    questionsArray: questions.kurumsalKlima1, 
                    mainQuestion: questions.kurumsalKlima1[0], 
                    imgSource: kurumsalImage 
                })
            case 'kurumsalKlima2':
                return this.countAnswers({ 
                    questionsArray: questions.kurumsalKlima2, 
                    mainQuestion: questions.kurumsalKlima2[0], 
                    imgSource: kurumsalImage 
                })
            case 'kurumsalKlima3':
                return this.countAnswers({ 
                    questionsArray: questions.kurumsalKlima3, 
                    mainQuestion: questions.kurumsalKlima3[0], 
                    imgSource: kurumsalImage 
                })
            case 'kurumsalKlima4':
                return this.countAnswers({ 
                    questionsArray: questions.kurumsalKlima4, 
                    mainQuestion: questions.kurumsalKlima4[0], 
                    imgSource: kurumsalImage 
                })
            case 'bireyselBuzdolabi1':
                return this.countAnswers({ 
                    questionsArray: questions.bireyselBuzdolabi1, 
                    mainQuestion: questions.bireyselBuzdolabi1[0], 
                    imgSource: bireyselImage 
                })
            case 'bireyselBuzdolabi2':
                return this.countAnswers({ 
                    questionsArray: questions.bireyselBuzdolabi2, 
                    mainQuestion: questions.bireyselBuzdolabi2[0], 
                    imgSource: bireyselImage 
                })
            case 'bireyselBuzdolabi3':
                return this.countAnswers({ 
                    questionsArray: questions.bireyselBuzdolabi3, 
                    mainQuestion: questions.bireyselBuzdolabi3[0], 
                    imgSource: bireyselImage 
                })
            case 'bireyselBuzdolabi4':
                return this.countAnswers({ 
                    questionsArray: questions.bireyselBuzdolabi4, 
                    mainQuestion: questions.bireyselBuzdolabi4[0], 
                    imgSource: bireyselImage 
                })
            case 'bireyselBuzdolabi5':
                return this.countAnswers({ 
                    questionsArray: questions.bireyselBuzdolabi5, 
                    mainQuestion: questions.bireyselBuzdolabi5[0], 
                    imgSource: bireyselImage 
                })
            case 'bireyselBuzdolabi6':
                return this.countAnswers({ 
                    questionsArray: questions.bireyselBuzdolabi6, 
                    mainQuestion: questions.bireyselBuzdolabi6[0], 
                    imgSource: bireyselImage 
                })
            case 'bireyselBuzdolabi7':
                return this.countAnswers({ 
                    questionsArray: questions.bireyselBuzdolabi7, 
                    mainQuestion: questions.bireyselBuzdolabi7[0], 
                    imgSource: bireyselImage 
                })
            case 'bireyselBuzdolabi8':
                return this.countAnswers({ 
                    questionsArray: questions.bireyselBuzdolabi8, 
                    mainQuestion: questions.bireyselBuzdolabi8[0], 
                    imgSource: bireyselImage 
                })
            case 'bireyselDerinDondurucu1':
                return this.countAnswers({ 
                    questionsArray: questions.bireyselDerinDondurucu1, 
                    mainQuestion: questions.bireyselDerinDondurucu1[0], 
                    imgSource: bireyselImage 
                })
            case 'bireyselDerinDondurucu2':
                return this.countAnswers({ 
                    questionsArray: questions.bireyselDerinDondurucu2, 
                    mainQuestion: questions.bireyselDerinDondurucu2[0], 
                    imgSource: bireyselImage 
                })
            case 'bireyselDerinDondurucu3':
                return this.countAnswers({ 
                    questionsArray: questions.bireyselDerinDondurucu3, 
                    mainQuestion: questions.bireyselDerinDondurucu3[0], 
                    imgSource: bireyselImage 
                })
            case 'bireyselDerinDondurucu4':
                return this.countAnswers({ 
                    questionsArray: questions.bireyselDerinDondurucu4, 
                    mainQuestion: questions.bireyselDerinDondurucu4[0], 
                    imgSource: bireyselImage 
                })
            case 'bireyselDerinDondurucu5':
                return this.countAnswers({ 
                    questionsArray: questions.bireyselDerinDondurucu5, 
                    mainQuestion: questions.bireyselDerinDondurucu5[0], 
                    imgSource: bireyselImage 
                })
            case 'bireyselDerinDondurucu6':
                return this.countAnswers({ 
                    questionsArray: questions.bireyselDerinDondurucu6, 
                    mainQuestion: questions.bireyselDerinDondurucu6[0], 
                    imgSource: bireyselImage 
                })
            case 'bireyselDerinDondurucu7':
                return this.countAnswers({ 
                    questionsArray: questions.bireyselDerinDondurucu7, 
                    mainQuestion: questions.bireyselDerinDondurucu7[0], 
                    imgSource: bireyselImage 
                })
            case 'bireyselCamasirMakinesi1':
                return this.countAnswers({ 
                    questionsArray: questions.bireyselCamasirMakinesi1, 
                    mainQuestion: questions.bireyselCamasirMakinesi1[0], 
                    imgSource: bireyselImage 
                })
            case 'bireyselCamasirMakinesi2':
                return this.countAnswers({ 
                    questionsArray: questions.bireyselCamasirMakinesi2, 
                    mainQuestion: questions.bireyselCamasirMakinesi2[0], 
                    imgSource: bireyselImage 
                })
            case 'bireyselCamasirMakinesi3':
                return this.countAnswers({ 
                    questionsArray: questions.bireyselCamasirMakinesi3, 
                    mainQuestion: questions.bireyselCamasirMakinesi3[0], 
                    imgSource: bireyselImage 
                })
            case 'bireyselCamasirMakinesi4':
                return this.countAnswers({ 
                    questionsArray: questions.bireyselCamasirMakinesi4, 
                    mainQuestion: questions.bireyselCamasirMakinesi4[0], 
                    imgSource: bireyselImage 
                })
            case 'bireyselBulasikMakinesi1':
                return this.countAnswers({ 
                    questionsArray: questions.bireyselBulasikMakinesi1, 
                    mainQuestion: questions.bireyselBulasikMakinesi1[0], 
                    imgSource: bireyselImage 
                })
            case 'bireyselBulasikMakinesi2':
                return this.countAnswers({ 
                    questionsArray: questions.bireyselBulasikMakinesi2, 
                    mainQuestion: questions.bireyselBulasikMakinesi2[0], 
                    imgSource: bireyselImage 
                })
            case 'bireyselBulasikMakinesi3':
                return this.countAnswers({ 
                    questionsArray: questions.bireyselBulasikMakinesi3, 
                    mainQuestion: questions.bireyselBulasikMakinesi3[0], 
                    imgSource: bireyselImage 
                })
            case 'bireyselKurutmaMakinesi1':
                return this.countAnswers({ 
                    questionsArray: questions.bireyselKurutmaMakinesi1, 
                    mainQuestion: questions.bireyselKurutmaMakinesi1[0], 
                    imgSource: bireyselImage 
                })
            case 'bireyselKurutmaMakinesi2':
                return this.countAnswers({ 
                    questionsArray: questions.bireyselKurutmaMakinesi2, 
                    mainQuestion: questions.bireyselKurutmaMakinesi2[0], 
                    imgSource: bireyselImage 
                })
            case 'bireyselKurutmaMakinesi3':
                return this.countAnswers({ 
                    questionsArray: questions.bireyselKurutmaMakinesi3, 
                    mainQuestion: questions.bireyselKurutmaMakinesi3[0], 
                    imgSource: bireyselImage 
                })
            case 'bireyselKurutmaMakinesi4':
                return this.countAnswers({ 
                    questionsArray: questions.bireyselKurutmaMakinesi4, 
                    mainQuestion: questions.bireyselKurutmaMakinesi4[0], 
                    imgSource: bireyselImage 
                })
            case 'bireyselKurutmaliCamasirMakinesi1':
                return this.countAnswers({ 
                    questionsArray: questions.bireyselKurutmaliCamasirMakinesi1, 
                    mainQuestion: questions.bireyselKurutmaliCamasirMakinesi1[0], 
                    imgSource: bireyselImage 
                })
            case 'bireyselKurutmaliCamasirMakinesi2':
                return this.countAnswers({ 
                    questionsArray: questions.bireyselKurutmaliCamasirMakinesi2, 
                    mainQuestion: questions.bireyselKurutmaliCamasirMakinesi2[0], 
                    imgSource: bireyselImage 
                })
            case 'bireyselFirin1':
                return this.countAnswers({ 
                    questionsArray: questions.bireyselFirin1, 
                    mainQuestion: questions.bireyselFirin1[0], 
                    imgSource: bireyselImage 
                })
            case 'bireyselFirin2':
                return this.countAnswers({ 
                    questionsArray: questions.bireyselFirin2, 
                    mainQuestion: questions.bireyselFirin2[0], 
                    imgSource: bireyselImage 
                })
            case 'bireyselFirin3':
                return this.countAnswers({ 
                    questionsArray: questions.bireyselFirin3, 
                    mainQuestion: questions.bireyselFirin3[0], 
                    imgSource: bireyselImage 
                })
            case 'bireyselFirin4':
                return this.countAnswers({ 
                    questionsArray: questions.bireyselFirin4, 
                    mainQuestion: questions.bireyselFirin4[0], 
                    imgSource: bireyselImage 
                })
            case 'bireyselFirin5':
                return this.countAnswers({ 
                    questionsArray: questions.bireyselFirin5, 
                    mainQuestion: questions.bireyselFirin5[0], 
                    imgSource: bireyselImage 
                })
            case 'bireyselFirin6':
                return this.countAnswers({ 
                    questionsArray: questions.bireyselFirin6, 
                    mainQuestion: questions.bireyselFirin6[0], 
                    imgSource: bireyselImage 
                })
            case 'bireyselSetUstuFirin1':
                return this.countAnswers({ 
                    questionsArray: questions.bireyselSetUstuFirin1, 
                    mainQuestion: questions.bireyselSetUstuFirin1[0], 
                    imgSource: bireyselImage 
                })
            case 'bireyselSetUstuFirin2':
                return this.countAnswers({ 
                    questionsArray: questions.bireyselSetUstuFirin2, 
                    mainQuestion: questions.bireyselSetUstuFirin2[0], 
                    imgSource: bireyselImage 
                })
            case 'bireyselSetUstuFirin3':
                return this.countAnswers({ 
                    questionsArray: questions.bireyselSetUstuFirin3, 
                    mainQuestion: questions.bireyselSetUstuFirin3[0], 
                    imgSource: bireyselImage 
                })
            case 'bireyselMikrodalgaFirin1':
                return this.countAnswers({ 
                    questionsArray: questions.bireyselMikrodalgaFirin1, 
                    mainQuestion: questions.bireyselMikrodalgaFirin1[0], 
                    imgSource: bireyselImage 
                })
            case 'bireyselMikrodalgaFirin2':
                return this.countAnswers({ 
                    questionsArray: questions.bireyselMikrodalgaFirin2, 
                    mainQuestion: questions.bireyselMikrodalgaFirin2[0], 
                    imgSource: bireyselImage 
                })
            case 'bireyselSetUstuOcak1':
                return this.countAnswers({ 
                    questionsArray: questions.bireyselSetUstuOcak1, 
                    mainQuestion: questions.bireyselSetUstuOcak1[0], 
                    imgSource: bireyselImage 
                })
            case 'bireyselSetUstuOcak2':
                return this.countAnswers({ 
                    questionsArray: questions.bireyselSetUstuOcak2, 
                    mainQuestion: questions.bireyselSetUstuOcak2[0], 
                    imgSource: bireyselImage 
                })
            case 'bireyselSetUstuOcak3':
                return this.countAnswers({ 
                    questionsArray: questions.bireyselSetUstuOcak3, 
                    mainQuestion: questions.bireyselSetUstuOcak3[0], 
                    imgSource: bireyselImage 
                })
            case 'bireyselSetUstuOcak4':
                return this.countAnswers({ 
                    questionsArray: questions.bireyselSetUstuOcak4, 
                    mainQuestion: questions.bireyselSetUstuOcak4[0], 
                    imgSource: bireyselImage 
                })
            case 'bireyselAspirator1':
                return this.countAnswers({ 
                    questionsArray: questions.bireyselAspirator1, 
                    mainQuestion: questions.bireyselAspirator1[0], 
                    imgSource: bireyselImage 
                })
            case 'bireyselSuAritmaCihazi1':
                return this.countAnswers({ 
                    questionsArray: questions.bireyselSuAritmaCihazi1, 
                    mainQuestion: questions.bireyselSuAritmaCihazi1[0], 
                    imgSource: bireyselImage 
                })
            case 'bireyselSuAritmaCihazi2':
                return this.countAnswers({ 
                    questionsArray: questions.bireyselSuAritmaCihazi2, 
                    mainQuestion: questions.bireyselSuAritmaCihazi2[0], 
                    imgSource: bireyselImage 
                })
            case 'bireyselSuAritmaCihazi3':
                return this.countAnswers({ 
                    questionsArray: questions.bireyselSuAritmaCihazi3, 
                    mainQuestion: questions.bireyselSuAritmaCihazi3[0], 
                    imgSource: bireyselImage 
                })
            case 'bireyselAnkastreFirin1':
                return this.countAnswers({ 
                    questionsArray: questions.bireyselAnkastreFirin1, 
                    mainQuestion: questions.bireyselAnkastreFirin1[0], 
                    imgSource: bireyselImage 
                })
            case 'bireyselAnkastreFirin2':
                return this.countAnswers({ 
                    questionsArray: questions.bireyselAnkastreFirin2, 
                    mainQuestion: questions.bireyselAnkastreFirin2[0], 
                    imgSource: bireyselImage 
                })
            case 'bireyselAnkastreFirin3':
                return this.countAnswers({ 
                    questionsArray: questions.bireyselAnkastreFirin3, 
                    mainQuestion: questions.bireyselAnkastreFirin3[0], 
                    imgSource: bireyselImage 
                })
            case 'bireyselAnkastreFirin4':
                return this.countAnswers({ 
                    questionsArray: questions.bireyselAnkastreFirin4, 
                    mainQuestion: questions.bireyselAnkastreFirin4[0], 
                    imgSource: bireyselImage 
                })
            case 'bireyselAnkastreMikrodalgaFirin1':
                return this.countAnswers({ 
                    questionsArray: questions.bireyselAnkastreMikrodalgaFirin1, 
                    mainQuestion: questions.bireyselAnkastreMikrodalgaFirin1[0], 
                    imgSource: bireyselImage 
                })
            case 'bireyselAnkastreMikrodalgaFirin2':
                return this.countAnswers({ 
                    questionsArray: questions.bireyselAnkastreMikrodalgaFirin2, 
                    mainQuestion: questions.bireyselAnkastreMikrodalgaFirin2[0], 
                    imgSource: bireyselImage 
                })
            case 'bireyselAnkastreOcak1':
                return this.countAnswers({ 
                    questionsArray: questions.bireyselAnkastreOcak1, 
                    mainQuestion: questions.bireyselAnkastreOcak1[0], 
                    imgSource: bireyselImage 
                })
            case 'bireyselAnkastreDavlumbaz1':
                return this.countAnswers({ 
                    questionsArray: questions.bireyselAnkastreDavlumbaz1, 
                    mainQuestion: questions.bireyselAnkastreDavlumbaz1[0], 
                    imgSource: bireyselImage 
                })
            case 'bireyselAnkastreDavlumbaz2':
                return this.countAnswers({ 
                    questionsArray: questions.bireyselAnkastreDavlumbaz2, 
                    mainQuestion: questions.bireyselAnkastreDavlumbaz2[0], 
                    imgSource: bireyselImage 
                })
            case 'bireyselAnkastreBulasikMakinesi1':
                return this.countAnswers({ 
                    questionsArray: questions.bireyselAnkastreBulasikMakinesi1, 
                    mainQuestion: questions.bireyselAnkastreBulasikMakinesi1[0], 
                    imgSource: bireyselImage 
                })
            case 'bireyselAnkastreBulasikMakinesi2':
                return this.countAnswers({ 
                    questionsArray: questions.bireyselAnkastreBulasikMakinesi2, 
                    mainQuestion: questions.bireyselAnkastreBulasikMakinesi2[0], 
                    imgSource: bireyselImage 
                })
            case 'bireyselAnkastreBuzdolabi1':
                return this.countAnswers({ 
                    questionsArray: questions.bireyselAnkastreBuzdolabi1, 
                    mainQuestion: questions.bireyselAnkastreBuzdolabi1[0], 
                    imgSource: bireyselImage 
                })
            case 'bireyselAnkastreBuzdolabi2':
                return this.countAnswers({ 
                    questionsArray: questions.bireyselAnkastreBuzdolabi2, 
                    mainQuestion: questions.bireyselAnkastreBuzdolabi2[0], 
                    imgSource: bireyselImage 
                })
            case 'bireyselAnkastreBuzdolabi3':
                return this.countAnswers({ 
                    questionsArray: questions.bireyselAnkastreBuzdolabi3, 
                    mainQuestion: questions.bireyselAnkastreBuzdolabi3[0], 
                    imgSource: bireyselImage 
                })
            case 'bireyselAnkastreAspirator1':
                return this.countAnswers({ 
                    questionsArray: questions.bireyselAnkastreAspirator1, 
                    mainQuestion: questions.bireyselAnkastreAspirator1[0], 
                    imgSource: bireyselImage 
                })
            case 'bireyselAnkastreAspirator2':
                return this.countAnswers({ 
                    questionsArray: questions.bireyselAnkastreAspirator2, 
                    mainQuestion: questions.bireyselAnkastreAspirator2[0], 
                    imgSource: bireyselImage 
                })
            case 'bireyselAnkastreSicakTutmaMakinesi1':
                return this.countAnswers({ 
                    questionsArray: questions.bireyselAnkastreSicakTutmaMakinesi1, 
                    mainQuestion: questions.bireyselAnkastreSicakTutmaMakinesi1[0], 
                    imgSource: bireyselImage 
                })
            case 'bireyselAnkastreCamasirMakinesi1':
                return this.countAnswers({ 
                    questionsArray: questions.bireyselAnkastreCamasirMakinesi1, 
                    mainQuestion: questions.bireyselAnkastreCamasirMakinesi1[0], 
                    imgSource: bireyselImage 
                })
            case 'bireyselAnkastreKurutmaliCamasirMakinesi1':
                return this.countAnswers({ 
                    questionsArray: questions.bireyselAnkastreKurutmaliCamasirMakinesi1, 
                    mainQuestion: questions.bireyselAnkastreKurutmaliCamasirMakinesi1[0], 
                    imgSource: bireyselImage 
                })
            case 'bireyselAnkastreKahveMakinesi1':
                return this.countAnswers({ 
                    questionsArray: questions.bireyselAnkastreKahveMakinesi1, 
                    mainQuestion: questions.bireyselAnkastreKahveMakinesi1[0], 
                    imgSource: bireyselImage 
                })
            case 'bireyselAnkastreSet1':
                return this.countAnswers({ 
                    questionsArray: questions.bireyselAnkastreSet1, 
                    mainQuestion: questions.bireyselAnkastreSet1[0], 
                    imgSource: bireyselImage 
                })
            case 'bireyselAnkastreSet2':
                return this.countAnswers({ 
                    questionsArray: questions.bireyselAnkastreSet2, 
                    mainQuestion: questions.bireyselAnkastreSet2[0], 
                    imgSource: bireyselImage 
                })
            case 'bireyselKombi1':
                return this.countAnswers({ 
                    questionsArray: questions.bireyselKombi1, 
                    mainQuestion: questions.bireyselKombi1[0], 
                    imgSource: bireyselImage 
                })
            case 'bireyselKombi2':
                return this.countAnswers({ 
                    questionsArray: questions.bireyselKombi2, 
                    mainQuestion: questions.bireyselKombi2[0], 
                    imgSource: bireyselImage 
                })
            case 'bireyselKombi3':
                return this.countAnswers({ 
                    questionsArray: questions.bireyselKombi3, 
                    mainQuestion: questions.bireyselKombi3[0], 
                    imgSource: bireyselImage 
                })
            case 'bireyselSofben1':
                return this.countAnswers({ 
                    questionsArray: questions.bireyselSofben1, 
                    mainQuestion: questions.bireyselSofben1[0], 
                    imgSource: bireyselImage 
                })
            case 'bireyselTermosifon1':
                return this.countAnswers({ 
                    questionsArray: questions.bireyselTermosifon1, 
                    mainQuestion: questions.bireyselTermosifon1[0], 
                    imgSource: bireyselImage 
                })
            case 'bireyselTermosifon2':
                return this.countAnswers({ 
                    questionsArray: questions.bireyselTermosifon2, 
                    mainQuestion: questions.bireyselTermosifon2[0], 
                    imgSource: bireyselImage 
                })
            case 'bireyselAniSuIsiticisi1':
                return this.countAnswers({ 
                    questionsArray: questions.bireyselAniSuIsiticisi1, 
                    mainQuestion: questions.bireyselAniSuIsiticisi1[0], 
                    imgSource: bireyselImage 
                })
            case 'bireyselVantilator1':
                return this.countAnswers({ 
                    questionsArray: questions.bireyselVantilator1, 
                    mainQuestion: questions.bireyselVantilator1[0], 
                    imgSource: bireyselImage 
                })
            case 'bireyselOdaTermostati1':
                return this.countAnswers({ 
                    questionsArray: questions.bireyselOdaTermostati1, 
                    mainQuestion: questions.bireyselOdaTermostati1[0], 
                    imgSource: bireyselImage 
                })
            case 'bireyselOdaTermostati2':
                return this.countAnswers({ 
                    questionsArray: questions.bireyselOdaTermostati2, 
                    mainQuestion: questions.bireyselOdaTermostati2[0], 
                    imgSource: bireyselImage 
                })
            case 'bireyselFlavelSoba1':
                return this.countAnswers({ 
                    questionsArray: questions.bireyselFlavelSoba1, 
                    mainQuestion: questions.bireyselFlavelSoba1[0], 
                    imgSource: bireyselImage 
                })
            case 'bireyselFlavelSoba2':
                return this.countAnswers({ 
                    questionsArray: questions.bireyselFlavelSoba2, 
                    mainQuestion: questions.bireyselFlavelSoba2[0], 
                    imgSource: bireyselImage 
                })
            case 'bireyselFlavelSoba3':
                return this.countAnswers({ 
                    questionsArray: questions.bireyselFlavelSoba3, 
                    mainQuestion: questions.bireyselFlavelSoba3[0], 
                    imgSource: bireyselImage 
                })
            case 'bireyselKlima1':
                return this.countAnswers({ 
                    questionsArray: questions.bireyselKlima1, 
                    mainQuestion: questions.bireyselKlima1[0], 
                    imgSource: bireyselImage 
                })
            case 'bireyselKlima2':
                return this.countAnswers({ 
                    questionsArray: questions.bireyselKlima2, 
                    mainQuestion: questions.bireyselKlima2[0], 
                    imgSource: bireyselImage 
                })
            case 'bireyselKlima3':
                return this.countAnswers({ 
                    questionsArray: questions.bireyselKlima3, 
                    mainQuestion: questions.bireyselKlima3[0], 
                    imgSource: bireyselImage 
                })
            case 'bireyselKlima4':
                return this.countAnswers({ 
                    questionsArray: questions.bireyselKlima4, 
                    mainQuestion: questions.bireyselKlima4[0], 
                    imgSource: bireyselImage 
                })
            case 'finalPage':
                return this.props.navigation.navigate('ProductList', { answers: this.state.questionsForAnswersArray, noGoBack: true })
            default:
                return null;
        }
    }

    handleAnswerSelect = (userAnswer, userTo) => {

        console.log(userAnswer)
        let newAnswers = this.state.answers;
        let newQuestionsForAnswersArray = this.state.questionsForAnswersArray;
        //newQuestionsForAnswersArray.push(this.state.mainQuestion);
        newAnswers.push(userAnswer);
        
        if(userAnswer.len != undefined){
            if(userAnswer.len < 5 && userAnswer.value != null)
            {
                return this.props.navigation.navigate('ProductList', { answers: newQuestionsForAnswersArray, noGoBack: true })
            }
        }     

        return (
            this.setState({ 
                answers: newAnswers,
                questionsForAnswersArray: newQuestionsForAnswersArray
            }),
            this.props.navigation.navigate('QuestionsPage', { 
                counter: this.state.counter + 1, 
                to: userTo,
                answers: this.state.answers,
                questionsForAnswersArray: this.state.questionsForAnswersArray
            })
        );
    }

    handleGoFirstPageButton = () => {
        const resetAction= NavigationActions.reset({
          index:0,
          actions:[
                NavigationActions.navigate({ routeName: 'AirCondSelectionRobot' })
          ],
        })
        this.props.navigation.dispatch(resetAction)
    }

    itemCounter = (item) => {
        if(item.len != undefined) {
            if(item.value != null)
                return `(${item.len})`
        }
        else {
            return ''
        }
    }

    render() {
        return (
            <View style={{ flex: 1, backgroundColor: colors.white }} >
                <ImageBackground source={bgImage} resizeMode='cover' style={{ width: WINDOW_WIDTH, height: WINDOW_HEIGHT - 100 }} >
                    <WhichPageHeader headerTitle='ARÇELİK ÜRÜN SEÇİM ASİSTANI' />
                    <ExplanationHeader content={this.state.mainQuestion.question} />
                    <FlatList
                        data={this.state.questionsArray}
                        renderItem={({item}) => (
                            <TouchableOpacity onPress={() => this.handleAnswerSelect(item, item.to) }>
                                <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'flex-start', marginTop: 24 }} >
                                    <View style={{ width: 10, height: 10, borderRadius: 5, backgroundColor: colors.white, marginLeft: 16 }} />
                                    <Text 
                                        style={{ 
                                            color: colors.white, 
                                            marginLeft: 16, 
                                            fontSize: 20 }} 
                                            >{item.content} { this.itemCounter(item) }</Text>
                                </View>
                            </TouchableOpacity>
                        )}
                        keyExtractor={(item, index) => item.id}
                        showsHorizontalScrollIndicator={true}
                    />

                    <View style={{ position: 'absolute', bottom: 96, right: 16, zIndex: 12 }} >
                        <Image source={this.state.imgSource} style={{ width: 40, height: 40, resizeMode: 'contain' }} />
                    </View>

                    <View style={{ width: WINDOW_WIDTH, height: 40, backgroundColor: colors.red, flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between' }} >
                        <TouchableOpacity onPress={() => this.handleGoFirstPageButton()} >
                            <Text style={{ paddingLeft: 16, color: colors.white, fontSize: 18 }} >Başa Dön</Text>
                        </TouchableOpacity>
                        <Text style={{ paddingRight: 16, color: colors.white, fontSize: 18 }} >{this.state.counter}. Adım</Text>
                    </View>

                    <View style={{ width: WINDOW_WIDTH, height: 40, backgroundColor: colors.white, flexDirection: 'row', alignItems: 'center', justifyContent: 'flex-start' }} >
                        <View>
                            <Text style={{ paddingHorizontal: 16, color: colors.grey, fontSize: 12 }} >*Aradığınız Arçelik ürününü seçmek için lütfen tüm sorulara eksiksiz cevap veriniz.</Text>
                        </View>
                    </View>

                    {/*//console.log(this.state.questionsForAnswersArray)*/}
                    
                </ImageBackground>
                
            </View>
        );
    }
}

export default QuestionsPage;