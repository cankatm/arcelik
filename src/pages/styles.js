import React from 'react';
import { StyleSheet, Dimensions } from 'react-native';

import * as colors from '../helpers/ColorPalette';

const WINDOW_WIDTH = Dimensions.get('window').width;
const WINDOW_HEIGHT = Dimensions.get('window').height;

const HEADER_HEIGHT = 60;

export default StyleSheet.create({
    homeSliderButtonTextStyle: { 

        color: colors.white,
        fontSize: 14
    },
    homeSliderButtonContainerStyle: { 
        width : WINDOW_WIDTH / 2,
        backgroundColor: colors.red, 
        paddingHorizontal: 16,
        paddingVertical: 12,
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 10
    },
    productListFiltreHeaderContainerStyle: {
        width: WINDOW_WIDTH,
        height: HEADER_HEIGHT,
        backgroundColor: colors.darkestGrey,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
    },
    productListFiltreContainerStyle: { 
        width: WINDOW_WIDTH, 
        height: WINDOW_HEIGHT - 150, 
        backgroundColor: 'rgba(0, 0, 0, 0.85)', 
        position: 'absolute', 
        left: 0, 
        top: HEADER_HEIGHT, 
        zIndex: 4 
    },
    productListFiltreButtonsContainerStyle: { 
        width: WINDOW_WIDTH, 
        height: HEADER_HEIGHT,
        flexDirection: 'row' 
    },
    productListClearButtonContainerStyle: { 
        width: WINDOW_WIDTH / 3, 
        height: HEADER_HEIGHT, 
        backgroundColor: colors.darkestGrey, 
        alignItems: 'center', 
        justifyContent: 'center' 
    },
    productListClearButtonTextStyle: { 
        color: colors.white 
    },
    productListFiltreButtonContainerStyle: { 
        width: (WINDOW_WIDTH / 3) * 2, 
        height: HEADER_HEIGHT, 
        backgroundColor: colors.red, 
        alignItems: 'center', 
        justifyContent: 'center' 
    },
    productListFiltreButtonTextStyle: { 
        color: colors.white 
    },
    contactPageMapStyle: {
        width: WINDOW_WIDTH,
        height: (WINDOW_WIDTH / 4) * 3,
    },
    whoAreWeInformationContainerStyle: {
        width: WINDOW_WIDTH,
        paddingVertical: 32,
        paddingHorizontal: 16,
        backgroundColor: colors.white,
    },
    searchPageSearchAreaContainerStyle: {
        width: WINDOW_WIDTH,
        height: HEADER_HEIGHT, 
        backgroundColor: colors.darkestGrey,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between'
    },
    settingsPageContainerStyle: {
        width: WINDOW_WIDTH - 32,
        marginHorizontal: 16,
        backgroundColor: colors.white,
    },
    settingsPageSettingsContainerStyle: { 
        flexDirection: 'row', 
        alignItems: 'center', 
        justifyContent: 'space-between', 
        marginVertical: 32 
    },
    settingsPageSettingsTextStyle: { 
        fontSize: 16
    },
    settingsPageAboutAppTextStyle: { 
        marginVertical: 16, 
        color: colors.lightGrey, 
        fontSize: 14 
    },
    settingsPageLineStyle: {
        width: 32, 
        height: 2, 
        marginBottom: 16, 
        backgroundColor: colors.lightGrey 
    },
    settingsPageGeneralTextStyle: { 
        fontSize: 12,
        color: colors.black,
    },
    settingsPageGeneralBolderTextStyle: {
        fontSize: 12,
        color: colors.red
    },
});