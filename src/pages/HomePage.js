import React, { Component } from 'react';
import { 
    View, 
    Text, 
    StyleSheet, 
    StatusBar,
    TouchableOpacity,
    ScrollView,
    InteractionManager
} from 'react-native';

import * as colors from '../helpers/ColorPalette';
import styles from './styles';
import { HomeSlider, FeaturedProductsSlider } from '../components/Sliders';
import { ContactArea } from '../components/ContactArea';

/* Services */
import Api from '../services/WooCommerce/Api'

class HomePage extends Component {
    // constructor(props) {
    //     super(props);
    //     this._scrollToTop = this._scrollToTop.bind(this);
    // }

    // componentWillMount() {
    //     InteractionManager.runAfterInteractions(() => {
    //         this.props.navigation.setParams({
    //             scrollToTop: this._scrollToTop,
    //         })
    //     })
    // }

    // _scrollToTop = () => {
    //   if (!!this.refs.scrollView.wrappedInstance.getScrollResponder) {
    //     const scrollResponder = this.refs.scrollView.wrappedInstance.getScrollResponder(reactNode)
  
    //     scrollResponder.scrollTo({x: 0, y: 0, animated: true})
    //   }
    // }

    
    

    async componentWillMount() {

        try {
            const update = await Expo.Updates.checkForUpdateAsync();
            if (update.isAvailable) {
              await Expo.Updates.fetchUpdateAsync();
              // ... notify user of update ...
              Expo.Updates.reloadFromCache();
            }
          } catch (e) {
            // handle or log error
        }

        // Api.get('products',{
        //     per_page: 100, // tum kategorileri alsin diye
        //     category: 231,
        //     status:'publish'
        // })
        // .then((data) => {
        //     //console.log('buzdolabi')
        //     //console.log(data);            
        //     //console.log(data.filter(item => {
        //         let fil = item.attributes.find(at => at.name == "Soğutma Sistemi")
        //         if(fil.options.includes("No Frost"))
        //             return true
        //         else
        //             return false
        //     }))
        // });
        
        // /* Ana Kategorileri Getirir */
        // Api.get('products/categories',{
        //     per_page: 100, // tum kategorileri alsin diye
        //     parent:0
        // })
        // .then(function (data) {
        //     //console.log(data);
        // });

        // /* isitma sogutma kategori ornek */
        // Api.get('products/categories',{
        //     per_page: 100, // tum kategorileri alsin diye
        //     parent:40
        // })
        // .then(function (data) {
        //     //console.log(data);
        // });

        // /* klima kategori ornek */
        // Api.get('products/categories',{
        //     per_page: 100, // tum kategorileri alsin diye
        //     parent:15
        // })
        // .then(function (data) {
        //     // 22,23,24
        //     //console.log(data);
        // });        

        // /* Ev tipi klima urunler ornek */
        // Api.get('products/',{
        //     per_page: 100, // tum kategorileri alsin diye
        //     category: 22
        //     //status:'publish'
        // })
        // .then(function (data) {
        //     //console.log(data);
        // });

        /* One cikan urunler */
        // Api.get('products/',{
        //     per_page: 100, // tum kategorileri alsin diye
        //     featured: true
        //     //status:'publish'
        // })
        // .then((data) => {
        //     this.setState({
        //         featuredProducts: data,
        //       });
        //     //console.log(data);
        // });

        /* Arama ornegi */
        //this.aramaStandart('klima') // isme gore
        //this.aramaKategori('klima', 22) // isme ve kategoriye gore

        /* Urun tagleri listeleme */
        // Api.get('products/tags/',{
        //     per_page: 100,
        // })
        // .then(function (data) {
        //     //console.log(data);
        // });
        
        /* Tag'e gore urun getirme */
        // Api.get('products/',{
        //     per_page: 100, // tum urunleri alsin diye
        //     tag : 199 // ONECIKANURUN
        // })
        // .then(function (data) {
        //     //console.log(data);
        // });        

    }

    aramaStandart = (text) => {
        Api.get('products/',{
            per_page: 100, // tum kategorileri alsin diye
            search: text
        })
        .then(function (data) {
            //console.log(data);
        });        
    }

    aramaKategori = (text, catId) => {
        Api.get('products/',{
            per_page: 100, // tum kategorileri alsin diye
            search: text,
            category: catId
        })
        .then(function (data) {
            //console.log(data);
        });        
    }

    render() {
        return (
            <View style={{ flex: 1, backgroundColor: colors.darkestGrey}} >
                <ScrollView ref="scrollView">
                    <HomeSlider/>
                    <FeaturedProductsSlider />
                    <ContactArea />
                </ScrollView>
            </View>
        );
    }
}

export default HomePage;