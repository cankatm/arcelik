import React, { Component } from 'react';
import { View, Text, TextInput, Dimensions, TouchableOpacity, Alert, ScrollView } from 'react-native';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';

import Icon from 'react-native-vector-icons/Ionicons';
import * as colors from '../helpers/ColorPalette';
import styles from './styles';

import ModalFilterPicker from 'react-native-modal-filter-picker'

const WINDOW_WIDTH = Dimensions.get('window').width;
const WINDOW_HEIGHT = Dimensions.get('window').height;

const pickerStyle = {
    backgroundColor: colors.darkestGrey,
    color: colors.white
};

const textSyle = {
    marginTop: '10',
}

const btnStyle = {
    padding: 10,
    fontWeight: 'bold',
}
const respStyle = {
    paddingLeft: 10,
}

var arr1 = [
    {
        key: 0,
        label: 'Seçiniz...'
    },
    {
      key: 1,
      label: 'Ev',
    },
    {
      key: 2,
      label: 'Mağaza',
    }]

var arr2 = [
    {
        key: 0,
        label: 'Seçiniz...'
    },
    {
    key: 1,
    label: 'Akdeniz',
    },
    {
    key: 2,
    label: 'Doğu Anadolu',
    },
    {
    key: 3,
    label: 'İç Anadolu',
    },
    {
    key: 4,
    label: 'Güney Doğu Anadolu',
    },
    {
    key: 5,
    label: 'Marmara',
    },
    {
    key: 6,
    label: 'Karadeniz',
    },
    {
    key: 7,
    label: 'Ege',
    }
]

var arr3 = [
    {
        key: 0,
        label: 'Seçiniz...'
    },
    {
      key: 1,
      label: 'Isıtma',
    },
    {
      key: 2,
      label: 'Soğutma',
    },
    {
      key: 3,
      label: 'Herikiside',
    }
]

var arr4 = [
    {
        key: 0,
        label: 'Seçiniz...'
    },
    {
      key: 1,
      label: 'Zemin',
    },
    {
      key: 2,
      label: 'Ara Kat',
    },
    {
      key: 3,
      label: 'Çatı / En Üst',
    }
]

var arr5 = [
    {
        key: 0,
        label: 'Seçiniz...'
    },
    {
      key: 1,
      label: 'Çok Kötü',
    },
    {
      key: 2,
      label: 'Orta',
    },
    {
      key: 3,
      label: 'Çok İyi',
    }
]

class BTUCalculator extends Component {
    constructor(props) {
        super(props);
        this.state = {    
            v1: false,
            v2: false,
            v3: false,
            v4: false,
            v5: false,
            options: arr1,
            nerede: 0,
            mTv:0,
            mBuz:0,
            mBilg:0,
            mFlor:0,
            mHaloj:0,
            mNormal:0,
            bolge: 0,
            amac: 0,
            metrekare : '',
            kisisayisi: '',
            kat: 0,
            yalitim: 0,
            loading: false,
            sonuc: ''
        }
    }


    handlePicker = data => {
        this.setState({ pickedData: data });
    };

    _validation = () => {
        Alert.alert(
            'Eksik Bilgi',
            'Lütfen Tüm Değerleri Doldurunuz',
            [
            {text: 'OK', onPress: () => console.log('OK Pressed')},
            ],
            { cancelable: false }
        )
    }

    _calculateBTU = () => {        
        var { metrekare, kisisayisi, nerede, amac, kat, yalitim, mBilg,mBuz,mFlor,mHaloj,mNormal,mTv } = this.state

        if (this.state.bolge == 0 || amac == 0 || nerede == 0 || metrekare == "" || kisisayisi == "")
        {    
            this._validation()
            return 0
        }
        else if (nerede == 1 && amac == 1 && kat == 0)
        {
            this._validation()
            return 0
        }    
        else if (yalitim == 0 && kat == 2) {
            this._validation()
            return 0 
        }


        var bolge = 0
        var kisi = 0
        var score = 0
        var alan = 0.0

        switch (this.state.bolge) {
            case 1:
                bolge = 450
                alan = ( metrekare <= 25 ) ? 1.1 : 1.0 // Akdeniz
                break; 
            case 2:
                bolge = 310
                alan = ( metrekare <= 25 ) ? 1.5 : 1.5 // Doğu Anadolu
                break;        
            case 3:
                bolge = 350
                alan = ( metrekare <= 25 ) ? 1.4 : 1.4 // İç Anadolu
                break;        
            case 4:
                bolge = 470
                alan = ( metrekare <= 25 ) ? 1.1 : 1.0 // Güney Doğu Anadolu
                break;        
            case 5:
                bolge = 395
                alan = ( metrekare <= 25 ) ? 1.2 : 1.1 // Marmara
                break;  
            case 6:
            bolge = 395
                alan = ( metrekare <= 25 ) ? 1.2 : 1.1 // Karadeniz
                break;
            case 7:
                bolge = 395
                alan = ( metrekare <= 25 ) ? 1.2 : 1.0 // Ege
                break;   
        }     

        kisi = (parseInt(kisisayisi) <= 5) ? 600 : (parseInt(kisisayisi) - 4) * 600
        var sabit = 3.412

        if(nerede == 2 ) {
            //Magaza secildi ise          
            var televizyon = mTv * 60;            
            var bilgisayar = mBilg * 40;
            var buzdolabi = mBuz * 100;
            var Hlamba = mHaloj * 500;
            var Flamba = mFlor  * 50;
            var Nlamba = mNormal * 100;

            var urun = televizyon + bilgisayar + buzdolabi + Hlamba + Flamba + Nlamba
            if (urun < 500) {
                urun = 500;
            }
            score = ((bolge * metrekare) + kisi + (urun - 500) * sabit)
            if (amac == 1) {
                score = score * alan;
                score = score * 1.2;
            }
        }
        else if(nerede == 1) {
            // Ev secildi ise
            score = (bolge * parseInt(metrekare)) + kisi;
               
            if (amac == 1)
            {
                score = score * alan;
                if (kat  == 1 || kat == 3)
                    score = score * 1.2
                else if (kat == 2)
                { 
                    score = score * 1.2;
                }
            }
        }

        //SONUC
        // var sonuc = '';
        // if (score <= 10500)
        //     sonuc = "9.000 BTU"
        // else if (score <= 15000)
        //     sonuc = "12.000 BTU"
        // else if (score <= 21000)
        //     sonuc = "18.000 BTU"
        // else if (score > 21000)
        //     sonuc = "24.000 BTU"

        //this.setState({ sonuc : sonuc })
        this.setState({ sonuc : parseInt(score) })
    }

    viewIzolasyon() {
        if(this.state.kat == 2)
            return(<View>
            <TouchableOpacity style={btnStyle} onPress={this.onShow5}>                    
                <Text>Mekanın izolasyonu ve yalıtımı ne durumdadır?</Text>
                </TouchableOpacity>
                <ModalFilterPicker
                    visible={this.state.v5}
                    onSelect={this.onSelect5}
                    onCancel={this.onCancel}
                    options={arr4}
                    />
                    <Text style={respStyle}>{arr5[this.state.yalitim].label}</Text>
                {/* <Picker
                    selectedValue={this.state.yalitim}
                    style={pickerStyle}
                    onValueChange={(itemValue, itemIndex) => this.setState({ yalitim: itemValue})}>
                    <Picker.Item label='Seçiniz...' value={0} />
                    <Picker.Item label="Çok Kötü" value={1} />
                    <Picker.Item label="Orta" value={2} />
                    <Picker.Item label="Çok İyi" value={3} />
                </Picker> */}
                </View>)
    }

    viewKat() {
        if(this.state.amac == 1)
            return (
                <View>
                <TouchableOpacity style={btnStyle} onPress={this.onShow4}>                    
                    <Text>Eviniz kaçıncı katta yer alıyor?</Text>
                </TouchableOpacity>
                <ModalFilterPicker
                    visible={this.state.v4}
                    onSelect={this.onSelect4}
                    onCancel={this.onCancel}
                    options={arr4}
                    />
                    <Text style={respStyle}>{arr4[this.state.kat].label}</Text>
                
                {/* <Picker
                    selectedValue={this.state.kat}
                    style={pickerStyle}
                    onValueChange={(itemValue, itemIndex) => this.setState({ kat: itemValue})}>
                    <Picker.Item label='Seçiniz...' value={0} />
                    <Picker.Item label="Zemin" value={1} />
                    <Picker.Item label="Ara Kat" value={2} />
                    <Picker.Item label="Çatı / En Üst" value={3} />
                </Picker> */}
                </View>
            )
    }

    viewMagaza() {
        if(this.state.nerede == 2)
        return(
            <View>
                <Text>Kullanacağınız alanda ürünlerden kaç adet bulunuyor?</Text>
                <Text>Televizyon</Text>
                <TextInput 
                    placeholder='Sayısını yazınız...'
                    keyboardType='numeric'
                    returnKeyType='done'
                    underlineColorAndroid='rgba(0,0,0,0)'
                    autoCorrect={false}
                    onSubmitEditing={() => {}}
                    placeholderTextColor={colors.lightestGrey}
                    selectionColor={colors.black}
                    blurOnSubmit
                    value={this.state.mTv}
                    onChangeText={value => this.setState({ mTv: value })}
                    selectionColor={colors.red}
                    maxLength={160}
                    style={{
                        width: '100%',
                        backgroundColor: colors.darkestGrey,                                
                        color: colors.white,
                        alignItems: 'center',
                        justifyContent: 'center',
                        fontSize: 14,
                        paddingHorizontal: 16,
                        paddingVertical: 8
                    }}
                />
                <Text>Bilgisayar</Text>
                <TextInput 
                    placeholder='Sayısını yazınız...'
                    keyboardType='numeric'
                    returnKeyType='done'
                    underlineColorAndroid='rgba(0,0,0,0)'
                    autoCorrect={false}
                    onSubmitEditing={() => {}}
                    placeholderTextColor={colors.lightestGrey}
                    selectionColor={colors.black}
                    blurOnSubmit
                    value={this.state.mBilg}
                    onChangeText={value => this.setState({ mBilg: value })}
                    selectionColor={colors.red}
                    maxLength={160}
                    style={{
                        width: '100%',
                        backgroundColor: colors.darkestGrey,                                
                        color: colors.white,
                        alignItems: 'center',
                        justifyContent: 'center',
                        fontSize: 14,
                        paddingHorizontal: 16,
                        paddingVertical: 8
                    }}
                />
                <Text>Buzdolabı</Text>
                <TextInput 
                    placeholder='Sayısını yazınız...'
                    keyboardType='numeric'
                    returnKeyType='done'
                    underlineColorAndroid='rgba(0,0,0,0)'
                    autoCorrect={false}
                    onSubmitEditing={() => {}}
                    placeholderTextColor={colors.lightestGrey}
                    selectionColor={colors.black}
                    blurOnSubmit
                    value={this.state.mBuz}
                    onChangeText={value => this.setState({ mBuz: value })}
                    selectionColor={colors.red}
                    maxLength={160}
                    style={{
                        width: '100%',
                        backgroundColor: colors.darkestGrey,                                
                        color: colors.white,
                        alignItems: 'center',
                        justifyContent: 'center',
                        fontSize: 14,
                        paddingHorizontal: 16,
                        paddingVertical: 8
                    }}
                />
                <Text>Floresan Lamba</Text>
                <TextInput 
                    placeholder='Sayısını yazınız...'
                    keyboardType='numeric'
                    returnKeyType='done'
                    underlineColorAndroid='rgba(0,0,0,0)'
                    autoCorrect={false}
                    onSubmitEditing={() => {}}
                    placeholderTextColor={colors.lightestGrey}
                    selectionColor={colors.black}
                    blurOnSubmit
                    value={this.state.mFlor}
                    onChangeText={value => this.setState({ mFlor: value })}
                    selectionColor={colors.red}
                    maxLength={160}
                    style={{
                        width: '100%',
                        backgroundColor: colors.darkestGrey,                                
                        color: colors.white,
                        alignItems: 'center',
                        justifyContent: 'center',
                        fontSize: 14,
                        paddingHorizontal: 16,
                        paddingVertical: 8
                    }}
                />
                <Text>Halojen Lamba</Text>
                <TextInput 
                    placeholder='Sayısını yazınız...'
                    keyboardType='numeric'
                    returnKeyType='done'
                    underlineColorAndroid='rgba(0,0,0,0)'
                    autoCorrect={false}
                    onSubmitEditing={() => {}}
                    placeholderTextColor={colors.lightestGrey}
                    selectionColor={colors.black}
                    blurOnSubmit
                    value={this.state.mHaloj}
                    onChangeText={value => this.setState({ mHaloj: value })}
                    selectionColor={colors.red}
                    maxLength={160}
                    style={{
                        width: '100%',
                        backgroundColor: colors.darkestGrey,                                
                        color: colors.white,
                        alignItems: 'center',
                        justifyContent: 'center',
                        fontSize: 14,
                        paddingHorizontal: 16,
                        paddingVertical: 8
                    }}
                />
                <Text>Normal Lamba</Text>
                <TextInput 
                    placeholder='Sayısını yazınız...'
                    keyboardType='numeric'
                    returnKeyType='done'
                    underlineColorAndroid='rgba(0,0,0,0)'
                    autoCorrect={false}
                    onSubmitEditing={() => {}}
                    placeholderTextColor={colors.lightestGrey}
                    selectionColor={colors.black}
                    blurOnSubmit
                    value={this.state.mNormal}
                    onChangeText={value => this.setState({ mNormal: value })}
                    selectionColor={colors.red}
                    maxLength={160}
                    style={{
                        width: '100%',
                        backgroundColor: colors.darkestGrey,                                
                        color: colors.white,
                        alignItems: 'center',
                        justifyContent: 'center',
                        fontSize: 14,
                        paddingHorizontal: 16,
                        paddingVertical: 8
                    }}
                />
            </View>
        )
    }

    viewSonuc(){
        if(this.state.sonuc != '')
            return(
                <View>
                    <Text style={{ fontSize: 20, marginTop: 10, alignSelf:'flex-start' }}>BTU : {this.state.sonuc}</Text>            
                    <Text style={{ fontSize: 20, marginTop: 10, alignSelf:'flex-end' }}>Çıkan değer yaklaşık kapasitedir. Kesin kapasite için keşif ekibi gerekmektedir. Cihaz seçimi ve detaylı bilgi için bize ulaşın.</Text>
                    <View style={styles.homeSliderButtonOutContainerStyle}>
                        <TouchableOpacity onPress={() => this.props.navigation.navigate('ContactPage')} >
                            <View style={styles.homeSliderButtonContainerStyle} >
                                <Text style={styles.homeSliderButtonTextStyle} >DETAYLI BİLGİ</Text>
                            </View>
                        </TouchableOpacity>
                    </View>
                </View>
            )
    }

    onShow1 = () => {   
        this.setState({ v1: true });
    }  
    onShow2 = () => {   
        this.setState({ v2: true });
    }  
    onShow3 = () => {   
        this.setState({ v3: true });
    }  
    onShow4 = () => {   
        this.setState({ v4: true });
    }  
    onShow5 = () => {   
        this.setState({ v5: true });
    }  
    onShow6 = () => {   
        this.setState({ v6: true });
    }   

    onSelect1 = (picked) => {
        this.setState({
          nerede: picked,
          v1: false
        })
    }
    onSelect2 = (picked) => {
        this.setState({
          bolge: picked,
          v2: false
        })
    }
    onSelect3 = (picked) => {
        this.setState({
          amac: picked,
          v3: false
        })
    }
    onSelect4 = (picked) => {
        this.setState({
          kat: picked,
          v4: false
        })
    }
    onSelect5 = (picked) => {
        this.setState({
          nerede: picked,
          v5: false
        })
    }
    onSelect6 = (picked) => {
        this.setState({
          nerede: picked,
          v6: false
        })
    }
    
    onCancel = () => {
        this.setState({
            visible: false
        });
    }

    render() {
        const { v1,v2,v3 } = this.state;
        return (
            <KeyboardAwareScrollView  style={{ flex: 1 }} >
                <TouchableOpacity style={btnStyle} onPress={this.onShow1}>
                    <Text>Nerede kullanacaksınız?</Text>
                </TouchableOpacity>
                <ModalFilterPicker
                    visible={v1}
                    onSelect={this.onSelect1}
                    onCancel={this.onCancel}
                    options={arr1}
                    />
                <Text style={respStyle}>{arr1[this.state.nerede].label}</Text>
                {/* <Picker
                    selectedValue={this.state.nerede}
                    style={pickerStyle}
                    onValueChange={(itemValue, itemIndex) => this.setState({nerede: itemValue})}>
                    <Picker.Item label='Seçiniz...' value={0} />
                    <Picker.Item label="Ev" value={1} />
                    <Picker.Item label="Mağaza" value={2} />
                </Picker> */}                
                <TouchableOpacity style={btnStyle} onPress={this.onShow2}>
                    <Text>Hangi bölgede yaşıyorsunuz?</Text>
                </TouchableOpacity>
                <ModalFilterPicker
                    visible={v2}
                    onSelect={this.onSelect2}
                    onCancel={this.onCancel}
                    options={arr2}
                    />
                    <Text style={respStyle}>{arr2[this.state.bolge].label}</Text>
                {/* <Picker
                    selectedValue={this.state.bolge}
                    style={pickerStyle}
                    onValueChange={(itemValue) => this.setState({ bolge: itemValue })}>
                    <Picker.Item label='Seçiniz...' value={0} />
                    <Picker.Item label="Akdeniz" value={1} />
                    <Picker.Item label="Doğu Anadolu" value={2} />
                    <Picker.Item label="İç Anadolu" value={3} />
                    <Picker.Item label="Güney Doğu Anadolu" value={4} />
                    <Picker.Item label="Marmara" value={5} />
                    <Picker.Item label="Karadeniz" value={6} />
                    <Picker.Item label="Ege" value={7} />
                </Picker> */}
                <TouchableOpacity style={btnStyle} onPress={this.onShow3}>                    
                    <Text>Hangi amaçla kullanacaksınız ?</Text>
                </TouchableOpacity>
                <ModalFilterPicker
                    visible={v3}
                    onSelect={this.onSelect3}
                    onCancel={this.onCancel}
                    options={arr3}
                    />
                    <Text style={respStyle}>{arr3[this.state.amac].label}</Text>
                {/* <Picker
                    selectedValue={this.state.amac}
                    style={pickerStyle}
                    pickerStyleType={{ color: '#fff' }}
                    onValueChange={(itemValue, itemIndex) => this.setState({ amac: itemValue })}>
                    <Picker.Item label='Seçiniz...' value={0} />
                    <Picker.Item label="Isıtma" value={1} />
                    <Picker.Item label="Soğutma" value={2} />
                    <Picker.Item label="Herikiside" value={3} />
                </Picker> */}
                <Text style={btnStyle}>Kullanıcağınız odanın metrekaresi nedir?</Text>
                <TextInput 
                            placeholder='Metrekareyi yazınız...'
                            keyboardType='numeric'
                            returnKeyType='done'
                            underlineColorAndroid='rgba(0,0,0,0)'
                            autoCorrect={false}
                            onSubmitEditing={() => {}}
                            placeholderTextColor={colors.lightestGrey}
                            selectionColor={colors.black}
                            blurOnSubmit
                            value={this.state.metrekare}
                            onChangeText={value => this.setState({ metrekare: value })}
                            selectionColor={colors.red}
                            maxLength={160}
                            style={{
                                width: '100%',
                                backgroundColor: colors.darkestGrey,                                
                                color: colors.white,
                                alignItems: 'center',
                                justifyContent: 'center',
                                fontSize: 14,
                                paddingHorizontal: 16,
                                paddingVertical: 8
                            }}
                        />
                <Text style={btnStyle}>Mekandaki maksimum kişi sayısı nedir?</Text>
                <TextInput 
                            placeholder='Kişi sayısını yazınız...'
                            keyboardType='numeric' 
                            returnKeyType='done'
                            underlineColorAndroid='rgba(0,0,0,0)'
                            autoCorrect={false}
                            onSubmitEditing={() => {}}
                            placeholderTextColor={colors.lightestGrey}
                            selectionColor={colors.black}
                            blurOnSubmit
                            value={this.state.kisisayisi}
                            onChangeText={value => this.setState({ kisisayisi: value })}
                            selectionColor={colors.red}
                            maxLength={160}
                            style={{
                                width: '100%',
                                backgroundColor: colors.darkestGrey,
                                alignItems: 'center',
                                justifyContent: 'center',
                                fontSize: 14,
                                paddingHorizontal: 16,
                                paddingVertical: 8,
                                color: colors.white
                            }}
                        />
                { this.viewKat() }

                { this.viewIzolasyon() }

                { this.viewMagaza() }
                

                <TouchableOpacity onPress={() => this._calculateBTU() } >
                    <View style={{ backgroundColor: '#ED1C24', paddingHorizontal: 16, paddingVertical: 12, marginTop: 10}}>
                        <Text style={{ color: '#FFFFFF', fontSize: 14 }} >HESAPLA</Text>
                    </View>
                </TouchableOpacity>
                
                { this.viewSonuc()}

            </KeyboardAwareScrollView>
        );
    }
}

export default BTUCalculator;