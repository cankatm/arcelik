import React, { Component } from 'react';
import { 
    View, 
    TextInput,
    Dimensions,
    TouchableOpacity,
    ScrollView,
    ActivityIndicator,
    Text
} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';

import { ProductItemSmall } from '../components/ProductItems';

import * as colors from '../helpers/ColorPalette';
import styles from './styles';

const WINDOW_WIDTH = Dimensions.get('window').width;
const WINDOW_HEIGHT = Dimensions.get('window').height;

/* Services */
import Api from '../services/WooCommerce/Api'
//ScrollView yerine FlatList kullanılacak

//TODO: touchableopacity'ye basıldığı zaman şu anda sadece state'te loading'i true yapıyor, o kısmı server'a istek yollandığında
//      loading'i true yapıp, serverdan data geldiği zaman callback'te de loading'i false yapmak gerekiyor
class SearchPage extends Component {
    constructor(props) {
        super(props);
        this.state = {
            searchText: '',
            loading: false,
            urunler: [],
            showNotFound: false
        }
    }

    aramaStandart = () => {        
        //console.log('araniyor')
        this.setState({ loading: true, showNotFound: false })

        Api.get('products/', {
            per_page: 100,
            search: this.state.searchText
        })
        .then((data) => {
            console.log(data)
            this.setState({ loading: false, urunler:data })
            if (data.length < 1) {
                this.setState({ showNotFound: true })
            }
        })     
    }

    renderContent = () => {
        if (this.state.loading) {
            return (
                <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center', marginTop: 32 }} >
                    <ActivityIndicator size='large' color={colors.red} />
                </View>
            )
        }

        if (this.state.showNotFound) {
            return (
                <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center', marginTop: 32}} >
                    <Text>Ürün bulunamadı</Text>
                </View>
            )
        }
        if(this.state.urunler.length == 0)
            return(null)

        return (
            this.state.urunler.map((p) => <ProductItemSmall  key={p.id} item={p}/>)            
        )
    }

    render() { 
        return (
            <View style={{ flex: 1, backgroundColor: colors.white }} >
                <View style={styles.searchPageSearchAreaContainerStyle} >
                    <View style={{ height: 40, alignItems: 'center', justifyContent: 'center' }}>
                        <TextInput 
                            placeholder='Aramak istediğiniz ürünü yazınız...'
                            keyboardType='default' 
                            returnKeyType='done'
                            underlineColorAndroid='rgba(0,0,0,0)'
                            autoCorrect={false}
                            onSubmitEditing={() => this.aramaStandart()}
                            placeholderTextColor={colors.lightestGrey}
                            selectionColor={colors.black}
                            blurOnSubmit
                            value={this.state.searchText}
                            onChangeText={value => this.setState({ searchText: value })}
                            selectionColor={colors.red}
                            maxLength={160}
                            style={{
                                width: (WINDOW_WIDTH * 4) / 5,
                                backgroundColor: colors.darkestGrey,
                                alignItems: 'center',
                                justifyContent: 'center',
                                fontSize: 14,
                                paddingHorizontal: 16,
                                paddingVertical: 8,
                                color: colors.white
                            }}
                        />
                    </View>

                    <TouchableOpacity onPress={() => this.aramaStandart()} >
                        <View style={{ width: WINDOW_WIDTH / 5, height: 40, alignItems: 'flex-end', justifyContent: 'center' }} >
                            <View style={{ marginRight: 16 }} >
                                <Icon size={24} name='ios-search' color={colors.lightestGrey} />
                            </View>
                        </View>
                    </TouchableOpacity>
                </View>

                <ScrollView
                    style={{ flex: 1, backgroundColor: colors.white }}
                >
                    {this.renderContent()}

                </ScrollView>

            </View>
        );
    }
}

export default SearchPage;