import React, { Component } from 'react';
import { 
    View, 
    Text, 
    TouchableOpacity,
    ScrollView,
    Dimensions,
    SectionList,
    FlatList,
    ActivityIndicator
} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';

import _ from'lodash'


import * as colors from '../helpers/ColorPalette';
import { BackHeader, FiltreHeader } from '../components/Headers';
import { ProductItemSmall, ProductItemMedium } from '../components/ProductItems'; 
import { SelectionItem, SelectionItemHeader } from '../components/SelectionItems';
import styles from './styles';

const WINDOW_WIDTH = Dimensions.get('window').width;
const WINDOW_HEIGHT = Dimensions.get('window').height;


import Api from '../services/WooCommerce/Api'

class ProductList extends Component {
//     const denemeArr = [
//     {id: 1, content: 'Mert'},
//     {id: 2, content: 'Cankat'},
//     {id: 3, content: 'Abcd'},
//     {id: 4, content: 'Zxcv'},
//     {id: 5, content: 'pullu'},
//     {id: 6, content: 'geyik'},
// ];

// const denemeArr2 = [
//     {id: 7, content: 'Bas'},
//     {id: 8, content: 'Kıraaaal'},
//     {id: 9, content: 'bişeyler'},
//     {id: 10, content: 'sıkıldım'},
//     {id: 11, content: 'lan'},
//     {id: 12, content: 'bu ne'},
// ];

// const denemeSections = [ 
//     { title: 'Klima', data: denemeArr, key: 'Klima' },
//     { title: 'Buzdolabı', data: denemeArr2, key: 'Buzdolabı' },
//  ];

    constructor(props) {
        super(props);
        this.state= {
            filterOpened: false,
            toggledShowItems: true,
            selectedItem : null,
            data: [{key:"key1", label:"label1"}, {key:"key2", label:"label2"}],
            selected: (new Map(): Map<string, boolean>),
            selectedItems: [],
            Products:[],
            filteredProducts:[],
            loading: true,
            titles: [],
            filtreArr: [],
            valuesFinal: [],
            showNotFound: false
        }
    }

    _generateFilter(data) {
        var filters = _.uniqBy(data, function (e) {
            return e.id;
        });
    }

    _takeTitles = (data) => {
        let arr = [];
        data[0].attributes.forEach((att) => {
            arr.push({
                title: att.name,
                data:[],
                key: att.id,
                id: att.id
            })
        })

        let options = []
        data.forEach(el => {             
            el.attributes.forEach(at => {
                if(!Array.isArray(options[at.name]))
                    options[at.name] = []
                options[at.name] = options[at.name].concat(at.options)
            })
        })

        Object.keys(options).forEach((el) => {
            options[el] = _.uniq(options[el])
            arr.forEach(r => {
                if(r.title == el)
                    r.data = _.uniq(options[el])
            })
        })
        
        return this.setState({ titles: arr });
    }

    componentWillMount(){
        if(this.props.navigation.state.params.showAll){
            Api.get('products/',{
                per_page: 100,
                //category: this.props.navigation.state.params.catId,
                status:'publish'
            })
            .then((data) => {
                this.setState({
                    Products: data,
                    loading: false,
                    filteredProducts: data,
                    showNotFound: true
                })
                this._takeTitles(data)
            })
        } 
        else if(this.props.navigation.state.params.catId) 
        {
            Api.get('products/',{
                per_page: 100,
                category: this.props.navigation.state.params.catId,
                status:'publish'
            })
            .then((data) => {
                this.setState({
                    Products: data,
                    loading: false,
                    filteredProducts: data,
                    showNotFound: true
                })
                this._takeTitles(data)
            })
        }
        else
        {            
            console.log('cevaplar')
            console.log(this.props.navigation.state.params.answers)
            var asn = []
            this.props.navigation.state.params.answers.forEach((el, index) => {
                if(el.value !== null)
                    asn.push(el)
                // if(index % 2 == 0)
                //     asn.push(el)
            })        
           
            if(typeof asn[1] === 'object') {
                // console.log('asn', asn)
                var catId = asn[2].value

                let attrFilters = asn.filter((word,index) => index > 2)
                // console.log(attrFilters)
                // console.log('catId', catId)
                Api.get('products',{
                    per_page: 100,
                    category: catId,
                    status:'publish'
                })
                .then((data) => {
                    //console.log('d', data)
                    webPoducts = data
                    attrFilters.forEach(el => {
                        data = data.filter(item => {
                            try {
                                let fil = item.attributes.find(at => at.name == el.value)
                                if(fil.options.includes(el.content))
                                    return true
                                else
                                    return false
                            } catch (error) {
                                console.log('hata',item) // urunlere attr konulmadiginda hata veriyor
                            }
                            
                        })
                    });
                    this._takeTitles(data)

                    this.setState({
                        Products: data,
                        loading: false,
                        filteredProducts: data,
                        showNotFound: true
                    })

                    // //console.log(data.filter(item => {
                    //     let fil = item.attributes.find(at => at.name == asn[2].value)
                    //     if(fil.options.includes(asn[2].content))
                    //         return true
                    //     else
                    //         return false
                    // }))
        
                    // //console.log(data.filter(item => {
                    //     let fil = item.attributes.find(at => at.name == "Soğutma Sistemi")
                    //     if(fil.options.includes("No Frost"))
                    //         return true
                    //     else
                    //         return false
                    // }))
                });
            }
            this.setState({ loading: false })
        }
    }

    _onPressItem = (id: string) => {
        this.setState({ filtreArr: [] })
        // updater functions are preferred for transactional updates
        this.setState((state) => {
          // copy the map rather than modifying state.
            const selected = new Map(state.selected);
            if(selected.get(id))
                selected.delete(id)
            else
                selected.set(id, true); // toggle
            return {selected};
        });
    }

    deneme = () => {
        for (var [key, value] of this.state.selected) {
            if(value) {
                this.state.selectedItems.push(key);
            }
        }
    }

    static navigationOptions = ({navigation}) => ({
        header: <BackHeader noGoBack={navigation.state.params.noGoBack} onPress={() => navigation.goBack()} />
    })

    toggleShowItems = () => {
        this.setState({ toggledShowItems: !this.state.toggledShowItems })
    }

    showItems = () => {
        if(this.state.toggledShowItems) {
            return (
                <View style={{ flex: 1, flexDirection: 'row', flexWrap: 'wrap', backgroundColor: colors.white }} >
                    <FlatList
                        key={1}
                        data={this.state.filteredProducts}
                        renderItem={({item}) => <ProductItemMedium bgColor={colors.white} item={item}/>}
                        keyExtractor={(item, index) => item.id.toString()}
                        showsHorizontalScrollIndicator={false}
                        numColumns={2}
                    />
                </View>
            );
        }
        return (
            <View style={{ flex: 1, backgroundColor: colors.white }} >
                <FlatList
                    key={2}
                    data={this.state.filteredProducts}
                    renderItem={({item}) => <ProductItemSmall item={item}/>}
                    keyExtractor={(item, index) => item.id.toString()}
                    showsHorizontalScrollIndicator={false}
                />
            </View>
        );
    }

    handleFiltreButton = () => {
        console.log(this.state.Products)
        this.setState({ filtreArr: [] })
        for (var [key, value] of this.state.selected) {
            this.state.filtreArr.push(key)
            console.log('this.state.filtreArr')
            console.log(this.state.filtreArr)
        }

        var nonValues = [], values = [], nonVals = [], valuesFinal = [];

        this.state.titles.forEach((title) => {
            nonValues.push(_.difference(title.data, this.state.filtreArr))
        })

        nonValues = _.concat(nonValues)

        nonValues.forEach((nonValue) => {
            nonValue.forEach(nonVal => {
                nonVals.push(nonVal)
            });
        })

        this.state.titles.forEach((title) => {
            values.push(_.difference(title.data, nonVals))
        })

        values.forEach((val) => {
            val.forEach(vallue => {
                valuesFinal.push(vallue)
            })
        })

        this.setState({ valuesFinal })

        console.log('this.state.valuesFinal')
        console.log(this.state.valuesFinal)

        var finalProducts = [];

        this.state.Products.forEach(product => {
            product.attributes.forEach(att => {
                valuesFinal.forEach(valueFinal => {
                    if (_.includes(att.options, valueFinal)) {
                        finalProducts.push(product);
                    }
                })
            })
        });

        // var uniqProducts = [];
        // this.state.Products.forEach(product => { 
        //     uniqProducts = _.uniqBy(finalProducts, product.id);
        // })
        let uniqProducts = _.uniqBy(finalProducts, 'id');

        console.log('finalProducts')
        console.log(finalProducts)

        this.setState({ filteredProducts: uniqProducts })

        if (this.state.filtreArr.length < 1) {
            this.setState({ filteredProducts: this.state.Products })
        }

    }

    showFiltre = () => {
        if(this.state.filterOpened) {
            return (
                <View style={styles.productListFiltreContainerStyle}>
                    <SectionList
                        sections={this.state.titles}
                        stickySectionHeadersEnabled={false}
                        renderItem={({item}) => <SelectionItem 
                            content={item} 
                            selected={!!this.state.selected.get(item)}
                            onPressItem={this._onPressItem}
                            />
                        }
                        keyExtractor={(item, index) => index}
                        renderSectionHeader = {
                            ({section}) => <SelectionItemHeader title={section.title} />
                        }
                    />
                    
                    <View style={styles.productListFiltreButtonsContainerStyle} >
                        <TouchableOpacity onPress={() => this.setState({ selected: (new Map(): Map<string, boolean>), selectedItems: [], filtreArr: [] })} >
                            <View style={styles.productListClearButtonContainerStyle} >
                                <Text style={styles.productListClearButtonTextStyle} >Temizle</Text>
                            </View>
                        </TouchableOpacity>

                        <TouchableOpacity onPress={() => {this.handleFiltreButton(), this.setState({ filterOpened: false })}} >
                            <View style={styles.productListFiltreButtonContainerStyle} >
                                <Text style={styles.productListFiltreButtonTextStyle} >Filtrele</Text>
                            </View>
                        </TouchableOpacity>
                    </View>
                </View>
            )
        }
    }

    render() {
        if (this.state.loading) {
            return (
                <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center', marginTop: 32, minHeight: 250 }} >
                    <ActivityIndicator size='large' color={colors.red} />
                </View>
            )
        }
        return (
            <View style={{ flex: 1 }} >
                <View style={styles.productListFiltreHeaderContainerStyle} >
                    <TouchableOpacity onPress={() => this.setState({ filterOpened: !this.state.filterOpened })} >
                        <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'center', marginLeft: 16 }} >
                            <Icon size={24} name='ios-aperture' color={colors.white} />
                            <Text style={{ color: colors.white, marginLeft: 8 }} >{this.state.filterOpened ? 'Ürün Filtelerini kapat' :'Ürün Filteleri'}</Text>
                        </View>
                    </TouchableOpacity>

                    <TouchableOpacity onPress={() => this.toggleShowItems()} >
                        <View style={{ marginRight: 16 }} >
                            <Icon size={24} name='ios-podium' color={colors.white} />
                        </View>
                    </TouchableOpacity>
                </View>

                {this.showFiltre()}

                <ScrollView style={{ flex: 1, backgroundColor: colors.white }} >
                    {
                        (this.state.filteredProducts.length < 1 && this.state.showNotFound) && 
                        <View style={{ width: WINDOW_WIDTH, alignItems: 'center', justifyContent: 'center', marginTop: 16 }} >
                            <Text>Aradığınız kriterlerde ürün bulunamadı</Text>
                        </View>
                    }
                    {this.showItems()}
                </ScrollView>
            </View>
        );
    }
}

export default ProductList;