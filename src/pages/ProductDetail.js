import React, { Component } from 'react';
import { 
    View, 
    Text, 
    StyleSheet, 
    StatusBar,
    TouchableOpacity
} from 'react-native';
import { withNavigation } from 'react-navigation';

import { BackHeader } from '../components/Headers';
import { ProductItemLarge } from '../components/ProductItems';
import styles from './styles';

class ProductDetail extends Component {
    static navigationOptions = ({navigation}) => ({
        header: <BackHeader onPress={() => navigation.goBack()} />
    })   

    render() {
        return (
            <View style={{ flex: 1 }} >
                <ProductItemLarge itemId={this.props.navigation.getParam('itemId', 0)} />
            </View>
        );
    }
}

export default withNavigation(ProductDetail);