import React, { Component } from 'react';
import { 
    View, 
    Text, 
    StyleSheet, 
    StatusBar,
    TouchableOpacity,
    ScrollView
} from 'react-native';

import * as colors from '../helpers/ColorPalette';
import { BackHeader, WhichPageHeader } from '../components/Headers';
import { ContactArea } from '../components/ContactArea';

import { ABOUT_US } from '../helpers/Writings';

import styles from './styles';

class WhoAreWe extends Component {
    render() {
        return (
            <View style={{ flex: 1, backgroundColor: colors.darkestGrey}}>
                <WhichPageHeader headerTitle='HAKKIMIZDA' />
                <ScrollView style={{ flex: 1, backgroundColor: colors.darkestGrey }} >
                    <View style={styles.whoAreWeInformationContainerStyle} >
                        <Text>{ABOUT_US}</Text>
                    </View>
                    <ContactArea />
                </ScrollView>
            </View>
        );
    }
}

export default WhoAreWe;