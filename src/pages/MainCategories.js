import React, { Component } from 'react';
import { 
    View, 
    Text, 
    StyleSheet, 
    StatusBar,
    TouchableOpacity,
    ScrollView,
    FlatList,
    ActivityIndicator
} from 'react-native';

import { CategoryItem } from '../components/CategoryItems';
import styles from './styles';
import * as colors from '../helpers/ColorPalette';

import Api from '../services/WooCommerce/Api'

class MainCategories extends Component {
    constructor(props) {
        super(props);
        this.state = {
            loading: true,
            categories:[]
        }
    }

    componentWillMount() {
        // /* Ana Kategorileri Getirir */
        Api.get('products/categories',{
            per_page: 100, // tum kategorileri alsin diye
            parent:0
        })
        .then((data) => {
            //console.log(data)
            var vrs_ozellikleri = data[data.length - 1];
            var vrs = data[data.length - 2];
            data.pop()
            data.pop()
            data = [vrs_ozellikleri, vrs, ...data]
            this.setState({
                categories:data,
                loading: false
            })
        })
    }

    render() {
        if (this.state.loading) {
            return (
                <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center', marginTop: 32, minHeight: 250 }} >
                    <ActivityIndicator size='large' color={colors.red} />
                </View>
            )
        }
        return (
            <ScrollView style={{ flex: 1, backgroundColor: colors.white }} >
                <View style={{ flex: 1, backgroundColor: colors.white, alignItems: 'center' }} >
                    <FlatList
                        data={this.state.categories}
                        renderItem={({item}) => <CategoryItem goTo='SubCategories' from={item.slug} catId={item.id} imgSource={item.image} content={item.name} />}
                        keyExtractor={(item, index) => item.id.toString()}
                        showsHorizontalScrollIndicator={false}
                    />
                    <View style={{ height: 24 }} />
                </View>
            </ScrollView>
        );
    }
}

export default MainCategories;