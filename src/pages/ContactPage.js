import React, { Component } from 'react';
import { 
    View, 
    Text, 
    StyleSheet, 
    StatusBar,
    ScrollView 
} from 'react-native';
import MapView, { Marker } from 'react-native-maps';

import { BackHeader, WhichPageHeader } from '../components/Headers';
import { ContactAreaLight } from '../components/ContactArea';

import markerImg from '../../assets/images/marker.png';
import markerMiniImg from '../../assets/images/marker_mini.png';
import styles from './styles';
import * as colors from '../helpers/ColorPalette';

const LATITUDE = 39.902498;
const LONGITUDE = 32.853102;

// TODO: bu kısmı kapatmaışsın, bi nedeni var mıydı?

class ContactPage extends Component {
    constructor(props) {
        super(props);
        this.state = {
            region: {
              latitude: LATITUDE,
              longitude: LONGITUDE,
              latitudeDelta: 0.0222,
              longitudeDelta: 0.0121,
            }
        }
    }

    onRegionChange = (region) => {
        this.setState({ region });
    }
    render() {
        return (
            <View style={{ flex: 1, backgroundColor: colors.white}} >
                <WhichPageHeader headerTitle='BİZE ULAŞIN' />
                <ScrollView style={{ flex: 1, backgroundColor: colors.white }} >
                    { <MapView
                        initialRegion={this.state.region}
                        style={styles.contactPageMapStyle}
                    >
                        <Marker 
                            coordinate={{latitude: LATITUDE, longitude: LONGITUDE }}
                            image={markerMiniImg}
                        />
                    </MapView> }

                    <ContactAreaLight />
                </ScrollView>
            </View>
        );
    }
}

export default ContactPage;