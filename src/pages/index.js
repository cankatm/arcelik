import HomePage from './HomePage';
import MainCategories from './MainCategories';
import SubCategories from './SubCategories';
import ProductList from './ProductList';
import ProductDetail from './ProductDetail';
import ProductImageShowPage from './ProductImageShowPage';
import SearchPage from './SearchPage';
import WhoAreWe from './WhoAreWe';
import ContactPage from './ContactPage';
import Settings from './Settings';
import AirCondBuyPage from './AirCondBuyPage';
import NotificationsPage from './NotificationsPage';
import AirCondSelectionRobot from './AirCondSelectionRobot';
import QuestionsPage from './QuestionsPage';
import BTUCalculator from './BTUCalculator';
import FormPage from './FormPage';

export {
    HomePage,
    MainCategories,
    SubCategories,
    ProductList,
    ProductDetail,
    ProductImageShowPage,
    SearchPage,
    WhoAreWe,
    ContactPage,
    Settings,
    AirCondBuyPage,
    NotificationsPage,
    AirCondSelectionRobot,
    QuestionsPage,
    BTUCalculator,
    FormPage,
};