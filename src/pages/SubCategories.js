import React, { Component } from 'react';
import { 
    View, 
    Text, 
    StyleSheet,
    StatusBar,
    TouchableOpacity,
    ScrollView,
    FlatList,
    ActivityIndicator
} from 'react-native';

import { CategoryItem } from '../components/CategoryItems';
import { BackHeader } from '../components/Headers';
import styles from './styles';
import * as colors from '../helpers/ColorPalette';

import Api from '../services/WooCommerce/Api'

class SubCategories extends Component {
    constructor(props) {
        super(props);

        this.state = {
            loading: true,
            subcategories: []
        }
    }

    static navigationOptions = ({navigation}) => ({
        header: <BackHeader onPress={() => navigation.goBack()} />
    });

    componentWillMount() {
         // /* Ana Kategorileri Getirir */
         Api.get('products/categories',{
            per_page: 100, // tum kategorileri alsin diye
            parent: this.props.navigation.state.params.catId
        })
        .then((data) => {
            this.setState({
                subcategories:data,
                loading: false
            })
            if(data.length == 0)
                this.props.navigation.navigate('ProductList', { catId: this.props.navigation.state.params.catId })
            //console.log(data, this.props.navigation.state.params.catId);
        })
    }

    render() {
        if (this.state.loading) {
            return (
                <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center', marginTop: 32, minHeight: 250 }} >
                    <ActivityIndicator size='large' color={colors.red} />
                </View>
            )
        }
        if(this.props.navigation.state.params.catId == 724)
        return (
            <ScrollView style={{ flex: 1, backgroundColor: colors.white }} >
                <View style={{ flex: 1, backgroundColor: colors.white, alignItems: 'center' }} >
                    <FlatList
                        data={this.state.subcategories}
                        renderItem={({item}) => <CategoryItem goTo='SubCategories' catId={item.id} imgSource={item.image} content={item.name} />}
                        keyExtractor={(item, index) => item.id.toString()}
                        showsHorizontalScrollIndicator={false}
                    />
                    {/*console.log(this.props.navigation.state.params.from)*/}
                    <View style={{ height: 24 }} />
                </View>
            </ScrollView>
        );
        return (
            <ScrollView style={{ flex: 1, backgroundColor: colors.white }} >
                <View style={{ flex: 1, backgroundColor: colors.white, alignItems: 'center' }} >
                    <FlatList
                        data={this.state.subcategories}
                        renderItem={({item}) => <CategoryItem goTo='ProductList' catId={item.id} imgSource={item.image} content={item.name} />}
                        keyExtractor={(item, index) => item.id.toString()}
                        showsHorizontalScrollIndicator={false}
                    />
                    {/*console.log(this.props.navigation.state.params.from)*/}
                    <View style={{ height: 24 }} />
                </View>
            </ScrollView>
        );
    }
}

export default SubCategories;