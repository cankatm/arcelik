import React, { Component } from 'react';
import { 
    View, 
    Text, 
    StyleSheet, 
    StatusBar,
    ScrollView
} from 'react-native';

import { BackHeader, WhichPageHeader } from '../components/Headers';
import { NotificationItem } from '../components/NotificationItems';
import * as colors from '../helpers/ColorPalette';

import styles from './styles';

class NotificationsPage extends Component {
    render() {
        return (
            <View style={{ flex: 1, backgroundColor: colors.white}} >
                <WhichPageHeader headerTitle='BİLDİRİMLER' />
                <ScrollView style={{ flex: 1, backgroundColor: colors.white }} >
                    <NotificationItem />
                    <NotificationItem />
                    <NotificationItem />
                </ScrollView>
            </View>
        );
    }
}

export default NotificationsPage;