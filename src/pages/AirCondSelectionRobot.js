import _ from 'lodash';
import React, { Component } from 'react';
import { 
    View, 
    Text, 
    StyleSheet, 
    StatusBar,
    FlatList,
    TouchableOpacity,
    SectionList
} from 'react-native';

import styles from './styles';
import * as colors from '../helpers/ColorPalette';
import { SelectionItem } from '../components/SelectionItems';
import { WhichPageHeader, ExplanationHeader } from '../components/Headers';
import { BigButton } from '../components/Buttons';



// <FlatList
// data={denemeArr}
// renderItem={({item}) => <SelectionItem 
//     content={item.content} 
//     id={item.id} 
//     selected={!!this.state.selected.get(item.id)}
//     onPressItem={this._onPressItem}
//     />
// }
// keyExtractor={(item, index) => item.content}
// />

class AirCondSelectionRobot extends Component {
    render() {
        return (
            <View style={{ flex: 1, backgroundColor: colors.white }}>
                <WhichPageHeader headerTitle='ARÇELİK ÜRÜN SEÇİM ASİSTANI' />
                <ExplanationHeader content='Lütfen aradığınız ürünü kapsayan kategoriyi seçiniz.' />
                <BigButton 
                    content='KURUMSAL MÜŞTERİ' 
                    bgColor={colors.darkestGrey} 
                    image='kurumsal' 
                    mgTop={0}
                    to='kurumsal'
                />
                <BigButton 
                    content='BİREYSEL MÜŞTERİ' 
                    bgColor={colors.red} 
                    image='bireysel' 
                    mgTop={24}
                    to='bireysel'
                />
            </View>
        );
    }
}

export default AirCondSelectionRobot;