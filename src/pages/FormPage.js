import React, { Component } from 'react';
import { 
    View,
    Text,
    TextInput,
    Alert,
    Dimensions,
    TouchableOpacity
} from 'react-native';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import Communications from 'react-native-communications';

import styles from './styles';
import * as colors from '../helpers/ColorPalette';

const { width, height } = Dimensions.get('window');
const marginValue = 16;

let writing = null;

class FormPage extends Component {
    constructor(props) {
        super(props);

        this.state = {
            productPiece: null,
            name: null,
            firmName: null,
            projectName: null,
            email: null,
            phone: null
        }
    }

    render() {
        const { Product } = this.props.navigation.state.params;
        const {
            productPiece,
            name,
            firmName,
            projectName,
            email,
            phone
        } = this.state;

        const inputStyle = {
            height: 40, 
            borderColor: colors.darkGrey, 
            borderWidth: 1,
            width: width - (marginValue * 2),
            marginLeft: marginValue,
            marginVertical: marginValue / 2,
            paddingHorizontal: marginValue
        }

        handleButtonPress = () => {
            if (!(productPiece && name && firmName && projectName && email && phone)) {
                return (
                    Alert.alert(
                        'Eksik Alan',
                        'Lütfen eksik alanları doldurunuz',
                        [
                            {text: 'Tamam', onPress: () => console.log('OK Pressed')},
                        ],
                        { cancelable: true }
                    )
                )
            }

            return (
                //bilgi@arcelikankara.com
                writing = `Ürün Adı: ${Product.name} \n Email Adresi: ${email} \n Telefon numarası: ${phone} \n Firma Adı: ${firmName} \n Proje Adı: ${projectName} \n Ürün Adedi: ${productPiece} `,
                Communications.email(['app@arcelikelmas.com'], null, null, 'Ürün Fiyat İsteği', writing)
            )
            
        }

        return (
            <View style={{ flex: 1, backgroundColor: 'white' }} >
                <KeyboardAwareScrollView>

                    <View style={{ marginHorizontal: marginValue, marginTop: marginValue }}>
                        <Text >Ürün Adı: </Text>
                    </View>

                    <TextInput
                        style={inputStyle}
                        onChangeText={(text) => this.setState({ name: text})}
                        value={Product.name}
                        selectionColor={colors.red}
                        editable={false}
                    />

                    <View style={{ marginHorizontal: marginValue, marginTop: marginValue }}>
                        <Text >Adınız Soyadınız: </Text>
                    </View>

                    <TextInput
                        style={inputStyle}
                        onChangeText={(text) => this.setState({ name: text})}
                        value={name}
                        placeholder='Adınız Soyadınız'
                        selectionColor={colors.red}
                        blurOnSubmit
                    />

                    <View style={{ marginHorizontal: marginValue, marginTop: marginValue }}>
                        <Text >Firma Adı: </Text>
                    </View>

                    <TextInput
                        style={inputStyle}
                        onChangeText={(text) => this.setState({ firmName: text})}
                        value={firmName}
                        placeholder='Firma Adı'
                        selectionColor={colors.red}
                        blurOnSubmit
                    />

                    <View style={{ marginHorizontal: marginValue, marginTop: marginValue }}>
                        <Text >Proje Adı: </Text>
                    </View>

                    <TextInput
                        style={inputStyle}
                        onChangeText={(text) => this.setState({ projectName: text})}
                        value={projectName}
                        placeholder='Proje Adı'
                        selectionColor={colors.red}
                        blurOnSubmit
                    />

                    <View style={{ marginHorizontal: marginValue, marginTop: marginValue }}>
                        <Text >Email: </Text>
                    </View>

                    <TextInput
                        style={inputStyle}
                        onChangeText={(text) => this.setState({ email: text})}
                        value={email}
                        placeholder='Email'
                        selectionColor={colors.red}
                        keyboardType='email-address'
                        blurOnSubmit
                    />

                    <View style={{ marginHorizontal: marginValue, marginTop: marginValue }}>
                        <Text >Ürün Adedi: </Text>
                    </View>

                    <TextInput
                        style={inputStyle}
                        onChangeText={(text) => this.setState({ productPiece: text})}
                        value={productPiece}
                        placeholder='Ürün Adedi'
                        selectionColor={colors.red}
                        keyboardType='numeric'
                        blurOnSubmit
                    />

                    <View style={{ marginHorizontal: marginValue, marginTop: marginValue }}>
                        <Text >Telefon numaranız: </Text>
                    </View>

                    <TextInput
                        style={inputStyle}
                        onChangeText={(text) => this.setState({ phone: text})}
                        value={phone}
                        placeholder='Telefon numaranız'
                        selectionColor={colors.red}
                        keyboardType='numeric'
                        blurOnSubmit
                    />

                    <TouchableOpacity onPress={() => handleButtonPress()} >
                        <View style={{ width: width - (marginValue * 2), height: 60, backgroundColor: colors.red, marginLeft: marginValue, alignItems: 'center', justifyContent: 'center', marginVertical: 24 }} >
                            <Text style={{ color: colors.white }} >Gönder</Text>
                        </View>
                    </TouchableOpacity>

                </KeyboardAwareScrollView>
                
            </View>
        );
    }
}

export default FormPage;