import React, { Component } from 'react';
import { 
    View, 
    Text, 
    StyleSheet, 
    StatusBar,
    Switch
} from 'react-native';

import { BackHeader, WhichPageHeader } from '../components/Headers';

import * as colors from '../helpers/ColorPalette';
import styles from './styles';

class Settings extends Component {
    constructor() {
        super();
        this.state = {
           switch1Value: false,
        }
     }
     
     toggleSwitch1 = (value) => {
        this.setState({switch1Value: value})
     }

    render() {
        return (
            <View style={{ flex: 1, backgroundColor: colors.white}} >
                <WhichPageHeader headerTitle='AYARLAR' />

                <View style={styles.settingsPageContainerStyle}>
                    <View style={styles.settingsPageSettingsContainerStyle} >
                        <Text style={styles.settingsPageSettingsTextStyle} >Uygulama Bildirimlerini Al</Text>
                        <Switch 
                            onTintColor={colors.darkGrey}
                            value={this.state.switch1Value}
                            onValueChange={(val) => this.toggleSwitch1(val)}
                            thumbTintColor={colors.red}
                            tintColor={colors.red}
                        />
                    </View>

                    <Text style={styles.settingsPageAboutAppTextStyle} >UYGULAMA HAKKINDA</Text>
                    <View style={styles.settingsPageLineStyle}  />
                    <Text style={styles.settingsPageGeneralTextStyle} >Bu uygulama <Text style={{ fontSize: 12}}>KuarkTek</Text> ve <Text style={{ fontSize: 12}}>SinsApp</Text> tarafından tasarlanmış ve geliştirilmiştir.</Text>

                    <View style={{ marginVertical: 16 }} >
                        <View style={{ flexDirection: 'row' }} >
                            <View style={{ width: 60 }} >
                                <Text style={styles.settingsPageGeneralBolderTextStyle}>UI Design</Text>
                            </View>
                            <Text style={styles.settingsPageGeneralTextStyle}>: Emre Yılmaz</Text>
                        </View>
                        <View style={{ flexDirection: 'row' }} >
                            <View style={{ width: 60 }} >
                                <Text style={styles.settingsPageGeneralBolderTextStyle}>Mobile</Text>
                            </View>
                            <Text style={styles.settingsPageGeneralTextStyle}>: Mert Cankat</Text>
                        </View>
                        <View style={{ flexDirection: 'row' }} >
                            <View style={{ width: 60 }} >
                                <Text style={styles.settingsPageGeneralBolderTextStyle}>Back-End</Text>
                            </View>
                            <Text style={styles.settingsPageGeneralTextStyle}>: Salih Çağlar İspirli</Text>
                        </View>
                    </View>

                    <Text style={styles.settingsPageGeneralTextStyle} >Detaylı bilgi için <Text style={styles.settingsPageGeneralBolderTextStyle} >www.kuarktek.com.tr</Text> adresini ziyaret edebilir veya <Text style={styles.settingsPageGeneralBolderTextStyle} >info@kuarktek.com.tr</Text> adresine e-posta atabilirsiniz.</Text>
                    <Text style={styles.settingsPageGeneralTextStyle} >v 1.1.3</Text>
                </View>

            </View>
        );
    }
}

export default Settings;