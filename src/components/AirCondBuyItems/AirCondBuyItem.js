import React, { Component } from 'react';
import { 
    View, 
    Text, 
    StyleSheet, 
    StatusBar,
    Image 
} from 'react-native';

import styles from './styles';
import itemPic from '../../../assets/images/buying-icon1.png';

class AirCondBuyItem extends Component {
    render() {
        return (
            <View style={styles.airCondBuyItemContainerStyle} >
                <View style={styles.airCondBuyItemImageAndTitleContainerStyle} >
                    <Image source={this.props.imgSource} style={{ width: 80, height: 80}} />
                    <Text style={styles.airCondBuyItemTitleTextStyle} >{this.props.headerTitle}</Text>
                </View>
                <View style={styles.airCondBuyItemExpContainerStyle} >
                    <Text style={styles.airCondBuyItemExpTextStyle}>{this.props.content}</Text>
                </View>
            </View>
        );
    }
}

export default AirCondBuyItem;