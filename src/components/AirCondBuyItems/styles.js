import React from 'react';
import { StyleSheet, Dimensions } from 'react-native';

import * as colors from '../../helpers/ColorPalette';

const WINDOW_WIDTH = Dimensions.get('window').width;
const WINDOW_HEIGHT = Dimensions.get('window').height;

const MARGIN = 16;

export default StyleSheet.create({
    airCondBuyItemContainerStyle: {
        width: WINDOW_WIDTH,
        flexDirection: 'row',
        alignItems: 'center',
        borderBottomWidth: 1,
        borderColor: colors.lightGrey
    },
    airCondBuyItemImageAndTitleContainerStyle: {
        width: WINDOW_WIDTH / 2,
        alignItems: 'center',
        justifyContent: 'center',
        marginVertical: 16
    },
    airCondBuyItemTitleTextStyle: {
        fontSize: 18,
        color: colors.black,
        marginTop: 4
    },
    airCondBuyItemExpContainerStyle: {
        width: WINDOW_WIDTH / 2,
        alignItems: 'center',
        justifyContent: 'center',
        marginVertical: 16
    },
    airCondBuyItemExpTextStyle: {
        fontSize: 14,
        color: colors.lightGrey,
        textAlign: 'center',
        paddingHorizontal: 16,
        marginRight: 50
    },
});