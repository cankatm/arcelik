import React, { Component } from 'react';
import { 
    View, 
    Text, 
    StyleSheet, 
    StatusBar,
    TouchableOpacity 
} from 'react-native';

import styles from './styles';
import * as colors from '../../helpers/ColorPalette';

class SelectionItem extends Component {
    _onPress = () => {
        this.props.onPressItem(this.props.content);
    };

    render() {
        const textColor = this.props.selected ? colors.white : colors.lightestGrey;
        const circleColor = this.props.selected ? colors.red : "transparent";
        return (
            <View>
                <TouchableOpacity onPress={this._onPress} >
                    <View style={styles.selectionItemContainerStyle} > 
                        <View style={[styles.selectionItemCircleStyle, { backgroundColor: circleColor }]} />
                        <Text style={[styles.selectionItemTextStyle ,{ color: textColor }]} >{this.props.content}</Text>
                    </View>
                </TouchableOpacity>
            </View>
        );
    }
}

export default SelectionItem;