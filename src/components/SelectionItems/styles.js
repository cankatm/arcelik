import React from 'react';
import { StyleSheet, Dimensions } from 'react-native';

import * as colors from '../../helpers/ColorPalette';

const WINDOW_WIDTH = Dimensions.get('window').width;
const WINDOW_HEIGHT = Dimensions.get('window').height;

const SMALL_IMAGE_WIDTH = 120;

const MEDIUM_WINDOW_WIDTH = (WINDOW_WIDTH / 2) - 24;
const MEDIUM_IMAGE_SIZE = (WINDOW_WIDTH / 2) - 36;

export default StyleSheet.create({
    selectionItemContainerStyle: {
        flexDirection: 'row',
        alignItems: 'center',
    },
    selectionItemCircleStyle: {
        width: 12,
        height: 12,
        borderRadius: 6,
        borderWidth: 1,
        borderColor: colors.white,
        marginVertical: 4,
        marginLeft: 32
    },
    selectionItemTextStyle: {
        marginLeft: 8,
        marginVertical: 4,
    },
    selectionItemHeaderContainerStyle: {
        marginLeft: 16,
        marginVertical: 8,
    },
    selectionItemHeaderTextStyle: {
        color: colors.white
    },
});