import React, { Component } from 'react';
import { View, Text, StyleSheet, StatusBar } from 'react-native';

import styles from './styles';

class SelectionItemHeader extends Component {
    render() {
        return (
            <View style={styles.selectionItemHeaderContainerStyle} >
                <Text style={styles.selectionItemHeaderTextStyle}>{this.props.title}</Text>
            </View>
        );
    }
}

export default SelectionItemHeader;