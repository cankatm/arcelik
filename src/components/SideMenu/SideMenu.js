import React, {Component} from 'react';
import styles from './styles';
import {NavigationActions, withNavigation} from 'react-navigation';
import { 
    View, 
    Text, 
    ScrollView
} from 'react-native';

class SideMenu extends Component {
  navigateToScreen = (route) => () => {
    const navigateAction = NavigationActions.navigate({
      routeName: route
    });
    this.props.navigation.dispatch(navigateAction);
  }

  render () {
    return (
      <View style={styles.container}>
        <View style={styles.headerContainer}>
          <Text> </Text>
        </View>
        <ScrollView
          showsVerticalScrollIndicator={false}
        >
          <View>
            <View style={styles.navSectionStyle}>
              <Text style={styles.navItemStyle} onPress={this.navigateToScreen('Home')}>
              Ana Sayfa
              </Text>
            </View>
          </View>
          <View>
            <View style={styles.navSectionStyle}>
              <Text style={styles.navItemStyle} onPress={this.navigateToScreen('BTUCalculator')}>
              BTU Hesaplayıcı
              </Text>
            </View>
          </View>
          <View>
            <View style={styles.navSectionStyle}>
              <Text style={styles.navItemStyle} onPress={this.navigateToScreen('AirCond')}>
                Ürün Seçim Asistanı
              </Text>
            </View>
          </View>
          <View>
            <View style={styles.navSectionStyle}>
              <Text style={styles.navItemStyle} onPress={this.navigateToScreen('Categories')}>
                Ürünler
              </Text>
            </View>
          </View>
          <View>
            <View style={styles.navSectionStyle}>
              <Text style={styles.navItemStyle} onPress={this.navigateToScreen('AirCondBuyPage')}>
                Klima Satın Alma Rehberi
              </Text>
            </View>
          </View>          
          <View>
            <View style={styles.navSectionStyle}>
              <Text style={styles.navItemStyle} onPress={this.navigateToScreen('WhoAreWe')}>
                Elmaş Arçelik Hakkında
              </Text>
            </View>
          </View>
   
          {/* <View>
            <View style={styles.navSectionStyle}>
              <Text style={styles.navItemStyle} onPress={this.navigateToScreen('NotificationsPage')}>
                Bildirimler
              </Text>
            </View>
          </View> */}
          <View>
            <View style={styles.navSectionStyle}>
              <Text style={styles.navItemStyle} onPress={this.navigateToScreen('Settings')}>
                Uygulama Ayarları
              </Text>
            </View>
          </View>
          <View>
            <View style={styles.navSectionStyle}>
              <Text style={styles.navItemStyle} onPress={this.navigateToScreen('ContactPage')}>
                İletişim
              </Text>
            </View>
          </View>
        </ScrollView>
      </View>
    );
  }
}

export default SideMenu;