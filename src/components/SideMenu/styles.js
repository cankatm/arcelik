import React from 'react';
import { StyleSheet, Dimensions } from 'react-native';

import * as colors from '../../helpers/ColorPalette';

const WINDOW_WIDTH = Dimensions.get('window').width;
const WINDOW_HEIGHT = Dimensions.get('window').height;

export default StyleSheet.create({
  headerContainer: {
    padding: 16,
    backgroundColor: colors.lightestGrey
  },
  container: {
    paddingTop: 20,
    flex: 1,
    backgroundColor: colors.white
  },
  navItemStyle: {
    padding: 16,
    color: colors.darkestGrey
  },
  navSectionStyle: {
    backgroundColor: colors.white
  },
});