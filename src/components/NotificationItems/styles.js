import React from 'react';
import { StyleSheet, Dimensions } from 'react-native';

import * as colors from '../../helpers/ColorPalette';

const WINDOW_WIDTH = Dimensions.get('window').width;
const WINDOW_HEIGHT = Dimensions.get('window').height;

const HEADER_HEIGHT = 40;

export default StyleSheet.create({
    notificationItemContainerStyle: {
        width: WINDOW_WIDTH,
        alignItems: 'center',
        justifyContent: 'center',
        borderBottomWidth: 1,
        borderColor: colors.lightGrey,
    },
    notificationItemTextContainerStyle: {
        width: WINDOW_WIDTH - 16,
    },
    notificationItemTextStyle: {
        color: colors.darkestGrey,
    },
    notificationItemDateContainerStyle: {
        width: WINDOW_WIDTH - 16,
        alignItems: 'flex-end',
        justifyContent: 'center',
    },
    notificationItemDateTextStyle: {
        color: colors.lightGrey,
        fontSize: 10
    },
});