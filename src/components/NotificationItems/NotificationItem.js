import React, { Component } from 'react';
import { View, Text, StyleSheet, StatusBar } from 'react-native';

import styles from './styles';

class NotificationItem extends Component {
    render() {
        return (
            <View style={styles.notificationItemContainerStyle} >
                <View style={{ height: 8 }} />

                <View style={styles.notificationItemTextContainerStyle} >
                    <Text style={styles.notificationItemTextStyle} >Tüm Arçelik Ev Tipi Klimalarda yaz kampanyası başladı! Detaylı bilgi için sizleri Elmaş Arçelik mağazamıza bekliyoruz!</Text>
                </View>

                <View style={{ height: 8 }} />

                <View style={styles.notificationItemDateContainerStyle} >
                    <Text style={styles.notificationItemDateTextStyle}>07.05.2018 - 12:30</Text>
                </View>

                <View style={{ height: 8 }} />
            </View>
        );
    }
}

export default NotificationItem;