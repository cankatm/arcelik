import React, { Component } from 'react';
import {
  View,
  Text,
  StyleSheet,
  StatusBar,
  TouchableOpacity,
  Image,
  Keyboard
} from 'react-native';
import { withNavigation } from 'react-navigation';
import Icon from 'react-native-vector-icons/Ionicons';

import * as colors from '../../helpers/ColorPalette';
import styles from './styles';

// import arcelikLogo from '../../../assets/images/arcelik_logo.png';
import arcelikLogo from '../../../assets/images/arcelik_logo2_new.png';

class MainHeader extends Component {
  render() {
    return (
      <View style={styles.mainHeaderContainerStyle}>
        <View style={{ position: 'absolute', left: 16, top: 4 }}>
          <TouchableOpacity
            onPress={() => {
              this.props.navigation.navigate('DrawerOpen'), Keyboard.dismiss();
            }}
          >
            <View style={{ width: 80, height: 30, justifyContent: 'center' }}>
              <View style={{ marginLeft: 0 }}>
                <Icon size={32} name='ios-menu' color={colors.darkGrey} />
              </View>
            </View>
          </TouchableOpacity>
        </View>

        <TouchableOpacity
          onPress={() => {
            this.props.navigation.navigate('Home'), Keyboard.dismiss();
          }}
        >
          <Image
            source={arcelikLogo}
            style={{ width: 200, height: 40, resizeMode: 'contain' }}
          />
        </TouchableOpacity>
      </View>
    );
  }
}

export default withNavigation(MainHeader);
