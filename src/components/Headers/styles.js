import React from 'react';
import { StyleSheet, Dimensions } from 'react-native';

import * as colors from '../../helpers/ColorPalette';

const WINDOW_WIDTH = Dimensions.get('window').width;
const WINDOW_HEIGHT = Dimensions.get('window').height;

const HEADER_HEIGHT = 40;

export default StyleSheet.create({
    mainHeaderContainerStyle: {
        width: WINDOW_WIDTH,
        height: HEADER_HEIGHT,
        backgroundColor: colors.white,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        borderBottomWidth: 1,
        borderColor: colors.darkestGrey
    },
    backHeaderContainerStyle: {
        width: WINDOW_WIDTH,
        height: HEADER_HEIGHT,
        backgroundColor: colors.white,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        borderBottomWidth: 1,
        borderColor: colors.darkestGrey
    },
    whichPageHeaderContainerStyle: {
        width: WINDOW_WIDTH,
        height: HEADER_HEIGHT,
        backgroundColor: colors.darkestGrey,
        justifyContent: 'center',
    },
    whichPageHeaderTextStyle: {
        fontSize: 14,
        color: colors.white,
        marginLeft: 16
    },
    explanationHeaderContainerStyle: {
        width: WINDOW_WIDTH,
        backgroundColor: colors.white
    },
    explanationHeaderTextStyle: {
        fontSize: 14,
        color: colors.grey,
        marginHorizontal: 16,
        marginVertical: 16,
    },
});