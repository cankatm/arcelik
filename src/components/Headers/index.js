import MainHeader from './MainHeader';
import BackHeader from './BackHeader';
import WhichPageHeader from './WhichPageHeader';
import ExplanationHeader from './ExplanationHeader';
import styles from './styles';

export { 
    MainHeader, 
    BackHeader, 
    WhichPageHeader, 
    ExplanationHeader, 
    styles 
};
