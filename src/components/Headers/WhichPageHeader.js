import React, { Component } from 'react';
import { View, Text, StyleSheet, StatusBar } from 'react-native';

import styles from './styles';

class WhichPageHeader extends Component {
    render() {
        return (
            <View style={styles.whichPageHeaderContainerStyle} >
                <Text style={styles.whichPageHeaderTextStyle} >{this.props.headerTitle}</Text>
            </View>
        );
    }
}

export default WhichPageHeader;