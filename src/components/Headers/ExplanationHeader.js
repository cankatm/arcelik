import React, { Component } from 'react';
import { 
    View, 
    Text, 
    StyleSheet, 
    StatusBar, 
} from 'react-native';

import * as colors from '../../helpers/ColorPalette';
import styles from './styles';

class ExplanationHeader extends Component {
    render() {
        const { content } = this.props;
        return (
            <View style={styles.explanationHeaderContainerStyle} >
                <Text style={styles.explanationHeaderTextStyle} >{content}</Text>
            </View>
        );
    }
}

export default ExplanationHeader;