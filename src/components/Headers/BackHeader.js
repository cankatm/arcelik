import React, { Component } from 'react';
import {
  View,
  Text,
  StyleSheet,
  StatusBar,
  TouchableOpacity,
  Image
} from 'react-native';
import { withNavigation, NavigationActions } from 'react-navigation';
import Icon from 'react-native-vector-icons/Ionicons';

import * as colors from '../../helpers/ColorPalette';
import styles from './styles';

// import arcelikLogo from '../../../assets/images/arcelik_logo.png';
import arcelikLogo from '../../../assets/images/arcelik_logo2_new.png';

class BackHeader extends Component {
  //params eklemeyi deneyecem
  constructor(props) {
    super(props);

    this.state = {
      routeName: 'Home'
    };
  }
  componentWillMount() {
    switch (this.props.navigation.state.routeName) {
      case 'Home':
        this.setState({ routeName: 'HomePage' });
        break;

      case 'Categories':
        this.setState({ routeName: 'MainCategories' });
        break;

      case 'AirCond':
        this.setState({ routeName: 'AirCondSelectionRobot' });
        break;

      case 'Search':
        this.setState({ routeName: 'SearchPage' });
        break;

      case 'Others':
        this.setState({ routeName: 'AirCondBuyPage' });
        break;

      default:
        break;
    }
  }

  handleImageButtonPress = () => {
    const resetAction = NavigationActions.reset({
      index: 0,
      actions: [NavigationActions.navigate({ routeName: this.state.routeName })]
    });
    this.props.navigation.dispatch(resetAction);
  };

  render() {
    return (
      <View style={styles.backHeaderContainerStyle}>
        {!this.props.noGoBack && (
          <View style={{ position: 'absolute', left: 0, top: 0 }}>
            <TouchableOpacity
              onPress={() => {
                this.props.onPress
                  ? this.props.onPress()
                  : this.props.navigation.goBack();
              }}
            >
              <View style={{ width: 80, height: 40, justifyContent: 'center' }}>
                <View style={{ marginLeft: 20 }}>
                  <Icon
                    size={24}
                    name='ios-arrow-back'
                    color={colors.lightGrey}
                  />
                </View>
              </View>
            </TouchableOpacity>
          </View>
        )}
        <View>
          <TouchableOpacity onPress={() => this.handleImageButtonPress()}>
            <Image
              source={arcelikLogo}
              style={{ width: 200, height: 40, resizeMode: 'contain' }}
            />
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

export default withNavigation(BackHeader);
