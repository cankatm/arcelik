import React, { Component } from 'react';
import { 
    View, 
    Text, 
    ScrollView,
    //Image,
    FlatList,
    TouchableOpacity,
    Dimensions,
    Share
} from 'react-native';
import { withNavigation } from 'react-navigation';
import Icon from 'react-native-vector-icons/Ionicons';

import * as colors from '../../helpers/ColorPalette';
import airCondImage from '../../../assets/images/airCond.jpg';
import styles from './styles';
import LargeItemExpArea from './LargeItemExpArea';

import Api from '../../services/WooCommerce/Api'
import { rhtmld } from '../../helpers/DataHelpers'
import HTML from 'react-native-render-html'

import Image from 'react-native-image-progress';
import ProgressBar from 'react-native-progress/Bar';

class ProductItemLarge extends Component {
    constructor(props) {
        super(props);
        this.state = {
            explanationSelected: true,
            Product: {
                name :'',
                images: [],
            },
            disabled: true
        }
    }

    componentWillMount() {
        //console.log(this.props.itemId)
        Api.get('products/'+this.props.itemId)
        .then((data) => {            
            data.description = data.description.replace(/width="[^\"]*"/gm ,"")
            data.description = data.description.replace(/height="[^\"]*"/gm ,"")
            this.setState({
                Product: data,
                disabled: false
            })
        })
    }

    shareItem = (itemsLink) => {
        Share.share({
            message: itemsLink
        })
    }

    selectExplanationArea = () => {
        this.setState({ explanationSelected: true })
    }

    selectExtraInfoArea = () => {
        this.setState({ explanationSelected: false })
    }

    renderArea = () => {
        console.log(this.state.Product.description)
        if (this.state.explanationSelected) {
            return (
                <View style={styles.largeProductExplainAreaContainerStyle} >
                    <HTML 
                        html={this.state.Product.description } 
                        imagesMaxWidth={ Dimensions.get('window').width }
                        tagsStyles= {{ 
                            h3: { fontSize: 20, marginBottom: 16 }, 
                            li: { flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between', fontSize: 12, color: '#B2B2B2' }, 
                            ul: { paddingLeft: 0 },
                            p: { width: '100%' },
                            img: { width: '100%' },
                            a: { width: '100%' }
                        }}
                        listsPrefixesRenderers={{
                            ul: (htmlAttribs, children, convertedCSSStyles, passProps) => {
                                //console.log(convertedCSSStyles)
                                return null
                            }
                        }}
                        renderers= {{
                            //p: () => <View style={{ width: Dimensions.get('window').width }}/>,
                            img: (htmlAttribs, children, convertedCSSStyles, passProps) => 
                            <TouchableOpacity onPress={() => { this.props.navigation.navigate('ProductImageShowPage', { imgSource: htmlAttribs.src })}} >
                            <Image 
                                source={{uri: htmlAttribs.src}}
                                indicator={ProgressBar} 
                                indicatorProps={{
                                    size: 40,
                                    borderWidth: 0,
                                    color: 'rgba(237, 28, 36, 1)',
                                    unfilledColor: 'rgba(255, 30, 30, 0.2)'
                                }}
                                style={styles.mediumDescImageStyle}
                                resizeMode="contain" />
                            </TouchableOpacity>
                        }}
                        classesStyles={{ 'last-paragraph': { textAlign: 'right', color: 'teal', fontWeight: '800' } }}
                        />
                    <Text style={{ marginTop: 8 }} >Seçtiğiniz ürünle ilgili detaylı bilgiye bu bölümden ulaşabilirsiniz.</Text>
                    <View style={{ height: 16 }} />
                </View>
            );
        }

        return (
            <View style={{ height: 3000 }} ></View>
        );
    }

    handleFormButton = (Product) => {
        this.props.navigation.navigate('FormPage', { Product })
    }

    render() {
        return (
            <ScrollView style={{ flex: 1, backgroundColor: colors.white }} >
                <View style={{ flex: 1, backgroundColor: colors.white }} >
                    <FlatList
                        data={this.state.Product.images}
                        renderItem={({item}) => (
                            <TouchableOpacity onPress={() => { this.props.navigation.navigate('ProductImageShowPage', { imgSource: item.src })}} >
                            <Image 
                                source={{uri: item.src}}
                                indicator={ProgressBar} 
                                indicatorProps={{
                                    size: 40,
                                    borderWidth: 0,
                                    color: 'rgba(237, 28, 36, 1)',
                                    unfilledColor: 'rgba(255, 30, 30, 0.2)'
                                }}
                                style={styles.largeProductItemImageStyle} />
                            </TouchableOpacity>
                        )}
                        keyExtractor={(item, index) => item.id.toString()}
                        horizontal
                        pagingEnabled
                        showsHorizontalScrollIndicator={false}
                        style={{ backgroundColor: colors.white }}
                    />
                    
                    <View style={styles.largeProductInformationContainerStyle} >
                        <Text style={styles.largeProductInformationMainHeaderTextStyle}>{this.state.Product.name}</Text>
                        <View style={styles.largeProductInformationLineStyle} />
                        <Text style={styles.largeProductInformationMainTextStyle}>{rhtmld(this.state.Product.short_description)}</Text>

                        <TouchableOpacity disabled={this.state.disabled} onPress={() => this.shareItem(this.state.Product.permalink)} >
                            <View style={styles.largeProductShareButtonContainerStyle} >
                                <View style={{ marginLeft: 16, paddingVertical: 8 }} >
                                    <Icon size={32} name='md-share' color={colors.white} />
                                </View>
                                <Text style={styles.largeProductShareButtonTextStyle} >Bu ürünü paylaş!</Text>
                            </View>
                        </TouchableOpacity>

                        <TouchableOpacity disabled={this.state.disabled} onPress={() => this.handleFormButton(this.state.Product)} >
                            <View style={styles.largeProductShareButtonContainerStyle} >
                                <View style={{ marginLeft: 16, paddingVertical: 8 }} >
                                    <Icon size={32} name='md-mail' color={colors.white} />
                                </View>
                                <Text style={styles.largeProductShareButtonTextStyle} >Bu ürün için fiyat al!</Text>
                            </View>
                        </TouchableOpacity>
                    </View>

                    <View style={styles.largeProductExplainAndExtraInfoContainerStyle} >
                        <TouchableOpacity onPress={() => this.selectExplanationArea()} >
                            <View style={[styles.largeProductExplainAndExtraInfoButtonStyle, {
                                borderColor: this.state.explanationSelected ? colors.red : colors.lightGrey,
                                borderBottomWidth: this.state.explanationSelected ? 3 : 1
                            }]} >
                                <Text style={[styles.largeProductExplainAndExtraInfoButtonTextStyle, {color: this.state.explanationSelected ? colors.black : colors.lightGrey}]}>Açıklama</Text>
                            </View>
                        </TouchableOpacity>

                        {/* <TouchableOpacity onPress={() => this.selectExtraInfoArea()}>
                            <View style={[styles.largeProductExplainAndExtraInfoButtonStyle, {
                                borderColor: this.state.explanationSelected ? colors.lightGrey : colors.red,
                                borderBottomWidth: this.state.explanationSelected ? 1 : 3
                            }]} >
                                <Text style={[styles.largeProductExplainAndExtraInfoButtonTextStyle, {color: this.state.explanationSelected ? colors.lightGrey : colors.black}]}>Ek Bilgi</Text>
                            </View>
                        </TouchableOpacity> */}
                    </View>

                    {this.renderArea()}
                </View>
            </ScrollView>
        );
    }
}

export default withNavigation(ProductItemLarge);