import React, { Component } from 'react';
import { 
    View, 
    Text, 
    StyleSheet, 
    StatusBar,
    TouchableOpacity
} from 'react-native';
import { withNavigation } from 'react-navigation';
import Image from 'react-native-image-progress'
import ProgressBar from 'react-native-progress/Bar'

import styles from './styles';

import { rhtmld } from '../../helpers/DataHelpers'

class ProductItemSmall extends Component {
    render() {
        return (
            <TouchableOpacity onPress={() => this.props.navigation.navigate('ProductDetail', { itemId: this.props.item.id })} >
                <View style={styles.smallProductItemGenelContainerStyle} >
                    <View>
                        <Image 
                           source={{uri: this.props.item.images[0].src}}
                           indicator={ProgressBar} 
                           indicatorProps={{
                               size: 10,
                               borderWidth: 0,
                               color: 'rgba(237, 28, 36, 1)',
                               unfilledColor: 'rgba(255, 30, 30, 0.2)'
                               }}
                               style={styles.smallProductItemImageStyle}
                        />
                    </View>
                    <View style={styles.smallProductItemInformationContainerStyle} >
                        <Text style={styles.smallProductItemInformationHeaderTextStyle}>{this.props.item.name}</Text>
                        <Text style={styles.smallProductItemInformationTextStyle}>{rhtmld(this.props.item.short_description)}</Text>
                        <View style={{ height: 16 }} />
                    </View>
                </View>
            </TouchableOpacity>
        );
    }
}

export default withNavigation(ProductItemSmall);