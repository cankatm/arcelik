import React, { Component } from 'react';
import { View, Text, StyleSheet, StatusBar } from 'react-native';

import styles from './styles';

class LargeItemExpArea extends Component {
    render() {
        return (
            <View style={styles.largeItemExpAreaContainerStyle} >
                <Text style={styles.largeItemExpAreaHeaderTextStyle}>Kapasite</Text>

                <View style={styles.largeItemExpAreaMiniExpContainerStyle} >
                    <Text style={styles.largeItemExpAreaMiniExpTextStyle} >Kapasite (Isıtma)</Text>
                    <Text style={styles.largeItemExpAreaMiniExpTextStyle}>9000 Btu/h</Text>
                </View>
                <View style={styles.largeItemExpAreaMiniExpContainerStyle} >
                    <Text style={styles.largeItemExpAreaMiniExpTextStyle} >Kapasite (Isıtma)</Text>
                    <Text style={styles.largeItemExpAreaMiniExpTextStyle}>9000 Btu/h</Text>
                </View>
                <View style={styles.largeItemExpAreaMiniExpContainerStyle} >
                    <Text style={styles.largeItemExpAreaMiniExpTextStyle} >Kapasite (Isıtma)</Text>
                    <Text style={styles.largeItemExpAreaMiniExpTextStyle}>9000 Btu/h</Text>
                </View>
                <View style={styles.largeItemExpAreaMiniExpContainerStyle} >
                    <Text style={styles.largeItemExpAreaMiniExpTextStyle} >Kapasite (Isıtma)</Text>
                    <Text style={styles.largeItemExpAreaMiniExpTextStyle}>9000 Btu/h</Text>
                </View>
                <View style={styles.largeItemExpAreaMiniExpContainerStyle} >
                    <Text style={styles.largeItemExpAreaMiniExpTextStyle} >Kapasite (Isıtma)</Text>
                    <Text style={styles.largeItemExpAreaMiniExpTextStyle}>9000 Btu/h</Text>
                </View>
                <View style={styles.largeItemExpAreaMiniExpContainerStyle} >
                    <Text style={styles.largeItemExpAreaMiniExpTextStyle} >Kapasite (Isıtma)</Text>
                    <Text style={styles.largeItemExpAreaMiniExpTextStyle}>9000 Btu/h</Text>
                </View>
            </View>
        );
    }
}

export default LargeItemExpArea;