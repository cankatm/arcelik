import React from 'react';
import { StyleSheet, Dimensions } from 'react-native';

import * as colors from '../../helpers/ColorPalette';

const WINDOW_WIDTH = Dimensions.get('window').width;
const WINDOW_HEIGHT = Dimensions.get('window').height;

const SMALL_IMAGE_WIDTH = 120;

const MEDIUM_WINDOW_WIDTH = (WINDOW_WIDTH / 2) - 24;
const MEDIUM_IMAGE_SIZE = (WINDOW_WIDTH / 2) - 36;

export default StyleSheet.create({
    smallProductItemGenelContainerStyle: {
        width: WINDOW_WIDTH,
        backgroundColor: colors.white,
        alignItems: 'center',
        borderBottomWidth: 1,
        borderColor: colors.lightGrey,
        flexDirection: 'row'
    },
    smallProductItemImageStyle: {
        width: SMALL_IMAGE_WIDTH,
        height: SMALL_IMAGE_WIDTH,
        resizeMode: 'contain',
        marginLeft: 16
    },
    smallProductItemInformationContainerStyle: {
        marginLeft: 16,
        width: (WINDOW_WIDTH - SMALL_IMAGE_WIDTH) - 56
    },
    smallProductItemInformationHeaderTextStyle: {
        fontSize: 20,
        marginTop: 16,
        marginBottom: 4,
    },
    smallProductItemInformationTextStyle: {
        marginTop: 4,
        fontSize: 14
    },
    mediumProductItemGenelContainerStyle: {
        width: WINDOW_WIDTH / 2,
        backgroundColor: colors.lightestGrey,
        alignItems: 'center',
        justifyContent: 'center',
        flexDirection: 'column',
        marginBottom: 8
    },
    mediumProductItemContainerStyle: {
        width: MEDIUM_WINDOW_WIDTH,
        backgroundColor: colors.white,
        alignItems: 'center',
        justifyContent: 'center',
        flexDirection: 'column'
    },
    mediumProductItemImageStyle: {
        width: MEDIUM_IMAGE_SIZE,
        height: MEDIUM_IMAGE_SIZE,
        paddingVertical: 24
    },
    mediumProductItemDetailContainerStyle: {
        width: MEDIUM_IMAGE_SIZE,
        justifyContent: 'center',
        flexDirection: 'column',
        paddingLeft: 4
    },
    mediumProductItemDetailTextStyle: {
        flexShrink: 1,
        paddingTop: 4,
        textAlign: 'left',
        paddingTop: 4
    },
    mediumDescImageStyle: {
        width: MEDIUM_IMAGE_SIZE * 2,
        height: MEDIUM_IMAGE_SIZE,
        paddingVertical: 24
    },
    largeProductItemImageStyle: {
        minHeight: 200,
        width: WINDOW_WIDTH,
        height: WINDOW_WIDTH,
        resizeMode: 'contain'
    },
    largeProductInformationContainerStyle: {
        width: WINDOW_WIDTH - 32,
        marginLeft: 16,
        marginVertical: 8,
    },
    largeProductInformationMainHeaderTextStyle: { 
        fontSize: 20, 
        color: colors.black 
    },
    largeProductInformationLineStyle: { 
        marginVertical: 16, 
        width: 32, 
        height: 1, 
        backgroundColor: colors.black
    },
    largeProductInformationMainTextStyle: { 
        marginBottom: 4, 
        color: colors.lightGrey 
    },
    largeProductShareButtonContainerStyle: { 
        flexDirection: 'row', 
        width: WINDOW_WIDTH - 32,
        backgroundColor: colors.darkestGrey,
        alignItems: 'center',
        marginTop: 8,
        marginBottom: 16
    },
    largeProductShareButtonTextStyle: { 
        marginLeft: 8, 
        color: colors.white, 
        fontSize: 18 
    },
    largeProductExplainAndExtraInfoContainerStyle: { 
        flexDirection: 'row', 
        width: WINDOW_WIDTH, 
        height: 60, 
        borderBottomWidth: 1,
        borderColor: colors.lightGrey
    },
    largeProductExplainAndExtraInfoButtonStyle: {
        width: 140, 
        borderBottomWidth: 5, 
        backgroundColor: colors.white, 
        height: 60,
        alignItems: 'center',
        justifyContent: 'center'
    },
    largeProductExplainAndExtraInfoButtonTextStyle: {
        fontSize: 18
    },
    largeProductExplainAreaContainerStyle: { 
        width: WINDOW_WIDTH - 32, 
        marginLeft: 16
    },
    largeItemExpAreaContainerStyle: { 
        width: WINDOW_WIDTH - 32, 
        marginTop: 16
    },
    largeItemExpAreaHeaderTextStyle: { 
        fontSize: 20,
        marginBottom: 16
    },
    largeItemExpAreaMiniExpContainerStyle: { 
        flexDirection: 'row', 
        alignItems: 'center', 
        justifyContent: 'space-between'
    },
    largeItemExpAreaMiniExpTextStyle: { 
        fontSize: 12,
        color: colors.lightGrey,
        marginBottom: 4
    },
});