import React, { Component } from 'react';
import { 
    View, 
    Text, 
    StyleSheet, 
    StatusBar,
    //Image,
    TouchableOpacity
} from 'react-native';
import { withNavigation } from 'react-navigation';

import * as colors from '../../helpers/ColorPalette';
import airCondImage from '../../../assets/images/airCond.jpg';
import styles from './styles';

import { rhtmld } from '../../helpers/DataHelpers'

import Image from 'react-native-image-progress';
import ProgressBar from 'react-native-progress/Bar';

class ProductItemMedium extends Component {

    render() {
        return (
            <TouchableOpacity onPress={() => this.props.navigation.navigate('ProductDetail', { itemId: this.props.item.id })} >
                <View style={[styles.mediumProductItemGenelContainerStyle, { backgroundColor: this.props.bgColor ? this.props.bgColor : colors.lightestGrey}]} >
                    <View style={styles.mediumProductItemContainerStyle} >
                    <Image 
                        source={{uri: this.props.item.images[0].src}}
                        resizeMode= 'contain'
                        indicator={ProgressBar} 
                        indicatorProps={{
                            size: 40,
                            borderWidth: 0,
                            color: 'rgba(237, 28, 36, 1)',
                            unfilledColor: 'rgba(255, 30, 30, 0.2)'
                          }}
                        style={styles.mediumProductItemImageStyle}/>
                       

                        <View style={styles.mediumProductItemDetailContainerStyle} >
                            <Text style={styles.mediumProductItemDetailTextStyle}>{this.props.item.name}</Text>
                        </View>
                        <View style={styles.mediumProductItemDetailContainerStyle} >
                            <Text style={styles.mediumProductItemDetailTextStyle}>{rhtmld(this.props.item.short_description)}</Text>
                            {/* <Text style={styles.mediumProductItemDetailTextStyle}>Soğutma Kapasitesi (Btu/h): 22519 Btu/h</Text>
                            <Text style={styles.mediumProductItemDetailTextStyle}>Enerji Sınıfı-Soğutma: A++</Text> */}
                            <View style={{ height: 8 }} />
                        </View>
                    </View>
                </View>
            </TouchableOpacity>
        );
    }
}

export default withNavigation(ProductItemMedium);