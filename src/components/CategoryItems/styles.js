import React from 'react';
import { StyleSheet, Dimensions } from 'react-native';

import * as colors from '../../helpers/ColorPalette';

const WINDOW_WIDTH = Dimensions.get('window').width;
const WINDOW_HEIGHT = Dimensions.get('window').height;

const MARGIN = 16;

export default StyleSheet.create({
    categoryItemImageStyle: {
        width: WINDOW_WIDTH - 32,
        height: 180, 
        marginTop: 16
    },
    categoryItemContainerStyle: {
        width: WINDOW_WIDTH - 32,
        height: 180,
        backgroundColor: 'rgba(0, 0, 0, 0.2)',
        borderRadius: 8,
        justifyContent: 'space-between'
    },
    categoryItemHeaderTextStyle: { 
        marginLeft: MARGIN,
        marginTop: MARGIN / 2,
        color: colors.white,
        fontSize: 20
    },
    categoryItemLineStyle: { 
        marginLeft: MARGIN,
        marginVertical: MARGIN / 2,
        width: 32, 
        height: 1, 
        backgroundColor: colors.white 
    },
    categoryItemBottomTextStyle: { 
        marginLeft: MARGIN,
        marginBottom: MARGIN,
        color: colors.white,
        fontSize: 14
    },
});