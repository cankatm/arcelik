import React, { Component } from 'react';
import { 
    View, 
    Text, 
    StyleSheet, 
    StatusBar,
    ImageBackground,
    TouchableOpacity,
    Linking
} from 'react-native';
import { withNavigation } from 'react-navigation';

import styles from './styles';

import beyazEsyaBg from '../../../assets/images/beyazEsya.jpg';

class CategoryItem extends Component {
    render() {
        const { imgSource, content, goTo } = this.props;
        if(imgSource == null ) 
            return null
        return (
            <TouchableOpacity onPress={() => { 
                if(this.props.catId == 767)
                    return Linking.openURL(`http://www.arcelikelmas.com/wp-content/uploads/2019/05/vrs-ozellikleri.pdf`)
                return this.props.navigation.navigate(goTo, { catId: this.props.catId })
            }}>
                <ImageBackground imageStyle={{ borderRadius: 8 }} source={{ uri: imgSource.src }} style={styles.categoryItemImageStyle} >
                    <View style={styles.categoryItemContainerStyle} >
                        <View>
                            <Text style={styles.categoryItemHeaderTextStyle} >{content}</Text>
                            <View style={styles.categoryItemLineStyle} />
                        </View>
                        <Text style={styles.categoryItemBottomTextStyle} >Keşfet</Text>
                    </View>
                </ImageBackground>
            </TouchableOpacity>
        );
    }
}

export default withNavigation(CategoryItem);