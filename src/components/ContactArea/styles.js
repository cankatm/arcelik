import React from 'react';
import { StyleSheet, Dimensions } from 'react-native';

import * as colors from '../../helpers/ColorPalette';

const WINDOW_WIDTH = Dimensions.get('window').width;
const WINDOW_HEIGHT = Dimensions.get('window').height;

export default StyleSheet.create({
    contactAreaContainerStyle: {
        width: WINDOW_WIDTH,
        backgroundColor: colors.darkestGrey,
        alignItems: 'center',
        justifyContent: 'center',
    },
    contactAreaContainerInnerStyle: {
        width: WINDOW_WIDTH - 32,
        marginVertical: 32
    },
    contactAreaTitleStyle: { 
        fontSize: 18, 
        marginBottom: 16,
        color: colors.white
    },
    contactAreaLineStyle: { 
        marginBottom: 16, 
        height: 2, 
        width: 32, 
        backgroundColor: colors.white
    },
    contactAreaItemContainerStyle: { 
        marginBottom: 16, 
        width: ((WINDOW_WIDTH - 32) / 4) * 3,
        flexDirection: 'row',
    },
    contactAreaItemTextStyle: { 
        marginLeft: 16, 
        color: colors.lightGrey,
        fontSize: 16
    },


    contactAreaLightContainerStyle: {
        width: WINDOW_WIDTH,
        backgroundColor: colors.white,
        alignItems: 'center',
        justifyContent: 'center',
        marginLeft: 16
    },
    contactAreaLightContainerInnerStyle: {
        width: WINDOW_WIDTH - 32,
        marginVertical: 32
    },
    contactAreaLightTitleStyle: { 
        fontSize: 18, 
        marginBottom: 16,
        color: colors.black
    },
    contactAreaLineLightStyle: { 
        marginBottom: 16, 
        height: 2, 
        width: 32, 
        backgroundColor: colors.black
    },
    contactAreaLightItemContainerStyle: { 
        marginBottom: 16, 
        width: ((WINDOW_WIDTH - 32) / 4) * 3,
        flexDirection: 'row',
    },
    contactAreaLightItemTextStyle: { 
        marginLeft: 16, 
        color: colors.black,
        fontSize: 16
    },
});