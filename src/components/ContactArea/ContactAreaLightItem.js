import React, { Component } from 'react';
import { 
    View, 
    Text, 
    StyleSheet, 
    StatusBar 
} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';

import * as colors from '../../helpers/ColorPalette';

import styles from './styles';

class ContactAreaLightItem extends Component {
    render() {
        const { iconName, infoText } = this.props;
        return (
            <View style={styles.contactAreaItemContainerStyle}>
                <View>
                    <Icon size={24} name={iconName} color={colors.lightGrey} />
                </View>
                <Text style={styles.contactAreaLightItemTextStyle} >{infoText}</Text>
            </View>
        );
    }
}

export default ContactAreaLightItem;