import React, { Component } from 'react';
import { 
    View, 
    Text, 
    TouchableOpacity,
    Linking
} from 'react-native';
import Communications from 'react-native-communications';

import ContactAreaItem from './ContactAreaItem';
import styles from './styles';

const whatsappText = '';
const phoneNumber = '+90 530 821 46 74';

class ContactArea extends Component {
    render() {
        return (
            <View style={styles.contactAreaContainerStyle} >
                <View style={styles.contactAreaContainerInnerStyle}>
                    <Text style={styles.contactAreaTitleStyle} >Bize Ulaşın</Text>
                    <View style={styles.contactAreaLineStyle} />
                    <TouchableOpacity onPress={() => Linking.openURL('https://www.google.com/maps/place/Arçelik+Elmaş/@39.9024349,32.8509403,17z/data=!3m1!4b1!4m5!3m4!1s0x0:0xd4f681c4c24044f1!8m2!3d39.9024349!4d32.853129')}>
                        <ContactAreaItem iconName='ios-map' infoText='Güvenlik Caddesi No:73/A Ayrancı Ankara' />
                    </TouchableOpacity>

                    <TouchableOpacity onPress={() => Communications.phonecall('+90 312 428 26 46', true)}>
                        <ContactAreaItem iconName='ios-call' infoText='+90 312 428 26 46' />
                    </TouchableOpacity>

                    <TouchableOpacity onPress={() => Communications.email(['app@arcelikelmas.com'], null, null, null, '')} >
                        <ContactAreaItem iconName='ios-mail' infoText='app@arcelikelmas.com' />
                    </TouchableOpacity>

                    <TouchableOpacity onPress={() => Linking.openURL(`whatsapp://send?text=${whatsappText}&phone=${phoneNumber}`)} >
                        <ContactAreaItem iconName='logo-whatsapp' infoText='+90 530 821 46 74' />
                    </TouchableOpacity>
                </View>
            </View>
        );
    }
}

export default ContactArea;