import ContactArea from './ContactArea';
import ContactAreaLight from './ContactAreaLight';
import styles from './styles';

export { ContactArea, ContactAreaLight, styles };
