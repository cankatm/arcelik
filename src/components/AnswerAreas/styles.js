import React from 'react';
import { StyleSheet, Dimensions } from 'react-native';

import * as colors from '../../helpers/ColorPalette';

const WINDOW_WIDTH = Dimensions.get('window').width;
const WINDOW_HEIGHT = Dimensions.get('window').height;

const BUTTON_HEIGHT = 140;

export default StyleSheet.create({
    bigButtonContainerStyle: {
        width: WINDOW_WIDTH - 32,
        marginLeft: 16,
        height: BUTTON_HEIGHT,
        backgroundColor: colors.red,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
    },
    bigButtonTextStyle: { 
        color: colors.white,
        fontSize: 22
    },
    bigButtonLineStyle: { 
        marginVertical: 12,
        width: 32, 
        height: 1, 
        backgroundColor: colors.white 
    },
});