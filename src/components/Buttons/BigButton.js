import React, { Component } from 'react';
import { 
    View, 
    Text, 
    StyleSheet, 
    StatusBar,
    TouchableOpacity,
    Image
} from 'react-native';
import { withNavigation } from 'react-navigation';
import Icon from 'react-native-vector-icons/Ionicons';
import bireysel from '../../../assets/images/avatar.png';
import kurumsal from '../../../assets/images/briefcase.png';

import * as colors from '../../helpers/ColorPalette';
import styles from './styles';

class BigButton extends Component {
    render() {
        const { content, bgColor, image, iconName, mgTop, to } = this.props;
        let imageSource = (image) => {
            if(image === 'kurumsal') {
                return kurumsal;
            }
            return bireysel;
        }

        return (
            <TouchableOpacity onPress={() => this.props.navigation.navigate('QuestionsPage', { to, counter: 1, answers: [content] }) } >
                <View style={[styles.bigButtonContainerStyle, { backgroundColor: bgColor, marginTop: mgTop }]} >
                    <View style={{ marginLeft: 24, width: 140 }}>
                        <Text style={styles.bigButtonTextStyle} >{content}</Text>
                        <View style={styles.bigButtonLineStyle} />
                    </View>

                    <View style={{ marginRight: 24, marginTop: 50 }}>
                        <Image source={imageSource(image)} style={{ width: 40, height: 40, resizeMode: 'contain' }} />
                    </View>
                </View>
            </TouchableOpacity>
        );
    }
}

export default withNavigation(BigButton);