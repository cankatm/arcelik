import React, { Component } from 'react'
import { 
    ScrollView, 
    Image, 
    TouchableHighlight,
    Dimensions
} from 'react-native'

import * as colors from '../../helpers/ColorPalette';

const WINDOW_WIDTH = Dimensions.get('window').width;
const WINDOW_HEIGHT = Dimensions.get('window').height;

export default class ZoomView extends Component {
    static defaultProps = {
        doAnimateZoomReset: false,
        maximumZoomScale: 2,
        minimumZoomScale: 1,
        zoomHeight: WINDOW_WIDTH, 
        zoomWidth: WINDOW_HEIGHT,
    }
    handleResetZoomScale = (event) => {
        this.scrollResponderRef.scrollResponderZoomTo({ 
            x: 0, 
            y: 0, 
            width: this.props.zoomWidth, 
            height: this.props.zoomHeight, 
            animated: true 
        })
    }
    setZoomRef = node => { 
        if (node) {
            this.zoomRef = node
            this.scrollResponderRef = this.zoomRef.getScrollResponder()
        }
    }
    render() {
        const { source, maximumZoomScale, minimumZoomScale } = this.props;
        return (
            <ScrollView
                contentContainerStyle={{ alignItems: 'center', justifyContent: 'center' }}
                centerContent
                maximumZoomScale={maximumZoomScale}
                minimumZoomScale={minimumZoomScale}
                showsVerticalScrollIndicator={false}
                showsHorizontalScrollIndicator={false}
                ref={this.setZoomRef}
                style={{ overflow: 'hidden', backgroundColor: colors.white }}
            >
                <TouchableHighlight
                    onPress={this.handleResetZoomScale}
                    underlayColor={colors.white}
                >
                    <Image
                        source={source}
                        style={{ width: WINDOW_WIDTH, resizeMode: 'contain' }}
                    />
                </TouchableHighlight>
            </ScrollView>
        )
    }
}