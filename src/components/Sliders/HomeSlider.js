import React, { Component } from 'react';
import { 
    View, 
    Text,
    ScrollView,
    ActivityIndicator
} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';

import HomeSliderItem from './HomeSliderItem';

import * as colors from '../../helpers/ColorPalette';
import styles from './styles';
/* Services */
import Api from '../../services/WooCommerce/Api'

class HomeSlider extends Component {
    constructor(props) {
        super(props);
        this.state = {
            urunler: [],
            loading:true,
        }
    }
    componentDidMount(){
        Api.get('products/',{
            per_page: 100, // tum urunleri alsin diye
            tag : 199 // ONECIKANURUN
        })
        .then((data) => {            
            //console.log(data);
            this.setState({ urunler: data, loading: false })
        });  
    }

    render() {
        if (this.state.loading) {
            return (
                <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center', marginTop: 32, minHeight: 250 }} >
                    <ActivityIndicator size='large' color={colors.red} />
                </View>
            )
        }
        return (
            <View style={{ flex: 1}}>
                <ScrollView
                    horizontal
                    showsHorizontalScrollIndicator={false}
                    style={{ flex: 1, backgroundColor: colors.darkestGrey }}
                    pagingEnabled
                >
                  {this.state.urunler.map((p) => <HomeSliderItem key={p.id} item={p}/>)}
                </ScrollView>
                <View style={styles.homeSliderTitleContainerStyle} >
                    <Text style={styles.homeSliderTitleTextStyle} >POPÜLER ÜRÜNLER</Text>
                    <View style={styles.homeSliderTitleLineStyle} />
                </View>
                <View style={{ position: 'absolute', bottom: 16, right: 16 }} >
                    <Icon size={24} name='ios-swap' color={colors.grey} />
                </View>
            </View>
        );
    }
}

export default HomeSlider;