import React, { Component } from 'react';
import { 
    View, 
    Text, 
    TouchableOpacity,
    //Image
} from 'react-native';
import { withNavigation } from 'react-navigation';

import styles from './styles';

import Image from 'react-native-image-progress'
import ProgressBar from 'react-native-progress/Bar'

class HomeSliderItem extends Component {
    render() {
        return (
            <View style={styles.homeSliderItemContainerStyle} >
                <View style={styles.homeSliderItemTitleContainerStyle}>
                    <Text style={styles.homeSliderItemTitleTextStyle}>{this.props.item.name}</Text>
                </View>

                 <Image 
                    source={{uri: this.props.item.images[0].src}}
                    indicator={ProgressBar} 
                    indicatorProps={{
                        size: 40,
                        borderWidth: 0,
                        color: 'rgba(237, 28, 36, 1)',
                        unfilledColor: 'rgba(255, 30, 30, 0.2)'
                        }}
                        style={styles.homeSliderItemImageStyle}/>

                <View style={styles.homeSliderButtonOutContainerStyle}>
                    <TouchableOpacity onPress={() => this.props.navigation.navigate('ProductDetail', { itemId: this.props.item.id })} >
                        <View style={styles.homeSliderButtonContainerStyle} >
                            <Text style={styles.homeSliderButtonTextStyle} >DETAYLI BİLGİ</Text>
                        </View>
                    </TouchableOpacity>
                </View>
            </View>
        );
    }
}

export default withNavigation(HomeSliderItem);