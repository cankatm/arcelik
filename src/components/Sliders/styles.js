import React from 'react';
import { StyleSheet, Dimensions } from 'react-native';

import * as colors from '../../helpers/ColorPalette';

const WINDOW_WIDTH = Dimensions.get('window').width;
const WINDOW_HEIGHT = Dimensions.get('window').height;

export default StyleSheet.create({
    homeSliderItemContainerStyle: {
        width: WINDOW_WIDTH,
        height: WINDOW_WIDTH,
        backgroundColor: colors.darkestGrey,
        alignItems: 'center',
        justifyContent: 'center'
    },
    homeSliderItemTitleContainerStyle: { 
        position: 'absolute', 
        top: 64, 
        left: 16, 
        width: (WINDOW_WIDTH / 3 ) * 2,
        zIndex: 999
    },
    homeSliderItemTitleTextStyle: { 
        color: colors.white,
        fontSize: 18,
        zIndex: 999
    },
    homeSliderItemImageStyle: { 
        width: (WINDOW_WIDTH / 3) * 2, 
        height: 300, 
        resizeMode: 'center' 
    },
    homeSliderButtonOutContainerStyle: { 
        position: 'absolute', 
        left: 16, 
        bottom: 16,
        zIndex: 5
    },
    homeSliderButtonContainerStyle: { 
        backgroundColor: colors.red, 
        paddingHorizontal: 16,
        paddingVertical: 12,
    },
    homeSliderButtonTextStyle: { 
        color: colors.white,
        fontSize: 14
    },
    homeSliderTitleContainerStyle: {
        position: 'absolute', 
        top: 16, 
        left: 16
    },
    homeSliderTitleTextStyle: { 
        color: colors.grey,
        zIndex: 16
    },
    homeSliderTitleLineStyle: { 
        width: 32, 
        height: 1, 
        backgroundColor: colors.grey, 
        marginTop: 16,
        zIndex: 16
    },
});