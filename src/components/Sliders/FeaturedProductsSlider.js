import React, { Component } from 'react';
import { 
    View, 
    Text, 
    StyleSheet, 
    StatusBar,
    ScrollView,
    TouchableOpacity,
    ActivityIndicator
} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import { withNavigation } from 'react-navigation';

import { ProductItemMedium } from '../ProductItems';

import * as colors from '../../helpers/ColorPalette';

import styles from './styles';

/* Services */
import Api from '../../services/WooCommerce/Api'

class FeaturedProductsSlider extends Component {
    constructor(props) {
        super(props);
    
        this.state = {
          Products: [],
          loading: true
        };
    }
    componentWillMount() {

        /* One cikan urunler */
        Api.get('products/',{
            per_page: 100, // tum kategorileri alsin diye
            featured: true
            //status:'publish'
        })
        .then((data) => {
            this.setState({
                Products: data,
                loading: false
              });
        });

    }

    render() {
        if (this.state.loading) {
            return (
                <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center', marginTop: 32, minHeight: 200 }} >
                    
                </View>
            )
        }
        return (
            <View style={{ backgroundColor: colors.lightestGrey }} >
                <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginVertical: 16 }} >
                    <View style={{ marginLeft: 16}} >
                        <Text>Öne Çıkanlar</Text>
                        <View style={{ width: 32, height: 1, backgroundColor: colors.darkestGrey, marginTop: 8 }} />
                    </View>

                    <View style={{ marginRight: 16, flexDirection: 'row' }}>
                        <TouchableOpacity onPress={() => this.props.navigation.navigate('ProductList', { noGoBack: false, showAll: true })} >
                            <Text>Tümü ></Text>
                        </TouchableOpacity>
                    </View>
                </View>

                <ScrollView
                    horizontal
                    showsHorizontalScrollIndicator={false}
                    style={{ backgroundColor: colors.ligtestGrey }}
                >
                    <View style={{ flexDirection: 'row' }} >
                        {this.state.Products.map((p) => <ProductItemMedium key={p.id} item={p}/>)}
                    </View>
                </ScrollView>
            </View>
        );
    }
}

export default withNavigation(FeaturedProductsSlider);