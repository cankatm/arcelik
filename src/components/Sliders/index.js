import HomeSlider from './HomeSlider';
import FeaturedProductsSlider from './FeaturedProductsSlider';
import styles from './styles';

export { HomeSlider, FeaturedProductsSlider, styles };
