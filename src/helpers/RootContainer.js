import React, { Component } from 'react';
import { View } from 'react-native';
import { connect, mapDispatchToProps } from 'react-redux';

import ReduxNavigator from './ReduxNavigator';

class RootContainer extends Component {
    render () {
      return (
        <View style={{ flex: 1 }}>
          <ReduxNavigator />
        </View>
      )
    }
  }
  export default connect(null, mapDispatchToProps)(RootContainer);