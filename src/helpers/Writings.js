import air1 from '../../assets/images/buying-icon1.png';
import air2 from '../../assets/images/buying-icon2.png';
import air3 from '../../assets/images/buying-icon3.png';
import air4 from '../../assets/images/buying-icon4.png';
import air5 from '../../assets/images/buying-icon5.png';

import beyazEsya from '../../assets/images/beyazEsya.jpg';

//Beyaz eşya sub imageler
import buzdolabi from '../../assets/images/buzdolabi.jpg';
import derinDondurucu from '../../assets/images/derin-dondurucu.jpg';
import camasirMakinesi from '../../assets/images/camasir-mk.jpg';
import bulasikMakinesi from '../../assets/images/bulasik-mk.jpg';
import kurutmaMakinesi from '../../assets/images/Kurutma-Mk.jpg';
import mikrodalgaFirin from '../../assets/images/mikrodalga.jpg';
import kurutmaliCamasirMakinesi from '../../assets/images/Kurutma-Mk.jpg';
import firin from '../../assets/images/Firin-1.jpg';
import suAritmaCihazi from '../../assets/images/su-aritma-cihazi.jpg';
import setUstuOcak from '../../assets/images/set-ustu-ocak.jpg';
import aspirator from '../../assets/images/Aspirator.jpg';

//Ankastre sub imageler
import ankastreFirin from '../../assets/images/ankastre-firin.jpg';
import ankastreMikrodalga from '../../assets/images/ankastre-mikrodalga-firin.jpg';
import ankastreOcak from '../../assets/images/ankastre-ocak.jpg';
import ankastreDavlumbaz from '../../assets/images/ankastre-davlumbaz.jpg';
import ankastreKahveMakinesi from '../../assets/images/ankastre-kahve.png';
import ankastreBulasikMakinesi from '../../assets/images/ankastre-bulasik-mk.jpg';
import ankastreBuzdolabi from '../../assets/images/ankastre-buzdolabi.jpg';
import ankastreAspirator from '../../assets/images/ankastre-aspirator.jpg';
import ankastreCamasirMakinesi from '../../assets/images/ankastre-camasir-mk.jpg';
import ankastreKurutmaliCamasirMakinesi from '../../assets/images/ankastre-kurutmali-camasir-mk.jpg';
import sicakTutmaCekmecesi from '../../assets/images/sicak-tutma-cekmecesi.jpg';
import ankastreSet from '../../assets/images/ankastre-set.jpg';

//Isıtma-Soğutma sub imageler
import klima from '../../assets/images/klima.jpg';
import kombi from '../../assets/images/kombi.jpg';
import sofben from '../../assets/images/sofben.jpg';
import termosifon from '../../assets/images/termosifon.jpg';
import aniSuIsitici from '../../assets/images/ani-su-isitici.jpg';
import flavelSoba from '../../assets/images/flavel-isitici.jpg';
import vantilator from '../../assets/images/vantilator.jpg';
import odaTermostati from '../../assets/images/oda-termostati.jpg';

export const ABOUT_US = 'Şirketimiz 1977 yılında Elmaş A.Ş. adı altında kurulmuştur. Elmaş A.Ş. ile Arçelik A.Ş.\'nin birlikteliği 1979 yılından beri sürmektedir. Firmamız klima, mutfak ve led aydınlatma sektöründe çözüm ihtiyacı olan firma ve müşterilerin; konut, işyeri, otel ve yaşam alanlarına yönelik tüm projelerinde; hazır mutfak, ankastre ve iklimlendirme ürünleri başta olmak üzere tüm ihtiyaçlarına cevap verebilecek geniş ürün yelpazesi ve konusunda uzman ekipleri ile çözüm ortağı yaklaşımı sergilemektedir.';

export const airCondItems = [
    {
        id: '1',
        headerTitle: 'Klima Tipleri',
        image: air1,
        content: `Doğru klimayı seçebilmek için öncelikle klima çeşitlerini incelemeli ve hangi klima tipinin size uygun olduğuna karar vermelisiniz.`
    },
    {
        id: '2',
        headerTitle: 'Kapasite',
        image: air2,
        content: `Klima BTU değerini ve klima kapasitesini hesaplamak doğru klimayı almada en önemli adımlardan biridir. Klimanın mekana yetersiz ya da fazla gelmemesi için bu kriterler doğrultusunda bir klima seçmelisiniz.`
    },
    {
        id: '3',
        headerTitle: 'Enerji Tasarrufu',
        image: air3,
        content: `Hem fatura tutarınızı azaltmak hem de doğayı korumak için enerji verimliliğini arttıracak klima model ve özelliklerini tercih etmelisiniz.`
    },
    {
        id: '4',
        headerTitle: 'Hijyen ve Ses',
        image: air4,
        content: `Alacağınız klimanın ses seviyesine ve kolay temizlenebilir olmasına dikkat etmelisiniz. Hijyen sağlayacak gelişmiş özellikler mekandaki havanın daha temiz olmasını sağlayacaktır.`
    },
    {
        id: '5',
        headerTitle: 'Konfor',
        image: air5,
        content: `Klimalı hayat kolay ama klimanızdaki bazı özellikler sayesinde yaşam kalitenizi arttırmak isterseniz tercihiniz Uzaktan Kontrol Wi-Fi gibi konforlu kullanım sağlayan özelliklere sahip klimalardan yana kullanabilirsiniz.`
    }
];

export const mainCategories = [
    {
        id: '1',
        image: derinDondurucu,
        content: `BEYAZ EŞYA`,
        from: 'beyazEsya'
    },
    {
        id: '2',
        image: ankastreFirin,
        content: `ANKASTRE`,
        from: 'ankastre'
    },
    {
        id: '3',
        image: vantilator,
        content: `ISITMA-SOĞUTMA`,
        from: 'isitmaSogutma'
    },
];

export const subCategoriesBeyazEsya = [
    {
        id: '1',
        image: buzdolabi,
        content: `Buzdolabı`
    },
    {
        id: '2',
        image: derinDondurucu,
        content: `Derin Dondurucu`
    },
    {
        id: '3',
        image: camasirMakinesi,
        content: `Çamaşır Makinesi`
    },
    {
        id: '4',
        image: bulasikMakinesi,
        content: `Bulaşık Makinesi`
    },
    {
        id: '5',
        image: kurutmaMakinesi,
        content: `Kurutma Makinesi`
    },
    {
        id: '6',
        image: mikrodalgaFirin,
        content: `Mikrodalga Fırın`
    },
    {
        id: '7',
        image: kurutmaliCamasirMakinesi,
        content: `Kurutmalı Çamaşır Makinesi`
    },
    {
        id: '8',
        image: firin,
        content: `Fırın`
    },
    {
        id: '9',
        image: suAritmaCihazi,
        content: `Su Arıtma Cihazı`
    },
    {
        id: '10',
        image: setUstuOcak,
        content: `Set Üstü Ocak`
    },
    {
        id: '11',
        image: aspirator,
        content: `Aspiratör`
    },
];

export const subCategoriesAnkastre = [
    {
        id: '1',
        image: ankastreFirin,
        content: `Ankastre Fırın`
    },
    {
        id: '2',
        image: ankastreMikrodalga,
        content: `Ankastre Mikrodalga`
    },
    {
        id: '3',
        image: ankastreOcak,
        content: `Ankastre Ocak`
    },
    {
        id: '4',
        image: ankastreDavlumbaz,
        content: `Ankastre Davlumbaz`
    },
    {
        id: '5',
        image: ankastreKahveMakinesi,
        content: `Ankastre Kahve Makinesi`
    },
    {
        id: '6',
        image: ankastreBulasikMakinesi,
        content: `Ankastre Bulaşık Makinesi`
    },
    {
        id: '7',
        image: ankastreBuzdolabi,
        content: `Ankastre Buzdolabı`
    },
    {
        id: '8',
        image: ankastreAspirator,
        content: `Ankastre Aspiratör`
    },
    {
        id: '9',
        image: ankastreCamasirMakinesi,
        content: `Ankastre Çamaşır Makinesi`
    },
    {
        id: '10',
        image: ankastreKurutmaliCamasirMakinesi,
        content: `Ankastre Kurutmalı Çamaşır Makinesi`
    },
    {
        id: '11',
        image: sicakTutmaCekmecesi,
        content: `Sıcak Tutma Çekmecesi`
    },
    {
        id: '12',
        image: ankastreSet,
        content: `Ankastre Set`
    },
];

export const subCategoriesIsitmaSogutma = [
    {
        id: '1',
        image: klima,
        content: `Klima`
    },
    {
        id: '2',
        image: kombi,
        content: `Kombi`
    },
    {
        id: '3',
        image: sofben,
        content: `Şofben`
    },
    {
        id: '4',
        image: termosifon,
        content: `Termosifon`
    },
    {
        id: '5',
        image: aniSuIsitici,
        content: `Ani Su Isıtıcısı`
    },
    {
        id: '6',
        image: flavelSoba,
        content: `Flavel Soba`
    },
    {
        id: '7',
        image: vantilator,
        content: `Vantilatör`
    },
    {
        id: '8',
        image: odaTermostati,
        content: `Oda Termostatı`
    },
];

const questionKurumsal = `Lütfen aradığınız ürünü kapsayan kategoriyi seçiniz.`;
const questionKurumsalBeyazEsya = `Lütfen aşağıdaki listeden aradığınız ürün grubunu seçiniz.`;
const questionKurumsalBuzdolabi1 = `Hangi tip soğutma sistemini tercih edersiniz?`;
const questionKurumsalBuzdolabi2 = `Hangi enerji sınıfından faydalanmak istersiniz?`;

//QUESTIONS
//Kurumsal
export const mainQuestionsKurumsal = [
    {
        id: '1',
        question: questionKurumsal,
        content: `Beyaz Eşya`,
        to: 'kurumsalBeyazEsya'
    },
    {
        id: '2',
        question: questionKurumsal,
        content: `Ankastre`,
        to: 'kurumsalAnkastre'
    },
    {
        id: '3',
        question: questionKurumsal,
        content: `Isıtma Soğutma`,
        to: 'kurumsalIsitmaSogutma'
    },
];

export const mainQuestionsKurumsalBeyazEsya = [
    {
        id: '1',
        question: questionKurumsalBeyazEsya,
        content: `Buzdolabı`,
        to: 'kurumsalBuzdolabi1'
    },
    {
        id: '1',
        question: questionKurumsalBeyazEsya,
        content: `Derin Dondurucu`,
        to: 'kurumsalDerinDondurucu1'
    },
];

export const kurumsalBuzdolabi1 = [
    {
        id: '1',
        question: questionKurumsalBuzdolabi1,
        content: `Gardırop Tipi Buzdolabı`,
        to: 'kurumsalBuzdolabi2'
    },
    {
        id: '2',
        question: questionKurumsalBuzdolabi1,
        content: `Tezgah Altı Buzdolabı`,
        to: 'kurumsalBuzdolabi2'
    },
    {
        id: '3',
        question: questionKurumsalBuzdolabi1,
        content: `No Frost Buzdolabı`,
        to: 'kurumsalBuzdolabi2'
    },
    {
        id: '4',
        question: questionKurumsalBuzdolabi1,
        content: `Tek Kapılı Buzdolabı`,
        to: 'kurumsalBuzdolabi2'
    },
    {
        id: '5',
        question: questionKurumsalBuzdolabi1,
        content: `Çift Kapılı Buzdolabı`,
        to: 'kurumsalBuzdolabi2'
    },
];

export const kurumsalBuzdolabi2 = [
    {
        id: '1',
        question: questionKurumsalBuzdolabi2,
        content: `A Enerji Sınıfı`,
        to: 'finalPage'
    },
    {
        id: '2',
        question: questionKurumsalBuzdolabi2,
        content: `B Enerji Sınıfı`,
        to: 'finalPage'
    },
    {
        id: '3',
        question: questionKurumsalBuzdolabi2,
        content: `C Enerji Sınıfı`,
        to: 'finalPage'
    },
    {
        id: '4',
        question: questionKurumsalBuzdolabi2,
        content: `D Enerji Sınıfı`,
        to: 'finalPage'
    },
    {
        id: '5',
        question: questionKurumsalBuzdolabi2,
        content: `E Enerji Sınıfı`,
        to: 'finalPage'
    },
];

//Bireysel
export const mainQuestionsBireysel = [
    {
        id: '1',
        content: `Beyaz Eşya bireysel`
    },
    {
        id: '2',
        content: `Ankastre`
    },
    {
        id: '3',
        content: `Isıtma Soğutma`
    },
];