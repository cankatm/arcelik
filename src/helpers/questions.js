//QUESTIONS 
//KURUMSAL
const questionKurumsal = `Lütfen aradığınız ürünü kapsayan kategoriyi seçiniz.`;
const questionKurumsalBeyazEsya = `Lütfen aşağıdaki listeden aradığınız ürün grubunu seçiniz.`;
const questionKurumsalAnkastre = `Lütfen aşağıdaki listeden aradığınız ürün grubunu seçiniz.`;
const questionKurumsalIsitmaSogutma = `Lütfen aşağıdaki listeden aradığınız ürün grubunu seçiniz.`;



//QUESTIONS 
//Kurumsal-Beyaz Eşya

const questionKurumsalBuzdolabi1 = `Hangi tip soğutma sistemini tercih edersiniz? (Karlanma yapmaması için No Frost tercih edebilirsiniz)`;
const questionKurumsalBuzdolabi2 = `Hangi enerji sınıfından faydalanmak istersiniz? (Enerji sınıfı yükseldikçe fiyat artar)`;
const questionKurumsalBuzdolabi3 = `Hangi dondurucu konumu tercihinizdir? (Dondurucu altta olursa hacmi daha büyük olur)`;
const questionKurumsalBuzdolabi4 = `Hangi renkte bir buzdolabı istersiniz? (İnox olursa fiyat artar)`;
const questionKurumsalBuzdolabi5 = `Hangi hacimde bir buzdolabı istersiniz?`;
const questionKurumsalBuzdolabi6 = `Hangi genişlikte bir buzdolabı istersiniz?`;
const questionKurumsalBuzdolabi7 = `Hangi yükseklikte bir buzdolabı istersiniz?`;
const questionKurumsalBuzdolabi8 = `Hangi derinlikte bir buzdolabı istersiniz?`;

const questionKurumsalDerinDondurucu1 = `Hangi dondurucu konumu tercihinizdir? (Yatay dondurucuların kullanım alanı daha fonksiyoneldir)`;
const questionKurumsalDerinDondurucu2 = `Hangi enerji sınıfından faydalanmak istersiniz? (Enerji sınıfı yükseldikçe fiyat artar)`;
const questionKurumsalDerinDondurucu3 = `Hangi renkte bir derin dondurucu istersiniz?`;
const questionKurumsalDerinDondurucu4 = `Hangi hacimde bir derin dondurucu istersiniz? (Sezonluk kullanım için küçük hacim tercih edebilirsiniz)`;
const questionKurumsalDerinDondurucu5 = `Hangi genişlikte bir derin dondurucu istersiniz?`;
const questionKurumsalDerinDondurucu6 = `Hangi yükseklikte bir derin dondurucu istersiniz?`;
const questionKurumsalDerinDondurucu7 = `Hangi derinlikte bir derin dondurucu istersiniz?`;

const questionKurumsalCamasirMakinesi1 = `Hangi sıkma devrini tercih edersiniz?`;
const questionKurumsalCamasirMakinesi2 = `Hangi enerji performansından faydalanmak istersiniz? (Enerji sınıfı yükseldikçe fiyat artar)`;
const questionKurumsalCamasirMakinesi3 = `Hangi renkte bir çamaşır makinesi istersiniz?`;
const questionKurumsalCamasirMakinesi4 = `Hangi kapasitede bir çamaşır makinesi istersiniz?`;

const questionKurumsalBulasikMakinesi1 = `Hangi program sayısı tercihinizdir?`;
const questionKurumsalBulasikMakinesi2 = `Hangi enerji sınıfından faydalanmak istersiniz? (Enerji sınıfı yükseldikçe fiyat artar)`;
const questionKurumsalBulasikMakinesi3 = `Hangi renkte bir bulaşık makinesi istersiniz?`;

const questionKurumsalKurutmaMakinesi1 = `Hangi kurutma teknolojisinden faydalanmak istersiniz?`;
const questionKurumsalKurutmaMakinesi2 = `Hangi enerji performansından faydalanmak istersiniz? (Enerji sınıfı yükseldikçe fiyat artar)`;
const questionKurumsalKurutmaMakinesi3 = `Hangi kapasitede bir kurutma makinesi istersiniz?`;
const questionKurumsalKurutmaMakinesi4 = `Hangi renkte bir kurutma makinesi istersiniz?`;

const questionKurumsalKurutmaliCamasirMakinesi1 = `Hangi kurutma teknolojisi tercihinizdir?`;
const questionKurumsalKurutmaliCamasirMakinesi2 = `Hangi kapasitede bir makine istersiniz?`;

const questionKurumsalFirin1 = `Hangi kategoriden bir fırın tercih edersiniz?`;
const questionKurumsalFirin2 = `Hangi ısı fonksiyon yönlü fırını tercih edersiniz?`;
const questionKurumsalFirin3 = `Hangi pişirme fonksiyon sayısını tercih edersiniz?`;
const questionKurumsalFirin4 = `Hangi kontrol tipini tercih edersiniz?`;
const questionKurumsalFirin5 = `Hangi gaz tipini tercih edersiniz?`;
const questionKurumsalFirin6 = `Hangi renkte bir fırın istersiniz?`;

const questionKurumsalSetUstuFirin1 = `Hangi hacimde pişirme bölmesi tercih edersiniz?`;
const questionKurumsalSetUstuFirin2 = `Hangi pişirme sisteminde bir set üstü fırın istersiniz?`;
const questionKurumsalSetUstuFirin3 = `Hangi renkte bir set üstü fırın istersiniz?`;

const questionKurumsalMikrodalgaFirin1 = `Hangi kontrol tipini tercih edersiniz?`;
const questionKurumsalMikrodalgaFirin2 = `Hangi renkte bir mikrodalga fırın istersiniz?`;

const questionKurumsalSetUstuOcak1 = `Hangi malzemeden ocak yüzeyi tercih edersiniz?`;
const questionKurumsalSetUstuOcak2 = `Hangi gaz tipini tercih edersiniz?`;
const questionKurumsalSetUstuOcak3 = `Hangi ocak tipini ve göz sayısını tercih edersiniz?`;
const questionKurumsalSetUstuOcak4 = `Hangi renkte bir set üstü ocak istersiniz?`;

const questionKurumsalAspirator1 = `Hangi renkte bir aspiratör istersiniz?`;

const questionKurumsalSuAritmaCihazi1 = `Hangi filtre adedini tercih edersiniz?`;
const questionKurumsalSuAritmaCihazi2 = `Hangi ek foksiyonları tercih edersiniz?`;
const questionKurumsalSuAritmaCihazi3 = `Hangi konumda yer almasını tercih edersiniz?`;


//QUESTIONS 
//Kurumsal-Ankastre

const questionKurumsalAnkastreFirin1 = `Hangi kategoriden bir fırın tercih edersiniz?`;
const questionKurumsalAnkastreFirin2 = `Hangi kontrol tipini tercih edersiniz?`;
const questionKurumsalAnkastreFirin3 = `Hangi pişirme fonksiyon sayısını tercih edersiniz? (Fonksiyon sayısı arttıkça fiyat artar)`;
const questionKurumsalAnkastreFirin4 = `Hangi renkte bir fırın istersiniz?`;

const questionKurumsalAnkastreMikrodalgaFirin1 = `Hangi kontrol tipini tercih edersiniz?`;
const questionKurumsalAnkastreMikrodalgaFirin2 = `Hangi renkte bir mikrodalga fırın istersiniz? (İnox olursa fiyat artar)`;

const questionKurumsalAnkastreOcak1 = `Hangi gaz tipini tercih edersiniz?`;
const questionKurumsalAnkastreOcak2 = `Hangi emniyet sistemini tercih edersiniz? (Gazlı ocaklarda gaz emniyet sistemi tavsiye edilir)`;
const questionKurumsalAnkastreOcak3 = `Hangi ocak tipi ve göz sayısını tercih edersiniz?`;
const questionKurumsalAnkastreOcak4 = `Hangi renkte bir ocak istersiniz?`;

const questionKurumsalAnkastreDavlumbaz1 = `Hangi tip davlumbaz tercih edersiniz?`;
const questionKurumsalAnkastreDavlumbaz2 = `Hangi genişlikte bir davlumbaz istersiniz?`;

const questionKurumsalAnkastreBulasikMakinesi1 = `Hangi program sayısı tercihinizdir?`;
const questionKurumsalAnkastreBulasikMakinesi2 = `Hangi renkte bir bulaşık makinesi istersiniz?`;

const questionKurumsalAnkastreBuzdolabi1 = `Hangi dondurucu konumu tercihinizdir? (Dondurucu altta olursa hacmi daha büyük olur)`;
const questionKurumsalAnkastreBuzdolabi2 = `Hangi hacimde bir buzdolabı istersiniz?`;
const questionKurumsalAnkastreBuzdolabi3 = `Hangi renkte bir buzdolabı istersiniz?`;

const questionKurumsalAnkastreAspirator1 = `Hangi ürün tipi tercihinizdir?`;
const questionKurumsalAnkastreAspirator2 = `Hangi genişlikte bir aspiratör tercih edersiniz?`;

const questionKurumsalAnkastreSicakTutmaCekmecesi1 = `Hangi renkte bir sıcak tutma çekmecesi istersiniz?`;

const questionKurumsalAnkastreCamasirMakinesi1 = `-`;

const questionKurumsalAnkastreKurutmaliCamasirMakinesi1 = `-`;

const questionKurumsalAnkastreKahveMakinesi1 = `-`;

const questionKurumsalAnkastreSet1 = `Hangi malzemeden bir set tercih edersiniz?`;
const questionKurumsalAnkastreSet2 = `Hangi renkte bir set tercih edersiniz?`;


//QUESTIONS 
//Kurumsal-IsitmaSogutma

const questionKurumsalKombi1 = `Hangi ısıtma kapasitesinde bir kombi tercih edersiniz?`;
const questionKurumsalKombi2 = `Hangi verimde bir cihaz istersiniz?`;
const questionKurumsalKombi3 = `Hangi cihaz tipi tercihinizdir?`;

const questionKurumsalSofben1 = `-`;

const questionKurumsalTermosifon1 = `Hangi ısıtma kapasitesinde bir termosifon tercih edersiniz?`;
const questionKurumsalTermosifon2 = `Hangi litredeki termosifon ihtiyacınıza yöneliktir?`;

const questionKurumsalAniSuIsiticisi1 = `-`;

const questionKurumsalVantilator1 = `Hangi güçte bir vantilatör tercih edersiniz?`;

const questionKurumsalOdaTermostati1 = `Hangi kontrol teknolojisi tercihinizdir?`;
const questionKurumsalOdaTermostati2 = `Hangi tipte bir oda termostatı istersiniz?`;

const questionKurumsalFlavelSoba1 = `Hangi W gücünde flavel soba tercih edersiniz?`;
const questionKurumsalFlavelSoba2 = `Hangi emniyet tipindeki flavel soba tercihinizdir?`;
const questionKurumsalFlavelSoba3 = `Hangi elektrik tüketim kapasitesi tercihinize uygundur?`;

const questionKurumsalKlima1 = `Hangi BTU'luk klima ihtiyacınıza yöneliktir?`;
const questionKurumsalKlima2 = `Hangi çalışma özellikli bir klima tercih edersiniz?`;
const questionKurumsalKlima3 = `Hangi bağlantı özellikli klimayı tercih edersiniz?`;
const questionKurumsalKlima4 = `Hangi hava tazeleyici özellikli klimayı tercih edersiniz?`;


//QUESTIONS 
//Kurumsal
export const mainQuestionsKurumsal = [
    {
        id: '1',
        question: questionKurumsal,
        content: `Beyaz Eşya`,
        to: 'kurumsalBeyazEsya'
    },
    {
        id: '2',
        question: questionKurumsal,
        content: `Ankastre`,
        to: 'kurumsalAnkastre'
    },
    {
        id: '3',
        question: questionKurumsal,
        content: `Isıtma Soğutma`,
        to: 'kurumsalIsitmaSogutma'
    },

//QUESTIONS 
//Kurumsal-Beyaz Eşya
];
export const mainQuestionsKurumsalBeyazEsya = [
    {
        id: '4',
        question: questionKurumsalBeyazEsya,
        content: `Buzdolabı`,
        value: 231,
        to: 'kurumsalBuzdolabi1'
    },
    {
        id: '5',
        question: questionKurumsalBeyazEsya,
        content: `Derin Dondurucu`,
        value: 489,
        to: 'kurumsalDerinDondurucu1'
    },
    {
        id: '6',
        question: questionKurumsalBeyazEsya,
        content: `Çamaşır Makinesi`,
        value: 200,
        to: 'kurumsalCamasirMakinesi1'
    },
    {
        id: '7',
        question: questionKurumsalBeyazEsya,
        content: `Bulaşık Makinesi`,
        value: 176,
        to: 'kurumsalBulasikMakinesi1'
    },
    {
        id: '8',
        question: questionKurumsalBeyazEsya,
        content: `Kurutma Makinesi`,
        value: 492,
        to: 'kurumsalKurutmaMakinesi1'
    },
    {
        id: '9',
        question: questionKurumsalBeyazEsya,
        content: `Kurutmalı Çamaşır Makinesi`,
        value: 493,
        to: 'kurumsalKurutmaliCamasirMakinesi1'
    },
    {
        id: '10',
        question: questionKurumsalBeyazEsya,
        content: `Fırın`,
        value: 494,
        to: 'kurumsalFirin1'
    },
    {
        id: '11',
        question: questionKurumsalBeyazEsya,
        content: `Set Üstü Fırın`,
        value: 497,
        to: 'kurumsalSetUstuFirin1'
    },
    {
        id: '12',
        question: questionKurumsalBeyazEsya,
        content: `Mikrodalga Fırın`,
        value: 498,
        to: 'kurumsalMikrodalgaFirin1'
    },
    {
        id: '13',
        question: questionKurumsalBeyazEsya,
        content: `Set Üstü Ocak`,
        value: 499,
        to: 'kurumsalSetUstuOcak1'
    },
    {
        id: '14',
        question: questionKurumsalBeyazEsya,
        content: `Aspiratör`,
        value: 500,
        to: 'kurumsalAspirator1'
    },
    {
        id: '15',
        question: questionKurumsalBeyazEsya,
        content: `Su Arıtma Cihazı`,
        value: 501,
        to: 'kurumsalSuAritmaCihazi1'
    },

//QUESTIONS 
//Kurumsal-Ankastre  
];
export const mainQuestionsKurumsalAnkastre = [
    {
        id: '16',
        question: questionKurumsalAnkastre,
        content: `Ankastre Fırın`,
        value: 340,
        to: 'kurumsalAnkastreFirin1'
    },
    {
        id: '17',
        question: questionKurumsalAnkastre,
        content: `Ankastre Mikrodalga Fırın`,
        value: 473,
        to: 'kurumsalAnkastreMikrodalgaFirin1'
    },
    {
        id: '18',
        question: questionKurumsalAnkastre,
        content: `Ankastre Ocak`,
        value: 389,
        to: 'kurumsalAnkastreOcak1'
    },
    {
        id: '19',
        question: questionKurumsalAnkastre,
        content: `Ankastre Davlumbaz`,
        value: 400,
        to: 'kurumsalAnkastreDavlumbaz1'
    },
    {
        id: '20',
        question: questionKurumsalAnkastre,
        content: `Ankastre Bulaşık Makinesi`,
        value: 392,
        to: 'kurumsalAnkastreBulasikMakinesi1'
    },
    {
        id: '21',
        question: questionKurumsalAnkastre,
        content: `Ankastre Buzdolabı`,
        value: 323,
        to: 'kurumsalAnkastreBuzdolabi1'
    },
    {
        id: '22',
        question: questionKurumsalAnkastre,
        content: `Ankastre Aspiratör`,
        value: 477,
        to: 'kurumsalAnkastreAspirator1'
    },
    {
        id: '23',
        question: questionKurumsalAnkastre,
        content: `Ankastre Sıcak Tutma Makinesi`,
        value: 471,
        to: 'kurumsalAnkastreSicakTutmaMakinesi1'
    },
    {
        id: '24',
        question: questionKurumsalAnkastre,
        content: `Ankastre Çamaşır Makinesi`,
        value: 469,
        to: 'kurumsalAnkastreCamasirMakinesi1'
    },
    {
        id: '25',
        question: questionKurumsalAnkastre,
        content: `Ankastre Kurutmalı Çamaşır Makinesi`,
        value: 470,
        to: 'kurumsalAnkastreKurutmaliCamasirMakinesi1'
    },
    {
        id: '26',
        question: questionKurumsalAnkastre,
        content: `Ankastre Kahve Makinesi`,
        value: 414,
        to: 'kurumsalAnkastreKahveMakinesi1'
    },
    {
        id: '27',
        question: questionKurumsalAnkastre,
        content: `Ankastre Set`,
        to: 'kurumsalAnkastreSet1'
    },


//QUESTIONS 
//Kurumsal-Isıtma Soğutma  
];
export const mainQuestionsKurumsalIsitmaSogutma = [
    {
        id: '28',
        question: questionKurumsalIsitmaSogutma,
        content: `Kombi`,
        value: 89,
        to: 'kurumsalKombi1'
    },
    {
        id: '29',
        question: questionKurumsalIsitmaSogutma,
        content: `Şofben`,
        value: 109,
        to: 'kurumsalSofben1'
    },
    {
        id: '30',
        question: questionKurumsalIsitmaSogutma,
        content: `Termosifon`,
        value: 114,
        to: 'kurumsalTermosifon1'
    },
    {
        id: '31',
        question: questionKurumsalIsitmaSogutma,
        content: `Ani Su Isıtıcısı`,
        value: 128,
        to: 'kurumsalAniSuIsiticisi1'
    },
    {
        id: '32',
        question: questionKurumsalIsitmaSogutma,
        content: `Vantilatör`,
        value: 422,
        to: 'kurumsalVantilator1'
    },
    {
        id: '33',
        question: questionKurumsalIsitmaSogutma,
        content: `Oda Termostatı`,
        value: 152,
        to: 'kurumsalOdaTermostati1'
    },
    {
        id: '34',
        question: questionKurumsalIsitmaSogutma,
        content: `Flavel Soba`,
        value: 133,
        to: 'kurumsalFlavelSoba1'
    },
    {
        id: '35',
        question: questionKurumsalIsitmaSogutma,
        content: `Klima`,
        value: 15,
        to: 'kurumsalKlima1'
    },

//QUESTIONS - BEYAZ EŞYA
//Kurumsal-Buzdolabı
];

var kurumsalBuzdolabi1Value = 'Soğutma Sistemi'
export const kurumsalBuzdolabi1 = [
    {
        id: '36',
        question: questionKurumsalBuzdolabi1,
        content: `No Frost`,
        value: kurumsalBuzdolabi1Value,
        to: 'kurumsalBuzdolabi2'
    },
    {
        id: '37',
        question: questionKurumsalBuzdolabi1,
        content: `Statik`,
        value: kurumsalBuzdolabi1Value,
        to: 'kurumsalBuzdolabi2'
    },
    {
        id: '38',
        question: questionKurumsalBuzdolabi1,
        content: `Yarı No Frost`,
        value: kurumsalBuzdolabi1Value,
        to: 'kurumsalBuzdolabi2'
    },
    {
        id: '39',
        question: questionKurumsalBuzdolabi1,
        content: `Bu soruyu atlamak istiyorum`,
        value: null,
        to: 'kurumsalBuzdolabi2'
    },
];

var kurumsalBuzdolabi2Value = `Enerji Sınıfı`
export const kurumsalBuzdolabi2 = [
    {
        id: '40',
        question: questionKurumsalBuzdolabi2,
        content: `A+++`,
        value: kurumsalBuzdolabi2Value,
        to: 'kurumsalBuzdolabi3'
    },
    {
        id: '41',
        question: questionKurumsalBuzdolabi2,
        content: `A++`,
        value: kurumsalBuzdolabi2Value,
        to: 'kurumsalBuzdolabi3'
    },
    {
        id: '42',
        question: questionKurumsalBuzdolabi2,
        content: `A+`,
        value: kurumsalBuzdolabi2Value,
        to: 'kurumsalBuzdolabi3'
    },
    {
        id: '43',
        question: questionKurumsalBuzdolabi2,
        content: `Bu soruyu atlamak istiyorum`,
        value: null,
        to: 'kurumsalBuzdolabi3'
    },
];
var kurumsalBuzdolabi3Value = `Dondurucu Yeri`
export const kurumsalBuzdolabi3 = [
    {
        id: '44',
        question: questionKurumsalBuzdolabi3,
        content: `Dondurucu Üstte`,
        value: kurumsalBuzdolabi3Value,
        to: 'kurumsalBuzdolabi4'
    },
    {
        id: '45',
        question: questionKurumsalBuzdolabi3,
        content: `Dondurucu Altta`,
        value: kurumsalBuzdolabi3Value,
        to: 'kurumsalBuzdolabi4'
    },
    {
        id: '46',
        question: questionKurumsalBuzdolabi3,
        content: `Dondurucu Solda`,
        value: kurumsalBuzdolabi3Value,
        to: 'kurumsalBuzdolabi4'
    },
    {
        id: '47',
        question: questionKurumsalBuzdolabi3,
        content: `Bu soruyu atlamak istiyorum`,
        value: null,
        to: 'kurumsalBuzdolabi4'
    },
];
var kurumsalBuzdolabi4Value = `Ürün Rengi`
export const kurumsalBuzdolabi4 = [
    {
        id: '48',
        question: questionKurumsalBuzdolabi4,
        content: `Beyaz`,
        value: kurumsalBuzdolabi4Value,
        to: 'kurumsalBuzdolabi5'
    },
    {
        id: '49',
        question: questionKurumsalBuzdolabi4,
        content: `Beyaz Cam`,
        value: kurumsalBuzdolabi4Value,
        to: 'kurumsalBuzdolabi5'
    },
    {
        id: '50',
        question: questionKurumsalBuzdolabi4,
        content: `Beyaz Deri Desenli`,
        value: kurumsalBuzdolabi4Value,
        to: 'kurumsalBuzdolabi5'
    },
    {
        id: '51',
        question: questionKurumsalBuzdolabi4,
        content: `Gümüş`,
        value: kurumsalBuzdolabi4Value,
        to: 'kurumsalBuzdolabi5'
    },
    {
        id: '52',
        question: questionKurumsalBuzdolabi4,
        content: `Leke Tutmayan İnoks`,
        value: kurumsalBuzdolabi4Value,
        to: 'kurumsalBuzdolabi5'
    },
    {
        id: '53',
        question: questionKurumsalBuzdolabi4,
        content: `Siyah Cam`,
        value: kurumsalBuzdolabi4Value,
        to: 'kurumsalBuzdolabi5'
    },
    {
        id: '54',
        question: questionKurumsalBuzdolabi4,
        content: `Bu soruyu atlamak istiyorum`,
        value: null,
        to: 'kurumsalBuzdolabi5'
    },
];

var kurumsalBuzdolabi5Value = `Toplam Brüt Hacim (Buzdolabı)`
export const kurumsalBuzdolabi5 = [
    {
        id: '55',
        question: questionKurumsalBuzdolabi5,
        content: `90 L - 500 L arasında`,
        value: kurumsalBuzdolabi5Value,
        to: 'kurumsalBuzdolabi6'
    },
    {
        id: '56',
        question: questionKurumsalBuzdolabi5,
        content: `501 L - 750 L arasında`,
        value: kurumsalBuzdolabi5Value,
        to: 'kurumsalBuzdolabi6'
    },
    {
        id: '57',
        question: questionKurumsalBuzdolabi5,
        content: `Bu soruyu atlamak istiyorum`,
        value: kurumsalBuzdolabi5Value,
        value: null,
        to: 'kurumsalBuzdolabi6'
    },
];
var kurumsalBuzdolabi6Value = `Genişlik (Buzdolabı)`       
export const kurumsalBuzdolabi6 = [
    {
        id: '58',
        question: questionKurumsalBuzdolabi6,
        content: `45 cm - 69 cm arasında`,
        value: kurumsalBuzdolabi6Value,
        to: 'kurumsalBuzdolabi7'
    },
    {
        id: '59',
        question: questionKurumsalBuzdolabi6,
        content: `70 cm - 95 cm arasında`,
        value: kurumsalBuzdolabi6Value,
        to: 'kurumsalBuzdolabi7'
    },
    {
        id: '60',
        question: questionKurumsalBuzdolabi6,
        content: `Bu soruyu atlamak istiyorum`,
        value: null,
        to: 'kurumsalBuzdolabi7'
    },
];
var kurumsalBuzdolabi7Value = `Yükseklik (Buzdolabı)`      
export const kurumsalBuzdolabi7 = [
    {
        id: '61',
        question: questionKurumsalBuzdolabi7,
        content: `80 cm - 175 cm arasında`,
        value: kurumsalBuzdolabi7Value,
        to: 'kurumsalBuzdolabi8'
    },
    {
        id: '62',
        question: questionKurumsalBuzdolabi7,
        content: `176 cm - 195 cm arasında`,
        value: kurumsalBuzdolabi7Value,
        to: 'kurumsalBuzdolabi8'
    },
    {
        id: '63',
        question: questionKurumsalBuzdolabi7,
        content: `Bu soruyu atlamak istiyorum`,
        value: null,
        to: 'kurumsalBuzdolabi8'
    },
];    
var kurumsalBuzdolabi8Value = `Derinlik (Buzdolabı)`      
export const kurumsalBuzdolabi8 = [
    {
        id: '64',
        question: questionKurumsalBuzdolabi8,
        content: `53 cm - 69 cm arasında`,
        value: kurumsalBuzdolabi8Value,
        to: 'finalPage'
    },
    {
        id: '65',
        question: questionKurumsalBuzdolabi8,
        content: `70 cm - 81 cm arasında`,
        value: kurumsalBuzdolabi8Value,
        to: 'finalPage'
    },
    {
        id: '66',
        question: questionKurumsalBuzdolabi8,
        content: `Bu soruyu atlamak istiyorum`,
        value: null,
        to: 'finalPage'
    },

//QUESTIONS 
//Kurumsal-Derin Dondurucu
];
var kurumsalDerinDondurucu1Value = `Dondurucu Yeri`      
export const kurumsalDerinDondurucu1 = [
    {
        id: '67',
        question: questionKurumsalDerinDondurucu1,
        content: `Dikey Dondurucu`,
        value: kurumsalDerinDondurucu1Value,
        to: 'kurumsalDerinDondurucu2'
    },
    {
        id: '68',
        question: questionKurumsalDerinDondurucu1,
        content: `Yatay Dondurucu`,
        value: kurumsalDerinDondurucu1Value,
        to: 'kurumsalDerinDondurucu2'
    },
    {
        id: '69',
        question: questionKurumsalDerinDondurucu1,
        content: `Joker Dondurucu`,
        value: kurumsalDerinDondurucu1Value,
        to: 'kurumsalDerinDondurucu2'
    },
    {
        id: '70',
        question: questionKurumsalDerinDondurucu1,
        content: `Bu soruyu atlamak istiyorum`,
        value: null,
        to: 'kurumsalDerinDondurucu2'
    },
];
var kurumsalDerinDondurucu2Value = `Enerji Sınıfı`
export const kurumsalDerinDondurucu2 = [
    {
        id: '71',
        question: questionKurumsalDerinDondurucu2,
        content: `A+++`,
        value: kurumsalDerinDondurucu2Value,
        to: 'kurumsalDerinDondurucu3'
    },
    {
        id: '72',
        question: questionKurumsalDerinDondurucu2,
        content: `A++`,
        value: kurumsalDerinDondurucu2Value,
        to: 'kurumsalDerinDondurucu3'
    },
    {
        id: '73',
        question: questionKurumsalDerinDondurucu2,
        content: `A+`,
        value: kurumsalDerinDondurucu2Value,
        to: 'kurumsalDerinDondurucu3'
    },
    {
        id: '74',
        question: questionKurumsalDerinDondurucu2,
        content: `Bu soruyu atlamak istiyorum`,
        value: null,
        to: 'kurumsalDerinDondurucu3'
    },
];
var kurumsalDerinDondurucu3Value = `Ürün Rengi`
export const kurumsalDerinDondurucu3 = [
    {
        id: '75',
        question: questionKurumsalDerinDondurucu3,
        content: `Beyaz`,
        value: kurumsalDerinDondurucu3Value,
        to: 'kurumsalDerinDondurucu4'
    },
    {
        id: '76',
        question: questionKurumsalDerinDondurucu3,
        content: `Gümüş`,
        value: kurumsalDerinDondurucu3Value,
        to: 'kurumsalDerinDondurucu4'
    },
    {
        id: '77',
        question: questionKurumsalDerinDondurucu3,
        content: `Leke Tutmayan İnoks`,
        value: kurumsalDerinDondurucu3Value,
        to: 'kurumsalDerinDondurucu4'
    },
    {
        id: '78',
        question: questionKurumsalDerinDondurucu3,
        content: `Bu soruyu atlamak istiyorum`,
        value: null,
        to: 'kurumsalDerinDondurucu4'
    },
];
var kurumsalDerinDondurucu4Value = `Toplam Brüt Hacim (Derin Dondurucu)`
export const kurumsalDerinDondurucu4 = [
    {
        id: '79',
        question: questionKurumsalDerinDondurucu4,
        content: `100 L – 250 L arasında`,
        value: kurumsalDerinDondurucu4Value,
        to: 'kurumsalDerinDondurucu5'
    },
    {
        id: '80',
        question: questionKurumsalDerinDondurucu4,
        content: `251 L – 750 L arasında`,
        value: kurumsalDerinDondurucu4Value,
        to: 'kurumsalDerinDondurucu5'
    },
    {
        id: '81',
        question: questionKurumsalDerinDondurucu4,
        content: `Bu soruyu atlamak istiyorum`,
        value: null,
        to: 'kurumsalDerinDondurucu5'
    },
];
var kurumsalDerinDondurucu5Value = `Genişlik (Derin Dondurucu)`
export const kurumsalDerinDondurucu5 = [
    {
        id: '82',
        question: questionKurumsalDerinDondurucu5,
        content: `50 cm - 74 cm arasında`,
        value: kurumsalDerinDondurucu5Value,
        to: 'kurumsalDerinDondurucu6'
    },
    {
        id: '83',
        question: questionKurumsalDerinDondurucu5,
        content: `75 cm - 160 cm arasında`,
        value: kurumsalDerinDondurucu5Value,
        to: 'kurumsalDerinDondurucu6'
    },
    {
        id: '84',
        question: questionKurumsalDerinDondurucu5,
        content: `Bu soruyu atlamak istiyorum`,
        value: null,
        to: 'kurumsalDerinDondurucu6'
    },
];
var kurumsalDerinDondurucu6Value = `Yükseklik (Derin Dondurucu)`
export const kurumsalDerinDondurucu6 = [
    {
        id: '85',
        question: questionKurumsalDerinDondurucu6,
        content: `75 cm - 149 cm arasında`,
        value: kurumsalDerinDondurucu6Value,
        to: 'kurumsalDerinDondurucu7'
    },
    {
        id: '86',
        question: questionKurumsalDerinDondurucu6,
        content: `150 cm - 200 cm arasında`,
        value: kurumsalDerinDondurucu6Value,
        to: 'kurumsalDerinDondurucu7'
    },
    {
        id: '87',
        question: questionKurumsalDerinDondurucu6,
        content: `Bu soruyu atlamak istiyorum`,
        value: null,
        to: 'kurumsalDerinDondurucu7'
    },
];
var kurumsalDerinDondurucu7Value = `Derinlik (Derin Dondurucu)`
export const kurumsalDerinDondurucu7 = [
    {
        id: '88',
        question: questionKurumsalDerinDondurucu7,
        content: `50 cm - 64 cm arasında`,
        value: kurumsalDerinDondurucu7Value,
        to: 'finalPage'
    },
    {
        id: '89',
        question: questionKurumsalDerinDondurucu7,
        content: `65 cm - 80 cm arasında`,
        value: kurumsalDerinDondurucu7Value,
        to: 'finalPage'
    },
    {
        id: '90',
        question: questionKurumsalDerinDondurucu7,
        content: `Bu soruyu atlamak istiyorum`,
        value: null,
        to: 'finalPage'
    },

//QUESTIONS 
//Kurumsal-Çamaşır Makinesi
];
var kurumsalCamasirMakinesi1Value = `Maksimum Sıkma Devri`
export const kurumsalCamasirMakinesi1 = [
    {
        id: '91',
        question: questionKurumsalCamasirMakinesi1,
        content: `600 d/dk`,
        value: kurumsalCamasirMakinesi1Value,
        to: 'kurumsalCamasirMakinesi2'
    },
    {
        id: '92',
        question: questionKurumsalCamasirMakinesi1,
        content: `800 d/dk`,
        value: kurumsalCamasirMakinesi1Value,
        to: 'kurumsalCamasirMakinesi2'
    }, 
    {
        id: '93',
        question: questionKurumsalCamasirMakinesi1,
        content: `1000 d/dk`,
        value: kurumsalCamasirMakinesi1Value,
        to: 'kurumsalCamasirMakinesi2'
    },
    {
        id: '94',
        question: questionKurumsalCamasirMakinesi1,
        content: `1200 d/dk`,
        value: kurumsalCamasirMakinesi1Value,
        to: 'kurumsalCamasirMakinesi2'
    },
    {
        id: '95',
        question: questionKurumsalCamasirMakinesi1,
        content: `1400 d/dk`,
        value: kurumsalCamasirMakinesi1Value,
        to: 'kurumsalCamasirMakinesi2'
    },
    {
        id: '96',
        question: questionKurumsalCamasirMakinesi1,
        content: `Bu soruyu atlamak istiyorum`,
        value: null,
        to: 'kurumsalCamasirMakinesi2'
    },
];
var kurumsalCamasirMakinesi2Value = `Enerji Sınıfı`
export const kurumsalCamasirMakinesi2 = [
    {
        id: '97',
        question: questionKurumsalCamasirMakinesi2,
        content: `A+++`,
        value: kurumsalCamasirMakinesi2Value,
        to: 'kurumsalCamasirMakinesi3'
    },
    {
        id: '98',
        question: questionKurumsalCamasirMakinesi2,
        content: `A++`,
        value: kurumsalCamasirMakinesi2Value,
        to: 'kurumsalCamasirMakinesi3'
    },
    {
        id: '99',
        question: questionKurumsalCamasirMakinesi2,
        content: `A+`,
        value: kurumsalCamasirMakinesi2Value,
        to: 'kurumsalCamasirMakinesi3'
    },
    {
        id: '100',
        question: questionKurumsalCamasirMakinesi2,
        content: `Bu soruyu atlamak istiyorum`,
        value: null,
        to: 'kurumsalCamasirMakinesi3'
    },
];
var kurumsalCamasirMakinesi3Value = `Ürün Rengi`
export const kurumsalCamasirMakinesi3 = [
    {
        id: '101',
        question: questionKurumsalCamasirMakinesi3,
        content: `Antrasit`,
        value: kurumsalCamasirMakinesi3Value,
        to: 'kurumsalCamasirMakinesi4'
    },
    {
        id: '102',
        question: questionKurumsalCamasirMakinesi3,
        content: `Beyaz`,
        value: kurumsalCamasirMakinesi3Value,
        to: 'kurumsalCamasirMakinesi4'
    },
    {
        id: '103',
        question: questionKurumsalCamasirMakinesi3,
        content: `Bordo`,
        value: kurumsalCamasirMakinesi3Value,
        to: 'kurumsalCamasirMakinesi4'
    },
    {
        id: '104',
        question: questionKurumsalCamasirMakinesi3,
        content: `Gri`,
        value: kurumsalCamasirMakinesi3Value,
        to: 'kurumsalCamasirMakinesi4'
    },
    {
        id: '105',
        question: questionKurumsalCamasirMakinesi3,
        content: `Siyah`,
        value: kurumsalCamasirMakinesi3Value,
        to: 'kurumsalCamasirMakinesi4'
    },
    {
        id: '106',
        question: questionKurumsalCamasirMakinesi3,
        content: `Bu soruyu atlamak istiyorum`,
        value: null,
        to: 'kurumsalCamasirMakinesi4'
    },
];
var kurumsalCamasirMakinesi4Value = `Kapasite`
export const kurumsalCamasirMakinesi4 = [
    {
        id: '107',
        question: questionKurumsalCamasirMakinesi4,
        content: `5 Kg`,
        value: kurumsalCamasirMakinesi4Value,
        to: 'finalPage'
    },
    {
        id: '108',
        question: questionKurumsalCamasirMakinesi4,
        content: `6 Kg`,
        value: kurumsalCamasirMakinesi4Value,
        to: 'finalPage'
    },
    {
        id: '109',
        question: questionKurumsalCamasirMakinesi4,
        content: `7 Kg`,
        value: kurumsalCamasirMakinesi4Value,
        to: 'finalPage'
    },
    {
        id: '110',
        question: questionKurumsalCamasirMakinesi4,
        content: `8 Kg`,
        value: kurumsalCamasirMakinesi4Value,
        to: 'finalPage'
    },
    {
        id: '111',
        question: questionKurumsalCamasirMakinesi4,
        content: `9 Kg`,
        value: kurumsalCamasirMakinesi4Value,
        to: 'finalPage'
    },
    {
        id: '112',
        question: questionKurumsalCamasirMakinesi4,
        content: `10 Kg`,
        value: kurumsalCamasirMakinesi4Value,
        to: 'finalPage'
    },
    {
        id: '113',
        question: questionKurumsalCamasirMakinesi4,
        content: `12 Kg`,
        value: kurumsalCamasirMakinesi4Value,
        to: 'finalPage'
    },
    {
        id: '114',
        question: questionKurumsalCamasirMakinesi4,
        content: `Bu soruyu atlamak istiyorum`,
        value: null,
        to: 'finalPage'
    },    

//QUESTIONS 
//Kurumsal-Bulaşık Makinesi
];
var kurumsalBulasikMakinesi1Value = `Program Sayısı`
export const kurumsalBulasikMakinesi1 = [
    {
        id: '115',
        question: questionKurumsalBulasikMakinesi1,
        content: `2`,
        value: kurumsalBulasikMakinesi1Value,
        to: 'kurumsalBulasikMakinesi2'
    },
    {
        id: '116',
        question: questionKurumsalBulasikMakinesi1,
        content: `3`,
        value: kurumsalBulasikMakinesi1Value,
        to: 'kurumsalBulasikMakinesi2'
    },
    {
        id: '117',
        question: questionKurumsalBulasikMakinesi1,
        content: `4`,
        value: kurumsalBulasikMakinesi1Value,
        to: 'kurumsalBulasikMakinesi2'
    },
    {
        id: '118',
        question: questionKurumsalBulasikMakinesi1,
        content: `5`,
        value: kurumsalBulasikMakinesi1Value,
        to: 'kurumsalBulasikMakinesi2'
    },
    {
        id: '119',
        question: questionKurumsalBulasikMakinesi1,
        content: `6`,
        value: kurumsalBulasikMakinesi1Value,
        to: 'kurumsalBulasikMakinesi2'
    },
    {
        id: '120',
        question: questionKurumsalBulasikMakinesi1,
        content: `8`,
        value: kurumsalBulasikMakinesi1Value,
        to: 'kurumsalBulasikMakinesi2'
    },
    {
        id: '121',
        question: questionKurumsalBulasikMakinesi1,
        content: `10`,
        value: kurumsalBulasikMakinesi1Value,
        to: 'kurumsalBulasikMakinesi2'
    },
    {
        id: '122',
        question: questionKurumsalBulasikMakinesi1,
        content: `Bu soruyu atlamak istiyorum`,
        value: null,
        to: 'kurumsalBulasikMakinesi2'
    },
];
var kurumsalBulasikMakinesi2Value = `Enerji Sınıfı`
export const kurumsalBulasikMakinesi2 = [
    {
        id: '123',
        question: questionKurumsalBulasikMakinesi2,
        content: `A+++`,
        value: kurumsalBulasikMakinesi2Value,
        to: 'kurumsalBulasikMakinesi3'
    },
    {
        id: '124',
        question: questionKurumsalBulasikMakinesi2,
        content: `A++`,
        value: kurumsalBulasikMakinesi2Value,
        to: 'kurumsalBulasikMakinesi3'
    },
    {
        id: '125',
        question: questionKurumsalBulasikMakinesi2,
        content: `A+`,
        value: kurumsalBulasikMakinesi2Value,
        to: 'kurumsalBulasikMakinesi3'
    },
    {
        id: '126',
        question: questionKurumsalBulasikMakinesi2,
        content: `Bu soruyu atlamak istiyorum`,
        value: null,
        to: 'kurumsalBulasikMakinesi3'
    },
];
var kurumsalBulasikMakinesi3Value = `Ürün Rengi`
export const kurumsalBulasikMakinesi3 = [
    {
        id: '127',
        question: questionKurumsalBulasikMakinesi3,
        content: `Beyaz`,
        value: kurumsalBulasikMakinesi3Value,
        to: 'finalPage'
    },
    {
        id: '128',
        question: questionKurumsalBulasikMakinesi3,
        content: `Beyaz Cam`,
        value: kurumsalBulasikMakinesi3Value,
        to: 'finalPage'
    },
    {
        id: '129',
        question: questionKurumsalBulasikMakinesi3,
        content: `Gümüş`,
        value: kurumsalBulasikMakinesi3Value,
        to: 'finalPage'
    },
    {
        id: '130',
        question: questionKurumsalBulasikMakinesi3,
        content: `Siyah Cam`,
        value: kurumsalBulasikMakinesi3Value,
        to: 'finalPage'
    },
    {
        id: '131',
        question: questionKurumsalBulasikMakinesi3,
        content: `İnoks - Parmak İzi Bırakmayan`,
        value: kurumsalBulasikMakinesi3Value,
        to: 'finalPage'
    },
    {
        id: '132',
        question: questionKurumsalBulasikMakinesi3,
        content: `Bu soruyu atlamak istiyorum`,
        value: null,
        to: 'finalPage'
    },

//QUESTIONS 
//Kurumsal-Kurutma Makinesi
];
var kurumsalKurutmaMakinesi1Value = `Kurutma Teknolojisi`
export const kurumsalKurutmaMakinesi1 = [
    {
        id: '133',
        question: questionKurumsalKurutmaMakinesi1,
        content: `Hibrit`,
        value: kurumsalKurutmaMakinesi1Value,
        to: 'kurumsalKurutmaMakinesi2'
    },
    {
        id: '134',
        question: questionKurumsalKurutmaMakinesi1,
        content: `Yoğuşturmalı`,
        value: kurumsalKurutmaMakinesi1Value,
        to: 'kurumsalKurutmaMakinesi2'
    },
    {
        id: '135',
        question: questionKurumsalKurutmaMakinesi1,
        content: `Yoğuşturmalı (Isı Pompalı)`,
        value: kurumsalKurutmaMakinesi1Value,
        to: 'kurumsalKurutmaMakinesi2'
    },
    {
        id: '136',
        question: questionKurumsalKurutmaMakinesi1,
        content: `Bu soruyu atlamak istiyorum`,
        value: null,
        to: 'kurumsalKurutmaMakinesi2'
    },
];
var kurumsalKurutmaMakinesi2Value = `Enerji Sınıfı`
export const kurumsalKurutmaMakinesi2 = [
    {
        id: '137',
        question: questionKurumsalKurutmaMakinesi2,
        content: `A+++`,
        value: kurumsalKurutmaMakinesi2Value,
        to: 'kurumsalKurutmaMakinesi3'
    },
    {
        id: '138',
        question: questionKurumsalKurutmaMakinesi2,
        content: `A++`,
        value: kurumsalKurutmaMakinesi2Value,
        to: 'kurumsalKurutmaMakinesi3'
    },
    {
        id: '139',
        question: questionKurumsalKurutmaMakinesi2,
        content: `A+`,
        value: kurumsalKurutmaMakinesi2Value,
        to: 'kurumsalKurutmaMakinesi3'
    },
    {
        id: '140',
        question: questionKurumsalKurutmaMakinesi2,
        content: `B`,
        value: kurumsalKurutmaMakinesi2Value,
        to: 'kurumsalKurutmaMakinesi3'
    },
    {
        id: '141',
        question: questionKurumsalKurutmaMakinesi2,
        content: `Bu soruyu atlamak istiyorum`,
        value: null,
        to: 'kurumsalKurutmaMakinesi3'
    },
];
var kurumsalKurutmaMakinesi3Value = `Maksimum Kurutma Kapasitesi`
export const kurumsalKurutmaMakinesi3 = [
    {
        id: '142',
        question: questionKurumsalKurutmaMakinesi3,
        content: `7 kg`,
        value: kurumsalKurutmaMakinesi3Value,
        to: 'kurumsalKurutmaMakinesi4'
    },
    {
        id: '143',
        question: questionKurumsalKurutmaMakinesi3,
        content: `8 kg`,
        value: kurumsalKurutmaMakinesi3Value,
        to: 'kurumsalKurutmaMakinesi4'
    },
    {
        id: '144',
        question: questionKurumsalKurutmaMakinesi3,
        content: `9 kg`,
        value: kurumsalKurutmaMakinesi3Value,
        to: 'kurumsalKurutmaMakinesi4'
    },
    {
        id: '145',
        question: questionKurumsalKurutmaMakinesi3,
        content: `Bu soruyu atlamak istiyorum`,
        value: null,
        to: 'kurumsalKurutmaMakinesi4'
    },
];
var kurumsalKurutmaMakinesi4Value = `Ürün Rengi`
export const kurumsalKurutmaMakinesi4 = [
    {
        id: '146',
        question: questionKurumsalKurutmaMakinesi4,
        content: `Beyaz`,
        value: kurumsalKurutmaMakinesi4Value,
        to: 'finalPage'
    },
    {
        id: '147',
        question: questionKurumsalKurutmaMakinesi4,
        content: `Bordo`,
        value: kurumsalKurutmaMakinesi4Value,
        to: 'finalPage'
    },
    {
        id: '148',
        question: questionKurumsalKurutmaMakinesi4,
        content: `Gri`,
        value: kurumsalKurutmaMakinesi4Value,
        to: 'finalPage'
    },
    {
        id: '149',
        question: questionKurumsalKurutmaMakinesi4,
        content: `Bu soruyu atlamak istiyorum`,
        value: null,
        to: 'finalPage'
    },

//QUESTIONS 
//Kurumsal-Kurutmalı Çamaşır Makinesi
];
var kurumsalKurutmaliCamasirMakinesi1Value = `Kurutma Teknolojisi`
export const kurumsalKurutmaliCamasirMakinesi1 = [
    {
        id: '150',
        question: questionKurumsalKurutmaliCamasirMakinesi1,
        content: `Hava Yoğuşturmalı`,
        value: kurumsalKurutmaliCamasirMakinesi1Value,
        to: 'kurumsalKurutmaliCamasirMakinesi2'
    },
    {
        id: '151',
        question: questionKurumsalKurutmaliCamasirMakinesi1,
        content: `Su Yoğuşturmalı`,
        value: kurumsalKurutmaliCamasirMakinesi1Value,
        to: 'kurumsalKurutmaliCamasirMakinesi2'
    },
    {
        id: '152',
        question: questionKurumsalKurutmaliCamasirMakinesi1,
        content: `Bu soruyu atlamak istiyorum`,
        value: null,
        to: 'kurumsalKurutmaliCamasirMakinesi2'
    },
];
var kurumsalKurutmaliCamasirMakinesi2Value = `Kapasite`
export const kurumsalKurutmaliCamasirMakinesi2 = [
    {
        id: '153',
        question: questionKurumsalKurutmaliCamasirMakinesi2,
        content: `8 kg`,
        value: kurumsalKurutmaliCamasirMakinesi2Value,
        to: 'finalPage'
    },
    {
        id: '154',
        question: questionKurumsalKurutmaliCamasirMakinesi2,
        content: `9 kg`,
        value: kurumsalKurutmaliCamasirMakinesi2Value,
        to: 'finalPage'
    },
    {
        id: '155',
        question: questionKurumsalKurutmaliCamasirMakinesi2,
        content: `Bu soruyu atlamak istiyorum`,
        value: null,
        to: 'finalPage'
    },

//QUESTIONS 
//Kurumsal-Fırın
];
var kurumsalFirin1Value = `Fırın Kategorisi`
export const kurumsalFirin1 = [
    {
        id: '156',
        question: questionKurumsalFirin1,
        content: `Ocaklı Fırın`,
        value: kurumsalFirin1Value,
        to: 'kurumsalFirin2'
    },
    {
        id: '157',
        question: questionKurumsalFirin1,
        content: `Set Üstü Fırın`,
        value: kurumsalFirin1Value,
        to: 'kurumsalFirin2'
    },
    {
        id: '158',
        question: questionKurumsalFirin1,
        content: `Kuzine Fırın`,
        value: kurumsalFirin1Value,
        to: 'kurumsalFirin2'
    },
    {
        id: '159',
        question: questionKurumsalFirin1,
        content: `Bu soruyu atlamak istiyorum`,
        value: null,
        to: 'kurumsalFirin2'
    },
];
var kurumsalFirin2Value = `Fırın Tipi`
export const kurumsalFirin2 = [
    {
        id: '160',
        question: questionKurumsalFirin2,
        content: `Multifonksiyon Fırın`,
        value: kurumsalFirin2Value,
        to: 'kurumsalFirin3'
    },
    {
        id: '161',
        question: questionKurumsalFirin2,
        content: `Turbo Fırın`,
        value: kurumsalFirin2Value,
        to: 'kurumsalFirin3'
    },
    {
        id: '162',
        question: questionKurumsalFirin2,
        content: `Bu soruyu atlamak istiyorum`,
        value: null,
        to: 'kurumsalFirin3'
    },
];
var kurumsalFirin3Value = `Pişirme Fonksiyon Sayısı`
export const kurumsalFirin3 = [
    {
        id: '163',
        question: questionKurumsalFirin3,
        content: `7`,
        value: kurumsalFirin3Value,
        to: 'kurumsalFirin4'
    },
    {
        id: '164',
        question: questionKurumsalFirin3,
        content: `8`,
        value: kurumsalFirin3Value,
        to: 'kurumsalFirin4'
    },
    {
        id: '165',
        question: questionKurumsalFirin3,
        content: `Bu soruyu atlamak istiyorum `,
        value: null,
        to: 'kurumsalFirin4'
    },  
];
var kurumsalFirin4Value = `Saat Tipi`
export const kurumsalFirin4 = [
    {
        id: '166',
        question: questionKurumsalFirin4,
        content: `LED Ekran - Buton Kontrol`,
        value: kurumsalFirin4Value,
        to: 'kurumsalFirin5'
    },
    {
        id: '167',
        question: questionKurumsalFirin4,
        content: `LED Ekran - Dokunmatik`,
        value: kurumsalFirin4Value,
        to: 'kurumsalFirin5'
    },
    {
        id: '168',
        question: questionKurumsalFirin4,
        content: `Mekanik Programlanabilir Saat`,
        value: kurumsalFirin4Value,
        to: 'kurumsalFirin5'
    },
    {
        id: '169',
        question: questionKurumsalFirin4,
        content: `Bu soruyu atlamak istiyorum`,
        value: null,
        to: 'kurumsalFirin5'
    },
];
var kurumsalFirin5Value = `Gaz Tipi`
export const kurumsalFirin5 = [
    {
        id: '170',
        question: questionKurumsalFirin5,
        content: `Doğalgaz`,
        value: kurumsalFirin5Value,
        to: 'kurumsalFirin6'
    },
    {
        id: '171',
        question: questionKurumsalFirin5,
        content: `Lpg`,
        value: kurumsalFirin5Value,
        to: 'kurumsalFirin6'
    },
    {
        id: '172',
        question: questionKurumsalFirin5,
        content: `Bu soruyu atlamak istiyorum`,
        value: null,
        to: 'kurumsalFirin6'
    },
];
var kurumsalFirin6Value = `Ürün Rengi`
export const kurumsalFirin6 = [
    {
        id: '173',
        question: questionKurumsalFirin6,
        content: `Beyaz`,
        value: kurumsalFirin6Value,
        to: 'finalPage'
    },
    {
        id: '174',
        question: questionKurumsalFirin6,
        content: `Gümüş`,
        value: kurumsalFirin6Value,
        to: 'finalPage'
    },
    {
        id: '175',
        question: questionKurumsalFirin6,
        content: `İnoks`,
        value: kurumsalFirin6Value,
        to: 'finalPage'
    },
    {
        id: '176',
        question: questionKurumsalFirin6,
        content: `Bu soruyu atlamak istiyorum`,
        value: null,
        to: 'finalPage'
    },

//QUESTIONS 
//Kurumsal-Set Üstü Fırın
];
var kurumsalSetUstuFirin1Value = `Pişirme Bölmesi Hacmi`
export const kurumsalSetUstuFirin1 = [
    {
        id: '177',
        question: questionKurumsalSetUstuFirin1,
        content: `28 L`,
        value: kurumsalSetUstuFirin1Value,
        to: 'kurumsalSetUstuFirin2'
    },
    {
        id: '178',
        question: questionKurumsalSetUstuFirin1,
        content: `40 L`,
        value: kurumsalSetUstuFirin1Value,
        to: 'kurumsalSetUstuFirin2'
    },
    {
        id: '179',
        question: questionKurumsalSetUstuFirin1,
        content: `50 L`,
        value: kurumsalSetUstuFirin1Value,
        to: 'kurumsalSetUstuFirin2'
    },
    {
        id: '180',
        question: questionKurumsalSetUstuFirin1,
        content: `Bu soruyu atlamak istiyorum`,
        value: null,
        to: 'kurumsalSetUstuFirin2'
    },
];
var kurumsalSetUstuFirin2Value = `Pişirme Sistemi`
export const kurumsalSetUstuFirin2 = [
    {
        id: '181',
        question: questionKurumsalSetUstuFirin2,
        content: `Izgaralı`,
        value: kurumsalSetUstuFirin2Value,
        to: 'kurumsalSetUstuFirin3'
    },
    {
        id: '182',
        question: questionKurumsalSetUstuFirin2,
        content: `Izgarasız`,
        value: kurumsalSetUstuFirin2Value,
        to: 'kurumsalSetUstuFirin3'
    },
    {
        id: '183',
        question: questionKurumsalSetUstuFirin2,
        content: `Bu soruyu atlamak istiyorum`,
        value: null,
        to: 'kurumsalSetUstuFirin3'
    },
];
var kurumsalSetUstuFirin3Value = `Ürün Rengi`
export const kurumsalSetUstuFirin3 = [
    {
        id: '184',
        question: questionKurumsalSetUstuFirin3,
        content: `Beyaz`,
        value: kurumsalSetUstuFirin3Value,
        to: 'finalPage'
    },
    {
        id: '185',
        question: questionKurumsalSetUstuFirin3,
        content: `Siyah`,
        value: kurumsalSetUstuFirin3Value,
        to: 'finalPage'
    },
    {
        id: '186',
        question: questionKurumsalSetUstuFirin3,
        content: `İnoks`,
        value: kurumsalSetUstuFirin3Value,
        to: 'finalPage'
    },
    {
        id: '187',
        question: questionKurumsalSetUstuFirin3,
        content: `Bu soruyu atlamak istiyorum`,
        value: null,
        to: 'finalPage'
    },

//QUESTIONS 
//Kurumsal-Mikrodalga Fırın
];
var kurumsalMikrodalgaFirin1Value = `Kontrol Tipi`
export const kurumsalMikrodalgaFirin1 = [
    {
        id: '188',
        question: questionKurumsalMikrodalgaFirin1,
        content: `Elektronik`,
        value: kurumsalMikrodalgaFirin1Value,
        to: 'kurumsalMikrodalgaFirin2'
    },
    {
        id: '189',
        question: questionKurumsalMikrodalgaFirin1,
        content: `Mekanik`,
        value: kurumsalMikrodalgaFirin1Value,
        to: 'kurumsalMikrodalgaFirin2'
    },
    {
        id: '190',
        question: questionKurumsalMikrodalgaFirin1,
        content: `Bu soruyu atlamak istiyorum`,
        value: null,
        to: 'kurumsalMikrodalgaFirin2'
    },
];
var kurumsalMikrodalgaFirin2Value = `Ürün Rengi`
export const kurumsalMikrodalgaFirin2 = [
    {
        id: '191',
        question: questionKurumsalMikrodalgaFirin2,
        content: `Beyaz`,
        value: kurumsalMikrodalgaFirin2Value,
        to: 'finalPage'
    },
    {
        id: '192  ',
        question: questionKurumsalMikrodalgaFirin2,
        content: `Gümüş`,
        value: kurumsalMikrodalgaFirin2Value,
        to: 'finalPage'
    },
    {
        id: '193',
        question: questionKurumsalMikrodalgaFirin2,
        content: `Siyah`,
        value: kurumsalMikrodalgaFirin2Value,
        to: 'finalPage'
    },
    {
        id: '194',
        question: questionKurumsalMikrodalgaFirin2,
        content: `İnoks`,
        value: kurumsalMikrodalgaFirin2Value,
        to: 'finalPage'
    },
    {
        id: '195',
        question: questionKurumsalMikrodalgaFirin2,
        content: `Bu soruyu atlamak istiyorum`,
        value: null,
        to: 'finalPage'
    },

//QUESTIONS 
//Kurumsal-Set Üstü Ocak
];
var kurumsalSetUstuOcak1Value = `Ocak Yüzeyi`
export const kurumsalSetUstuOcak1 = [
    {
        id: '196',
        question: questionKurumsalSetUstuOcak1,
        content: `Cam`,
        value: kurumsalSetUstuOcak1Value,
        to: 'kurumsalSetUstuOcak2'
    },
    {
        id: '197',
        question: questionKurumsalSetUstuOcak1,
        content: `Metal`,
        value: kurumsalSetUstuOcak1Value,
        to: 'kurumsalSetUstuOcak2'
    },
    {
        id: '198',
        question: questionKurumsalSetUstuOcak1,
        content: `Bu soruyu atlamak istiyorum`,
        value: null,
        to: 'kurumsalSetUstuOcak2'
    },
];
var kurumsalSetUstuOcak2Value = `Gaz Tipi`
export const kurumsalSetUstuOcak2 = [
    {
        id: '199',
        question: questionKurumsalSetUstuOcak2,
        content: `Doğalgaz`,
        value: kurumsalSetUstuOcak2Value,
        to: 'kurumsalSetUstuOcak3'
    },
    {
        id: '200',
        question: questionKurumsalSetUstuOcak2,
        content: `Lpg`,
        value: kurumsalSetUstuOcak2Value,
        to: 'kurumsalSetUstuOcak3'
    },
    {
        id: '201',
        question: questionKurumsalSetUstuOcak2,
        content: `Bu soruyu atlamak istiyorum`,
        value: null,
        to: 'kurumsalSetUstuOcak3'
    },
];
var kurumsalSetUstuOcak3Value = `Ocak Tipi ve Göz Sayısı`
export const kurumsalSetUstuOcak3 = [
    {
        id: '202',
        question: questionKurumsalSetUstuOcak3,
        content: `4 Gözü Gazlı`,
        value: kurumsalSetUstuOcak3Value,
        to: 'kurumsalSetUstuOcak4'
    },
    {
        id: '203',
        question: questionKurumsalSetUstuOcak3,
        content: `3 Gözü Gazlı + 1 Gözü Elektrikli`,
        value: kurumsalSetUstuOcak3Value,
        to: 'kurumsalSetUstuOcak4'
    },
    {
        id: '204',
        question: questionKurumsalSetUstuOcak3,
        content: `4 Gözü Gazlı (1'i WOK)`,
        value: kurumsalSetUstuOcak3Value,
        to: 'kurumsalSetUstuOcak4'
    },
    {
        id: '205',
        question: questionKurumsalSetUstuOcak3,
        content: `Bu soruyu atlamak istiyorum`,
        value: null,
        to: 'kurumsalSetUstuOcak4'
    },
];
var kurumsalSetUstuOcak4Value = `Ocak Tipi ve Göz Sayısı`
export const kurumsalSetUstuOcak4 = [
    {
        id: '206',
        question: questionKurumsalSetUstuOcak4,
        content: `Beyaz`,
        value: kurumsalSetUstuOcak4Value,
        to: 'finalPage'
    },
    {
        id: '207',
        question: questionKurumsalSetUstuOcak4,
        content: `Gümüş`,
        value: kurumsalSetUstuOcak4Value,
        to: 'finalPage'
    },
    {
        id: '208',
        question: questionKurumsalSetUstuOcak4,
        content: `Siyah`,
        value: kurumsalSetUstuOcak4Value,
        to: 'finalPage'
    },
    {
        id: '209',
        question: questionKurumsalSetUstuOcak4,
        content: `İnoks`,
        value: kurumsalSetUstuOcak4Value,
        to: 'finalPage'
    },
    {
        id: '210',
        question: questionKurumsalSetUstuOcak4,
        content: `Bu soruyu atlamak istiyorum`,
        value: null,
        to: 'finalPage'
    },

//QUESTIONS 
//Kurumsal-Aspiratör
];
var kurumsalAspirator1Value = `Ürün Rengi`
export const kurumsalAspirator1 = [
    {
        id: '211',
        question: questionKurumsalAspirator1,
        content: `Beyaz`,
        value: kurumsalAspirator1Value,
        to: 'finalPage'
    },
    {
        id: '212',
        question: questionKurumsalAspirator1,
        content: `Siyah`,
        value: kurumsalAspirator1Value,
        to: 'finalPage'
    },
    {
        id: '213',
        question: questionKurumsalAspirator1,
        content: `İnoks`,
        value: kurumsalAspirator1Value,
        to: 'finalPage'
    },
    {
        id: '214',
        question: questionKurumsalAspirator1,
        content: `Bu soruyu atlamak istiyorum`,
        value: null,
        to: 'finalPage'
    },

//QUESTIONS 
//Kurumsal-Su Arıtma Cihazı
];
var kurumsalSuAritmaCihazi1Value = `Filtre Adedi`
export const kurumsalSuAritmaCihazi1 = [
    {
        id: '215',
        question: questionKurumsalSuAritmaCihazi1,
        content: `3`,
        value: kurumsalSuAritmaCihazi1Value,
        to: 'kurumsalSuAritmaCihazi2'
    },
    {
        id: '216',
        question: questionKurumsalSuAritmaCihazi1,
        content: `4`,
        value: kurumsalSuAritmaCihazi1Value,
        to: 'kurumsalSuAritmaCihazi2'
    },
    {
        id: '217',
        question: questionKurumsalSuAritmaCihazi1,
        content: `Bu soruyu atlamak istiyorum`,
        value: null,
        to: 'kurumsalSuAritmaCihazi2'
    },
];
var kurumsalSuAritmaCihazi2Value = `Ek Fonksiyon`
export const kurumsalSuAritmaCihazi2 = [
    {
        id: '218',
        question: questionKurumsalSuAritmaCihazi2,
        content: `Su Soğutma`,
        value: kurumsalSuAritmaCihazi2Value,
        to: 'kurumsalSuAritmaCihazi3'
    },
    {
        id: '219',
        question: questionKurumsalSuAritmaCihazi2,
        content: `Su Isıtma`,
        value: kurumsalSuAritmaCihazi2Value,
        to: 'kurumsalSuAritmaCihazi3'
    },
    {
        id: '220',
        question: questionKurumsalSuAritmaCihazi2,
        content: `Bu soruyu atlamak istiyorum`,
        value: null,
        to: 'kurumsalSuAritmaCihazi3'
    },
];
var kurumsalSuAritmaCihazi3Value = `Ürün Çeşidi`
export const kurumsalSuAritmaCihazi3 = [
    {
        id: '221',
        question: questionKurumsalSuAritmaCihazi3,
        content: `Sebil`,
        value: kurumsalSuAritmaCihazi3Value,
        to: 'finalPage'
    },
    {
        id: '222',
        question: questionKurumsalSuAritmaCihazi3,
        content: `Tezgahüstü`,
        value: kurumsalSuAritmaCihazi3Value,
        to: 'finalPage'
    },
    {
        id: '223',
        question: questionKurumsalSuAritmaCihazi3,
        content: `Tezgahaltı`,
        value: kurumsalSuAritmaCihazi3Value,
        to: 'finalPage'
    },
    {
        id: '224',
        question: questionKurumsalSuAritmaCihazi3,
        content: `Bu soruyu atlamak istiyorum`,
        value: null,
        to: 'finalPage'
    },

//QUESTIONS - ANKASTRE
//Kurumsal-Ankastre Fırın
];
var kurumsalAnkastreFirin1Value = `Fırın Tipi`
export const kurumsalAnkastreFirin1 = [
    {
        id: '225',
        question: questionKurumsalAnkastreFirin1,
        content: `Multifonksiyon Fırın`,
        value: kurumsalAnkastreFirin1Value,
        to: 'kurumsalAnkastreFirin2'
    },
    {
        id: '226',
        question: questionKurumsalAnkastreFirin1,
        content: `Turbo Fırın`,
        value: kurumsalAnkastreFirin1Value,
        to: 'kurumsalAnkastreFirin2'
    },
    {
        id: '227',
        question: questionKurumsalAnkastreFirin1,
        content: `Bu soruyu atlamak istiyorum`,
        value: null,
        to: 'kurumsalAnkastreFirin2'
    },
];
var kurumsalAnkastreFirin2Value = `Saat Tipi`
export const kurumsalAnkastreFirin2 = [
    {
        id: '228',
        question: questionKurumsalAnkastreFirin2,
        content: `LCD – Ekran`,
        value: kurumsalAnkastreFirin2Value,
        to: 'kurumsalAnkastreFirin3'
    },
    {
        id: '229',
        question: questionKurumsalAnkastreFirin2,
        content: `LCD – Ekran Dokunmatik`,
        value: kurumsalAnkastreFirin2Value,
        to: 'kurumsalAnkastreFirin3'
    },
    {
        id: '230',
        question: questionKurumsalAnkastreFirin2,
        content: `LED – Ekran Dokunmatik`,
        value: kurumsalAnkastreFirin2Value,
        to: 'kurumsalAnkastreFirin3'
    },
    {
        id: '231',
        question: questionKurumsalAnkastreFirin2,
        content: `Mekanik Programlanabilir Saat`,
        value: kurumsalAnkastreFirin2Value,
        to: 'kurumsalAnkastreFirin3'
    },
    {
        id: '232',
        question: questionKurumsalAnkastreFirin2,
        content: `Rustik Saat`,
        value: kurumsalAnkastreFirin2Value,
        to: 'kurumsalAnkastreFirin3'
    },
    {
        id: '233',
        question: questionKurumsalAnkastreFirin2,
        content: `TFT Ekran – Dokunmatik`,
        value: kurumsalAnkastreFirin2Value,
        to: 'kurumsalAnkastreFirin3'
    },
    {
        id: '234',
        question: questionKurumsalAnkastreFirin2,
        content: `Bu soruyu atlamak istiyorum`,
        value: null,
        to: 'kurumsalAnkastreFirin3'
    },
];
var kurumsalAnkastreFirin3Value = `Pişirme Fonksiyon Sayısı`
export const kurumsalAnkastreFirin3 = [
    {
        id: '235',
        question: questionKurumsalAnkastreFirin3,
        content: `6 Fonksiyon`,
        value: kurumsalAnkastreFirin3Value,
        to: 'kurumsalAnkastreFirin4'
    },
    {
        id: '236',
        question: questionKurumsalAnkastreFirin3,
        content: `8 Fonksiyon`,
        value: kurumsalAnkastreFirin3Value,
        to: 'kurumsalAnkastreFirin4'
    },
    {
        id: '237',
        question: questionKurumsalAnkastreFirin3,
        content: `10 Fonksiyon`,
        value: kurumsalAnkastreFirin3Value,
        to: 'kurumsalAnkastreFirin4'
    },
    {
        id: '238',
        question: questionKurumsalAnkastreFirin3,
        content: `13 Fonksiyon`,
        value: kurumsalAnkastreFirin3Value,
        to: 'kurumsalAnkastreFirin4'
    },
    {
        id: '239',
        question: questionKurumsalAnkastreFirin3,
        content: `14 Fonksiyon`,
        value: kurumsalAnkastreFirin3Value,
        to: 'kurumsalAnkastreFirin4'
    },
    {
        id: '240',
        question: questionKurumsalAnkastreFirin3,
        content: `15 Fonksiyon`,
        value: kurumsalAnkastreFirin3Value,
        to: 'kurumsalAnkastreFirin4'
    },
    {
        id: '241',
        question: questionKurumsalAnkastreFirin3,
        content: `16 Fonksiyon (Pyrolytic)`,
        value: kurumsalAnkastreFirin3Value,
        to: 'kurumsalAnkastreFirin4'
    },
    {
        id: '242',
        question: questionKurumsalAnkastreFirin3,
        content: `20 Fonksiyon (Combi Steam)`,
        value: kurumsalAnkastreFirin3Value,
        to: 'kurumsalAnkastreFirin4'
    },
    {
        id: '243',
        question: questionKurumsalAnkastreFirin3,
        content: `Bu soruyu atlamak istiyorum`,
        value: null,
        to: 'kurumsalAnkastreFirin4'
    },
];
var kurumsalAnkastreFirin4Value = `Ürün Rengi`
export const kurumsalAnkastreFirin4 = [
    {
        id: '244',
        question: questionKurumsalAnkastreFirin4,
        content: `Antrasit Siyah`,
        value: kurumsalAnkastreFirin4Value,
        to: 'finalPage'
    },
    {
        id: '245',
        question: questionKurumsalAnkastreFirin4,
        content: `Avena (Desenli Krem)`,
        value: kurumsalAnkastreFirin4Value,
        to: 'finalPage'
    },
    {
        id: '246',
        question: questionKurumsalAnkastreFirin4,
        content: `Beyaz`,
        value: kurumsalAnkastreFirin4Value,
        to: 'finalPage'
    },
    {
        id: '247',
        question: questionKurumsalAnkastreFirin4,
        content: `Extraclear Beyaz`,
        value: kurumsalAnkastreFirin4Value,
        to: 'finalPage'
    },
    {
        id: '248',
        question: questionKurumsalAnkastreFirin4,
        content: `Grion`,
        value: kurumsalAnkastreFirin4Value,
        to: 'finalPage'
    },
    {
        id: '249',
        question: questionKurumsalAnkastreFirin4,
        content: `Kahverengi`,
        value: kurumsalAnkastreFirin4Value,
        to: 'finalPage'
    },
    {
        id: '250',
        question: questionKurumsalAnkastreFirin4,
        content: `İnoks - Parmak İzi Bırakmayan`,
        value: kurumsalAnkastreFirin4Value,
        to: 'finalPage'
    },
    {
        id: '251',
        question: questionKurumsalAnkastreFirin4,
        content: `Siyah`,
        value: kurumsalAnkastreFirin4Value,
        to: 'finalPage'
    },
    {
        id: '252',
        question: questionKurumsalAnkastreFirin4,
        content: `Bu soruyu atlamak istiyorum`,
        value: null,
        to: 'finalPage'
    },

//QUESTIONS 
//Kurumsal-Ankastre Mikrodalga Fırın
];
var kurumsalAnkastreMikrodalgaFirin1Value = `Kontrol Tipi`
export const kurumsalAnkastreMikrodalgaFirin1 = [
    {
        id: '253',
        question: questionKurumsalAnkastreMikrodalgaFirin1,
        content: `Elektronik`,
        value: kurumsalAnkastreMikrodalgaFirin1Value,
        to: 'kurumsalAnkastreMikrodalgaFirin2'
    },
    {
        id: '254',
        question: questionKurumsalAnkastreMikrodalgaFirin1,
        content: `Mekanik`,
        value: kurumsalAnkastreMikrodalgaFirin1Value,
        to: 'kurumsalAnkastreMikrodalgaFirin2'
    },
    {
        id: '255',
        question: questionKurumsalAnkastreMikrodalgaFirin1,
        content: `Bu soruyu atlamak istiyorum`,
        value: null,
        to: 'kurumsalAnkastreMikrodalgaFirin2'
    },
];
var kurumsalAnkastreMikrodalgaFirin2Value = `Kontrol Tipi`
export const kurumsalAnkastreMikrodalgaFirin2 = [
    {
        id: '256',
        question: questionKurumsalAnkastreMikrodalgaFirin2,
        content: `Beyaz`,
        value: kurumsalAnkastreMikrodalgaFirin2Value,
        to: 'finalPage'
    },
    {
        id: '257',
        question: questionKurumsalAnkastreMikrodalgaFirin2,
        content: `İnoks`,
        value: kurumsalAnkastreMikrodalgaFirin2Value,
        to: 'finalPage'
    },
    {
        id: '258',
        question: questionKurumsalAnkastreMikrodalgaFirin2,
        content: `Siyah`,
        value: kurumsalAnkastreMikrodalgaFirin2Value,
        to: 'finalPage'
    },
    {
        id: '259',
        question: questionKurumsalAnkastreMikrodalgaFirin2,
        content: `Bu soruyu atlamak istiyorum`,
        value: null,
        to: 'finalPage'
    },

//QUESTIONS 
//Kurumsal-Ankastre Ocak
];
var kurumsalAnkastreOcak1Value = `Gaz Tipi`
export const kurumsalAnkastreOcak1 = [
    {
        id: '260',
        question: questionKurumsalAnkastreOcak1,
        content: `Doğalgaz`,
        value: kurumsalAnkastreOcak1Value,
        to: 'kurumsalAnkastreOcak2'
    },
    {
        id: '261',
        question: questionKurumsalAnkastreOcak1,
        content: `Lpg`,
        value: kurumsalAnkastreOcak1Value,
        to: 'kurumsalAnkastreOcak2'
    },   
    {
        id: '262',
        question: questionKurumsalAnkastreOcak1,
        content: `Bu soruyu atlamak istiyorum`,
        value: null,
        to: 'kurumsalAnkastreOcak2'
    },      
];
var kurumsalAnkastreOcak2Value = `Emniyet Sistemi`
export const kurumsalAnkastreOcak2 = [
    {
        id: '263',
        question: questionKurumsalAnkastreOcak2,
        content: `Gaz Emniyet Sistemi`,
        value: kurumsalAnkastreOcak2Value,
        to: 'kurumsalAnkastreOcak3'
    },
    {
        id: '264',
        question: questionKurumsalAnkastreOcak2,
        content: `Aşırı Isınma Emniyeti`,
        value: kurumsalAnkastreOcak2Value,
        to: 'kurumsalAnkastreOcak3'
    },
    {
        id: '265',
        question: questionKurumsalAnkastreOcak2,
        content: `Bu soruyu atlamak istiyorum`,
        value: null,
        to: 'kurumsalAnkastreOcak3'
    },
];
var kurumsalAnkastreOcak3Value = `Ocak Tipi ve Göz Sayısı`
export const kurumsalAnkastreOcak3 = [
    {
        id: '266',
        question: questionKurumsalAnkastreOcak3,
        content: `11 İndüksiyon`,
        value: kurumsalAnkastreOcak3Value,
        to: 'kurumsalAnkastreOcak4'
    },
    {
        id: '267',
        question: questionKurumsalAnkastreOcak3,
        content: `2 Gözü Elektrikli`,
        value: kurumsalAnkastreOcak3Value,
        to: 'kurumsalAnkastreOcak4'
    },
    {
        id: '268',
        question: questionKurumsalAnkastreOcak3,
        content: `2 Gözü Gazlı`,
        value: kurumsalAnkastreOcak3Value,
        to: 'kurumsalAnkastreOcak4'
    },
    {
        id: '269',
        question: questionKurumsalAnkastreOcak3,
        content: `2 Gözü Vitroseramik `,
        value: kurumsalAnkastreOcak3Value,
        to: 'kurumsalAnkastreOcak4'
    },
    {
        id: '270',
        question: questionKurumsalAnkastreOcak3,
        content: `3 Gözü Gazlı + 1 Gözü Elektrikli`,
        value: kurumsalAnkastreOcak3Value,
        to: 'kurumsalAnkastreOcak4'
    },
    {
        id: '271',
        question: questionKurumsalAnkastreOcak3,
        content: `3 Gözü Gazlı + 1 Gözü Vitroseramik`,
        value: kurumsalAnkastreOcak3Value,
        to: 'kurumsalAnkastreOcak4'
    },
    {
        id: '272',
        question: questionKurumsalAnkastreOcak3,
        content: `4 Gözü Gazlı`,
        value: kurumsalAnkastreOcak3Value,
        to: 'kurumsalAnkastreOcak4'
    },
    {
        id: '273',
        question: questionKurumsalAnkastreOcak3,
        content: `4 Gözü Gazlı (1'i WOK)`,
        value: kurumsalAnkastreOcak3Value,
        to: 'kurumsalAnkastreOcak4'
    },
    {
        id: '274',
        question: questionKurumsalAnkastreOcak3,
        content: `4 Gözü Vitroseramik`,
        value: kurumsalAnkastreOcak3Value,
        to: 'kurumsalAnkastreOcak4'
    },
    {
        id: '275',
        question: questionKurumsalAnkastreOcak3,
        content: `4 Gözü İndüksiyon`,
        value: kurumsalAnkastreOcak3Value,
        to: 'kurumsalAnkastreOcak4'
    },
    {
        id: '276',
        question: questionKurumsalAnkastreOcak3,
        content: `5 Gözü Gazlı (1'i WOK)`,
        value: kurumsalAnkastreOcak3Value,
        to: 'kurumsalAnkastreOcak4'
    },
    {
        id: '277',
        question: questionKurumsalAnkastreOcak3,
        content: `5 Gözü Vitroseramik`,
        value: kurumsalAnkastreOcak3Value,
        to: 'kurumsalAnkastreOcak4'
    },
    {
        id: '278',
        question: questionKurumsalAnkastreOcak3,
        content: `8 Gözü İndüksiyon (2 Flexi Bölme)`,
        value: kurumsalAnkastreOcak3Value,
        to: 'kurumsalAnkastreOcak4'
    },
    {
        id: '279',
        question: questionKurumsalAnkastreOcak3,
        content: `Bu soruyu atlamak istiyorum`,
        value: null,
        to: 'kurumsalAnkastreOcak4'
    },
];
var kurumsalAnkastreOcak4Value = `Ürün Rengi`
export const kurumsalAnkastreOcak4 = [
    {
        id: '280',
        question: questionKurumsalAnkastreOcak4,
        content: `Antrasit`,
        value: kurumsalAnkastreOcak4Value,
        to: 'finalPage'
    },
    {
        id: '281',
        question: questionKurumsalAnkastreOcak4,
        content: `Avena`,
        value: kurumsalAnkastreOcak4Value,
        to: 'finalPage'
    },
    {
        id: '282',
        question: questionKurumsalAnkastreOcak4,
        content: `Beyaz`,
        value: kurumsalAnkastreOcak4Value,
        to: 'finalPage'
    },
    {
        id: '283',
        question: questionKurumsalAnkastreOcak4,
        content: `Beyaz Cam`,
        value: kurumsalAnkastreOcak4Value,
        to: 'finalPage'
    },
    {
        id: '284',
        question: questionKurumsalAnkastreOcak4,
        content: `Kahverengi`,
        value: kurumsalAnkastreOcak4Value,
        to: 'finalPage'
    },
    {
        id: '285',
        question: questionKurumsalAnkastreOcak4,
        content: `Siyah`,
        value: kurumsalAnkastreOcak4Value,
        to: 'finalPage'
    },
    {
        id: '286',
        question: questionKurumsalAnkastreOcak4,
        content: `Zara Gri`,
        value: kurumsalAnkastreOcak4Value,
        to: 'finalPage'
    },
    {
        id: '287',
        question: questionKurumsalAnkastreOcak4,
        content: `İnoks`,
        value: kurumsalAnkastreOcak4Value,
        to: 'finalPage'
    },
    {
        id: '288',
        question: questionKurumsalAnkastreOcak4,
        content: `Bu soruyu atlamak istiyorum`,
        value: null,
        to: 'finalPage'
    },

//QUESTIONS 
//Kurumsal-Ankastre Davlumbaz
];
var kurumsalAnkastreDavlumbaz1Value = `Ürün Tipi`
export const kurumsalAnkastreDavlumbaz1 = [
    {
        id: '289',
        question: questionKurumsalAnkastreDavlumbaz1,
        content: `Ada Tipi Davlumbaz`,
        value: kurumsalAnkastreDavlumbaz1Value,
        to: 'kurumsalAnkastreDavlumbaz2'
    },
    {
        id: '290',
        question: questionKurumsalAnkastreDavlumbaz1,
        content: `Duvar Tipi - Bükük Camlı T Tipi`,
        value: kurumsalAnkastreDavlumbaz1Value,
        to: 'kurumsalAnkastreDavlumbaz2'
    },
    {
        id: '291',
        question: questionKurumsalAnkastreDavlumbaz1,
        content: `Duvar Tipi - Düz Camlı T Tipi`,
        value: kurumsalAnkastreDavlumbaz1Value,
        to: 'kurumsalAnkastreDavlumbaz2'
    },
    {
        id: '292',
        question: questionKurumsalAnkastreDavlumbaz1,
        content: `Duvar tipi - Piramit`,
        value: kurumsalAnkastreDavlumbaz1Value,
        to: 'kurumsalAnkastreDavlumbaz2'
    },
    {
        id: '293',
        question: questionKurumsalAnkastreDavlumbaz1,
        content: `Duvar tipi Camsız T Tipi`,
        value: kurumsalAnkastreDavlumbaz1Value,
        to: 'kurumsalAnkastreDavlumbaz2'
    },
    {
        id: '294',
        question: questionKurumsalAnkastreDavlumbaz1,
        content: `Duvar tipi Eğimli`,
        value: kurumsalAnkastreDavlumbaz1Value,
        to: 'kurumsalAnkastreDavlumbaz2'
    },
    {
        id: '295',
        question: questionKurumsalAnkastreDavlumbaz1,
        content: `Bu soruyu atlamak istiyorum`,
        value: null,
        to: 'kurumsalAnkastreDavlumbaz2'
    },
];
var kurumsalAnkastreDavlumbaz2Value = `Davlumbaz Genişliği`
export const kurumsalAnkastreDavlumbaz2 = [
    {
        id: '296',
        question: questionKurumsalAnkastreDavlumbaz2,
        content: `40 cm`,
        value: kurumsalAnkastreDavlumbaz2Value,
        to: 'finalPage'
    },
    {
        id: '297',
        question: questionKurumsalAnkastreDavlumbaz2,
        content: `60 cm`,
        value: kurumsalAnkastreDavlumbaz2Value,
        to: 'finalPage'
    },
    {
        id: '298',
        question: questionKurumsalAnkastreDavlumbaz2,
        content: `80 cm`,
        value: kurumsalAnkastreDavlumbaz2Value,
        to: 'finalPage'
    },
    {
        id: '299',
        question: questionKurumsalAnkastreDavlumbaz2,
        content: `90 cm`,
        value: kurumsalAnkastreDavlumbaz2Value,
        to: 'finalPage'
    },
    {
        id: '300',
        question: questionKurumsalAnkastreDavlumbaz2,
        content: `110 cm`,
        value: kurumsalAnkastreDavlumbaz2Value,
        to: 'finalPage'
    },
    {
        id: '301',
        question: questionKurumsalAnkastreDavlumbaz2,
        content: `Bu soruyu atlamak istiyorum`,
        value: null,
        to: 'finalPage'
    },

//QUESTIONS 
//Kurumsal-Ankastre Bulaşık Makinesi
];
var kurumsalAnkastreBulasikMakinesi1Value = `Program Sayısı`
export const kurumsalAnkastreBulasikMakinesi1 = [
    {
        id: '302',
        question: questionKurumsalAnkastreBulasikMakinesi1,
        content: `4`,
        value: kurumsalAnkastreBulasikMakinesi1Value,
        to: 'kurumsalAnkastreBulasikMakinesi2'
    },
    {
        id: '303',
        question: questionKurumsalAnkastreBulasikMakinesi1,
        content: `6`,
        value: kurumsalAnkastreBulasikMakinesi1Value,
        to: 'kurumsalAnkastreBulasikMakinesi2'
    },
    {
        id: '304',
        question: questionKurumsalAnkastreBulasikMakinesi1,
        content: `8`,
        value: kurumsalAnkastreBulasikMakinesi1Value,
        to: 'kurumsalAnkastreBulasikMakinesi2'
    },
    {
        id: '305',
        question: questionKurumsalAnkastreBulasikMakinesi1,
        content: `9`,
        value: kurumsalAnkastreBulasikMakinesi1Value,
        to: 'kurumsalAnkastreBulasikMakinesi2'
    },
    {
        id: '306',
        question: questionKurumsalAnkastreBulasikMakinesi1,
        content: `10`,
        value: kurumsalAnkastreBulasikMakinesi1Value,
        to: 'kurumsalAnkastreBulasikMakinesi2'
    },
    {
        id: '307',
        question: questionKurumsalAnkastreBulasikMakinesi1,
        content: `Bu soruyu atlamak istiyorum`,
        value: null,
        to: 'kurumsalAnkastreBulasikMakinesi2'
    },
];
var kurumsalAnkastreBulasikMakinesi2Value = `Ürün Rengi`
export const kurumsalAnkastreBulasikMakinesi2 = [
    {
        id: '308',
        question: questionKurumsalAnkastreBulasikMakinesi2,
        content: `Beyaz`,
        value: kurumsalAnkastreBulasikMakinesi2Value,
        to: 'finalPage'
    },
    {
        id: '309',
        question: questionKurumsalAnkastreBulasikMakinesi2,
        content: `Beyaz Cam`,
        value: kurumsalAnkastreBulasikMakinesi2Value,
        to: 'finalPage'
    },
    {
        id: '310',
        question: questionKurumsalAnkastreBulasikMakinesi2,
        content: `Gri`,
        value: kurumsalAnkastreBulasikMakinesi2Value,
        to: 'finalPage'
    },
    {
        id: '311',
        question: questionKurumsalAnkastreBulasikMakinesi2,
        content: `Piyano Siyah`,
        value: kurumsalAnkastreBulasikMakinesi2Value,
        to: 'finalPage'
    },
    {
        id: '312',
        question: questionKurumsalAnkastreBulasikMakinesi2,
        content: `Siyah Cam`,
        value: kurumsalAnkastreBulasikMakinesi2Value,
        to: 'finalPage'
    },
    {
        id: '313',
        question: questionKurumsalAnkastreBulasikMakinesi2,
        content: `Vizon`,
        value: kurumsalAnkastreBulasikMakinesi2Value,
        to: 'finalPage'
    },
    {
        id: '314',
        question: questionKurumsalAnkastreBulasikMakinesi2,
        content: `İnoks - Parmak İzi Bırakmayan`,
        value: kurumsalAnkastreBulasikMakinesi2Value,
        to: 'finalPage'
    },
    {
        id: '315',
        question: questionKurumsalAnkastreBulasikMakinesi2,
        content: `Bu soruyu atlamak istiyorum`,
        value: null,
        to: 'finalPage'
    },

//QUESTIONS 
//Kurumsal-Ankastre Buzdolabı
];
var kurumsalAnkastreBuzdolabi1Value = `Dondurucu Yeri`
export const kurumsalAnkastreBuzdolabi1 = [
    {
        id: '316',
        question: questionKurumsalAnkastreBuzdolabi1,
        content: `Dondurucu Altta`,
        value: kurumsalAnkastreBuzdolabi1Value,
        to: 'kurumsalAnkastreBuzdolabi2'
    },
    {
        id: '317',
        question: questionKurumsalAnkastreBuzdolabi1,
        content: `Bu soruyu atlamak istiyorum`,
        value: null,
        to: 'kurumsalAnkastreBuzdolabi2'
    },
];
var kurumsalAnkastreBuzdolabi2Value = `Toplam Brüt Hacim (Ankastre Buzdolabı)`
export const kurumsalAnkastreBuzdolabi2 = [
    {
        id: '318',
        question: questionKurumsalAnkastreBuzdolabi2,
        content: `130 L`,
        value: kurumsalAnkastreBuzdolabi2Value,
        to: 'kurumsalAnkastreBuzdolabi3'
    },
    {
        id: '319',
        question: questionKurumsalAnkastreBuzdolabi2,
        content: `275 L`,
        value: kurumsalAnkastreBuzdolabi2Value,
        to: 'kurumsalAnkastreBuzdolabi3'
    },
    {
        id: '320',
        question: questionKurumsalAnkastreBuzdolabi2,
        content: `300 L`,
        value: kurumsalAnkastreBuzdolabi2Value,
        to: 'kurumsalAnkastreBuzdolabi3'
    },
    {
        id: '321',
        question: questionKurumsalAnkastreBuzdolabi2,
        content: `315 L`,
        value: kurumsalAnkastreBuzdolabi2Value,
        to: 'kurumsalAnkastreBuzdolabi3'
    },
    {
        id: '322',
        question: questionKurumsalAnkastreBuzdolabi2,
        content: `540 L`,
        value: kurumsalAnkastreBuzdolabi2Value,
        to: 'kurumsalAnkastreBuzdolabi3'
    },
    {
        id: '323',
        question: questionKurumsalAnkastreBuzdolabi2,
        content: `Bu soruyu atlamak istiyorum`,
        value: null,
        to: 'kurumsalAnkastreBuzdolabi3'
    },
];
var kurumsalAnkastreBuzdolabi3Value = `Ürün Rengi`
export const kurumsalAnkastreBuzdolabi3 = [
    {
        id: '324',
        question: questionKurumsalAnkastreBuzdolabi3,
        content: `Beyaz`,
        value: kurumsalAnkastreBuzdolabi3Value,
        to: 'finalPage'
    },
    {
        id: '325',
        question: questionKurumsalAnkastreBuzdolabi3,
        content: `Leke Tutmayan İnoks`,
        value: kurumsalAnkastreBuzdolabi3Value,
        to: 'finalPage'
    },
    {
        id: '326',
        question: questionKurumsalAnkastreBuzdolabi3,
        content: `Bu soruyu atlamak istiyorum`,
        value: null,
        to: 'finalPage'
    },

//QUESTIONS 
//Kurumsal-Ankastre Aspiratör
];
var kurumsalAnkastreAspirator1Value = `Ürün Tipi`
export const kurumsalAnkastreAspirator1 = [
    {
        id: '327',
        question: questionKurumsalAnkastreAspirator1,
        content: `Gömme Aspiratör`,
        value: kurumsalAnkastreAspirator1Value,
        to: 'kurumsalAnkastreAspirator2'
    },
    {
        id: '328',
        question: questionKurumsalAnkastreAspirator1,
        content: `Standart Aspiratör`,
        value: kurumsalAnkastreAspirator1Value,
        to: 'kurumsalAnkastreAspirator2'
    },
    {
        id: '329',
        question: questionKurumsalAnkastreAspirator1,
        content: `Sürgülü Aspiratör`,
        value: kurumsalAnkastreAspirator1Value,
        to: 'kurumsalAnkastreAspirator2'
    },
    {
        id: '330',
        question: questionKurumsalAnkastreAspirator1,
        content: `Bu soruyu atlamak istiyorum`,
        value: null,
        to: 'kurumsalAnkastreAspirator2'
    },
];
var kurumsalAnkastreAspirator2Value = `Davlumbaz Genişliği`
export const kurumsalAnkastreAspirator2 = [
    {
        id: '331',
        question: questionKurumsalAnkastreAspirator2,
        content: `50 cm`,
        value: kurumsalAnkastreAspirator2Value,
        to: 'finalPage'
    },
    {
        id: '332',
        question: questionKurumsalAnkastreAspirator2,
        content: `60 cm`,
        value: kurumsalAnkastreAspirator2Value,
        to: 'finalPage'
    },
    {
        id: '333',
        question: questionKurumsalAnkastreAspirator2,
        content: `70 cm`,
        value: kurumsalAnkastreAspirator2Value,
        to: 'finalPage'
    },
    {
        id: '334',
        question: questionKurumsalAnkastreAspirator2,
        content: `Bu soruyu atlamak istiyorum`,
        value: null,
        to: 'finalPage'
    },

//QUESTIONS 
//Kurumsal-Ankastre Sıcak Tutma Çekmecesi
];
var kurumsalAnkastreSicakTutmaCekmecesi1Value = `Ürün Rengi`
export const kurumsalAnkastreSicakTutmaCekmecesi1 = [
    {
        id: '335',
        question: questionKurumsalAnkastreSicakTutmaCekmecesi1,
        content: `Beyaz`,
        value: kurumsalAnkastreSicakTutmaCekmecesi1Value,
        to: 'finalPage'
    },
    {
        id: '336',
        question: questionKurumsalAnkastreSicakTutmaCekmecesi1,
        content: `Siyah`,
        value: kurumsalAnkastreSicakTutmaCekmecesi1Value,
        to: 'finalPage'
    },
    {
        id: '337',
        question: questionKurumsalAnkastreSicakTutmaCekmecesi1,
        content: `Bu soruyu atlamak istiyorum`,
        value: null,
        to: 'finalPage'
    },

//QUESTIONS 
//Kurumsal-Ankastre Çamaşır Makinesi
];
var kurumsalAnkastreCamasirMakinesi1Value = `-`
export const kurumsalAnkastreCamasirMakinesi1 = [
    {
        id: '338',
        question: questionKurumsalAnkastreCamasirMakinesi1,
        content: `-`,
        value: kurumsalAnkastreCamasirMakinesi1Value,
        to: 'finalPage'
    },
    {
        id: '339',
        question: questionKurumsalAnkastreCamasirMakinesi1,
        content: `Bu soruyu atlamak istiyorum`,
        value: null,
        to: 'finalPage'
    },

//QUESTIONS 
//Kurumsal-Ankastre Kurutmalı Çamaşır Makinesi
];
var kurumsalAnkastreKurutmaliCamasirMakinesi1Value = `-`
export const kurumsalAnkastreKurutmaliCamasirMakinesi1 = [
    {
        id: '340',
        question: questionKurumsalAnkastreKurutmaliCamasirMakinesi1,
        content: `-`,
        value: kurumsalAnkastreKurutmaliCamasirMakinesi1Value,
        to: 'finalPage'
    },
    {
        id: '341',
        question: questionKurumsalAnkastreKurutmaliCamasirMakinesi1,
        content: `Bu soruyu atlamak istiyorum`,
        value: null,
        to: 'finalPage'
    },

//QUESTIONS 
//Kurumsal-Ankastre Kahve Makinesi
];
var kurumsalAnkastreKahveMakinesi1Value = `-`
export const kurumsalAnkastreKahveMakinesi1 = [
    {
        id: '342',
        question: questionKurumsalAnkastreKahveMakinesi1,
        content: `-`,
        value: kurumsalAnkastreKahveMakinesi1Value,
        to: 'finalPage'
    },
    {
        id: '343',
        question: questionKurumsalAnkastreKahveMakinesi1,
        content: `Bu soruyu atlamak istiyorum`,
        value: null,
        to: 'finalPage'
    },

//QUESTIONS 
//Kurumsal-Ankastre Set
];
var kurumsalAnkastreSet1Value = `-`
export const kurumsalAnkastreSet1 = [
    {
        id: '344',
        question: questionKurumsalAnkastreSet1,
        content: `Cam`,
        value: kurumsalAnkastreSet1Value,
        to: 'kurumsalAnkastreSet2'
    },
    {
        id: '345',
        question: questionKurumsalAnkastreSet1,
        content: `İnoks`,
        value: kurumsalAnkastreSet1Value,
        to: 'kurumsalAnkastreSet2'
    },
    {
        id: '346',
        question: questionKurumsalAnkastreSet1,
        content: `Bu soruyu atlamak istiyorum`,
        value: null,
        to: 'kurumsalAnkastreSet2'
    },
];
var kurumsalAnkastreSet2Value = `-`
export const kurumsalAnkastreSet2 = [
    {
        id: '347',
        question: questionKurumsalAnkastreSet2,
        content: `Beyaz`,
        value: kurumsalAnkastreSet2Value,
        to: 'finalPage'
    },
    {
        id: '348',
        question: questionKurumsalAnkastreSet2,
        content: `Gri`,
        value: kurumsalAnkastreSet2Value,
        to: 'finalPage'
    },
    {
        id: '349',
        question: questionKurumsalAnkastreSet2,
        content: `İnoks`,
        value: kurumsalAnkastreSet2Value,
        to: 'finalPage'
    },
    {
        id: '350',
        question: questionKurumsalAnkastreSet2,
        content: `Kahverengi`,
        value: kurumsalAnkastreSet2Value,
        to: 'finalPage'
    },
    {
        id: '351',
        question: questionKurumsalAnkastreSet2,
        content: `Siyah`,
        value: kurumsalAnkastreSet2Value,
        to: 'finalPage'
    },
    {
        id: '352',
        question: questionKurumsalAnkastreSet2,
        content: `Bu soruyu atlamak istiyorum`,
        value: null,
        to: 'finalPage'
    },
    

//QUESTIONS - ISITMA SOĞUTMA
//Kurumsal-Kombi
];
var kurumsalKombi1Value = `Isıtma Kapasitesi`
export const kurumsalKombi1 = [
    {
        id: '353',
        question: questionKurumsalKombi1,
        content: `20600`,
        value: kurumsalKombi1Value,
        to: 'kurumsalKombi2'
    },
    {
        id: '354',
        question: questionKurumsalKombi1,
        content: `25500`,
        value: kurumsalKombi1Value,
        to: 'kurumsalKombi2'
    },
    {
        id: '355',
        question: questionKurumsalKombi1,
        content: `26700`,
        value: kurumsalKombi1Value,
        to: 'kurumsalKombi2'
    },
    {
        id: '356',
        question: questionKurumsalKombi1,
        content: `28400`,
        value: kurumsalKombi1Value,
        to: 'kurumsalKombi2'
    },
    {
        id: '357',
        question: questionKurumsalKombi1,
        content: `Bu soruyu atlamak istiyorum`,
        value: null,
        to: 'kurumsalKombi2'
    },

];
var kurumsalKombi2Value = `Verim`
export const kurumsalKombi2 = [
    {
        id: '358',
        question: questionKurumsalKombi2,
        content: `93%`,
        value: kurumsalKombi2Value,
        to: 'kurumsalKombi3'
    },
    {
        id: '359',
        question: questionKurumsalKombi2,
        content: `103%`,
        value: kurumsalKombi2Value,
        to: 'kurumsalKombi3'
    },
    {
        id: '360',
        question: questionKurumsalKombi2,
        content: `109%`,
        value: kurumsalKombi2Value,
        to: 'kurumsalKombi3'
    },
    {
        id: '361',
        question: questionKurumsalKombi2,
        content: `Bu soruyu atlamak istiyorum`,
        value: null,
        to: 'kurumsalKombi3'
    },

];
var kurumsalKombi3Value = `Cihaz Tipi`
export const kurumsalKombi3 = [
    {
        id: '362',
        question: questionKurumsalKombi3,
        content: `Konvansiyonel`,
        value: kurumsalKombi3Value,
        to: 'finalPage'
    },
    {
        id: '363',
        question: questionKurumsalKombi3,
        content: `Premix Tam Yoğuşmalı`,
        value: kurumsalKombi3Value,
        to: 'finalPage'
    },
    {
        id: '364',
        question: questionKurumsalKombi3,
        content: `Yoğuşmalı`,
        value: kurumsalKombi3Value,
        to: 'finalPage'
    },
    {
        id: '365',
        question: questionKurumsalKombi3,
        content: `Bu soruyu atlamak istiyorum`,
        value: null,
        to: 'finalPage'
    },


//QUESTIONS - ISITMA SOĞUTMA
//Kurumsal-Şofben
];
var kurumsalSofben1Value = `-`
export const kurumsalSofben1 = [
    {
        id: '366',
        question: questionKurumsalSofben1,
        content: `-`,
        value: kurumsalSofben1Value,
        to: 'finalPage'
    },
    {
        id: '367',
        question: questionKurumsalSofben1,
        content: `Bu soruyu atlamak istiyorum`,
        value: null,
        to: 'finalPage'
    },


//QUESTIONS - ISITMA SOĞUTMA
//Kurumsal-Termosifon
];
var kurumsalTermosifon1Value = `Isıtma Gücü`
export const kurumsalTermosifon1 = [
    {
        id: '368',
        question: questionKurumsalTermosifon1,
        content: `1800 W`,
        value: kurumsalTermosifon1Value,
        to: 'kurumsalTermosifon2'
    },
    {
        id: '369',
        question: questionKurumsalTermosifon1,
        content: `3000 W`,
        value: kurumsalTermosifon1Value,
        to: 'kurumsalTermosifon2'
    },
    {
        id: '370',
        question: questionKurumsalTermosifon1,
        content: `Bu soruyu atlamak istiyorum`,
        value: null,
        to: 'kurumsalTermosifon2'
    },
    
];
var kurumsalTermosifon2Value = `Kapasite`
export const kurumsalTermosifon2 = [    
    {
        id: '371',
        question: questionKurumsalTermosifon2,
        content: `50 L`,
        value: kurumsalTermosifon2Value,
        to: 'finalPage'
    },
    {
        id: '372',
        question: questionKurumsalTermosifon2,
        content: `65 L`,
        value: kurumsalTermosifon2Value,
        to: 'finalPage'
    },
    {
        id: '373',
        question: questionKurumsalTermosifon2,
        content: `80 L`,
        value: kurumsalTermosifon2Value,
        to: 'finalPage'
    },
    {
        id: '374',
        question: questionKurumsalTermosifon2,
        content: `Bu soruyu atlamak istiyorum`,
        value: null,
        to: 'finalPage'
    },


//QUESTIONS - ISITMA SOĞUTMA
//Kurumsal-Ani Su Isıtıcısı
];
var kurumsalAniSuIsiticisi1Value = `-`
export const kurumsalAniSuIsiticisi1 = [
    {
        id: '375',
        question: questionKurumsalAniSuIsiticisi1,
        content: `-`,
        value: kurumsalAniSuIsiticisi1Value,
        to: 'finalPage'
    },
    {
        id: '376',
        question: questionKurumsalAniSuIsiticisi1,
        content: `Bu soruyu atlamak istiyorum`,
        value: null,
        to: 'finalPage'
    },


//QUESTIONS - ISITMA SOĞUTMA
//Kurumsal-Vantilator
];
var kurumsalVantilator1Value = `Güç`
export const kurumsalVantilator1 = [
    {
        id: '377',
        question: questionKurumsalVantilator1,
        content: `35 W`,
        value: kurumsalVantilator1Value,
        to: 'finalPage'
    },
    {
        id: '378',
        question: questionKurumsalVantilator1,
        content: `38 W`,
        value: kurumsalVantilator1Value,
        to: 'finalPage'
    },
    {
        id: '379',
        question: questionKurumsalVantilator1,
        content: `50 W`,
        value: kurumsalVantilator1Value,
        to: 'finalPage'
    },
    {
        id: '380',
        question: questionKurumsalVantilator1,
        content: `55 W`,
        value: kurumsalVantilator1Value,
        to: 'finalPage'
    },
    {
        id: '381',
        question: questionKurumsalVantilator1,
        content: `Bu soruyu atlamak istiyorum`,
        value: null,
        to: 'finalPage'
    },


//QUESTIONS - ISITMA SOĞUTMA
//Kurumsal-Oda Termostatı
];
var kurumsalOdaTermostati1Value = `Kontrol Teknolojisi`
export const kurumsalOdaTermostati1 = [
    {
        id: '382',
        question: questionKurumsalOdaTermostati1,
        content: `Wifi Bağlantı`,
        value: kurumsalOdaTermostati1Value,
        to: 'kurumsalOdaTermostati2'
    },
    {
        id: '383',
        question: questionKurumsalOdaTermostati1,
        content: `Haftalık Programlama`,
        value: kurumsalOdaTermostati1Value,
        to: 'kurumsalOdaTermostati2'
    },
    {
        id: '384',
        question: questionKurumsalOdaTermostati1,
        content: `Bu soruyu atlamak istiyorum`,
        value: null,
        to: 'kurumsalOdaTermostati2'
    },

];
var kurumsalOdaTermostati2Value = `Ürün Tipi`
export const kurumsalOdaTermostati2 = [
    {
        id: '385',
        question: questionKurumsalOdaTermostati2,
        content: `Klima + Kombi Paketi`,
        value: kurumsalOdaTermostati2Value,
        to: 'finalPage'
    },
    {
        id: '386',
        question: questionKurumsalOdaTermostati2,
        content: `Klima Paketi`,
        value: kurumsalOdaTermostati2Value,
        to: 'finalPage'
    },
    {
        id: '387',
        question: questionKurumsalOdaTermostati2,
        content: `Kombi Paketi`,
        value: kurumsalOdaTermostati2Value,
        to: 'finalPage'
    },
    {
        id: '388',
        question: questionKurumsalOdaTermostati2,
        content: `Bu soruyu atlamak istiyorum`,
        value: null,
        to: 'finalPage'
    },


//QUESTIONS - ISITMA SOĞUTMA
//Kurumsal-Flavel Soba
];
var kurumsalFlavelSoba1Value = `Güç`
export const kurumsalFlavelSoba1 = [
    {
        id: '389',
        question: questionKurumsalFlavelSoba1,
        content: `1500 W`,
        value: kurumsalFlavelSoba1Value,
        to: 'kurumsalFlavelSoba2'
    },
    {
        id: '390',
        question: questionKurumsalFlavelSoba1,
        content: `2000 W`,
        value: kurumsalFlavelSoba1Value,
        to: 'kurumsalFlavelSoba2'
    },
    {
        id: '391',
        question: questionKurumsalFlavelSoba1,
        content: `2200 W`,
        value: kurumsalFlavelSoba1Value,
        to: 'kurumsalFlavelSoba2'
    },
    {
        id: '392',
        question: questionKurumsalFlavelSoba1,
        content: `2300 W`,
        value: kurumsalFlavelSoba1Value,
        to: 'kurumsalFlavelSoba2'
    },
    {
        id: '393',
        question: questionKurumsalFlavelSoba1,
        content: `2500 W`,
        value: kurumsalFlavelSoba1Value,
        to: 'kurumsalFlavelSoba2'
    },
    {
        id: '394',
        question: questionKurumsalFlavelSoba1,
        content: `3000 W`,
        value: kurumsalFlavelSoba1Value,
        to: 'kurumsalFlavelSoba2'
    },
    {
        id: '395',
        question: questionKurumsalFlavelSoba1,
        content: `Bu soruyu atlamak istiyorum`,
        value: null,
        to: 'kurumsalFlavelSoba2'
    },

];
var kurumsalFlavelSoba2Value = `Emniyet Sistemi`
export const kurumsalFlavelSoba2 = [
    {
        id: '396',
        question: questionKurumsalFlavelSoba2,
        content: `Aşırı Isınma Emniyeti`,
        value: kurumsalFlavelSoba2Value,
        to: 'kurumsalFlavelSoba3'
    },
    {
        id: '397',
        question: questionKurumsalFlavelSoba2,
        content: `Bu soruyu atlamak istiyorum`,
        value: null,
        to: 'kurumsalFlavelSoba3'
    },

];
var kurumsalFlavelSoba3Value = `Elektrik Tüketimi`
export const kurumsalFlavelSoba3 = [
    {
        id: '398',
        question: questionKurumsalFlavelSoba3,
        content: `1.5 kWh`,
        value: kurumsalFlavelSoba3Value,
        to: 'finalPage'
    },
    {
        id: '399',
        question: questionKurumsalFlavelSoba3,
        content: `2.0 kWh`,
        value: kurumsalFlavelSoba3Value,
        to: 'finalPage'
    },
    {
        id: '400',
        question: questionKurumsalFlavelSoba3,
        content: `2.2 kWh`,
        value: kurumsalFlavelSoba3Value,
        to: 'finalPage'
    },
    {
        id: '401',
        question: questionKurumsalFlavelSoba3,
        content: `2.5 kWh`,
        value: kurumsalFlavelSoba3Value,
        to: 'finalPage'
    },
    {
        id: '402',
        question: questionKurumsalFlavelSoba3,
        content: `Bu soruyu atlamak istiyorum`,
        value: null,
        to: 'finalPage'
    },



//QUESTIONS - ISITMA SOĞUTMA
//Kurumsal-Klima
];
var kurumsalKlima1Value = `Soğutma Kapasitesi`
export const kurumsalKlima1 = [
    {
        id: '403',
        question: questionKurumsalKlima1,
        content: `9000 Btu/h`,
        value: kurumsalKlima1Value,
        to: 'kurumsalKlima2'
    },
    {
        id: '404',
        question: questionKurumsalKlima1,
        content: `12000 Btu/h`,
        value: kurumsalKlima1Value,
        to: 'kurumsalKlima2'
    },
    {
        id: '405',
        question: questionKurumsalKlima1,
        content: `18000 Btu/h`,
        value: kurumsalKlima1Value,
        to: 'kurumsalKlima2'
    },
    {
        id: '406',
        question: questionKurumsalKlima1,
        content: `24000 Btu/h`,
        value: kurumsalKlima1Value,
        to: 'kurumsalKlima2'
    },
    {
        id: '407',
        question: questionKurumsalKlima1,
        content: `44000 Btu/h`,
        value: kurumsalKlima1Value,
        to: 'kurumsalKlima2'
    },
    {
        id: '408',
        question: questionKurumsalKlima1,
        content: `46100 Btu/h`,
        value: kurumsalKlima1Value,
        to: 'kurumsalKlima2'
    },
    {
        id: '409',
        question: questionKurumsalKlima1,
        content: `47000 Btu/h`,
        value: null,
        to: 'kurumsalKlima2'
    },


];
var kurumsalKlima2Value = `Klima Özelliği`
export const kurumsalKlima2 = [
    {
        id: '410',
        question: questionKurumsalKlima2,
        content: `Sessiz Çalışma`,
        value: kurumsalKlima2Value,
        to: 'kurumsalKlima3'
    },
    {
        id: '411',
        question: questionKurumsalKlima2,
        content: `Bu soruyu atlamak istiyorum`,
        value: null,
        to: 'kurumsalKlima3'
    },


];
var kurumsalKlima3Value = `Bağlantı Özelliği`
export const kurumsalKlima3 = [
    {
        id: '412',
        question: questionKurumsalKlima3,
        content: `Wi-Fi Bağlantı`,
        value: kurumsalKlima3Value,
        to: 'kurumsalKlima4'
    },
    {
        id: '413',
        question: questionKurumsalKlima3,
        content: `Bu soruyu atlamak istiyorum`,
        value: null,
        to: 'kurumsalKlima4'
    },

];
var kurumsalKlima4Value = `Hava Tazeleme`
export const kurumsalKlima4 = [
    {
        id: '414',
        question: questionKurumsalKlima4,
        content: `2 Saatte 25 m2 lik alan`,
        value: kurumsalKlima4Value,
        to: 'finalPage'
    },
    {
        id: '415',
        question: questionKurumsalKlima4,
        content: `Bu soruyu atlamak istiyorum`,
        value: null,
        to: 'finalPage'
    },
]














//QUESTIONS 
//BİREYSEL

const questionBireysel = `Lütfen aradığınız ürünü kapsayan kategoriyi seçin.`;
const questionBireyselBeyazEsya = `Lütfen aşağıdaki listeden aradığınız ürün grubunu seçin.`;
const questionBireyselAnkastre = `Lütfen aşağıdaki listeden aradığınız ürün grubunu seçin.`;
const questionBireyselIsitmaSogutma = `Lütfen aşağıdaki listeden aradığınız ürün grubunu seçin.`;

//QUESTIONS 
//Bireysel-Beyaz Eşya

const questionBireyselBuzdolabi1 = `İstenilen buzdolabı genişliği nedir?`;
const questionBireyselBuzdolabi2 = `İstenilen buzdolabı yüksekliği nedir?`;
const questionBireyselBuzdolabi3 = `İstenilen buzdolabı derinliği nedir?`;
const questionBireyselBuzdolabi4 = `İstenilen buzdolabı hacmi nedir?`;
const questionBireyselBuzdolabi5 = `İstenilen buzdolabı rengi nedir?`;
const questionBireyselBuzdolabi6 = `İstenilen soğutma sistemi nedir?`;
const questionBireyselBuzdolabi7 = `İstenilen enerji sınıfı nedir?`;
const questionBireyselBuzdolabi8 = `İstenilen dondurucu yeri neresidir?`;

const questionBireyselDerinDondurucu1 = `İstenilen derin dondurucu genişliği nedir?`;
const questionBireyselDerinDondurucu2 = `İstenilen derin dondurucu yüksekliği nedir?`;
const questionBireyselDerinDondurucu3 = `İstenilen derin dondurucu derinliği nedir?`;
const questionBireyselDerinDondurucu4 = `İstenilen derin dondurucu hacmi nedir?`;
const questionBireyselDerinDondurucu5 = `İstenilen derin dondurucu rengi nedir?`;
const questionBireyselDerinDondurucu6 = `İstenilen enerji sınıfı nedir?`;
const questionBireyselDerinDondurucu7 = `İstenilen dondurucu yeri neresidir?`;

const questionBireyselCamasirMakinesi1 = `İstenilen çamaşır makine kapasitesi nedir?`;
const questionBireyselCamasirMakinesi2 = `İstenilen çamaşır makinesi rengi nedir?`;
const questionBireyselCamasirMakinesi3 = `İstenilen enerji performansı nedir?`;
const questionBireyselCamasirMakinesi4 = `İstenilen sıkma devri nedir?`;

const questionBireyselBulasikMakinesi1 = `İstenilen bulaşık makinesi rengi nedir?`;
const questionBireyselBulasikMakinesi2 = `İstenilen enerji sınıfı nedir?`;
const questionBireyselBulasikMakinesi3 = `İstenilen program sayısı nedir?`;

const questionBireyselKurutmaMakinesi1 = `İstenilen kurutma makinesi rengi nedir?`;
const questionBireyselKurutmaMakinesi2 = `İstenilen kurutma makine kapasitesi nedir?`;
const questionBireyselKurutmaMakinesi3 = `İstenilen enerji performansı nedir?`;
const questionBireyselKurutmaMakinesi4 = `İstenilen kurutma teknolojisi nedir?`;

const questionBireyselKurutmaliCamasirMakinesi1 = `İstenilen makine kapasitesi nedir?`;
const questionBireyselKurutmaliCamasirMakinesi2 = `İstenilen kurutma teknolojisi nedir?`;

const questionBireyselFirin1 = `İstenilen fırın kategorisi nedir?`;
const questionBireyselFirin2 = `İstenilen fırın rengi nedir?`;
const questionBireyselFirin3 = `İstenilen ocak göz sayısı nedir?`;
const questionBireyselFirin4 = `İstenilen ısı fonksiyon yönü nedir?`;
const questionBireyselFirin5 = `İstenilen pişirme fonksiyon sayısı nedir?`;
const questionBireyselFirin6 = `İstenilen kontrol tipi nedir?`;

const questionBireyselSetUstuFirin1 = `İstenilen set üstü fırın rengi nedir?`;
const questionBireyselSetUstuFirin2 = `İstenilen pişirme sistemi nedir?`;
const questionBireyselSetUstuFirin3 = `İstenilen pişirme bölmesi hacmi nedir?`;

const questionBireyselMikrodalgaFirin1 = `İstenilen mikrodalga fırın rengi nedir?`;
const questionBireyselMikrodalgaFirin2 = `İstenilen mikrodalga fırın kontrol tipi nedir?`;

const questionBireyselSetUstuOcak1 = `İstenilen set üstü ocak tipi nedir?`;
const questionBireyselSetUstuOcak2 = `İstenilen set üstü ocak rengi nedir?`;
const questionBireyselSetUstuOcak3 = `İstenilen ocak yüzeyinin malzemesi nedir?`;
const questionBireyselSetUstuOcak4 = `İstenilen set üstü ocak gaz tipi nedir?`;

const questionBireyselAspirator1 = `İstenilen aspiratör rengi nedir?`;

const questionBireyselSuAritmaCihazi1 = `İstenilen ürünün filtre adedi nedir?`;
const questionBireyselSuAritmaCihazi2 = `İstenilen ürünün konum yeri neresidir?`;
const questionBireyselSuAritmaCihazi3 = `İstenilen ek fonksiyon var mıdır?`;

//QUESTIONS 
//Bireysel-Ankastre

const questionBireyselAnkastreFirin1 = `İstenilen fırın kategorisi nedir?`;
const questionBireyselAnkastreFirin2 = `İstenilen fırın rengi nedir?`;
const questionBireyselAnkastreFirin3 = `İstenilen pişirme fonksiyon sayısı nedir?`;
const questionBireyselAnkastreFirin4 = `İstenilen kontrol tipi nedir?`;

const questionBireyselAnkastreMikrodalgaFirin1 = `İstenilen mikrodalga fırın rengi nedir?`;
const questionBireyselAnkastreMikrodalgaFirin2 = `İstenilen mikrodalga fırın kontrol tipi nedir?`;

const questionBireyselAnkastreOcak1 = `İstenilen ocak rengi nedir?`;
const questionBireyselAnkastreOcak2 = `İstenilen ocak tipi nedir?`;
const questionBireyselAnkastreOcak3 = `İstenilen gaz tipi nedir?`;

const questionBireyselAnkastreDavlumbaz1 = `İstenilen davlumbaz genişliği nedir?`;
const questionBireyselAnkastreDavlumbaz2 = `İstenilen davlumbaz tipi nedir?`;

const questionBireyselAnkastreBulasikMakinesi1 = `İstenilen bulaşık makinesi rengi nedir?`;
const questionBireyselAnkastreBulasikMakinesi2 = `İstenilen program sayısı nedir?`;

const questionBireyselAnkastreBuzdolabi1 = `İstenilen buzdolabı rengi nedir?`;
const questionBireyselAnkastreBuzdolabi2 = `İstenilen buzdolabı hacmi nedir?`;
const questionBireyselAnkastreBuzdolabi3 = `İstenilen dondurucu yeri neresidir?`;

const questionBireyselAnkastreAspirator1 = `İstenilen aspiratör genişliği nedir?`;
const questionBireyselAnkastreAspirator2 = `İstenilen aspiratör tipi nedir?`;

const questionBireyselAnkastreSicakTutmaMakinesi1 = `İstenilen sıcak tutma çekmecesi rengi nedir?`;

const questionBireyselAnkastreCamasirMakinesi1 = `-`;

const questionBireyselAnkastreKurutmaliCamasirMakinesi1 = `-`;

const questionBireyselAnkastreKahveMakinesi1 = `-`;

const questionBireyselAnkastreSet1 = `İstenilen set rengi nedir?`;
const questionBireyselAnkastreSet2 = `İstenilen set malzemesi nedir?`;


//QUESTIONS 
//Bireysel-Isıtma Soğutma

const questionBireyselKombi1 = `İstenilen cihaz tipi nedir?`;
const questionBireyselKombi2 = `İstenilen verim nedir?`;
const questionBireyselKombi3 = `İstenilen ısıtma kapasitesi nedir?`;

const questionBireyselSofben1 = `-`;

const questionBireyselTermosifon1 = `İstenilen termosifon litresi nedir?`;
const questionBireyselTermosifon2 = `İstenilen ısıtma kapasitesi nedir?`;

const questionBireyselAniSuIsiticisi1 = `-`;

const questionBireyselVantilator1 = `İstenilen vantilatör gücü nedir?`;

const questionBireyselOdaTermostati1 = `İstenilen oda termostat tipi nedir?`;
const questionBireyselOdaTermostati2 = `İstenilen kontrol teknolojisi nedir?`;

const questionBireyselFlavelSoba1 = `İstenilen elektrik tüketim kapasitesi nedir?`;
const questionBireyselFlavelSoba2 = `İstenilen emniyet çeşidi nedir?`;
const questionBireyselFlavelSoba3 = `İstenilen W gücü nedir?`;

const questionBireyselKlima1 = `İstenilen BTU değeri nedir?`;
const questionBireyselKlima2 = `İstenilen hava tazeleyici klima özelliği nedir?`;
const questionBireyselKlima3 = `İstenilen bağlantı özellikli klima nedir?`;
const questionBireyselKlima4 = `İstenilen çalışma özellikli klima nedir?`;


//QUESTIONS 
//Bireysel
export const mainQuestionsBireysel = [
    {
        id: '3000',
        question: questionBireysel,
        content: `Beyaz Eşya`,
        to: 'bireyselBeyazEsya'
    },
    {
        id: '3001',
        question: questionBireysel,
        content: `Ankastre`,
        to: 'bireyselAnkastre'
    },
    {
        id: '3002',
        question: questionBireysel,
        content: `Isıtma Soğutma`,
        to: 'bireyselIsitmaSogutma'
    },
];

//QUESTIONS 
//Bireysel-Beyaz Eşya
export const mainQuestionsBireyselBeyazEsya = [
    {
        id: '3003',
        question: questionBireyselBeyazEsya,
        content: `Buzdolabı`,
        value: 231,
        to: 'bireyselBuzdolabi1'
    },
    {
        id: '3004',
        question: questionBireyselBeyazEsya,
        content: `Derin Dondurucu`,
        value: 489,
        to: 'bireyselDerinDondurucu1'
    },
    {
        id: '3005',
        question: questionBireyselBeyazEsya,
        content: `Çamaşır Makinesi`,
        value: 200,
        to: 'bireyselCamasirMakinesi1'
    },
    {
        id: '3006',
        question: questionBireyselBeyazEsya,
        content: `Bulaşık Makinesi`,
        value: 176,
        to: 'bireyselBulasikMakinesi1'
    },
    {
        id: '3007',
        question: questionBireyselBeyazEsya,
        content: `Kurutma Makinesi`,
        value: 492,
        to: 'bireyselKurutmaMakinesi1'
    },
    {
        id: '3008',
        question: questionBireyselBeyazEsya,
        content: `Kurutmalı Çamaşır Makinesi`,
        value: 493,
        to: 'bireyselKurutmaliCamasirMakinesi1'
    },
    {
        id: '3009',
        question: questionBireyselBeyazEsya,
        content: `Fırın`,
        value: 494,
        to: 'bireyselFirin1'
    },
    {
        id: '3010',
        question: questionBireyselBeyazEsya,
        content: `Set Üstü Fırın`,
        value: 497,
        to: 'bireyselSetUstuFirin1'
    },
    {
        id: '3011',
        question: questionBireyselBeyazEsya,
        content: `Mikrodalga Fırın`,
        value: 498,
        to: 'bireyselMikrodalgaFirin1'
    },
    {
        id: '3012',
        question: questionBireyselBeyazEsya,
        content: `Set Üstü Ocak`,
        value: 499,
        to: 'bireyselSetUstuOcak1'
    },
    {
        id: '3013',
        question: questionBireyselBeyazEsya,
        content: `Aspiratör`,
        value: 500,
        to: 'bireyselAspirator1'
    },
    {
        id: '3014',
        question: questionBireyselBeyazEsya,
        content: `Su Arıtma Cihazı`,
        value: 501,
        to: 'bireyselSuAritmaCihazi1'
    },

//QUESTIONS 
//Bireysel-Ankastre
];
export const mainQuestionsBireyselAnkastre = [
    {
        id: '3015',
        question: questionBireyselAnkastre,
        content: `Ankastre Fırın`,
        value: 340,
        to: 'bireyselAnkastreFirin1'
    },
    {
        id: '3016',
        question: questionBireyselAnkastre,
        content: `Ankastre Mikrodalga Fırın`,
        value: 473,
        to: 'bireyselAnkastreMikrodalgaFirin1'
    },
    {
        id: '3017',
        question: questionBireyselAnkastre,
        content: `Ankastre Ocak`,
        value: 389,
        to: 'bireyselAnkastreOcak1'
    },
    {
        id: '3018',
        question: questionBireyselAnkastre,
        content: `Ankastre Davlumbaz`,
        value: 400,
        to: 'bireyselAnkastreDavlumbaz1'
    },
    {
        id: '3019',
        question: questionBireyselAnkastre,
        content: `Ankastre Bulaşık Makinesi`,
        value: 392,
        to: 'bireyselAnkastreBulasikMakinesi1'
    },
    {
        id: '3020',
        question: questionBireyselAnkastre,
        content: `Ankastre Buzdolabı`,
        value: 323,
        to: 'bireyselBuzdolabi1'
    },
    {
        id: '3021',
        question: questionBireyselAnkastre,
        content: `Ankastre Aspiratör`,
        value: 477,
        to: 'bireyselAnkastreAspirator1'
    },
    {
        id: '3022',
        question: questionBireyselAnkastre,
        content: `Ankastre Sıcak Tutma Makinesi`,
        value: 471,
        to: 'bireyselAnkastreSicakTutmaMakinesi1'
    },
    {
        id: '3023',
        question: questionBireyselAnkastre,
        content: `Ankastre Çamaşır Makinesi`,
        value: 469,
        to: 'bireyselAnkastreCamasirMakinesi1'
    },
    {
        id: '3024',
        question: questionBireyselAnkastre,
        content: `Ankastre Kurutmalı Çamaşır Makinesi`,
        value: 470,
        to: 'bireyselAnkastreKurutmaliCamasirMakinesi1'
    },
    {
        id: '3025',
        question: questionBireyselAnkastre,
        content: `Ankastre Kahve Makinesi`,
        value: 414,
        to: 'bireyselAnkastreKahveMakinesi1'
    },
    {
        id: '3026',
        question: questionBireyselAnkastre,
        content: `Ankastre Set`,
        value: '-',
        to: 'bireyselAnkastreSet1'
    },



//QUESTIONS 
//Bireysel-Isıtma Soğutma
];
export const mainQuestionsBireyselIsitmaSogutma = [
    {
        id: '3027',
        question: questionBireyselIsitmaSogutma,
        content: `Kombi`,
        value: 89,
        to: 'bireyselIsitmaSogutma1'
    },
    {
        id: '3028',
        question: questionBireyselIsitmaSogutma,
        content: `Şofben`,
        value: 109,
        to: 'bireyselSofben1'
    },
    {
        id: '3029',
        question: questionBireyselIsitmaSogutma,
        content: `Termosifon`,
        value: 114,
        to: 'bireyselTermosifon1'
    }, 
    {
        id: '3030',
        question: questionBireyselIsitmaSogutma,
        content: `Ani Su Isıtıcısı`,
        value: 128,
        to: 'bireyselAniSuIsiticisi1'
    }, 
    {
        id: '3031',
        question: questionBireyselIsitmaSogutma,
        content: `Vantilatör`,
        value: 422,
        to: 'bireyselVantilator1'
    }, 
    {
        id: '3032',
        question: questionBireyselIsitmaSogutma,
        content: `Oda Termostati`,
        value: 152,
        to: 'bireyselOdaTermostati1'
    }, 
    {
        id: '3033',
        question: questionBireyselIsitmaSogutma,
        content: `FlavelSoba`,
        value: 133,
        to: 'bireyselFlavelSoba1'
    }, 
    {
        id: '3034',
        question: questionBireyselIsitmaSogutma,
        content: `Klima`,
        value: 15,
        to: 'bireyselKlima1'
    },    



//QUESTIONS 
//Bireysel-Buzdolabı
];
var bireyselBuzdolabi1Value = 'Genişlik (Buzdolabı)'
export const bireyselBuzdolabi1 = [
    {
        id: '3035',
        question: questionBireyselBuzdolabi1,
        content: `45 cm - 69 cm arasında`,
        value: bireyselBuzdolabi1Value,
        to: 'bireyselBuzdolabi2'
    },
    {
        id: '3036',
        question: questionBireyselBuzdolabi1,
        content: `70 cm - 95 cm arasında`,
        value: bireyselBuzdolabi1Value,
        to: 'bireyselBuzdolabi2'
    },
    {
        id: '3037',
        question: questionBireyselBuzdolabi1,
        content: `Farketmez`,
        value: null,
        to: 'bireyselBuzdolabi2'
    },
];
var bireyselBuzdolabi2Value = 'Yükseklik (Buzdolabı)'
export const bireyselBuzdolabi2 = [
    {
        id: '3038',
        question: questionBireyselBuzdolabi2,
        content: `80 cm - 175 cm arasında`,
        value: bireyselBuzdolabi2Value,
        to: 'bireyselBuzdolabi3'
    },
    {
        id: '3039',
        question: questionBireyselBuzdolabi2,
        content: `176 cm - 195 cm arasında`,
        value: bireyselBuzdolabi2Value,
        to: 'bireyselBuzdolabi3'
    },
    {
        id: '3040',
        question: questionBireyselBuzdolabi2,
        content: `Farketmez`,
        value: null,
        to: 'bireyselBuzdolabi3'
    },
];
var bireyselBuzdolabi3Value = 'Derinlik (Buzdolabı)'
export const bireyselBuzdolabi3 = [
    {
        id: '3041',
        question: questionBireyselBuzdolabi3,
        content: `53 cm - 69 cm arasında`,
        value: bireyselBuzdolabi3Value,
        to: 'bireyselBuzdolabi4'
    },
    {
        id: '3042',
        question: questionBireyselBuzdolabi3,
        content: `70 cm - 81 cm arasında`,
        value: bireyselBuzdolabi3Value,
        to: 'bireyselBuzdolabi4'
    },
    {
        id: '3043',
        question: questionBireyselBuzdolabi3,
        content: `Farketmez`,
        value: null,
        to: 'bireyselBuzdolabi4'
    },
];
var bireyselBuzdolabi4Value = 'Toplam Brüt Hacim (Buzdolabı)'
export const bireyselBuzdolabi4 = [
    {
        id: '3044',
        question: questionBireyselBuzdolabi4,
        content: `90 L - 500 L arasında`,
        value: bireyselBuzdolabi4Value,
        to: 'bireyselBuzdolabi5'
    },
    {
        id: '3045',
        question: questionBireyselBuzdolabi4,
        content: `501 L - 750 L arasında`,
        value: bireyselBuzdolabi4Value,
        to: 'bireyselBuzdolabi5'
    },
    {
        id: '3046',
        question: questionBireyselBuzdolabi4,
        content: `Farketmez`,
        value: null,
        to: 'bireyselBuzdolabi5'
    },
];
var bireyselBuzdolabi5Value = 'Ürün Rengi'
export const bireyselBuzdolabi5 = [
    {
        id: '3047',
        question: questionBireyselBuzdolabi5,
        content: `Beyaz`,
        value: bireyselBuzdolabi5Value,
        to: 'bireyselBuzdolabi6'
    },
    {
        id: '3048',
        question: questionBireyselBuzdolabi5,
        content: `Beyaz Cam`,
        value: bireyselBuzdolabi5Value,
        to: 'bireyselBuzdolabi6'
    },
    {
        id: '3049',
        question: questionBireyselBuzdolabi5,
        content: `Beyaz Deri Desenli`,
        value: bireyselBuzdolabi5Value,
        to: 'bireyselBuzdolabi6'
    },
    {
        id: '3050',
        question: questionBireyselBuzdolabi5,
        content: `Gümüş`,
        value: bireyselBuzdolabi5Value,
        to: 'bireyselBuzdolabi6'
    },
    {
        id: '3051',
        question: questionBireyselBuzdolabi5,
        content: `Leke Tutmayan İnoks`,
        value: bireyselBuzdolabi5Value,
        to: 'bireyselBuzdolabi6'
    },
    {
        id: '3052',
        question: questionBireyselBuzdolabi5,
        content: `Siyah Cam`,
        value: bireyselBuzdolabi5Value,
        to: 'bireyselBuzdolabi6'
    },
    {
        id: '3053',
        question: questionBireyselBuzdolabi5,
        content: `Farketmez`,
        value: null,
        to: 'bireyselBuzdolabi6'
    },
];
var bireyselBuzdolabi6Value = 'Soğutma Sistemi'
export const bireyselBuzdolabi6 = [
    {
        id: '3054',
        question: questionBireyselBuzdolabi6,
        content: `No Frost`,
        value: bireyselBuzdolabi6Value,
        to: 'bireyselBuzdolabi7'
    },
    {
        id: '3055',
        question: questionBireyselBuzdolabi6,
        content: `Statik`,
        value: bireyselBuzdolabi6Value,
        to: 'bireyselBuzdolabi7'
    },
    {
        id: '3056',
        question: questionBireyselBuzdolabi6,
        content: `Yarı No Frost`,
        value: bireyselBuzdolabi6Value,
        to: 'bireyselBuzdolabi7'
    },
    {
        id: '3057',
        question: questionBireyselBuzdolabi6,
        content: `Farketmez`,
        value: null,
        to: 'bireyselBuzdolabi7'
    },

];
var bireyselBuzdolabi7Value = 'Enerji Sınıfı'
export const bireyselBuzdolabi7 = [
    {
        id: '3058',
        question: questionBireyselBuzdolabi7,
        content: `A+++`,
        value: bireyselBuzdolabi7Value,
        to: 'bireyselBuzdolabi8'
    },
    {
        id: '3059',
        question: questionBireyselBuzdolabi7,
        content: `A++`,
        value: bireyselBuzdolabi7Value,
        to: 'bireyselBuzdolabi8'
    },
    {
        id: '3060',
        question: questionBireyselBuzdolabi7,
        content: `A+`,
        value: bireyselBuzdolabi7Value,
        to: 'bireyselBuzdolabi8'
    },
    {
        id: '3061',
        question: questionBireyselBuzdolabi7,
        content: `Farketmez`,
        value: null,
        to: 'bireyselBuzdolabi8'
    },
];
var bireyselBuzdolabi8Value = 'Dondurucu Yeri'
export const bireyselBuzdolabi8 = [
    {
        id: '3062',
        question: questionBireyselBuzdolabi8,
        content: `Dondurucu Altta`,
        value: bireyselBuzdolabi8Value,
        to: 'finalPage'
    },
    {
        id: '3063',
        question: questionBireyselBuzdolabi8,
        content: `Dondurucu Solda`,
        value: bireyselBuzdolabi8Value,
        to: 'finalPage'
    },
    {
        id: '3064',
        question: questionBireyselBuzdolabi8,
        content: `Dondurucu Üstte`,
        value: bireyselBuzdolabi8Value,
        to: 'finalPage'
    },
    {
        id: '3065',
        question: questionBireyselBuzdolabi8,
        content: `Farketmez`,
        value: null,
        to: 'finalPage'
    }
];

//QUESTIONS 
//Bireysel-Derin Dondurucu
var bireyselDerinDondurucu1Value = 'Genişlik (Derin Dondurucu)'
export const bireyselDerinDondurucu1 = [
    {
        id: '3066',
        question: questionBireyselDerinDondurucu1,
        content: `50 cm - 74 cm arasında`,
        value: bireyselDerinDondurucu1Value,
        to: 'bireyselDerinDondurucu2'
    },
    {
        id: '3067',
        question: questionBireyselDerinDondurucu1,
        content: `75 cm - 160 cm arasında`,
        value: bireyselDerinDondurucu1Value,
        to: 'bireyselDerinDondurucu2'
    },
    {
        id: '3068',
        question: questionBireyselDerinDondurucu1,
        content: `Farketmez`,
        value: null,
        to: 'bireyselDerinDondurucu2'
    },
];
var bireyselDerinDondurucu2Value = 'Yükseklik (Derin Dondurucu)'
export const bireyselDerinDondurucu2 = [
    {
        id: '3069',
        question: questionBireyselDerinDondurucu2,
        content: `75 cm - 149 cm arasında`,
        value: bireyselDerinDondurucu2Value,
        to: 'bireyselDerinDondurucu3'
    },  
    {
        id: '3070',
        question: questionBireyselDerinDondurucu2,
        content: `150 cm - 200 cm arasında`,
        value: bireyselDerinDondurucu2Value,
        to: 'bireyselDerinDondurucu3'
    }, 
    {
        id: '3071',
        question: questionBireyselDerinDondurucu2,
        content: `Farketmez`,
        value: null,
        to: 'bireyselDerinDondurucu3'
    },   
];
var bireyselDerinDondurucu3Value = 'Derinlik (Derin Dondurucu)'
export const bireyselDerinDondurucu3 = [
    {
        id: '3072',
        question: questionBireyselDerinDondurucu3,
        content: `50 cm - 64 cm arasında`,
        value: bireyselDerinDondurucu3Value,
        to: 'bireyselDerinDondurucu4'
    },
    {
        id: '3073',
        question: questionBireyselDerinDondurucu3,
        content: `65 cm - 80 cm arasında`,
        value: bireyselDerinDondurucu3Value,
        to: 'bireyselDerinDondurucu4'
    },
    {
        id: '3074',
        question: questionBireyselDerinDondurucu3,
        content: `Farketmez`,
        value: null,
        to: 'bireyselDerinDondurucu4'
    },
];
var bireyselDerinDondurucu4Value = 'Toplam Brüt Hacim (Derin Dondurucu)'
export const bireyselDerinDondurucu4 = [
    {
        id: '3075',
        question: questionBireyselDerinDondurucu4,
        content: `100 L - 250 L arasında`,
        value: bireyselDerinDondurucu4Value,
        to: 'bireyselDerinDondurucu5'
    },
    {
        id: '3076',
        question: questionBireyselDerinDondurucu4,
        content: `251 L - 750 L arasında`,
        value: bireyselDerinDondurucu4Value,
        to: 'bireyselDerinDondurucu5'
    },
    {
        id: '3077',
        question: questionBireyselDerinDondurucu4,
        content: `Farketmez`,
        value: null,
        to: 'bireyselDerinDondurucu5'
    },
];
var bireyselDerinDondurucu5Value = 'Ürün Rengi'
export const bireyselDerinDondurucu5 = [
    {
        id: '3078',
        question: questionBireyselDerinDondurucu5,
        content: `Beyaz`,
        value: bireyselDerinDondurucu5Value,
        to: 'bireyselDerinDondurucu6'
    },
    {
        id: '3079',
        question: questionBireyselDerinDondurucu5,
        content: `Gümüş`,
        value: bireyselDerinDondurucu5Value,
        to: 'bireyselDerinDondurucu6'
    },
    {
        id: '3080',
        question: questionBireyselDerinDondurucu5,
        content: `Leke Tutmayan İnoks`,
        value: bireyselDerinDondurucu5Value,
        to: 'bireyselDerinDondurucu6'
    },
    {
        id: '3081',
        question: questionBireyselDerinDondurucu5,
        content: `Farketmez`,
        value: null,
        to: 'bireyselDerinDondurucu6'
    },
];
var bireyselDerinDondurucu6Value = 'Enerji Sınıfı'
export const bireyselDerinDondurucu6 = [
    {
        id: '3082',
        question: questionBireyselDerinDondurucu6,
        content: `A+++`,
        value: bireyselDerinDondurucu6Value,
        to: 'bireyselDerinDondurucu7'
    },
    {
        id: '3083',
        question: questionBireyselDerinDondurucu6,
        content: `A++`,
        value: bireyselDerinDondurucu6Value,
        to: 'bireyselDerinDondurucu7'
    },
    {
        id: '3084',
        question: questionBireyselDerinDondurucu6,
        content: `A+`,
        value: bireyselDerinDondurucu6Value,
        to: 'bireyselDerinDondurucu7'
    },
    {
        id: '3085',
        question: questionBireyselDerinDondurucu6,
        content: `Farketmez`,
        value: null,
        to: 'bireyselDerinDondurucu7'
    },
];
var bireyselDerinDondurucu7Value = 'Dondurucu Yeri'
export const bireyselDerinDondurucu7 = [
    {
        id: '3086',
        question: questionBireyselDerinDondurucu7,
        content: `Dikey Dondurucu`,
        value: bireyselDerinDondurucu7Value,
        to: 'finalPage'
    },
    {
        id: '3087',
        question: questionBireyselDerinDondurucu7,
        content: `Yatay Dondurucu`,
        value: bireyselDerinDondurucu7Value,
        to: 'finalPage'
    },
    {
        id: '3088',
        question: questionBireyselDerinDondurucu7,
        content: `Joker Dondurucu`,
        value: bireyselDerinDondurucu7Value,
        to: 'finalPage'
    },
    {
        id: '3089',
        question: questionBireyselDerinDondurucu7,
        content: `Farketmez`,
        value: null,
        to: 'finalPage'
    },

//QUESTIONS 
//Bireysel-Çamaşır Makinesi
];
var bireyselCamasirMakinesi1Value = 'Kapasite'
export const bireyselCamasirMakinesi1 = [
    {
        id: '3090',
        question: questionBireyselCamasirMakinesi1,
        content: `5 kg`,
        value: bireyselCamasirMakinesi1Value,
        to: 'bireyselCamasirMakinesi2'
    },
    {
        id: '3091',
        question: questionBireyselCamasirMakinesi1,
        content: `6 kg`,
        value: bireyselCamasirMakinesi1Value,
        to: 'bireyselCamasirMakinesi2'
    },
    {
        id: '3092',
        question: questionBireyselCamasirMakinesi1,
        content: `7 kg`,
        value: bireyselCamasirMakinesi1Value,
        to: 'bireyselCamasirMakinesi2'
    },
    {
        id: '3093',
        question: questionBireyselCamasirMakinesi1,
        content: `8 kg`,
        value: bireyselCamasirMakinesi1Value,
        to: 'bireyselCamasirMakinesi2'
    },
    {
        id: '3094',
        question: questionBireyselCamasirMakinesi1,
        content: `9 kg`,
        value: bireyselCamasirMakinesi1Value,
        to: 'bireyselCamasirMakinesi2'
    },
    {
        id: '3095',
        question: questionBireyselCamasirMakinesi1,
        content: `10 kg`,
        value: bireyselCamasirMakinesi1Value,
        to: 'bireyselCamasirMakinesi2'
    },
    {
        id: '3096',
        question: questionBireyselCamasirMakinesi1,
        content: `12 kg`,
        value: bireyselCamasirMakinesi1Value,
        to: 'bireyselCamasirMakinesi2'
    },
    {
        id: '3097',
        question: questionBireyselCamasirMakinesi1,
        content: `Farketmez`,
        value: null,
        to: 'bireyselCamasirMakinesi2'
    },
];
var bireyselCamasirMakinesi2Value = 'Ürün Rengi'
export const bireyselCamasirMakinesi2 = [
    {
        id: '3098',
        question: questionBireyselCamasirMakinesi2,
        content: `Antrasit`,
        value: bireyselCamasirMakinesi2Value,
        to: 'bireyselCamasirMakinesi3'
    },
    {
        id: '3099',
        question: questionBireyselCamasirMakinesi2,
        content: `Beyaz`,
        value: bireyselCamasirMakinesi2Value,
        to: 'bireyselCamasirMakinesi3'
    },
    {
        id: '3100',
        question: questionBireyselCamasirMakinesi2,
        content: `Bordo`,
        value: bireyselCamasirMakinesi2Value,
        to: 'bireyselCamasirMakinesi3'
    },
    {
        id: '3101',
        question: questionBireyselCamasirMakinesi2,
        content: `Gri`,
        value: bireyselCamasirMakinesi2Value,
        to: 'bireyselCamasirMakinesi3'
    },
    {
        id: '3102',
        question: questionBireyselCamasirMakinesi2,
        content: `Siyah`,
        value: bireyselCamasirMakinesi2Value,
        to: 'bireyselCamasirMakinesi3'
    },
    {
        id: '3103',
        question: questionBireyselCamasirMakinesi2,
        content: `Farketmez`,
        value: null,
        to: 'bireyselCamasirMakinesi3'
    },
];
var bireyselCamasirMakinesi3Value = 'Enerji Sınıfı'
export const bireyselCamasirMakinesi3 = [
    {
        id: '3104',
        question: questionBireyselCamasirMakinesi3,
        content: `A++`,
        value: bireyselCamasirMakinesi3Value,
        to: 'bireyselCamasirMakinesi4'
    },
    {
        id: '3105',
        question: questionBireyselCamasirMakinesi3,
        content: `A++`,
        value: bireyselCamasirMakinesi3Value,
        to: 'bireyselCamasirMakinesi4'
    },
    {
        id: '3106',
        question: questionBireyselCamasirMakinesi3,
        content: `A+`,
        value: bireyselCamasirMakinesi3Value,
        to: 'bireyselCamasirMakinesi4'
    },
    {
        id: '3107',
        question: questionBireyselCamasirMakinesi3,
        content: `Farketmez`,
        value: null,
        to: 'bireyselCamasirMakinesi4'
    },
];
var bireyselCamasirMakinesi4Value = 'Maksimum Sıkma Devri'
export const bireyselCamasirMakinesi4 = [
    {
        id: '3108',
        question: questionBireyselCamasirMakinesi4,
        content: `600 d/dk`,
        value: bireyselCamasirMakinesi4Value,
        to: 'finalPage'
    },
    {
        id: '3109',
        question: questionBireyselCamasirMakinesi4,
        content: `800 d/dk`,
        value: bireyselCamasirMakinesi4Value,
        to: 'finalPage'
    },
    {
        id: '3110',
        question: questionBireyselCamasirMakinesi4,
        content: `1000 d/dk`,
        value: bireyselCamasirMakinesi4Value,
        to: 'finalPage'
    },
    {
        id: '3111',
        question: questionBireyselCamasirMakinesi4,
        content: `1200 d/dk`,
        value: bireyselCamasirMakinesi4Value,
        to: 'finalPage'
    },
    {
        id: '3112',
        question: questionBireyselCamasirMakinesi4,
        content: `1400 d/dk`,
        value: bireyselCamasirMakinesi4Value,
        to: 'finalPage'
    },
    {
        id: '3113',
        question: questionBireyselCamasirMakinesi4,
        content: `Farketmez`,
        value: null,
        to: 'finalPage'
    },

//QUESTIONS 
//Bireysel-Bulaşık Makinesi
];
var bireyselBulasikMakinesi1Value = 'Ürün Rengi'
export const bireyselBulasikMakinesi1 = [
    {
        id: '3114',
        question: questionBireyselBulasikMakinesi1,
        content: `Beyaz`,
        value: bireyselBulasikMakinesi1Value,
        to: 'bireyselBulasikMakinesi2'
    },
    {
        id: '3115',
        question: questionBireyselBulasikMakinesi1,
        content: `Beyaz Cam`,
        value: bireyselBulasikMakinesi1Value,
        to: 'bireyselBulasikMakinesi2'
    },
    {
        id: '3116',
        question: questionBireyselBulasikMakinesi1,
        content: `Gümüş`,
        value: bireyselBulasikMakinesi1Value,
        to: 'bireyselBulasikMakinesi2'
    },
    {
        id: '3117',
        question: questionBireyselBulasikMakinesi1,
        content: `Siyah`,
        value: bireyselBulasikMakinesi1Value,
        to: 'bireyselBulasikMakinesi2'
    },
    {
        id: '3118',
        question: questionBireyselBulasikMakinesi1,
        content: `İnoks - Parmak İzi Bırakmayan`,
        value: bireyselBulasikMakinesi1Value,
        to: 'bireyselBulasikMakinesi2'
    },
    {
        id: '3119',
        question: questionBireyselBulasikMakinesi1,
        content: `Farketmez`,
        value: null,
        to: 'bireyselBulasikMakinesi2'
    },
];
var bireyselBulasikMakinesi2Value = 'Enerji Sınıfı'
export const bireyselBulasikMakinesi2 = [
    {
        id: '3120',
        question: questionBireyselBulasikMakinesi2,
        content: `A+++`,
        value: bireyselBulasikMakinesi2Value,
        to: 'bireyselBulasikMakinesi3'
    },
    {
        id: '3121',
        question: questionBireyselBulasikMakinesi2,
        content: `A++`,
        value: bireyselBulasikMakinesi2Value,
        to: 'bireyselBulasikMakinesi3'
    },
    {
        id: '3122',
        question: questionBireyselBulasikMakinesi2,
        content: `A+`,
        value: bireyselBulasikMakinesi2Value,
        to: 'bireyselBulasikMakinesi3'
    },
    {
        id: '3123',
        question: questionBireyselBulasikMakinesi2,
        content: `Farketmez`,
        value: null,
        to: 'bireyselBulasikMakinesi3'
    },
];
var bireyselBulasikMakinesi3Value = 'Program Sayısı'
export const bireyselBulasikMakinesi3 = [
    {
        id: '3124',
        question: questionBireyselBulasikMakinesi3,
        content: `2`,
        value: bireyselBulasikMakinesi3Value,
        to: 'finalPage'
    },
    {
        id: '3125',
        question: questionBireyselBulasikMakinesi3,
        content: `3`,
        value: bireyselBulasikMakinesi3Value,
        to: 'finalPage'
    },
    {
        id: '3126',
        question: questionBireyselBulasikMakinesi3,
        content: `4`,
        value: bireyselBulasikMakinesi3Value,
        to: 'finalPage'
    },
    {
        id: '3127',
        question: questionBireyselBulasikMakinesi3,
        content: `5`,
        value: bireyselBulasikMakinesi3Value,
        to: 'finalPage'
    },
    {
        id: '3128',
        question: questionBireyselBulasikMakinesi3,
        content: `6`,
        value: bireyselBulasikMakinesi3Value,
        to: 'finalPage'
    },
    {
        id: '3129',
        question: questionBireyselBulasikMakinesi3,
        content: `8`,
        value: bireyselBulasikMakinesi3Value,
        to: 'finalPage'
    },
    {
        id: '3130',
        question: questionBireyselBulasikMakinesi3,
        content: `10`,
        value: bireyselBulasikMakinesi3Value,
        to: 'finalPage'
    },
    {
        id: '3131',
        question: questionBireyselBulasikMakinesi3,
        content: `Farketmez`,
        value: null,
        to: 'finalPage'
    },
    
//QUESTIONS 
//Bireysel-Kurutma Makinesi    
];
var bireyselKurutmaMakinesi1Value = 'Ürün Rengi'
export const bireyselKurutmaMakinesi1 = [
    {
        id: '3132',
        question: questionBireyselKurutmaMakinesi1,
        content: `Beyaz`,
        value: bireyselKurutmaMakinesi1Value,
        to: 'bireyselKurutmaMakinesi2'
    },
    {
        id: '3133',
        question: questionBireyselKurutmaMakinesi1,
        content: `Bordo`,
        value: bireyselKurutmaMakinesi1Value,
        to: 'bireyselKurutmaMakinesi2'
    },
    {
        id: '3134',
        question: questionBireyselKurutmaMakinesi1,
        content: `Gri`,
        value: bireyselKurutmaMakinesi1Value,
        to: 'bireyselKurutmaMakinesi2'
    },
    {
        id: '3135',
        question: questionBireyselKurutmaMakinesi1,
        content: `Farketmez`,
        value: null,
        to: 'bireyselKurutmaMakinesi2'
    },
];
var bireyselKurutmaMakinesi2Value = 'Maksimum Kurutma Kapasitesi'
export const bireyselKurutmaMakinesi2 = [
    {
        id: '3136',
        question: questionBireyselKurutmaMakinesi2,
        content: `7 kg`,
        value: bireyselKurutmaMakinesi2Value,
        to: 'bireyselKurutmaMakinesi3'
    },
    {
        id: '3137',
        question: questionBireyselKurutmaMakinesi2,
        content: `8 kg`,
        value: bireyselKurutmaMakinesi2Value,
        to: 'bireyselKurutmaMakinesi3'
    },
    {
        id: '3138',
        question: questionBireyselKurutmaMakinesi2,
        content: `9 kg`,
        value: bireyselKurutmaMakinesi2Value,
        to: 'bireyselKurutmaMakinesi3'
    },
    {
        id: '3139',
        question: questionBireyselKurutmaMakinesi2,
        content: `Farketmez`,
        value: null,
        to: 'bireyselKurutmaMakinesi3'
    },

];
var bireyselKurutmaMakinesi3Value = 'Enerji Sınıfı'
export const bireyselKurutmaMakinesi3 = [
    {
        id: '3140',
        question: questionBireyselKurutmaMakinesi3,
        content: `A+++`,
        value: bireyselKurutmaMakinesi3Value,
        to: 'bireyselKurutmaMakinesi4'
    },
    {
        id: '3141',
        question: questionBireyselKurutmaMakinesi3,
        content: `A++`,
        value: bireyselKurutmaMakinesi3Value,
        to: 'bireyselKurutmaMakinesi4'
    },
    {
        id: '3142',
        question: questionBireyselKurutmaMakinesi3,
        content: `A+`,
        value: bireyselKurutmaMakinesi3Value,
        to: 'bireyselKurutmaMakinesi4'
    },
    {
        id: '3143',
        question: questionBireyselKurutmaMakinesi3,
        content: `B`,
        value: bireyselKurutmaMakinesi3Value,
        to: 'bireyselKurutmaMakinesi4'
    },
    {
        id: '3144',
        question: questionBireyselKurutmaMakinesi3,
        content: `Farketmez`,
        value: null,
        to: 'bireyselKurutmaMakinesi4'
    },

];
var bireyselKurutmaMakinesi4Value = 'Kurutma Teknolojisi'
export const bireyselKurutmaMakinesi4 = [
    {
        id: '3145',
        question: questionBireyselKurutmaMakinesi4,
        content: `Hibrit`,
        value: bireyselKurutmaMakinesi4Value,
        to: 'finalPage'
    },
    {
        id: '3146',
        question: questionBireyselKurutmaMakinesi4,
        content: `Yoğuşturmalı`,
        value: bireyselKurutmaMakinesi4Value,
        to: 'finalPage'
    },
    {
        id: '3147',
        question: questionBireyselKurutmaMakinesi4,
        content: `Yoğuşturmalı (Isı Pompalı)`,
        value: bireyselKurutmaMakinesi4Value,
        to: 'finalPage'
    },
    {
        id: '3148',
        question: questionBireyselKurutmaMakinesi4,
        content: `Farketmez`,
        value: null,
        to: 'finalPage'
    },

//QUESTIONS 
//Bireysel-Kurutmalı Çamaşır Makinesi       
];
var bireyselKurutmaliCamasirMakinesi1Value = 'Kapasite'
export const bireyselKurutmaliCamasirMakinesi1 = [
    {
        id: '3149',
        question: questionBireyselKurutmaliCamasirMakinesi1,
        content: `8 kg`,
        value: bireyselKurutmaliCamasirMakinesi1Value,
        to: 'bireyselKurutmaliCamasirMakinesi2'
    },
    {
        id: '3150',
        question: questionBireyselKurutmaliCamasirMakinesi1,
        content: `9 kg`,
        value: bireyselKurutmaliCamasirMakinesi1Value,
        to: 'bireyselKurutmaliCamasirMakinesi2'
    },
    {
        id: '3151',
        question: questionBireyselKurutmaliCamasirMakinesi1,
        content: `Farketmez`,
        value: null,
        to: 'bireyselKurutmaliCamasirMakinesi2'
    },
];
var bireyselKurutmaliCamasirMakinesi2Value = 'Kurutma Teknolojisi'
export const bireyselKurutmaliCamasirMakinesi2 = [
    {
        id: '3152',
        question: questionBireyselKurutmaliCamasirMakinesi2,
        content: `Hava Yoğuşturmalı`,
        value: bireyselKurutmaliCamasirMakinesi2Value,
        to: 'finalPage'
    },
    {
        id: '3153',
        question: questionBireyselKurutmaliCamasirMakinesi2,
        content: `Su Yoğuşturmalı`,
        value: bireyselKurutmaliCamasirMakinesi2Value,
        to: 'finalPage'
    },
    {
        id: '3154',
        question: questionBireyselKurutmaliCamasirMakinesi2,
        content: `Farketmez`,
        value: null,
        to: 'finalPage'
    },
//QUESTIONS 
//Bireysel-Fırın      
];
var bireyselFirin1Value = 'Fırın Kategorisi'
export const bireyselFirin1 = [
    {
        id: '3155',
        question: questionBireyselFirin1,
        content: `Ocaklı Fırın`,
        value: bireyselFirin1Value,
        to: 'bireyselFirin2'
    },
    {
        id: '3156',
        question: questionBireyselFirin1,
        content: `Set Üstü Fırın`,
        value: bireyselFirin1Value,
        to: 'bireyselFirin2'
    },
    {
        id: '3157',
        question: questionBireyselFirin1,
        content: `Kuzine Fırın`,
        value: bireyselFirin1Value,
        to: 'bireyselFirin2'
    },
    {
        id: '3158',
        question: questionBireyselFirin1,
        content: `Farketmez`,
        value: null,
        to: 'bireyselFirin2'
    },
];
var bireyselFirin2Value = 'Ürün Rengi'
export const bireyselFirin2 = [
    {
        id: '3159',
        question: questionBireyselFirin2,
        content: `Beyaz`,
        value: bireyselFirin2Value,
        to: 'bireyselFirin3'
    },
    {
        id: '3160',
        question: questionBireyselFirin2,
        content: `Gümüş`,
        value: bireyselFirin2Value,
        to: 'bireyselFirin3'
    },
    {
        id: '3161',
        question: questionBireyselFirin2,
        content: `İnoks`,
        value: bireyselFirin2Value,
        to: 'bireyselFirin3'
    },
    {
        id: '3162',
        question: questionBireyselFirin2,
        content: `Farketmez`,
        value: null,
        to: 'bireyselFirin3'
    },
];
var bireyselFirin3Value = 'Ocak Tipi ve Göz Sayısı'
export const bireyselFirin3 = [
    {
        id: '3163',
        question: questionBireyselFirin3,
        content: `3 Gözü Gazlı + 1 Gözü Elektrikli`,
        value: bireyselFirin3Value,
        to: 'bireyselFirin4'
    },
    {
        id: '3164',
        question: questionBireyselFirin3,
        content: `4 Gözü Gazlı`,
        value: bireyselFirin3Value,
        to: 'bireyselFirin4'
    },
    {
        id: '3165',
        question: questionBireyselFirin3,
        content: `4 Gözü Vitroseramik`,
        value: bireyselFirin3Value,
        to: 'bireyselFirin4'
    },
    {
        id: '3166',
        question: questionBireyselFirin3,
        content: `4 Gözü Gazlı (1'i WOK)`,
        value: bireyselFirin3Value,
        to: 'bireyselFirin4'
    },
    {
        id: '3167',
        question: questionBireyselFirin3,
        content: `5 Gözü Gazlı`,
        value: bireyselFirin3Value,
        to: 'bireyselFirin4'
    },
    {
        id: '3168',
        question: questionBireyselFirin3,
        content: `5 Gözü Gazlı + 1 Gözü Vitroseramik`,
        value: bireyselFirin3Value,
        to: 'bireyselFirin4'
    },
    {
        id: '3169',
        question: questionBireyselFirin3,
        content: `Farketmez`,
        value: null,
        to: 'bireyselFirin4'
    },
];
var bireyselFirin4Value = 'Fırın Tipi'
export const bireyselFirin4 = [
    {
        id: '3170',
        question: questionBireyselFirin4,
        content: `Multifonksiyon Fırın`,
        value: bireyselFirin4Value,
        to: 'bireyselFirin5'
    },
    {
        id: '3171',
        question: questionBireyselFirin4,
        content: `Turbo Fırın`,
        value: bireyselFirin4Value,
        to: 'bireyselFirin5'
    },
    {
        id: '3172',
        question: questionBireyselFirin4,
        content: `Farketmez`,
        value: null,
        to: 'bireyselFirin5'
    },
];
var bireyselFirin5Value = 'Pişirme Fonksiyon Sayısı'
export const bireyselFirin5 = [
    {
        id: '3173',
        question: questionBireyselFirin5,
        content: `7`,
        value: bireyselFirin5Value,
        to: 'bireyselFirin6'
    },
    {
        id: '3174',
        question: questionBireyselFirin5,
        content: `8`,
        value: bireyselFirin5Value,
        to: 'bireyselFirin6'
    },
    {
        id: '3175',
        question: questionBireyselFirin5,
        content: `Farketmez`,
        value: null,
        to: 'bireyselFirin6'
    },
];
var bireyselFirin6Value = 'Saat Tipi'
export const bireyselFirin6 = [
    {
        id: '3176',
        question: questionBireyselFirin6,
        content: `LED Ekran - Buton Kontrol`,
        value: bireyselFirin6Value,
        to: 'finalPage'
    },
    {
        id: '3177',
        question: questionBireyselFirin6,
        content: `LED Ekran - Dokunmatik`,
        value: bireyselFirin6Value,
        to: 'finalPage'
    },
    {
        id: '3178',
        question: questionBireyselFirin6,
        content: `Mekanik Programlanabilir Saat`,
        value: bireyselFirin6Value,
        to: 'finalPage'
    },
    {
        id: '3179',
        question: questionBireyselFirin6,
        content: `Farketmez`,
        value: null,
        to: 'finalPage'
    },

//QUESTIONS 
//Bireysel-Set Üstü Fırın  
];
var bireyselSetUstuFirin1Value = 'Ürün Rengi'
export const bireyselSetUstuFirin1 = [
    {
        id: '3180',
        question: questionBireyselSetUstuFirin1,
        content: `Beyaz`,
        value: bireyselSetUstuFirin1Value,
        to: 'bireyselSetUstuFirin2'
    },
    {
        id: '3181',
        question: questionBireyselSetUstuFirin1,
        content: `Siyah`,
        value: bireyselSetUstuFirin1Value,
        to: 'bireyselSetUstuFirin2'
    },
    {
        id: '3182',
        question: questionBireyselSetUstuFirin1,
        content: `İnoks`,
        value: bireyselSetUstuFirin1Value,
        to: 'bireyselSetUstuFirin2'
    },
    {
        id: '3183',
        question: questionBireyselSetUstuFirin1,
        content: `Farketmez`,
        value: null,
        to: 'bireyselSetUstuFirin2'
    },
];
var bireyselSetUstuFirin2Value = 'Pişirme Sistemi'
export const bireyselSetUstuFirin2 = [
    {
        id: '3184',
        question: questionBireyselSetUstuFirin2,
        content: `Izgaralı`,
        value: bireyselSetUstuFirin2Value,
        to: 'bireyselSetUstuFirin3'
    },
    {
        id: '3185',
        question: questionBireyselSetUstuFirin2,
        content: `Izgarasız`,
        value: bireyselSetUstuFirin2Value,
        to: 'bireyselSetUstuFirin3'
    },
    {
        id: '3186',
        question: questionBireyselSetUstuFirin2,
        content: `Farketmez`,
        value: null,
        to: 'bireyselSetUstuFirin3'
    },
];
var bireyselSetUstuFirin3Value = 'Pişirme Bölmesi Hacmi'
export const bireyselSetUstuFirin3 = [
    {
        id: '3187',
        question: questionBireyselSetUstuFirin3,
        content: `28 L`,
        value: bireyselSetUstuFirin3Value,
        to: 'finalPage'
    },
    {
        id: '3188',
        question: questionBireyselSetUstuFirin3,
        content: `40 L`,
        value: bireyselSetUstuFirin3Value,
        to: 'finalPage'
    },
    {
        id: '3189',
        question: questionBireyselSetUstuFirin3,
        content: `50 L`,
        value: bireyselSetUstuFirin3Value,
        to: 'finalPage'
    },
    {
        id: '3190',
        question: questionBireyselSetUstuFirin3,
        content: `Farketmez`,
        value: null,
        to: 'finalPage'
    },

//QUESTIONS 
//Bireysel-Mikrodalga Fırın
];
var bireyselMikrodalgaFirin1Value = 'Ürün Rengi'
export const bireyselMikrodalgaFirin1 = [
    {
        id: '3191',
        question: questionBireyselMikrodalgaFirin1,
        content: `Beyaz`,
        value: bireyselMikrodalgaFirin1Value,
        to: 'bireyselMikrodalgaFirin2'
    },
    {
        id: '3192',
        question: questionBireyselMikrodalgaFirin1,
        content: `Gümüş`,
        value: bireyselMikrodalgaFirin1Value,
        to: 'bireyselMikrodalgaFirin2'
    },
    {
        id: '3193',
        question: questionBireyselMikrodalgaFirin1,
        content: `Siyah`,
        value: bireyselMikrodalgaFirin1Value,
        to: 'bireyselMikrodalgaFirin2'
    },
    {
        id: '3194',
        question: questionBireyselMikrodalgaFirin1,
        content: `İnoks`,
        value: bireyselMikrodalgaFirin1Value,
        to: 'bireyselMikrodalgaFirin2'
    },
    {
        id: '3195',
        question: questionBireyselMikrodalgaFirin1,
        content: `Farketmez`,
        value: null,
        to: 'bireyselMikrodalgaFirin2'
    },
];
var bireyselMikrodalgaFirin2Value = 'Kontrol Tipi'
export const bireyselMikrodalgaFirin2 = [
    {
        id: '3196',
        question: questionBireyselMikrodalgaFirin2,
        content: `Elektronik`,
        value: bireyselMikrodalgaFirin2Value,
        to: 'finalPage'
    },
    {
        id: '3197',
        question: questionBireyselMikrodalgaFirin2,
        content: `Mekanik`,
        value: bireyselMikrodalgaFirin2Value,
        to: 'finalPage'
    },
    {
        id: '3198',
        question: questionBireyselMikrodalgaFirin2,
        content: `Farketmez`,
        value: null,
        to: 'finalPage'
    },

//QUESTIONS 
//Bireysel-Set Üstü Ocak  
];
var bireyselSetUstuOcak1Value = 'Ocak Tipi'
export const bireyselSetUstuOcak1 = [
    {
        id: '3199',
        question: questionBireyselSetUstuOcak1,
        content: `Gazlı Ocak`,
        value: bireyselSetUstuOcak1Value,
        to: 'bireyselSetUstuOcak2'
    },
    {
        id: '3200',
        question: questionBireyselSetUstuOcak1,
        content: `Elektrikli ve Gazlı Ocak`,
        value: bireyselSetUstuOcak1Value,
        to: 'bireyselSetUstuOcak2'
    },
    {
        id: '3201',
        question: questionBireyselSetUstuOcak1,
        content: `Farketmez`,
        value: null,
        to: 'bireyselSetUstuOcak2'
    },
];
var bireyselSetUstuOcak2Value = 'Ürün Rengi'
export const bireyselSetUstuOcak2 = [
    {
        id: '3202',
        question: questionBireyselSetUstuOcak2,
        content: `Beyaz`,
        value: bireyselSetUstuOcak2Value,
        to: 'bireyselSetUstuOcak3'
    },
    {
        id: '3203',
        question: questionBireyselSetUstuOcak2,
        content: `Gümüş`,
        value: bireyselSetUstuOcak2Value,
        to: 'bireyselSetUstuOcak3'
    },
    {
        id: '3204',
        question: questionBireyselSetUstuOcak2,
        content: `Siyah`,
        value: bireyselSetUstuOcak2Value,
        to: 'bireyselSetUstuOcak3'
    },
    {
        id: '3205',
        question: questionBireyselSetUstuOcak2,
        content: `İnoks`,
        value: bireyselSetUstuOcak2Value,
        to: 'bireyselSetUstuOcak3'
    },
    {
        id: '3206',
        question: questionBireyselSetUstuOcak2,
        content: `Farketmez`,
        value: null,
        to: 'bireyselSetUstuOcak3'
    },
];
var bireyselSetUstuOcak3Value = 'Ocak Yüzeyi'
export const bireyselSetUstuOcak3 = [
    {
        id: '3207',
        question: questionBireyselSetUstuOcak3,
        content: `Cam`,
        value: bireyselSetUstuOcak3Value,
        to: 'bireyselSetUstuOcak4'
    },
    {
        id: '3208',
        question: questionBireyselSetUstuOcak3,
        content: `Metal`,
        value: bireyselSetUstuOcak3Value,
        to: 'bireyselSetUstuOcak4'
    },
    {
        id: '3209',
        question: questionBireyselSetUstuOcak3,
        content: `Farketmez`,
        value: null,
        to: 'bireyselSetUstuOcak4'
    },
];
var bireyselSetUstuOcak4Value = 'Gaz Tipi'
export const bireyselSetUstuOcak4 = [
    {
        id: '3210',
        question: questionBireyselSetUstuOcak4,
        content: `Doğalgaz`,
        value: bireyselSetUstuOcak4Value,
        to: 'finalPage'
    },
    {
        id: '3211',
        question: questionBireyselSetUstuOcak4,
        content: `Lpg`,
        value: bireyselSetUstuOcak4Value,
        to: 'finalPage'
    },
    {
        id: '3212',
        question: questionBireyselSetUstuOcak4,
        content: `Farketmez`,
        value: null,
        to: 'finalPage'
    },

//QUESTIONS 
//Bireysel-Aspiratör   
];
var bireyselAspirator1Value = 'Ürün Rengi'
export const bireyselAspirator1 = [
    {
        id: '3213',
        question: questionBireyselAspirator1,
        content: `İnoks`,
        value: bireyselAspirator1Value,
        to: 'finalPage'
    },
    {
        id: '3214',
        question: questionBireyselAspirator1,
        content: `Siyah`,
        value: bireyselAspirator1Value,
        to: 'finalPage'
    },
    {
        id: '3215',
        question: questionBireyselAspirator1,
        content: `Beyaz`,
        value: bireyselAspirator1Value,
        to: 'finalPage'
    },
    {
        id: '3216',
        question: questionBireyselAspirator1,
        content: `Farketmez`,
        value: null,
        to: 'finalPage'
    },




//QUESTIONS 
//Bireysel-Su Arıtma Cihazı
];
var bireyselSuAritmaCihazi1Value = 'Filtre Adedi'
export const bireyselSuAritmaCihazi1 = [
    {
        id: '3217',
        question: questionBireyselSuAritmaCihazi1,
        content: `3`,
        value: bireyselSuAritmaCihazi1Value,
        to: 'bireyselSuAritmaCihazi2'
    },
    {
        id: '3218',
        question: questionBireyselSuAritmaCihazi1,
        content: `4`,
        value: bireyselSuAritmaCihazi1Value,
        to: 'bireyselSuAritmaCihazi2'
    },
    {
        id: '3219',
        question: questionBireyselSuAritmaCihazi1,
        content: `Farketmez`,
        value: null,
        to: 'bireyselSuAritmaCihazi2'
    },
];
var bireyselSuAritmaCihazi2Value = 'Filtre Adedi'
export const bireyselSuAritmaCihazi2 = [
    {
        id: '3220',
        question: questionBireyselSuAritmaCihazi2,
        content: `Sebil`,
        value: bireyselSuAritmaCihazi2Value,
        to: 'bireyselSuAritmaCihazi3'
    },
    {
        id: '3221',
        question: questionBireyselSuAritmaCihazi2,
        content: `Tezgahüstü`,
        value: bireyselSuAritmaCihazi2Value,
        to: 'bireyselSuAritmaCihazi3'
    },
    {
        id: '3222',
        question: questionBireyselSuAritmaCihazi2,
        content: `Tezgahaltı`,
        value: bireyselSuAritmaCihazi2Value,
        to: 'bireyselSuAritmaCihazi3'
    },
    {
        id: '3223',
        question: questionBireyselSuAritmaCihazi2,
        content: `Farketmez`,
        value: null,
        to: 'bireyselSuAritmaCihazi3'
    },
];
var bireyselSuAritmaCihazi3Value = 'Ek Fonksiyon'
export const bireyselSuAritmaCihazi3 = [
    {
        id: '3224',
        question: questionBireyselSuAritmaCihazi3,
        content: `Su Soğutma`,
        value: bireyselSuAritmaCihazi3Value,
        to: 'finalPage'
    },
    {
        id: '3225',
        question: questionBireyselSuAritmaCihazi3,
        content: `Su Isıtma`,
        value: bireyselSuAritmaCihazi3Value,
        to: 'finalPage'
    },
    {
        id: '3226',
        question: questionBireyselSuAritmaCihazi3,
        content: `Farketmez`,
        value: null,
        to: 'finalPage'
    },

//QUESTIONS 
//Bireysel-Ankastre Fırın
];
var bireyselAnkastreFirin1Value = 'Fırın Tipi'
export const bireyselAnkastreFirin1 = [
    {
        id: '3227',
        question: questionBireyselAnkastreFirin1,
        content: `Multifonksiyon Fırın`,
        value: bireyselAnkastreFirin1Value,
        to: 'bireyselAnkastreFirin2'
    },
    {
        id: '3228',
        question: questionBireyselAnkastreFirin1,
        content: `Turbo Fırın`,
        value: bireyselAnkastreFirin1Value,
        to: 'bireyselAnkastreFirin2'
    },
    {
        id: '3229',
        question: questionBireyselAnkastreFirin1,
        content: `Farketmez`,
        value: null,
        to: 'bireyselAnkastreFirin2'
    },
];
var bireyselAnkastreFirin2Value = 'Ürün Rengi'
export const bireyselAnkastreFirin2 = [
    {
        id: '3230',
        question: questionBireyselAnkastreFirin2,
        content: `Antrasit Siyah`,
        value: bireyselAnkastreFirin2Value,
        to: 'bireyselAnkastreFirin3'
    },
    {
        id: '3231',
        question: questionBireyselAnkastreFirin2,
        content: `Avena (Desenli Krem)`,
        value: bireyselAnkastreFirin2Value,
        to: 'bireyselAnkastreFirin3'
    },
    {
        id: '3232',
        question: questionBireyselAnkastreFirin2,
        content: `Beyaz`,
        value: bireyselAnkastreFirin2Value,
        to: 'bireyselAnkastreFirin3'
    },
    {
        id: '3233',
        question: questionBireyselAnkastreFirin2,
        content: `Extraclear Beyaz`,
        value: bireyselAnkastreFirin2Value,
        to: 'bireyselAnkastreFirin3'
    },
    {
        id: '3234',
        question: questionBireyselAnkastreFirin2,
        content: `Grion`,
        value: bireyselAnkastreFirin2Value,
        to: 'bireyselAnkastreFirin3'
    },
    {
        id: '3235',
        question: questionBireyselAnkastreFirin2,
        content: `Kahverengi`,
        value: bireyselAnkastreFirin2Value,
        to: 'bireyselAnkastreFirin3'
    },
    {
        id: '3236',
        question: questionBireyselAnkastreFirin2,
        content: `İnoks - Parmak İzi Bırakmayan`,
        value: bireyselAnkastreFirin2Value,
        to: 'bireyselAnkastreFirin3'
    },
    {
        id: '3237',
        question: questionBireyselAnkastreFirin2,
        content: `Siyah`,
        value: bireyselAnkastreFirin2Value,
        to: 'bireyselAnkastreFirin3'
    },
    {
        id: '3238',
        question: questionBireyselAnkastreFirin2,
        content: `Farketmez`,
        value: null,
        to: 'bireyselAnkastreFirin3'
    },
];
var bireyselAnkastreFirin3Value = 'Pişirme Fonksiyon Sayısı'
export const bireyselAnkastreFirin3 = [
    {
        id: '3239',
        question: questionBireyselAnkastreFirin3,
        content: `6 Fonksiyon`,
        value: bireyselAnkastreFirin3Value,
        to: 'bireyselAnkastreFirin4'
    },
    {
        id: '3240',
        question: questionBireyselAnkastreFirin3,
        content: `8 Fonksiyon`,
        value: bireyselAnkastreFirin3Value,
        to: 'bireyselAnkastreFirin4'
    },
    {
        id: '3241',
        question: questionBireyselAnkastreFirin3,
        content: `10 Fonksiyon`,
        value: bireyselAnkastreFirin3Value,
        to: 'bireyselAnkastreFirin4'
    },
    {
        id: '3242',
        question: questionBireyselAnkastreFirin3,
        content: `13 Fonksiyon`,
        value: bireyselAnkastreFirin3Value,
        to: 'bireyselAnkastreFirin4'
    },
    {
        id: '3243',
        question: questionBireyselAnkastreFirin3,
        content: `14 Fonksiyon`,
        value: bireyselAnkastreFirin3Value,
        to: 'bireyselAnkastreFirin4'
    },
    {
        id: '3244',
        question: questionBireyselAnkastreFirin3,
        content: `15 Fonksiyon`,
        value: bireyselAnkastreFirin3Value,
        to: 'bireyselAnkastreFirin4'
    },
    {
        id: '3245',
        question: questionBireyselAnkastreFirin3,
        content: `16 Fonksiyon  (Pyrolytic)`,
        value: bireyselAnkastreFirin3Value,
        to: 'bireyselAnkastreFirin4'
    },
    {
        id: '3246',
        question: questionBireyselAnkastreFirin3,
        content: `20 Fonksiyon  (Combi Steam)`,
        value: bireyselAnkastreFirin3Value,
        to: 'bireyselAnkastreFirin4'
    },
    {
        id: '3247',
        question: questionBireyselAnkastreFirin3,
        content: `Farketmez`,
        value: null,
        to: 'bireyselAnkastreFirin4'
    },
];
var bireyselAnkastreFirin4Value = 'Saat Tipi'
export const bireyselAnkastreFirin4 = [
    {
        id: '3248',
        question: questionBireyselAnkastreFirin4,
        content: `LCD – Ekran`,
        value: bireyselAnkastreFirin4Value,
        to: 'finalPage'
    },
    {
        id: '3249',
        question: questionBireyselAnkastreFirin4,
        content: `LCD – Ekran Dokunmatik`,
        value: bireyselAnkastreFirin4Value,
        to: 'finalPage'
    },
    {
        id: '3250',
        question: questionBireyselAnkastreFirin4,
        content: `LED – Ekran Dokunmatik`,
        value: bireyselAnkastreFirin4Value,
        to: 'finalPage'
    },
    {
        id: '3251',
        question: questionBireyselAnkastreFirin4,
        content: `Mekanik Programlanabilir Saat`,
        value: bireyselAnkastreFirin4Value,
        to: 'finalPage'
    },
    {
        id: '3252',
        question: questionBireyselAnkastreFirin4,
        content: `Rustik Saat`,
        value: bireyselAnkastreFirin4Value,
        to: 'finalPage'
    },
    {
        id: '3253',
        question: questionBireyselAnkastreFirin4,
        content: `TFT Ekran – Dokunmatik`,
        value: bireyselAnkastreFirin4Value,
        to: 'finalPage'
    },
    {
        id: '3254',
        question: questionBireyselAnkastreFirin4,
        content: `Farketmez`,
        value: null,
        to: 'finalPage'
    },

//QUESTIONS 
//Bireysel-Ankastre Mikrodalga Fırın
];
var bireyselAnkastreMikrodalgaFirin1Value = 'Ürün Rengi'
export const bireyselAnkastreMikrodalgaFirin1 = [
    {
        id: '3255',
        question: questionBireyselAnkastreMikrodalgaFirin1,
        content: `Beyaz`,
        value: bireyselAnkastreMikrodalgaFirin1Value,
        to: 'bireyselAnkastreMikrodalgaFirin2'
    },
    {
        id: '3256',
        question: questionBireyselAnkastreMikrodalgaFirin1,
        content: `İnoks`,
        value: bireyselAnkastreMikrodalgaFirin1Value,
        to: 'bireyselAnkastreMikrodalgaFirin2'
    },
    {
        id: '3257',
        question: questionBireyselAnkastreMikrodalgaFirin1,
        content: `Siyah`,
        value: bireyselAnkastreMikrodalgaFirin1Value,
        to: 'bireyselAnkastreMikrodalgaFirin2'
    },
    {
        id: '3258',
        question: questionBireyselAnkastreMikrodalgaFirin1,
        content: `Farketmez`,
        value: null,
        to: 'bireyselAnkastreMikrodalgaFirin2'
    },
];
var bireyselAnkastreMikrodalgaFirin2Value = 'Kontrol Tipi'
export const bireyselAnkastreMikrodalgaFirin2 = [
    {
        id: '3259',
        question: questionBireyselAnkastreMikrodalgaFirin2,
        content: `Elektronik`,
        value: bireyselAnkastreMikrodalgaFirin2Value,
        to: 'finalPage'
    },
    {
        id: '3260',
        question: questionBireyselAnkastreMikrodalgaFirin2,
        content: `Farketmez`,
        value: null,
        to: 'finalPage'
    },

//QUESTIONS 
//Bireysel-Ankastre Ocak
];
var bireyselAnkastreOcak1Value = 'Ürün Rengi'
export const bireyselAnkastreOcak1 = [
    {
        id: '3261',
        question: questionBireyselAnkastreOcak1,
        content: `Antrasit`,
        value: bireyselAnkastreOcak1Value,
        to: 'bireyselAnkastreOcak2'
    },
    {
        id: '3262',
        question: questionBireyselAnkastreOcak1,
        content: `Avena`,
        value: bireyselAnkastreOcak1Value,
        to: 'bireyselAnkastreOcak2'
    },
    {
        id: '3263',
        question: questionBireyselAnkastreOcak1,
        content: `Beyaz`,
        value: bireyselAnkastreOcak1Value,
        to: 'bireyselAnkastreOcak2'
    },
    {
        id: '3264',
        question: questionBireyselAnkastreOcak1,
        content: `Beyaz Cam`,
        value: bireyselAnkastreOcak1Value,
        to: 'bireyselAnkastreOcak2'
    },
    {
        id: '3265',
        question: questionBireyselAnkastreOcak1,
        content: `Kahverengi`,
        value: bireyselAnkastreOcak1Value,
        to: 'bireyselAnkastreOcak2'
    },
    {
        id: '3266',
        question: questionBireyselAnkastreOcak1,
        content: `Siyah`,
        value: bireyselAnkastreOcak1Value,
        to: 'bireyselAnkastreOcak2'
    },
    {
        id: '3267',
        question: questionBireyselAnkastreOcak1,
        content: `Zara Gri`,
        value: bireyselAnkastreOcak1Value,
        to: 'bireyselAnkastreOcak2'
    },
    {
        id: '3268',
        question: questionBireyselAnkastreOcak1,
        content: `İnoks`,
        value: bireyselAnkastreOcak1Value,
        to: 'bireyselAnkastreOcak2'
    },
    {
        id: '3269',
        question: questionBireyselAnkastreOcak1,
        content: `Farketmez`,
        value: null,
        to: 'bireyselAnkastreOcak2'
    },

];
var bireyselAnkastreOcak2Value = 'Ocak Tipi'
export const bireyselAnkastreOcak2 = [
    {
        id: '3270',
        question: questionBireyselAnkastreOcak2,
        content: `Gazlı Ocak`,
        value: bireyselAnkastreOcak2Value,
        to: 'bireyselAnkastreOcak3'
    },
    {
        id: '3271',
        question: questionBireyselAnkastreOcak2,
        content: `Elektrikli Ocak`,
        value: bireyselAnkastreOcak2Value,
        to: 'bireyselAnkastreOcak3'
    },
    {
        id: '3272',
        question: questionBireyselAnkastreOcak2,
        content: `Elektrikli ve Gazlı Ocak`,
        value: bireyselAnkastreOcak2Value,
        to: 'bireyselAnkastreOcak3'
    },
    {
        id: '3273',
        question: questionBireyselAnkastreOcak2,
        content: `Vitroseramik Ocak`,
        value: bireyselAnkastreOcak2Value,
        to: 'bireyselAnkastreOcak3'
    },
    {
        id: '3274',
        question: questionBireyselAnkastreOcak2,
        content: `İndüksiyonlu Ocak`,
        value: bireyselAnkastreOcak2Value,
        to: 'bireyselAnkastreOcak3'
    },
    {
        id: '3275',
        question: questionBireyselAnkastreOcak2,
        content: `Farketmez`,
        value: null,
        to: 'bireyselAnkastreOcak3'
    },

];
var bireyselAnkastreOcak3Value = 'Gaz Tipi'
export const bireyselAnkastreOcak3 = [
    {
        id: '3276',
        question: questionBireyselAnkastreOcak3,
        content: `Doğalgaz`,
        value: bireyselAnkastreOcak3Value,
        to: 'finalPage'
    },
    {
        id: '3277',
        question: questionBireyselAnkastreOcak3,
        content: `Lpg`,
        value: bireyselAnkastreOcak3Value,
        to: 'finalPage'
    },
    {
        id: '3278',
        question: questionBireyselAnkastreOcak3,
        content: `Farketmez`,
        value: null,
        to: 'finalPage'
    },


//QUESTIONS 
//Bireysel-Ankastre Davlumbaz
];
var bireyselAnkastreDavlumbaz1Value = 'Davlumbaz Genişliği'
export const bireyselAnkastreDavlumbaz1 = [
    {
        id: '3279',
        question: questionBireyselAnkastreDavlumbaz1,
        content: `40 cm`,
        value: bireyselAnkastreDavlumbaz1Value,
        to: 'bireyselAnkastreDavlumbaz2'
    },
    {
        id: '3280',
        question: questionBireyselAnkastreDavlumbaz1,
        content: `60 cm`,
        value: bireyselAnkastreDavlumbaz1Value,
        to: 'bireyselAnkastreDavlumbaz2'
    },
    {
        id: '3281',
        question: questionBireyselAnkastreDavlumbaz1,
        content: `80 cm`,
        value: bireyselAnkastreDavlumbaz1Value,
        to: 'bireyselAnkastreDavlumbaz2'
    },
    {
        id: '3282',
        question: questionBireyselAnkastreDavlumbaz1,
        content: `90 cm`,
        value: bireyselAnkastreDavlumbaz1Value,
        to: 'bireyselAnkastreDavlumbaz2'
    },
    {
        id: '3283',
        question: questionBireyselAnkastreDavlumbaz1,
        content: `110 cm`,
        value: bireyselAnkastreDavlumbaz1Value,
        to: 'bireyselAnkastreDavlumbaz2'
    },
    {
        id: '3284',
        question: questionBireyselAnkastreDavlumbaz1,
        content: `Farketmez`,
        value: null,
        to: 'bireyselAnkastreDavlumbaz2'
    },
];
var bireyselAnkastreDavlumbaz2Value = 'Ürün Tipi'
export const bireyselAnkastreDavlumbaz2 = [
    {
        id: '3285',
        question: questionBireyselAnkastreDavlumbaz2,
        content: `Ada Tipi Davlumbaz`,
        value: bireyselAnkastreDavlumbaz2Value,
        to: 'finalPage'
    },
    {
        id: '3286',
        question: questionBireyselAnkastreDavlumbaz2,
        content: `Duvar Tipi - Bükük Camlı T Tipi`,
        value: bireyselAnkastreDavlumbaz2Value,
        to: 'finalPage'
    },
    {
        id: '3287',
        question: questionBireyselAnkastreDavlumbaz2,
        content: `Duvar Tipi - Düz Camlı T Tipi`,
        value: bireyselAnkastreDavlumbaz2Value,
        to: 'finalPage'
    },
    {
        id: '3288',
        question: questionBireyselAnkastreDavlumbaz2,
        content: `Duvar tipi - Piramit`,
        value: bireyselAnkastreDavlumbaz2Value,
        to: 'finalPage'
    },
    {
        id: '3289',
        question: questionBireyselAnkastreDavlumbaz2,
        content: `Duvar tipi Camsız T Tipi`,
        value: bireyselAnkastreDavlumbaz2Value,
        to: 'finalPage'
    },
    {
        id: '3290',
        question: questionBireyselAnkastreDavlumbaz2,
        content: `Duvar tipi Eğimli`,
        value: bireyselAnkastreDavlumbaz2Value,
        to: 'finalPage'
    },
    {
        id: '3291',
        question: questionBireyselAnkastreDavlumbaz2,
        content: `Farketmez`,
        value: null,
        to: 'finalPage'
    },

//QUESTIONS 
//Bireysel-Ankastre Bulaşık Makinesi
];
var bireyselAnkastreBulasikMakinesi1Value = 'Ürün Rengi'
export const bireyselAnkastreBulasikMakinesi1 = [
    {
        id: '3292',
        question: questionBireyselAnkastreBulasikMakinesi1,
        content: `Beyaz`,
        value: bireyselAnkastreBulasikMakinesi1Value,
        to: 'bireyselAnkastreBulasikMakinesi2'
    },
    {
        id: '3293',
        question: questionBireyselAnkastreBulasikMakinesi1,
        content: `Beyaz Cam`,
        value: bireyselAnkastreBulasikMakinesi1Value,
        to: 'bireyselAnkastreBulasikMakinesi2'
    },
    {
        id: '3294',
        question: questionBireyselAnkastreBulasikMakinesi1,
        content: `Gri`,
        value: bireyselAnkastreBulasikMakinesi1Value,
        to: 'bireyselAnkastreBulasikMakinesi2'
    },
    {
        id: '3295',
        question: questionBireyselAnkastreBulasikMakinesi1,
        content: `Piyano Siya`,
        value: bireyselAnkastreBulasikMakinesi1Value,
        to: 'bireyselAnkastreBulasikMakinesi2'
    },
    {
        id: '3296',
        question: questionBireyselAnkastreBulasikMakinesi1,
        content: `Siyah Cam`,
        value: bireyselAnkastreBulasikMakinesi1Value,
        to: 'bireyselAnkastreBulasikMakinesi2'
    },
    {
        id: '3297',
        question: questionBireyselAnkastreBulasikMakinesi1,
        content: `Vizon`,
        value: bireyselAnkastreBulasikMakinesi1Value,
        to: 'bireyselAnkastreBulasikMakinesi2'
    },
    {
        id: '3298',
        question: questionBireyselAnkastreBulasikMakinesi1,
        content: `İnoks - Parmak İzi Bırakmayan`,
        value: bireyselAnkastreBulasikMakinesi1Value,
        to: 'bireyselAnkastreBulasikMakinesi2'
    },
    {
        id: '3299',
        question: questionBireyselAnkastreBulasikMakinesi1,
        content: `Farketmez`,
        value: null,
        to: 'bireyselAnkastreBulasikMakinesi2'
    },
];
var bireyselAnkastreBulasikMakinesi2Value = 'Program Sayısı'
export const bireyselAnkastreBulasikMakinesi2 = [
    {
        id: '3300',
        question: questionBireyselAnkastreBulasikMakinesi2,
        content: `4`,
        value: bireyselAnkastreBulasikMakinesi2Value,
        to: 'finalPage'
    },
    {
        id: '3301',
        question: questionBireyselAnkastreBulasikMakinesi2,
        content: `6`,
        value: bireyselAnkastreBulasikMakinesi2Value,
        to: 'finalPage'
    },
    {
        id: '3302',
        question: questionBireyselAnkastreBulasikMakinesi2,
        content: `8`,
        value: bireyselAnkastreBulasikMakinesi2Value,
        to: 'finalPage'
    },
    {
        id: '3303',
        question: questionBireyselAnkastreBulasikMakinesi2,
        content: `9`,
        value: bireyselAnkastreBulasikMakinesi2Value,
        to: 'finalPage'
    },
    {
        id: '3304',
        question: questionBireyselAnkastreBulasikMakinesi2,
        content: `10`,
        value: bireyselAnkastreBulasikMakinesi2Value,
        to: 'finalPage'
    },
    {
        id: '3305',
        question: questionBireyselAnkastreBulasikMakinesi2,
        content: `Farketmez`,
        value: null,
        to: 'finalPage'
    },

//QUESTIONS 
//Bireysel-Ankastre Buzdolabı
];
var bireyselAnkastreBuzdolabi1Value = 'Ürün Rengi'
export const bireyselAnkastreBuzdolabi1 = [
    {
        id: '3306',
        question: questionBireyselAnkastreBuzdolabi1,
        content: `Beyaz`,
        value: bireyselAnkastreBuzdolabi1Value,
        to: 'bireyselAnkastreBuzdolabi2'
    },
    {
        id: '3307',
        question: questionBireyselAnkastreBuzdolabi1,
        content: `Leke Tutmayan İnoks`,
        value: bireyselAnkastreBuzdolabi1Value,
        to: 'bireyselAnkastreBuzdolabi2'
    },
    {
        id: '3308',
        question: questionBireyselAnkastreBuzdolabi1,
        content: `Farketmez`,
        value: null,
        to: 'bireyselAnkastreBuzdolabi2'
    },
];
var bireyselAnkastreBuzdolabi2Value = 'Toplam Brüt Hacim (Ankastre Buzdolabı)'
export const bireyselAnkastreBuzdolabi2 = [
    {
        id: '3309',
        question: questionBireyselAnkastreBuzdolabi2,
        content: `130 L`,
        value: bireyselAnkastreBuzdolabi2Value,
        to: 'bireyselAnkastreBuzdolabi3'
    },
    {
        id: '3310',
        question: questionBireyselAnkastreBuzdolabi2,
        content: `275 L`,
        value: bireyselAnkastreBuzdolabi2Value,
        to: 'bireyselAnkastreBuzdolabi3'
    },
    {
        id: '3311',
        question: questionBireyselAnkastreBuzdolabi2,
        content: `300 L`,
        value: bireyselAnkastreBuzdolabi2Value,
        to: 'bireyselAnkastreBuzdolabi3'
    },
    {
        id: '3312',
        question: questionBireyselAnkastreBuzdolabi2,
        content: `315 L`,
        value: bireyselAnkastreBuzdolabi2Value,
        to: 'bireyselAnkastreBuzdolabi3'
    },
    {
        id: '3314',
        question: questionBireyselAnkastreBuzdolabi2,
        content: `540 L`,
        value: bireyselAnkastreBuzdolabi2Value,
        to: 'bireyselAnkastreBuzdolabi3'
    },
    {
        id: '3315',
        question: questionBireyselAnkastreBuzdolabi2,
        content: `Farketmez`,
        value: null,
        to: 'bireyselAnkastreBuzdolabi3'
    },
];
var bireyselAnkastreBuzdolabi3Value = 'Dondurucu Yeri'
export const bireyselAnkastreBuzdolabi3 = [
    {
        id: '3316',
        question: questionBireyselAnkastreBuzdolabi3,
        content: `Dondurucu Altta`,
        value: bireyselAnkastreBuzdolabi3Value,
        to: 'finalPage'
    },
    {
        id: '3317',
        question: questionBireyselAnkastreBuzdolabi3,
        content: `Farketmez`,
        value: null,
        to: 'finalPage'
    },

//QUESTIONS 
//Bireysel-Ankastre Aspiratör
];
var bireyselAnkastreAspirator1Value = 'Davlumbaz Genişliği'
export const bireyselAnkastreAspirator1 = [
    {
        id: '3318',
        question: questionBireyselAnkastreAspirator1,
        content: `50 cm`,
        value: bireyselAnkastreAspirator1Value,
        to: 'bireyselAnkastreAspirator2'
    },
    {
        id: '3319',
        question: questionBireyselAnkastreAspirator1,
        content: `60 cm`,
        value: bireyselAnkastreAspirator1Value,
        to: 'bireyselAnkastreAspirator2'
    },
    {
        id: '3320',
        question: questionBireyselAnkastreAspirator1,
        content: `70 cm`,
        value: bireyselAnkastreAspirator1Value,
        to: 'bireyselAnkastreAspirator2'
    },
    {
        id: '3321',
        question: questionBireyselAnkastreAspirator1,
        content: `Farketmez`,
        value: null,
        to: 'bireyselAnkastreAspirator2'
    },
];
var bireyselAnkastreAspirator2Value = 'Ürün Tipi'
export const bireyselAnkastreAspirator2 = [
    {
        id: '3322',
        question: questionBireyselAnkastreAspirator2,
        content: `Gömme Aspiratör`,
        value: bireyselAnkastreAspirator2Value,
        to: 'finalPage'
    },
    {
        id: '3323',
        question: questionBireyselAnkastreAspirator2,
        content: `Standart Aspiratör`,
        value: bireyselAnkastreAspirator2Value,
        to: 'finalPage'
    },
    {
        id: '3324',
        question: questionBireyselAnkastreAspirator2,
        content: `Sürgülü Aspiratör`,
        value: bireyselAnkastreAspirator2Value,
        to: 'finalPage'
    },
    {
        id: '3325',
        question: questionBireyselAnkastreAspirator2,
        content: `Farketmez`,
        value: null,
        to: 'finalPage'
    },

//QUESTIONS 
//Bireysel-Ankastre Sıcak Tutma Makinesi
];
var bireyselAnkastreSicakTutmaMakinesi1Value = 'Ürün Rengi'
export const bireyselAnkastreSicakTutmaMakinesi1 = [
    {
        id: '3326',
        question: questionBireyselAnkastreSicakTutmaMakinesi1,
        content: `Beyaz`,
        value: bireyselAnkastreSicakTutmaMakinesi1Value,
        to: 'finalPage'
    },
    {
        id: '3327',
        question: questionBireyselAnkastreSicakTutmaMakinesi1,
        content: `Siyah`,
        to: 'finalPage'
    },
    {
        id: '3328',
        question: questionBireyselAnkastreSicakTutmaMakinesi1,
        content: `Farketmez`,
        value: null,
        to: 'finalPage'
    },

//QUESTIONS 
//Bireysel-Ankastre Çamaşır Makinesi
];
var bireyselAnkastreCamasirMakinesi1Value = '-'
export const bireyselAnkastreCamasirMakinesi1 = [
    {
        id: '3329',
        question: questionBireyselAnkastreCamasirMakinesi1,
        content: `-`,
        value: bireyselAnkastreCamasirMakinesi1Value,
        to: 'finalPage'
    },
    {
        id: '3330',
        question: questionBireyselAnkastreCamasirMakinesi1,
        content: `Farketmez`,
        value: null,
        to: 'finalPage'
    },
    
//QUESTIONS 
//Bireysel-Ankastre Kurutmalı Çamaşır Makinesi
];
var bireyselAnkastreKurutmaliCamasirMakinesi1Value = '-'
export const bireyselAnkastreKurutmaliCamasirMakinesi1 = [
    {
        id: '3331',
        question: questionBireyselAnkastreKurutmaliCamasirMakinesi1,
        content: `-`,
        value: bireyselAnkastreKurutmaliCamasirMakinesi1Value,
        to: 'finalPage'
    },
    {
        id: '3332',
        question: questionBireyselAnkastreKurutmaliCamasirMakinesi1,
        content: `Farketmez`,
        value: null,
        to: 'finalPage'
    },

//QUESTIONS 
//Bireysel-Ankastre Kahve Makinesi
];
var bireyselAnkastreKahveMakinesi1Value = '-'
export const bireyselAnkastreKahveMakinesi1 = [
    {
        id: '3333',
        question: questionBireyselAnkastreKahveMakinesi1,
        content: `-`,
        value: bireyselAnkastreKahveMakinesi1Value,
        to: 'finalPage'
    },
    {
        id: '3334',
        question: questionBireyselAnkastreKahveMakinesi1,
        content: `Farketmez`,
        value: null,
        to: 'finalPage'
    },

//QUESTIONS 
//Bireysel-Ankastre Set
];
var bireyselAnkastreSet1Value = '-'
export const bireyselAnkastreSet1 = [
    {
        id: '3335',
        question: questionBireyselAnkastreSet1,
        content: `Beyaz`,
        value: bireyselAnkastreSet1Value,
        to: 'bireyselAnkastreSet2'
    },
    {
        id: '3336',
        question: questionBireyselAnkastreSet1,
        content: `Gri`,
        value: bireyselAnkastreSet1Value,
        to: 'bireyselAnkastreSet2'
    },
    {
        id: '3337',
        question: questionBireyselAnkastreSet1,
        content: `İnoks`,
        value: bireyselAnkastreSet1Value,
        to: 'bireyselAnkastreSet2'
    },
    {
        id: '3338',
        question: questionBireyselAnkastreSet1,
        content: `Kahverengi`,
        value: bireyselAnkastreSet1Value,
        to: 'bireyselAnkastreSet2'
    },
    {
        id: '3339',
        question: questionBireyselAnkastreSet1,
        content: `Siyah`,
        value: bireyselAnkastreSet1Value,
        to: 'bireyselAnkastreSet2'
    },
    {
        id: '3340',
        question: questionBireyselAnkastreSet1,
        content: `Farketmez`,
        value: null,
        to: 'bireyselAnkastreSet2'
    },
];
var bireyselAnkastreSet2Value = '-'
export const bireyselAnkastreSet2 = [
    {
        id: '3341',
        question: questionBireyselAnkastreSet2,
        content: `Cam`,
        value: bireyselAnkastreSet2Value,
        to: 'finalPage'
    },
    {
        id: '3342',
        question: questionBireyselAnkastreSet2,
        content: `İnoks`,
        value: bireyselAnkastreSet2Value,
        to: 'finalPage'
    },
    {
        id: '3343',
        question: questionBireyselAnkastreSet2,
        content: `Farketmez`,
        value: null,
        to: 'finalPage'
    },
    
    

//QUESTIONS 
//Bireysel-Kombi
];
var bireyselKombi1Value = 'Cihaz Tipi'
export const bireyselKombi1 = [
    {
        id: '3344',
        question: questionBireyselKombi1,
        content: `Konvansiyonel`,
        value: bireyselKombi1Value,
        to: 'bireyselKombi2'
    },
    {
        id: '3345',
        question: questionBireyselKombi1,
        content: `Premix Tam Yoğuşmalı`,
        value: bireyselKombi1Value,
        to: 'bireyselKombi2'
    },
    {
        id: '3346',
        question: questionBireyselKombi1,
        content: `Yoğuşmalı`,
        value: bireyselKombi1Value,
        to: 'bireyselKombi2'
    },
    {
        id: '3347',
        question: questionBireyselKombi1,
        content: `Farketmez`,
        value: null,
        to: 'bireyselKombi2'
    },

];
var bireyselKombi2Value = 'Verim'
export const bireyselKombi2 = [
    {
        id: '3348',
        question: questionBireyselKombi2,
        content: `93%`,
        value: bireyselKombi2Value,
        to: 'bireyselKombi3'
    },
    {
        id: '3349',
        question: questionBireyselKombi2,
        content: `103%`,
        value: bireyselKombi2Value,
        to: 'bireyselKombi3'
    },
    {
        id: '3350',
        question: questionBireyselKombi2,
        content: `109%`,
        value: bireyselKombi2Value,
        to: 'bireyselKombi3'
    },
    {
        id: '3351',
        question: questionBireyselKombi2,
        content: `Farketmez`,
        value: null,
        to: 'bireyselKombi3'
    },

];
var bireyselKombi3Value = 'Isıtma Kapasitesi'
export const bireyselKombi3 = [
    {
        id: '3352',
        question: questionBireyselKombi3,
        content: `20600`,
        value: bireyselKombi3Value,
        to: 'finalPage'
    },
    {
        id: '3353',
        question: questionBireyselKombi3,
        content: `25500`,
        value: bireyselKombi3Value,
        to: 'finalPage'
    },
    {
        id: '3354',
        question: questionBireyselKombi3,
        content: `26700`,
        value: bireyselKombi3Value,
        to: 'finalPage'
    },
    {
        id: '3355',
        question: questionBireyselKombi3,
        content: `28400`,
        value: bireyselKombi3Value,
        to: 'finalPage'
    },
    {
        id: '3356',
        question: questionBireyselKombi3,
        content: `Farketmez`,
        value: null,
        to: 'finalPage'
    },


//QUESTIONS 
//Bireysel-Şofben
];
var bireyselSofben1Value = '-'
export const bireyselSofben1 = [
    {
        id: '3357',
        question: questionBireyselSofben1,
        content: `-`,
        value: bireyselSofben1Value,
        to: 'finalPage'
    },
    {
        id: '3358',
        question: questionBireyselSofben1,
        content: `Farketmez`,
        value: null,
        to: 'finalPage'
    },


//QUESTIONS 
//Bireysel-Termosifon
];
var bireyselTermosifon1Value = 'Kapasite'
export const bireyselTermosifon1 = [
    {
        id: '3359',
        question: questionBireyselTermosifon1,
        content: `50 L`,
        value: bireyselTermosifon1Value,
        to: 'bireyselTermosifon2'
    },
    {
        id: '3360',
        question: questionBireyselTermosifon1,
        content: `65 L`,
        value: bireyselTermosifon1Value,
        to: 'bireyselTermosifon2'
    },
    {
        id: '3361',
        question: questionBireyselTermosifon1,
        content: `80 L`,
        value: bireyselTermosifon1Value,
        to: 'bireyselTermosifon2'
    },
    {
        id: '3362',
        question: questionBireyselTermosifon1,
        content: `Farketmez`,
        value: null,
        to: 'bireyselTermosifon2'
    },
];
var bireyselTermosifon2Value = 'Isıtma Gücü'
export const bireyselTermosifon2 = [
    {
        id: '3363',
        question: questionBireyselTermosifon2,
        content: `1800 W`,
        value: bireyselTermosifon2Value,
        to: 'finalPage'
    },
    {
        id: '3364',
        question: questionBireyselTermosifon2,
        content: `3000 W`,
        value: bireyselTermosifon2Value,
        to: 'finalPage'
    },
    {
        id: '3365',
        question: questionBireyselTermosifon2,
        content: `Farketmez`,
        value: null,
        to: 'finalPage'
    },

//QUESTIONS 
//Bireysel-Ani Su Isıtıcısı
];
var bireyselAniSuIsiticisi1Value = '-'
export const bireyselAniSuIsiticisi1 = [
    {
        id: '3366',
        question: questionBireyselAniSuIsiticisi1,
        content: `-`,
        value: bireyselAniSuIsiticisi1Value,
        to: 'finalPage'
    },
    {
        id: '3367',
        question: questionBireyselAniSuIsiticisi1,
        content: `Farketmez`,
        value: null,
        to: 'finalPage'
    },
    


//QUESTIONS 
//Bireysel-Vantilatör
];
var bireyselVantilator1Value = 'Güç'
export const bireyselVantilator1 = [
    {
        id: '3368',
        question: questionBireyselVantilator1,
        content: `35 W`,
        value: bireyselVantilator1Value,
        to: 'finalPage'
    },
    {
        id: '3369',
        question: questionBireyselVantilator1,
        content: `38 W`,
        value: bireyselVantilator1Value,
        to: 'finalPage'
    },
    {
        id: '3370',
        question: questionBireyselVantilator1,
        content: `50 W`,
        value: bireyselVantilator1Value,
        to: 'finalPage'
    },
    {
        id: '3371',
        question: questionBireyselVantilator1,
        content: `55 W`,
        value: bireyselVantilator1Value,
        to: 'finalPage'
    },
    {
        id: '3372',
        question: questionBireyselVantilator1,
        content: `Farketmez`,
        value: null,
        to: 'finalPage'
    },


//QUESTIONS 
//Bireysel-Oda Termostatı
];
var bireyselOdaTermostati1Value = 'Ürün Tipi'
export const bireyselOdaTermostati1 = [
    {
        id: '3373',
        question: questionBireyselOdaTermostati1,
        content: `Klima + Kombi Paketi`,
        value: bireyselOdaTermostati1Value,
        to: 'bireyselOdaTermostati2'
    },
    {
        id: '3374',
        question: questionBireyselOdaTermostati1,
        content: `Klima Paketi`,
        value: bireyselOdaTermostati1Value,
        to: 'bireyselOdaTermostati2'
    },
    {
        id: '3375',
        question: questionBireyselOdaTermostati1,
        content: `Kombi Paketi`,
        value: bireyselOdaTermostati1Value,
        to: 'bireyselOdaTermostati2'
    },
    {
        id: '3376',
        question: questionBireyselOdaTermostati1,
        content: `Farketmez`,
        value: null,
        to: 'bireyselOdaTermostati2'
    },
];
var bireyselOdaTermostati2Value = 'Kontrol Teknolojisi'
export const bireyselOdaTermostati2 = [
    {
        id: '3377',
        question: questionBireyselOdaTermostati2,
        content: `Wifi Bağlantısı`,
        value: bireyselOdaTermostati2Value,
        to: 'finalPage'
    },
    {
        id: '3378',
        question: questionBireyselOdaTermostati2,
        content: `Haftalık Programlama`,
        value: bireyselOdaTermostati2Value,
        to: 'finalPage'
    },
    {
        id: '3379',
        question: questionBireyselOdaTermostati2,
        content: `Farketmez`,
        value: null,
        to: 'finalPage'
    },


//QUESTIONS 
//Bireysel-Flavel Soba
];
var bireyselFlavelSoba1Value = 'Elektrik Tüketimi'
export const bireyselFlavelSoba1 = [
    {
        id: '3380',
        question: questionBireyselFlavelSoba1,
        content: `1.5 kWh`,
        value: bireyselFlavelSoba1Value,
        to: 'bireyselFlavelSoba2'
    },
    {
        id: '3381',
        question: questionBireyselFlavelSoba1,
        content: `2.0 kWh`,
        value: bireyselFlavelSoba1Value,
        to: 'bireyselFlavelSoba2'
    },
    {
        id: '3382',
        question: questionBireyselFlavelSoba1,
        content: `2.2 kWh`,
        value: bireyselFlavelSoba1Value,
        to: 'bireyselFlavelSoba2'
    },    
    {
        id: '3383',
        question: questionBireyselFlavelSoba1,
        content: `2.5 kWh`,
        value: bireyselFlavelSoba1Value,
        to: 'bireyselFlavelSoba2'
    },
    {
        id: '3384',
        question: questionBireyselFlavelSoba1,
        content: `Farketmez`,
        value: null,
        to: 'bireyselFlavelSoba2'
    },

];
var bireyselFlavelSoba2Value = 'Emniyet Sistemi'
export const bireyselFlavelSoba2 = [
    {
        id: '3385',
        question: questionBireyselFlavelSoba2,
        content: `Aşırı Isınma Emniyeti`,
        value: bireyselFlavelSoba2Value,
        to: 'bireyselFlavelSoba3'
    },
    {
        id: '3386',
        question: questionBireyselFlavelSoba2,
        content: `Farketmez`,
        value: null,
        to: 'bireyselFlavelSoba3'
    },


];
var bireyselFlavelSoba3Value = 'Güç'
export const bireyselFlavelSoba3 = [
    {
        id: '3387',
        question: questionBireyselFlavelSoba3,
        content: `1500 W`,
        value: bireyselFlavelSoba3Value,
        to: 'finalPage'
    },
    {
        id: '3388',
        question: questionBireyselFlavelSoba3,
        content: `2000 W`,
        value: bireyselFlavelSoba3Value,
        to: 'finalPage'
    },
    {
        id: '3389',
        question: questionBireyselFlavelSoba3,
        content: `2200 W`,
        value: bireyselFlavelSoba3Value,
        to: 'finalPage'
    },
    {
        id: '3390',
        question: questionBireyselFlavelSoba3,
        content: `2300 W`,
        value: bireyselFlavelSoba3Value,
        to: 'finalPage'
    },      
    {
        id: '3391',
        question: questionBireyselFlavelSoba3,
        content: `2500 W`,
        value: bireyselFlavelSoba3Value,
        to: 'finalPage'
    },
    {
        id: '3392',
        question: questionBireyselFlavelSoba3,
        content: `3000 W`,
        value: bireyselFlavelSoba3Value,
        to: 'finalPage'
    },  
    {
        id: '3393',
        question: questionBireyselFlavelSoba3,
        content: `Farketmez`,
        value: null,
        to: 'finalPage'
    },



//QUESTIONS 
//Bireysel-Klima
];
var bireyselKlima1Value = 'Soğutma Kapasitesi'
export const bireyselKlima1 = [
    {
        id: '3394',
        question: questionBireyselKlima1,
        content: `9000 Btu/h`,
        value: bireyselKlima1Value,
        to: 'bireyselKlima2'
    },
    {
        id: '3395',
        question: questionBireyselKlima1,
        content: `9000 Btu/h`,
        value: bireyselKlima1Value,
        to: 'bireyselKlima2'
    }, 
    {
        id: '3396',
        question: questionBireyselKlima1,
        content: `18000 Btu/h`,
        value: bireyselKlima1Value,
        to: 'bireyselKlima2'
    }, 
    {
        id: '3397',
        question: questionBireyselKlima1,
        content: `24000 Btu/h`,
        value: bireyselKlima1Value,
        to: 'bireyselKlima2'
    }, 
    {
        id: '3398',
        question: questionBireyselKlima1,
        content: `44000 Btu/h`,
        value: bireyselKlima1Value,
        to: 'bireyselKlima2'
    }, 
    {
        id: '3399',
        question: questionBireyselKlima1,
        content: `46100 Btu/h`,
        value: bireyselKlima1Value,
        to: 'bireyselKlima2'
    }, 
    {
        id: '3400',
        question: questionBireyselKlima1,
        content: `47000 Btu/h`,
        value: bireyselKlima1Value,
        to: 'bireyselKlima2'
    }, 
    {
        id: '3401',
        question: questionBireyselKlima1,
        content: `Farketmez`,
        value: null,
        to: 'bireyselKlima2'
    }, 


];
var bireyselKlima2Value = 'Hava Tazeleme'
export const bireyselKlima2 = [
    {
        id: '3402',
        question: questionBireyselKlima2,
        content: `2 Saatte 25 m2 lik alan`,
        value: bireyselKlima2Value,
        to: 'bireyselKlima3'
    },
    {
        id: '3403',
        question: questionBireyselKlima2,
        content: `Farketmez`,
        value: null,
        to: 'bireyselKlima3'
    },

];
var bireyselKlima3Value = 'Bağlantı Özelliği'
export const bireyselKlima3 = [
    {
        id: '3404',
        question: questionBireyselKlima3,
        content: `Wi-Fi Bağlantı`,
        value: bireyselKlima3Value,
        to: 'bireyselKlima4'
    },
    {
        id: '3405',
        question: questionBireyselKlima3,
        content: `Farketmez`,
        value: null,
        to: 'bireyselKlima4'
    },

];
var bireyselKlima4Value = 'Klima Özelliği'
export const bireyselKlima4 = [
    {
        id: '3406',
        question: questionBireyselKlima4,
        content: `Sessiz Çalışma`,
        value: bireyselKlima4Value,
        to: 'finalPage'
    },
    {
        id: '3407',
        question: questionBireyselKlima4,
        content: `Farketmez`,
        value: null,
        to: 'finalPage'
    },
]