export const white = '#fff';
export const black = '#000';
export const red = '#ED1C24';
export const grey = '#918F90';
export const darkestGrey = '#393637';
export const darkGrey = '#47525E';
export const lightGrey = '#B2B2B2';
export const lightestGrey = '#F7F7F7';