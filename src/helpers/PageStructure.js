import React, { Component } from 'react';
import { 
  StyleSheet, 
  Text, 
  View, 
  Animated, 
  Easing, 
  StatusBar,
  Dimensions 
} from 'react-native';
import { StackNavigator, TabNavigator, DrawerNavigator, NavigationActions } from 'react-navigation';
import Icon from 'react-native-vector-icons/Ionicons';
import { Ionicons } from '@expo/vector-icons'

import { SideMenu } from '../components/SideMenu';
import { MainHeader, BackHeader } from '../components/Headers';
import * as colors from './ColorPalette';
import { 
    HomePage,
    MainCategories,
    SubCategories,
    ProductList,
    ProductDetail,
    ProductImageShowPage,
    SearchPage,
    WhoAreWe,
    ContactPage,
    Settings,
    AirCondBuyPage,
    NotificationsPage,
    AirCondSelectionRobot,
    QuestionsPage,
    BTUCalculator,
    FormPage,
} from '../pages';

const WINDOW_WIDTH = Dimensions.get('window').width;
const SIDE_MENU_WIDTH = (WINDOW_WIDTH / 4) * 3;

const HomeStackNavigator = StackNavigator(
  {
    HomePage: { screen: HomePage},
    ProductList: { screen: ProductList},
    ProductDetail: { screen: ProductDetail},
    ProductImageShowPage: { screen: ProductImageShowPage},
    FormPage: { screen: FormPage},
  },
  {
    initialRouteName : 'HomePage',
    headerMode: 'float',
    headerTransitionPreset: 'fade-in-place',
    navigationOptions: ({navigation}) => ({
      header: <MainHeader />
    })
  }
);

const CategoriesStackNavigator = StackNavigator(
  {
    MainCategories: { screen: MainCategories},
    SubCategories: { screen: SubCategories},
    ProductList: { screen: ProductList},
    ProductDetail: { screen: ProductDetail},
    ProductImageShowPage: { screen: ProductImageShowPage},
    FormPage: { screen: FormPage},
  },
  {
    initialRouteName : 'MainCategories',
    headerMode: 'float',
    headerTransitionPreset: 'fade-in-place',
    navigationOptions: ({navigation}) => ({
      header: <MainHeader />
    })
  }
);

//TODO: Yeni eklenen sayfanın adı BTUCalculator, AirCondSelectionStackNavigator'ın içinde
const AirCondSelectionStackNavigator = StackNavigator(
  {
    AirCondSelectionRobot: { screen: AirCondSelectionRobot},
    QuestionsPage: { screen: QuestionsPage},
    ProductList: { screen: ProductList},
    ProductDetail: { screen: ProductDetail},
    ProductImageShowPage: { screen: ProductImageShowPage},
    FormPage: { screen: FormPage},
    //BTUCalculator: { screen: BTUCalculator},
  },
  {
    initialRouteName : 'AirCondSelectionRobot',
    headerMode: 'float',
    headerTransitionPreset: 'fade-in-place',
    navigationOptions: ({navigation}) => ({
      header: <MainHeader />
    })
  }
);

const SearchStackNavigator = StackNavigator(
  {
    SearchPage: { screen: SearchPage},
    ProductList: { screen: ProductList},
    ProductDetail: { screen: ProductDetail},
    ProductImageShowPage: { screen: ProductImageShowPage},
    FormPage: { screen: FormPage},
  },
  {
    initialRouteName : 'SearchPage',
    headerMode: 'float',
    headerTransitionPreset: 'fade-in-place',
    navigationOptions: ({navigation}) => ({
      header: <MainHeader />
    })
  }
);

const OthersStackNavigator = StackNavigator(
  {
    AirCondBuyPage: { screen: AirCondBuyPage },
    WhoAreWe: { screen: WhoAreWe },
    BTUCalculator: { screen: BTUCalculator },
    NotificationsPage: { screen: NotificationsPage },
    Settings: { screen: Settings },
    ContactPage: { screen: ContactPage },
    FormPage: { screen: FormPage },
  },
  {
    initialRouteName : 'AirCondBuyPage',
    headerMode: 'float',
    headerTransitionPreset: 'fade-in-place',
    navigationOptions: ({navigation}) => ({
      header: <MainHeader />
    })
  }
);

//Bakın burası çok enteresan
const tabBarOnPress = navigation => config => {
  const {scene, index, jumpToIndex} = config

  if (!scene.focused) {
    jumpToIndex(scene.index) 
  } else {
    if (scene.route.routes.length > 1) {
      for (let i = 0; i < scene.route.routes.length - 1; i += 1) {
        const backAction = NavigationActions.back()

        navigation.dispatch(backAction)
      }
    } else {
      const homeNavigator = scene.route.routes[0]

      if (!!homeNavigator && !!homeNavigator.params && !!homeNavigator.params.scrollToTop) {
        homeNavigator.params.scrollToTop()
      }
    }
  }
}

const MainTabNavigator = TabNavigator(
  {
    Home: {
      screen: HomeStackNavigator,
      navigationOptions: {
        tabBarIcon: ({ tintColor }) => <Icon name="ios-home" size={26} color={tintColor} />
      }
    },

    AirCond: {
      screen: AirCondSelectionStackNavigator,
      navigationOptions: {
        tabBarIcon: ({ tintColor }) => <Icon name="ios-trophy" size={26} color={tintColor} />
      }
    },

    Categories: {
      screen: CategoriesStackNavigator,
      navigationOptions: {
        tabBarIcon: ({ tintColor }) => <Icon name="ios-apps" size={26} color={tintColor} />
      }
    },

    Search: {
      screen: SearchStackNavigator,
      navigationOptions: {
        tabBarIcon: ({ tintColor }) => <Icon name="ios-search" size={26} color={tintColor} />
      }
    },
    Others: {
      screen: OthersStackNavigator,
      navigationOptions: {
        tabBarIcon: ({ tintColor }) => <Icon name="ios-search" size={26} color={tintColor} />
      }
    },
  },
  {
    tabBarPosition: 'bottom',
    initialRouteName: 'Home',
    swipeEnabled: false,
    lazy: false,
    showIcon: true,
    navigationOptions: ({navigation}) => ({
      tabBarOnPress: tabBarOnPress(navigation)
    }),
    tabBarOptions: {
      activeTintColor: colors.red,
      inactiveTintColor: colors.darkestGrey,
      inactiveBackgroundColor: colors.white,
      activeBackgroundColor: colors.lightestGrey,
      renderIndicator: () => null,
      showIcon: true,
      showLabel: false,
      labelStyle: {
        fontSize: 12,
        color: colors.red
      },
      style: {
        height: 60,
        backgroundColor: colors.white,
        width: (WINDOW_WIDTH / 4) * 5
      },
    }
  }
);

// const MainTabNavigator2 = TabNavigator(
//   {
//     Categories: CategoriesStackNavigator,
//     AirCond: AirCondSelectionStackNavigator,
//     Home: HomeStackNavigator,
//     Search: SearchStackNavigator
//   },
//   {
//     ...TabNavigator.Presets.AndroidTopTabs,
//     tabBarPosition: 'bottom',
//     lazy: true,
//     tabBarOptions: {
//       style: {
//         backgroundColor: colors.red
//       },
//       labelStyle: {
//         fontSize: 12,
//         color: colors.red,
//         backgroundColor: 'red',
//         fontFamily: 'montserratRegular'
//       },
//       tabStyle: {
//         height: 40,
//       },
//       indicatorStyle: {
//         backgroundColor: colors.blue,
//         height: 4
//       },
//     },
//     initialRouteName: 'Categories',
//     navigationOptions: ({navigation}) => ({
//       tabBarOnPress: tabBarOnPress(navigation)
//     })
//   }
// );

//Drawer1 eklendi incele
export default MainDrawerNavigator = DrawerNavigator(
  {
    home: {screen: MainTabNavigator},
  },
  {
    contentComponent: SideMenu,
    drawerWidth: SIDE_MENU_WIDTH,
    navigationOptions: ({navigation}) => ({
      gesturesEnabled: false,
      drawerLockMode: 'locked-closed'
    })
  }
)

// export const MainDrawerNavigator = DrawerNavigator(
//   {
//     MainTab: {
//       screen: TabNavigator({
//         firstTab: {
//           screen: StackNavigator({
//             Home: { screen: HomePage },
//             ProductList: { screen: ProductList },
//             ProductDetail: {screen: ProductDetail }
//           },
//           {
//             initialRouteName : 'Home',
//             headerMode: 'float',
//             headerTransitionPreset: 'fade-in-place',
//             navigationOptions: ({navigation}) => ({
//               header: <MainHeader />
//             })
//           }
//         )
//         },
//         secondTab: {
//           screen: StackNavigator({
//             MainCategories: { screen: MainCategories },
//             SubCategories: { screen: SubCategories },
//             ProductList: { screen: ProductList },
//             ProductDetail: { screen: ProductDetail },
//           },
//           {
//             initialRouteName : 'MainCategories',
//             headerMode: 'float',
//             headerTransitionPreset: 'fade-in-place',
//             navigationOptions: ({navigation}) => ({
//               header: <MainHeader />
//             })
//           }
//         )
//         },
//       },
//       {
//         tabBarPosition: 'bottom',
//         initialRouteName: 'firstTab',
//         navigationOptions: ({navigation}) => ({
//           tabBarOnPress: tabBarOnPress(navigation)
//         })
//       }
//     )
//     },
//   },


//   {
//     initialRouteName: 'MainTab',
//   }
// )