import _ from 'lodash';
export const INCREASE_NUMBER = 'INCREASE_NUMBER';

export const increaseNumber = (number) => {
    return {
        type: INCREASE_NUMBER,
        payload: number
    };
}