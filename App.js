import React from 'react';
import { 
  StyleSheet, 
  View,
  StatusBar,
  Image
} from 'react-native';
import { StackNavigator, TabNavigator } from 'react-navigation';
import { Provider } from 'react-redux';
import { AppLoading, Asset, Font } from 'expo';

import { SideMenu } from './src/components/SideMenu';
import { MainDrawerNavigator } from './src/helpers/PageStructure';
import ReduxNavigator from './src/helpers/ReduxNavigator';
import * as colors from './src/helpers/ColorPalette';
import RootContainer from './src/helpers/RootContainer';

import store from './src/store';

function cacheImages(images) {
  return images.map(image => {
    if (typeof image === 'string') {
      return Image.prefetch(image);
    } else {
      return Asset.fromModule(image).downloadAsync();
    }
  });
}

export default class App extends React.Component {
  constructor(props) {
    super(props);

    this.state= {
      isReady: false
    }
  }

  async _loadAssetsAsync() {
    const imageAssets = cacheImages([
      require('./assets/images/arcelik_logo.png'),
      require('./assets/images/red-geometric-background.jpg')
    ]);

    await Promise.all([ ...imageAssets ]);
  }

  render() {
    if (!this.state.isReady) {
      return (
        <AppLoading
          startAsync={this._loadAssetsAsync}
          onFinish={() => this.setState({ isReady: true })}
          onError={console.warn}
        />
      );
    }
    
    return (
      <Provider store={store}>
        <View style={styles.container}>
          <StatusBar hidden />
          <ReduxNavigator />
        </View>
      </Provider>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});
